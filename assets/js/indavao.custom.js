(function($) {
	
    $(function() {
        // accordion
        $('#my_sidebar_nav').collapse();
        // Add Shadow on Top Nav
        $(window).scroll( function() {                        
            if( $("body").scrollTop() > 1 ) {
                $('#topnav').addClass('shadow');                
            } else {
                 $('#topnav').removeClass('shadow');   
            }
        });
        
        $('.unassigned').on('change', function() {
            $('.unassigned option').each(function() {
                    $('.unassigned option[value="'+$(this).val()+'"]').prop('disabled', false);
            });
            $('.unassigned option').each(function() {
                if( $(this).val() != "" && $(this).prop('selected') ) {
                    $('.unassigned option[value="'+$(this).val()+'"]').prop('disabled', true);
                } 
               
            });
            $('.unassigned option:selected').each(function() {
                if( $(this).val() != '' ) {
                    $(this).prop('disabled', false);
                }
            });
        });

        /* frontpage map
            var wrap = $(document).height();
            console.log( wrap - ( parseInt( $('#wrap').css('padding-bottom') ) * 2 ) );
            $('#map-canvas').height(wrap - ( parseInt( $('#wrap').css('padding-bottom') ) * 2 )  );
            $('#frontpage').css('margin-top', -Math.abs(wrap) );
         */   
        var pageLikeLock = function() {
            
        }
    
        $('.selectpicker').selectpicker({
            dropupAuto : true
        });
        
        $("img.lazy").lazyload({
			effect : "fadeIn"
		});
		
		$('#property-slider').bxSlider({
			pagerCustom: '#property-slider-thumbnails'
		});
		
		$('label.btn input[type="checkbox"]').each(function() { 
			$(this).click(function(){
				if( $(this).is(':checked') ) {
					$(this).parent('.btn').removeClass('btn-default').addClass('btn-success');
					$(this).parent('.btn').children('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');
				} else {
					$(this).parent('.btn').removeClass('btn-success').addClass('btn-default');
					$(this).parent('.btn').children('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
				}
			});
		});
		
		$('label.btn-radio').click(function() { 
			$('label.btn-radio').removeClass('btn-success').addClass('btn-default');
			$(this).removeClass('btn-default').addClass('btn-success');
		});
		$('.ui-slider.normal').each(function(){
			$( this ).slider({
			  min: $(this).attr('data-min'),
			  max: $(this).attr('data-max'),
			  step: $(this).attr('data-step'),
			  value: parseInt($(this).attr('data-default')),
			  slide: function( event, ui ) {
				$( $(this).attr('data-display') ).val( $(this).attr('data-prefix') + ui.value );
			  }
			});
			$( $(this).attr('data-display')  ).val( $(this).attr('data-prefix') + $( this ).slider( "value" ) );
		});
		$('.ui-slider.range').each(function(){
			$( this ).slider({
			  range: true,
			  min: $(this).attr('data-min'),
			  max: $(this).attr('data-max'),
			  step: $(this).attr('data-step'),
			  values: [ parseInt($(this).attr('data-min-default')), parseInt($(this).attr('data-max-default')) ],
			  slide: function( event, ui ) {
				$( $(this).attr('data-display') ).val( $(this).attr('data-prefix') + ui.values[ 0 ] + " - " + $(this).attr('data-prefix') + ui.values[ 1 ] );
			  }
			});
			$( $(this).attr('data-display')  ).val( $(this).attr('data-prefix') + $( this ).slider( "values", 0 ) +
			  " - " + $(this).attr('data-prefix') + $( this ).slider( "values", 1 ) );
		});
		
		$('.print-me').click(function(){
			window.print();
		});
		
		$('a.smooth-scroll').bind('click', function(event) {
			var $anchor = $(this);
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 1500, 'easeInOutExpo');
			event.preventDefault();
		});
    
		$(".fancybox").fancybox({
			openEffect	: 'none',
			closeEffect	: 'none',
			helpers		: {
				title	: { type : 'inside' },
				buttons	: {}
			}
		});
		
		
    });
    
})(jQuery);

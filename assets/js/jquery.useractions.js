(function($){
	$(function(){
		// save to bookmarks
		$('.save-to-bookmarks').click(function(){
			var self = $(this);
			var object_type = $(this).attr('data-type');
			var object_id = $(this).attr('data-id');
			$.post(ajaxPostURL, {
				object_id : object_id,
				object_type : object_type,
				action : 'save_bookmark',
			}, function(msg) {
				
				if( msg.error == false ) {
					self.unbind('click');
					$('<i />').addClass('glyphicon glyphicon-ok pull-right').appendTo( self );
				}
			}, 'json').fail(function(xhr, textStatus, errorThrown){
				console.log( xhr.responseText );
			});
		});
		$(".business-collapse").click(function() {
			$('.business-update').addClass('hidden');
			var id = $(this).attr('data-id');
			if( typeof id !== 'undefined' ) {
				$('.business-update.'+id).removeClass('hidden');
			}
		});
		$(".category-checkbox").click(function(){
			var id = $(this).attr('data-id');
			var checked = $(this).prop('checked');
			if( checked == true) {
				$("#category-parent-"+id).addClass('success');
				$("#category-children-"+id).removeClass('hidden');
			} else {
				$("#category-parent-"+id).removeClass('success');
				$("#category-children-"+id).addClass('hidden');
				$(".category-child-"+id).prop('checked', false);
			}
		});
		$(".location-checkbox").click(function(){
			var id = $(this).attr('data-id');
			var checked = $(this).prop('checked');
			if( checked == true) {
				$("#location-parent-"+id).addClass('success');
				$("#location-children-"+id).removeClass('hidden');
			} else {
				$("#location-parent-"+id).removeClass('success');
				$("#location-children-"+id).addClass('hidden');
				$(".location-child-"+id).prop('checked', false);
			}
		});
		$('#business-update-details').click(function() {
			var self = $(this);
			self.prop('disabled', true);
			var postData = {
					action : 'update_details',
					business_name : $('#business_name').val(),
					business_description : $('#business_description').val(),
					business_address : $('#business_address').val(),
					business_phone : $('#business_phone').val(),
					business_website : $('#business_website').val(),
					business_email : $('#business_email').val(),
					business_map_lat : $('#business_map_lat').val(),
					business_map_long : $('#business_map_long').val(),
				}
				$.post(currentURL, postData, function(msg) {
					$('.business-error').remove();
					$('.alert').slideUp(function(){
							$(this).remove();
						});
					if( msg.error == false ) {
						var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					} else {
						var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					}
					
					if( msg.postData.business_name != '' && msg.postData.business_phone != '' && msg.postData.business_address != '' ) {
						$('#panel-review-approval').removeClass('hidden');
					} else {
						$('#panel-review-approval').addClass('hidden');
					}
					
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
				
		});
		
		$('#business-update-category').click(function() {
			var self = $(this);
			
			var new_categories = [], remove_categories = [];
			var old_categories_val = $('#old_categories').val(),
			old_categories = old_categories_val.split(',');
			
			$('.category-item').each(function(){
				if( ($.inArray($(this).attr('data-id'), old_categories) == -1) && ($(this).prop('checked') == true) ) {
					new_categories.push( $(this).attr('data-id') );
				}
				
				if( ($.inArray($(this).attr('data-id'), old_categories) > -1) && ($(this).prop('checked') == false) ) {
					remove_categories.push( $(this).attr('data-id') );
				}
			});
			
			var postData = {
					action : 'update_categories',
					old_categories : old_categories,
					new_categories : new_categories,
					remove_categories : remove_categories,
				}
			
			if( new_categories.length == 0 && remove_categories.length == 0) {
				return;
			}
			
			self.prop('disabled', true);
			$.post(currentURL, postData, function(msg) {
					$('#old_categories').val( msg.new_categories );
					$('.business-error').remove();
					$('.alert').slideUp(function(){
							$(this).remove();
						});
					if( msg.error == false ) {
						var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					} else {
						var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			
		});
		
		$('#business-update-location').click(function() {
			var self = $(this);
			
			var new_locations = [], remove_locations = [];
			var old_locations_val = $('#old_locations').val(),
			old_locations = old_locations_val.split(',');
			
			$('.location-item').each(function(){
				if( ($.inArray($(this).attr('data-id'), old_locations) == -1) && ($(this).prop('checked') == true) ) {
					new_locations.push( $(this).attr('data-id') );
				}
				
				if( ($.inArray($(this).attr('data-id'), old_locations) > -1) && ($(this).prop('checked') == false) ) {
					remove_locations.push( $(this).attr('data-id') );
				}
			});
			
			var postData = {
					action : 'update_locations',
					old_locations : old_locations,
					new_locations : new_locations,
					remove_locations : remove_locations,
				}
			
			if( new_locations.length == 0 && remove_locations.length == 0) {
				return;
			}
			
			self.prop('disabled', true);
			$.post(currentURL, postData, function(msg) {
					$('#old_locations').val( msg.new_locations );
					$('.business-error').remove();
					$('.alert').slideUp(function(){
							$(this).remove();
						});
					if( msg.error == false ) {
						var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					} else {
						var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			
		});
		
		$('.delete-my-business-item').click(function(){
			if( confirm("Are you sure?") ) {
				var id = $(this).attr('data-id');
				
				$.post(currentURL, {
					action : 'delete',
					id : id
					}, function(msg) {
						$('.alert').slideUp(function(){
							$(this).remove();
						});
						if( msg.error == true ) {
							var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text('Business Successfully Deleted!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
							$('#business-item-'+msg.id).fadeOut('slow', function(){
								$(this).remove();
							});
						} else {
							var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text('Unable to delete Business!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
						}
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		
		$('#business_logo').change(function(evt){
			$('#business_logo_preview').remove();
			$('#business-update-logo').prop('disabled', true);
			if( typeof evt.target.files[0] != 'undefined' ) {
				var preview = $('<img />').insertBefore( $('#business_logo') );
				preview.attr('id', 'business_logo_preview');
				preview.css({
					maxWidth : '100%',
					marginBottom : '10px',
					});
				var reader = new FileReader();
				reader.onload = function(e) {
					preview.attr('src', e.target.result);
				}
				reader.readAsDataURL( evt.target.files[0] );
				$('#business-update-logo').prop('disabled', false);
			}
		});
		
		$('#business-update-logo').click(function() {
			var self = $(this);
			var business_logo = $('#business_logo').prop('files');
			var postData = new FormData();
			postData.append('action', 'update_logo');
			postData.append('userfile', business_logo[0]);
			self.prop('disabled', true);
			$.ajax({
				url: currentURL,
				type: 'POST',
				data:  postData,
				cache: false,
				dataType: 'json',
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				success: function(data, textStatus, jqXHR)
				{
					console.log( data );
				},
				error: function(xhr, desc, err) {
					console.log(xhr);
					console.log("Details: " + desc + "\\nError:" + err);
				}
			});
		});

		$('#business_tag_add_action').click(function(evt){
				var self = $(this);
				self.prop('disabled', true);
				$.post(currentURL, {
					action : 'update_tags',
					business_tag : $('#business_tag_name').val()
					}, function(msg) {
						$('.alert').slideUp(function(){
							$(this).remove();
						});
						if( msg.error == false ) {
							var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text('Business Tags Successfully Added!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
							var ul = $('#business-tags');
							var li = $('<li>').addClass('list-group-item tag-item enabled').text( msg.tag.tag_name ).appendTo(ul);
						} else {
							var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text('Unable to add tag!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
						}
						self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
				$('#business_tag_name').val('');
		});
		
		$('.business_tag_delete_action').click(function(evt){
				var self = $(this);
				self.prop('disabled', true);
				$.post(currentURL, {
					action : 'delete_tags',
					dir_tag_id : self.attr('data-tag_id'),
					}, function(msg) {
						$('.alert').slideUp(function(){
							$(this).remove();
						});
						if( msg.error == false ) {
							var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text('Business Tags Successfully Deleted!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
							$('#tag-item-'+msg.dir_tag_id).fadeOut(function(){
								$(this).remove();
							});
						} else {
							var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text('Unable to add tag!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
						}
						self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
				$('#business_tag_name').val('');
		});
	});
})(jQuery);

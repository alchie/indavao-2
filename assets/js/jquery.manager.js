(function($){
	$(function(){
		$('#register-points-program').click(function(){
			var self = $(this),
			user_id = $(this).attr('data-id'),
			username = $('#register-points-program-username').val();
				self.prop('disabled', true);
			
			if( username != '' ) {
				$.post(ajaxPostURL, { action : 'register-points-program', user_id : user_id, username : username }, function(msg) {
					if( msg == true ) {
						window.location.reload();
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			} else {
				$('#register-points-program-container').addClass('has-error');
				self.prop('disabled', false);
			}
			
		});
		
		$('#redeem-points-submit').click(function(){
			var self = $(this),
			user_id = $(this).attr('data-id'),
			points = $('#redeem-points-item option:selected').attr('data-points'),
			item = $('#redeem-points-item option:selected').attr('data-item');
			
			if( confirm( points + " points will be deducted to the total remaining points. Are you sure you want to redeem " + item + "?") ) {
			var postData = { action : 'redeem-points', user_id : user_id, points : points, item : item };
				self.prop('disabled', true);
				$.post(ajaxPostURL, postData, function(msg) {
					if( msg == true ) {
						window.location.reload();
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		
		$('.remove-users-points').click(function(){
			var self = $(this);
			var points_id = $(this).attr('data-id');
			var user_id = $(this).attr('data-uid');
			$.post(ajaxPostURL, { action : 'remove-users-points', user_id : user_id, points_id : points_id }, function(msg) {
					if( msg.deleted == true ) {
						$('#points-item-'+ msg.id).fadeOut('slow', function(){
							$(this).remove();
						});
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
		});
		
		$('.toggle-permission').click(function(){
			var self = $(this);
			var permission = $(this).attr('data-permission');
			var user_id = $(this).attr('data-uid');
			var status = $(this).prop('checked');
			self.prop('disabled', true);
			$.post(ajaxPostURL, { action : 'toggle-permission', user_id : user_id, permission : permission, status : status }, function(msg) {
				console.log(msg);
				if( msg.status == true ) {
					$('#permission-'+msg.permission).removeClass('danger').addClass('success');
				} else {
					$('#permission-'+msg.permission).removeClass('success').addClass('danger');
				}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown) {
					console.log( xhr.responseText );
				});
		});
		
		$('.business-approve').click(function(){
			if( confirm("Are you sure?") ) {

				var self = $(this);
				var dir_id = $(this).attr('data-id');
				
				self.prop('disabled', true);
				$.post(ajaxPostURL, { action : 'business-approve', dir_id : dir_id }, function(msg) {
					if( msg.error == false ) {
						$('#dir-'+msg.dir_id).addClass('success').delay(400).fadeOut('slow', function(){
							$(this).remove();
						});
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		
		$('#business-approve-update').click(function(){
			if( confirm("Are you sure?") ) {
				var self = $(this);
				var dir_id = $(this).attr('data-id');
				var points = $(this).attr('data-points');
				self.prop('disabled', true);
				$.post(ajaxPostURL, { action : 'business-approve', dir_id : dir_id, points : points }, function(msg) {
					if( msg.error == false ) {
						window.location.reload();
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		
		$('.business-publish').click(function(){
			if( confirm("Are you sure?") ) {
				var self = $(this);
				var dir_id = $(this).attr('data-id');
				self.prop('disabled', true);
				$.post(ajaxPostURL, { action : 'business-publish', dir_id : dir_id }, function(msg) {
					if( msg.error == false ) {
						$('#dir-'+msg.dir_id).addClass('success').delay(400).fadeOut('slow', function(){
							$(this).remove();
						});
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		
		$('.business-reclaim').click(function(){
			if( confirm("Are you sure?") ) {
				
			var self = $(this);
			var dir_id = $(this).attr('data-id');
			self.prop('disabled', true);
			$.post(ajaxPostURL, { action : 'business-reclaim', dir_id : dir_id }, function(msg) {
					if( msg.error == false ) {
						$('#dir-'+msg.dir_id).addClass('success').delay(400).fadeOut('slow', function(){
							$(this).remove();
						});
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		
		$('.business-reject').click(function(){
			if( confirm("Are you sure?")  ) {
				
				var self = $(this);
				var dir_id = $(this).attr('data-id');
				self.prop('disabled', true);
				$.post(ajaxPostURL, { action : 'business-reject', dir_id : dir_id }, function(msg) {
					if( msg.error == false ) {
						$('#dir-'+msg.dir_id).addClass('danger').delay(400).fadeOut('slow', function(){
							$(this).remove();
						});
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		
		$('#business-reject-update').click(function(){
			if( confirm("Are you sure?") ) {
				
				var self = $(this);
				var dir_id = $(this).attr('data-id');
				self.prop('disabled', true);
				$.post(ajaxPostURL, { action : 'business-reject', dir_id : dir_id }, function(msg) {
					if( msg.error == false ) {
						window.location.reload();
					}
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		
		$('.business-delete').click(function(){
			if( confirm("Are you sure?") ) {
				
				var self = $(this);
				var dir_id = $(this).attr('data-id');
				self.prop('disabled', true);
				$.post(ajaxPostURL, { action : 'business-delete', dir_id : dir_id }, function(msg) {
						if( msg.error == false ) {
						$('#dir-'+msg.dir_id).addClass('danger').delay(400).fadeOut('slow', function(){
							$(this).remove();
						});
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		
		$('.business-delete-permanently').click(function(){
			if( confirm("Are you sure?") ) {
		
				var self = $(this);
				var dir_id = $(this).attr('data-id');
				self.prop('disabled', true);
				$.post(ajaxPostURL, { action : 'business-delete-permanently', dir_id : dir_id }, function(msg) {
					if( msg.error == false ) {
						$('#dir-'+msg.dir_id).addClass('danger').delay(400).fadeOut('slow', function(){
							$(this).remove();
						});
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			}
		});
		$(".business-collapse").click(function() {
			$('.business-update').addClass('hidden');
			var id = $(this).attr('data-id');
			if( typeof id !== 'undefined' ) {
				$('.business-update.'+id).removeClass('hidden');
			}
		});
		$(".category-checkbox").click(function(){
			var id = $(this).attr('data-id');
			var checked = $(this).prop('checked');
			if( checked == true) {
				$("#category-parent-"+id).addClass('success');
				$("#category-children-"+id).removeClass('hidden');
			} else {
				$("#category-parent-"+id).removeClass('success');
				$("#category-children-"+id).addClass('hidden');
				$(".category-child-"+id).prop('checked', false);
			}
		});
		$(".location-checkbox").click(function(){
			var id = $(this).attr('data-id');
			var checked = $(this).prop('checked');
			if( checked == true) {
				$("#location-parent-"+id).addClass('success');
				$("#location-children-"+id).removeClass('hidden');
			} else {
				$("#location-parent-"+id).removeClass('success');
				$("#location-children-"+id).addClass('hidden');
				$(".location-child-"+id).prop('checked', false);
			}
		});
		$('#business-update-details').click(function() {
			var self = $(this);
			self.prop('disabled', true);
			var postData = {
					action : 'business-update-details',
					business_id : $('#business_id').val(),
					business_name : $('#business_name').val(),
					business_slug : $('#business_slug').val(),
					business_description : $('#business_description').val(),
					business_address : $('#business_address').val(),
					business_phone : $('#business_phone').val(),
					business_website : $('#business_website').val(),
					business_email : $('#business_email').val(),
					business_map_lat : $('#business_map_lat').val(),
					business_map_long : $('#business_map_long').val(),
				}
				$.post(ajaxPostURL, postData, function(msg) {
					$('.business-error').remove();
					if( msg.error == false ) {
						var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					} else {
						var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
				
		});
		
		$('#business-update-category').click(function() {
			var self = $(this);
			
			var new_categories = [], remove_categories = [];
			var old_categories_val = $('#old_categories').val(),
			old_categories = old_categories_val.split(',');
			
			$('.category-item').each(function(){
				if( ($.inArray($(this).attr('data-id'), old_categories) == -1) && ($(this).prop('checked') == true) ) {
					new_categories.push( $(this).attr('data-id') );
				}
				
				if( ($.inArray($(this).attr('data-id'), old_categories) > -1) && ($(this).prop('checked') == false) ) {
					remove_categories.push( $(this).attr('data-id') );
				}
			});
			
			var postData = {
					action : 'business-update-categories',
					old_categories : old_categories,
					new_categories : new_categories,
					remove_categories : remove_categories,
					business_id : $('#business_id').val(),
				}
			
			if( new_categories.length == 0 && remove_categories.length == 0) {
				return;
			}
			
			self.prop('disabled', true);
			$.post(ajaxPostURL, postData, function(msg) {
					$('#old_categories').val( msg.new_categories );
					$('.business-error').remove();
					if( msg.error == false ) {
						var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					} else {
						var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			
		});
		
		$('#business-update-location').click(function() {
			var self = $(this);
			
			var new_locations = [], remove_locations = [];
			var old_locations_val = $('#old_locations').val(),
			old_locations = old_locations_val.split(',');
			
			$('.location-item').each(function(){
				if( ($.inArray($(this).attr('data-id'), old_locations) == -1) && ($(this).prop('checked') == true) ) {
					new_locations.push( $(this).attr('data-id') );
				}
				
				if( ($.inArray($(this).attr('data-id'), old_locations) > -1) && ($(this).prop('checked') == false) ) {
					remove_locations.push( $(this).attr('data-id') );
				}
			});
			
			var postData = {
					action : 'business-update-locations',
					old_locations : old_locations,
					new_locations : new_locations,
					remove_locations : remove_locations,
					business_id : $('#business_id').val(),
				}
			
			if( new_locations.length == 0 && remove_locations.length == 0) {
				return;
			}
			
			self.prop('disabled', true);
			$.post(ajaxPostURL, postData, function(msg) {
					$('#old_locations').val( msg.new_locations );
					$('.business-error').remove();
					if( msg.error == false ) {
						var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					} else {
						var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
			
		});
		
		$('#business_logo').change(function(evt){
			$('#business_logo_preview').remove();
			$('#business-update-logo').prop('disabled', true);
			if( typeof evt.target.files[0] != 'undefined' ) {
				var preview = $('<img />').insertBefore( $('#business_logo') );
				preview.attr('id', 'business_logo_preview');
				preview.css({
					maxWidth : '100%',
					marginBottom : '10px',
					});
				var reader = new FileReader();
				reader.onload = function(e) {
					preview.attr('src', e.target.result);
				}
				reader.readAsDataURL( evt.target.files[0] );
				$('#business-update-logo').prop('disabled', false);
			}
		});
		
		$('#business-update-logo').click(function() {
			var self = $(this);
			var business_logo = $('#business_logo').prop('files');
			var postData = new FormData();
			postData.append('action', 'business-update-logo');
			postData.append('userfile', business_logo[0]);
			postData.append('business_id', $('#business_id').val());

			self.prop('disabled', true);
			$.ajax({
				url: ajaxPostURL,
				type: 'POST',
				data:  postData,
				cache: false,
				dataType: 'json',
				processData: false, // Don't process the files
				contentType: false, // Set content type to false as jQuery will tell the server its a query string request
				success: function(data, textStatus, jqXHR)
				{
					if( data.error == false ) {
						
					}
				},
				error: function(xhr, desc, err) {
					console.log(xhr);
					console.log("Details: " + desc + "\\nError:" + err);
				}
			});
		});
		
		$('#business-update-tasks').click(function() {
			var self = $(this);
			var business_logo = $('#business_logo').prop('files');
			var postData = {
				action : 'business-update-tasks',
				business_id : $('#business_id').val(),
				is_task : $('#business_add_to_tasks').prop('checked'),
				task_message : $('#business_task_message').val(),
			};
			self.prop('disabled', true);
			$.post(ajaxPostURL, postData, function(msg) {
					$('.business-error').remove();
					if( msg.error == false ) {
						var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					} else {
						var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text(msg.msg).insertBefore( $('#accordion') );
						alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
		});
		
		$('#business-remove-logo').click(function(){
			if( confirm("Are you sure?") == false ) {
				return 0;
			}
			var self = $(this);
			var dir_id = $(this).attr('data-id');
			self.prop('disabled', true);
			$.post(ajaxPostURL, { action : 'business-remove-logo', dir_id : dir_id }, function(msg) {
					if( msg.error == false ) {
						$('#logo-preview-'+msg.id).remove();
					}
					self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
		});
		
		$('#business_tag_add_action').click(function(evt){
				var self = $(this);
				self.prop('disabled', true);
				$.post(ajaxPostURL, {
					action : 'business_tags_add',
					business_tag : $('#business_tag_name').val(),
					business_id : $('#business_id').val(),
					}, function(msg) {
						$('.alert').slideUp(function(){
							$(this).remove();
						});
						if( msg.error == false ) {
							var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text('Business Tags Successfully Added!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
							var ul = $('#business-tags');
							var li = $('<li>').addClass('list-group-item tag-item enabled').text( msg.tag.tag_name ).appendTo(ul);
						} else {
							var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text('Unable to add tag!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
						}
						self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
				$('#business_tag_name').val('');
		});
		
		$('.business_tag_delete_action').click(function(evt){
				var self = $(this);
				self.prop('disabled', true);
				$.post(ajaxPostURL, {
					action : 'business_tags_delete',
					dir_tag_id : self.attr('data-tag_id'),
					}, function(msg) {
						$('.alert').slideUp(function(){
							$(this).remove();
						});
						if( msg.error == false ) {
							var alert = $('<div>').addClass('alert alert-success alert-dismissable business-error').text('Business Tags Successfully Deleted!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
							$('#tag-item-'+msg.dir_tag_id).fadeOut(function(){
								$(this).remove();
							});
						} else {
							var alert = $('<div>').addClass('alert alert-danger alert-dismissable business-error').text('Unable to add tag!').insertBefore( $('#accordion') );
							alert.prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>');
						}
						self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
				$('#business_tag_name').val('');
		});
		
		$('.business_promote_save').on('click', function(){
			var self = $(this);
			self.prop('disabled', true);
			var data_name = $(this).attr('data-name');
			var data_value = $('#'+data_name+'-input').val();
			$.post(ajaxPostURL, {
					action : 'business_promote',
					key : data_name,
					value : data_value,
					dir_id : $('#business_id').val(),
				}, function(msg) {
						console.log( msg );
						self.prop('disabled', false);
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
		});
		
	});
})(jQuery);

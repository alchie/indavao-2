$(function() {
    "use strict";
	var loadingBox = function(id) {
		var boxBody = $('#'+id+' .box-body');
		var loading = $('<div class="overlay '+id+'"></div><div class="loading-img '+id+'"></div>');
		loading.insertAfter( boxBody );
		return loading;
	};
	
	var showAlert = function(type, msg) {
		var alertContainer = $('#alert-container');
		alertContainer.empty();
		var alertType = type || 'danger';
		var alert = $('<div />').addClass('alert alert-dismissable alert-'+alertType);
			alert.appendTo( alertContainer );
			var icon = (alertType=='success') ? '<i class="fa fa-check"></i>' : '<i class="fa fa-ban"></i>';
			alert.html( icon+'<button type="button"\
			class="close" data-dismiss="alert" aria-hidden="true">×</button>\
			' + msg );
		
	};
	
	$('.auto-update-fields').click(function(){
		var self = $(this),
		fields = self.attr('data-fields').split(','),
		boxId = self.attr('data-box'),
		apiUri = self.attr('data-api'),
		loadingScreen = loadingBox( boxId );
		
		if( typeof apiUri == 'undefined' ) return;
		if( typeof fields == 'undefined' ) return;
		
		var postData = {
			box_id : boxId
		};
		
		var proceed = true;
		for (var i in fields) {
			var id_name = fields[i].trim();
			var field = $('#'+id_name).prop('name');
			var value = $('#'+id_name).val();
			postData[field] = value;
			if( $('#'+id_name).prop('required') == true && value == '' ) {
				proceed = false;
				$('#'+id_name).parent('.form-group').addClass('has-error');
			} else {
				$('#'+id_name).parent('.form-group').removeClass('has-error');
			}
		}
		
		if( proceed ) {
			$.post(myBaseURL+'/api/'+apiUri, postData, function(response) {
				if( response.error == true ) {
					showAlert( 'danger', response.message );
				} else {
					showAlert( 'success', response.message );
					if( response.redirect ) {
						window.location.href = response.redirect;
					}
				}
				loadingScreen.remove();
			}, 'json').fail(function(xhr, textStatus, errorThrown){
				console.log( xhr.responseText );
			});
		} else {
			loadingScreen.remove();
		}
	});
	
	window.inDavaoApp = {
		__businessDirectoryTags : function( returnData, returnAPI ){
			var listGroup = this.ListGroup.create( inDavaoApp.currentQueue, inDavaoApp.API+'-list' );
			inDavaoApp.currentQueue.children('#loading-img').remove();
			var deleteCallback = function(){
						var business_id = $('#business_id').attr('data-value');
						var tag_id = $(this).attr('data-id');
						window.inDavaoApp.__getData('business_directory/delete_tag/'+business_id, {tag_id : tag_id}, function(response){
							if( response.error == true ) {
								showAlert( 'danger', response.message );
							} else {
								showAlert( 'success', response.message );
								$('#dir_tag-'+ response.returnData).css('background-color', 'red').delay(200).slideUp(function(){
									$(this).remove();
								});
							}
						});
					};
					
			if( returnData !== false && returnData.length > 0) { 
				for( var i in returnData ) {
					listGroup.addDelete( deleteCallback );
					listGroup.addItem( returnData[i].dir_tag_id, returnData[i].tag_name, 'dir_tag-' );
				}
			}
			var agBox = $('<div/>').addClass('input-group').insertAfter('#'+inDavaoApp.API+'-list').css('margin-top','10px');
			var agBox_input = $('<input/>').addClass('form-control').attr('id', 'add-new-tag-input').attr('type', 'text').appendTo(agBox);
			var agBox_span = $('<span />').addClass('input-group-btn').appendTo(agBox);
			var agBox_button  = $('<button />').addClass('btn btn-success').appendTo(agBox_span).text('Add Tag');
			agBox_button.click(function(){
				var business_id = $('#business_id').attr('data-value');
				var new_tag = $('#add-new-tag-input').val();
				window.inDavaoApp.__getData('business_directory/add_tag/'+business_id, {tag : new_tag}, function(response){
					if( response.error == true ) {
						showAlert( 'danger', response.message );
					} else {
						showAlert( 'success', response.message );
						listGroup.addDelete( deleteCallback );
						listGroup.addItem( response.returnData.dir_tag_id, response.tag.tag_name, 'dir_tag-' );
						$('#add-new-tag-input').val('');
					}
				});
			});
		},
		
		__businessDirectoryPhotos : function( returnData, returnAPI ){
			inDavaoApp.currentQueue.children('#loading-img').remove();
			var preview = this.ImagePreview.create( inDavaoApp.currentQueue, returnAPI+'-preview' );
			for( var i in returnData ) {
				preview.addItem( returnData[i].file_name, uploadURL + returnData[i].file_name );
				preview.addDelete( returnData[i].file_name, returnData[i].dir_med_id, 'business_directory/delete_photo/' + returnData[i].dir_id );
			}
		},
		
		__businessDirectoryCategories : function( returnData, returnAPI ){
			var listGroup = this.ListGroup.create( inDavaoApp.currentQueue, inDavaoApp.API+'-list' );
			inDavaoApp.currentQueue.children('#loading-img').remove();
			var listGroupDeleteCallback = function(){
						var business_id = $('#business_id').attr('data-value');
						var tax_id = $(this).attr('data-id');
						var parent = $(this).attr('data-parent');
						window.inDavaoApp.__getData('business_directory/delete_category/'+business_id, {tax_id : tax_id , tax_name : $('#added_dir_cat-'+tax_id).text()}, function(response){
							if( response.error == true ) {
								showAlert( 'danger', response.message );
							} else {
								var category = $('#added_dir_cat-'+response.returnData);
								bDrop.addItem( response.returnData, category.text(), 'dir_categories-', bDropCallback, parent);
								showAlert( 'success', response.message );
								$('#added_dir_cat-'+ response.returnData).css('background-color', 'red').delay(200).slideUp(function(){
									$(this).remove();
								});
							}
						});
					};
							
			var bDropCallback = function(){
				var business_id = $('#business_id').attr('data-value');
				var cat_id = $(this).attr('data-id');
				var parent = $(this).attr('data-parent');
				var cat_name = $(this).text();
				window.inDavaoApp.__getData('business_directory/add_category/'+business_id, { tax_id : cat_id, tax_name :  cat_name}, function(response){
					if( response.error == true ) {
						showAlert( 'danger', response.message );
					} else {
						showAlert( 'success', response.message );
						listGroup.addDelete( listGroupDeleteCallback );
						listGroup.addItem( cat_id, cat_name, 'added_dir_cat-', 'li', false, parent );
						$('#li-dir_categories-'+cat_id).remove();
					}
				});
				
			};
			
			if( returnData !== false && returnData.length > 0) { 
				for( var i in returnData ) {
					listGroup.addDelete( listGroupDeleteCallback );
					listGroup.addItem( returnData[i].tax_id, returnData[i].tax_label, 'added_dir_cat-', 'li', false, returnAPI+'-add'  );
				}
			}
			
			var bDrop = this.ButtonDropdown.create( inDavaoApp.currentQueue, returnAPI+'-add', 'Add Category' );
			bDrop.Button.attr('data-loaded', 0);
			bDrop.Button.css('margin-top', '10px');
			bDrop.Button.click(function(){
				if( $(this).attr('data-loaded') == 0 ) {
					$(this).attr('data-loaded', 1);
					var parentMenu = $(this).attr('data-id');
					$('#'+parentMenu+' .dropdown-menu').html('');
					window.inDavaoApp.__getData('taxonomies/business_category', {}, function(response){
						if( response.returnData.length > 0 ) {
							for( var i in response.returnData ) {
								var id = response.returnData[i].tax_id;
								var label = response.returnData[i].tax_label;
								if( $('#added_dir_cat-'+id).length > 0 ) {
									continue;
								}
								bDrop.addItem( id, label, 'dir_categories-', bDropCallback, parentMenu);
							}
						}
					});
				}
			});
		},
		
		__businessDirectoryLocation : function( returnData, returnAPI ){
			inDavaoApp.currentQueue.children('#loading-img').remove();
			var lDropCallback = function(){
				var business_id = $('#business_id').attr('data-value');
				var loc_id = $(this).attr('data-id');
				var loc_name = $(this).text();
				window.inDavaoApp.__getData('business_directory/update_location/'+business_id, { loc_id : loc_id, loc_name :  loc_name}, function(response){
					if( response.error == true ) {
						showAlert( 'danger', response.message );
					} else {
						$('#'+response.api+'-change .dropdown-toggle').html(response.postData.loc_name + ' <span class="caret"></span>');
						showAlert( 'success', response.message );
					}
				});
				
			};
			
			var current_location = 'Select Location';
			if( returnData != false ) {
				current_location = returnData.loc_name;
			}
			
			var lDrop = this.ButtonDropdown.create( inDavaoApp.currentQueue, returnAPI+'-change', current_location );
			lDrop.Button.attr('data-loaded', 0);
			lDrop.Button.click(function(){
				if( $(this).attr('data-loaded') == 0 ) {
					$(this).attr('data-loaded', 1);
					var parentMenu = $(this).attr('data-id');
					$('#'+parentMenu+' .dropdown-menu').html('');
					window.inDavaoApp.__getData('locations/all', {}, function(response){
						if( response.returnData.length > 0 ) {
							for( var l in response.returnData ) {
								var id = response.returnData[l].loc_id;
								var label = response.returnData[l].loc_name;
								lDrop.addItem( id, label, 'dir_location-', lDropCallback, parentMenu);
							}
						}
					});
				}
			});
		},
		
		__businessDirectory : function( returnData, returnAPI ) {
			if( returnData !== false && returnData.length > 0) { 
				inDavaoApp.currentQueue.children('#loading-img').remove();
				var oBox = this.Box.init( inDavaoApp.currentQueue, inDavaoApp.colWidth, inDavaoApp.API+'-list' );
				for( var i in returnData ) {
					oBox.setId( 'business-'+ returnData[i].dir_id );
					oBox.addHeader( returnData[i].dir_name );
					oBox.addBody( returnData[i].address + '<br/>' + returnData[i].phone );
					
					var links = $('<span/>');
					var updateLink = $('<a/>').addClass('btn btn-primary btn-xs').appendTo(links);
					updateLink.attr('href', myBaseURL + '/business.html?id=' + returnData[i].dir_id);
					updateLink.text( 'Update' );
					
					var deleteLink = $('<a/>').addClass('btn btn-danger btn-xs pull-right').appendTo(links);
					deleteLink.attr('href', 'javascript:void(0);');
					deleteLink.attr( 'data-id', returnData[i].dir_id );
					deleteLink.text( 'Delete' );
					deleteLink.click(function(){
						var data_id = $(this).attr('data-id');
						if( confirm( "Are you sure?" ) ) {
							$.post(myBaseURL+'/api/business_directory/delete/' + data_id, {}, function(response) {
								console.log( response );
								if( response.error == true ) {
									showAlert( 'danger', response.message );
								} else {
									showAlert( 'success', response.message );
									$( '#outer-business-' + response.returnData.dir_id ).fadeOut(function(){
										$(this).remove();
									});
								}
							}, 'json').fail(function(xhr, textStatus, errorThrown){
								console.log( xhr.responseText );
							});
						}
					});
					
					oBox.addFooter( links );
					oBox.createBox(true);
				}
				var start = inDavaoApp.PostData.start || 0;
				var limit = inDavaoApp.PostData.limit || 0;
				oBox.addLoadMore( inDavaoApp.API, returnAPI+'-list', (parseInt(start)+parseInt(limit)), limit );
				oBox.showBox();
			}
		},
		
		__myProperties : function( returnData, returnAPI ) {
			if( returnData !== false && returnData.length > 0) { 
				inDavaoApp.currentQueue.children('#loading-img').remove();
				var oBox = this.Box.init( inDavaoApp.currentQueue, inDavaoApp.colWidth, inDavaoApp.API+'-list' );
				for( var i in returnData ) {
					oBox.setId( 'student-'+ returnData[i].re_id );
					oBox.addHeader( returnData[i].re_title );
										
					var boxFooterLeft = $('<a/>').addClass('btn btn-primary btn-xs');
					boxFooterLeft.attr('href', myBaseURL + '/property.html?id=' + returnData[i].re_id);
					boxFooterLeft.text( 'Update' );
					
					oBox.addFooter( boxFooterLeft );
					oBox.createBox(true);
				}
				var start = inDavaoApp.PostData.start || 0;
				var limit = inDavaoApp.PostData.limit || 0;
				oBox.addLoadMore( inDavaoApp.API, returnAPI+'-list', (parseInt(start)+parseInt(limit)), limit );
				oBox.showBox();
			}	
		},
		
		__changePageTitle : function( title ) {
			$('#page-header-section .sub-title').text( title );
		},
		
		__showBoxes : function( id ) {
			$('.current-box-display').slideUp(function(){
				$('#'+id).slideDown(function(){
					$('#'+id+'-breadcrumb').remove();
				});
				
			});
		},
		
		__hideBoxes : function( id, label ) {
			$('#'+id).slideUp(function(){ 
				$(this).addClass('current-box-display');
				var breadcrumbs = $('#page-header-section .breadcrumb');
				var newItem = $('<li/>');
				newItem.attr('id', id+'-breadcrumb');
				newItem.appendTo( breadcrumbs );
				var newItemLink = $('<a/>').appendTo( newItem )
				newItemLink.text( label );
				newItemLink.attr('href', '#');
				newItemLink.attr('data-id', id);
				newItemLink.click(function(){ 
					$(this).prop( 'disabled', true );
					inDavaoApp.__changePageTitle( $(this).text() );
					inDavaoApp.__showBoxes( $(this).attr('data-id') );
				});
			});
		},
		
		ImagePreview : {
			api : false,
			file_list : [],
			__addElement : function( classname, parent, id, element) {
				var element = element || 'div';
				var classname = classname || '';
				var id = id || false;
				
				if( id && $('#'+id).length > 0) {
					return $('#'+id);
				}
				
				var newElement = $('<'+element+'/>').addClass( classname ).appendTo( parent );
				
				if( id ) {
					newElement.attr('id', id );
				}
				
				return newElement;
			},
			
			__toSlug : function(Text) {
				return Text
					.toLowerCase()
					.replace(/[^\w ]+/g,'')
					.replace(/ +/g,'-')
					;
			},
			
			readFile : function(file, target) {
				var reader = new FileReader();

				reader.onload = function (e) {
					target.attr('src', e.target.result);
				}

				reader.readAsDataURL( file );
			},
			
			create : function(container, id, uploadApi, deleteApi) {
				this.uploadApi = uploadApi || false;
				this.deleteApi = deleteApi || false;
				this.Container = this.__addElement('row image-preview', container, id+'-container' );
				return this;
			},
			
			addItem : function( filename, src, prefix ) {
				var prefix = prefix || 'image_row_';
				var src = src || '/assets/images/ajax-loader.gif';
				var file_name = this.__toSlug( filename );
				var col = this.__addElement('col-md-2', this.Container, prefix + file_name );							
				var link = this.__addElement('thumbnail', col, 'image_link_'+file_name, 'a' );
				var image = this.__addElement('', link, 'image_file_'+file_name, 'img' );
				image.attr('src', src);
				link.css('background-color', '#DDD');				
				return image;
			},
			
			addDelete : function( filename, data_id, apiUri, prefix ) {
				var prefix = prefix || 'image_row_';
				var apiUri = apiUri || false;
				var file_name = this.__toSlug( filename );
				var col = this.__addElement('col-md-2', this.Container, prefix + file_name );
				col.css('position', 'relative');
				var image = $('#image_file_' + file_name );
				image.css('opacity', '1');
				var pbar = $('#' + prefix + file_name + ' .progress-container' ).remove();
				var link = this.__addElement('delete', col, false, 'a' );
				var image = this.__addElement('glyphicon glyphicon-remove', link, false, 'i' );
				
				link.css({
					position : 'absolute',
					top : 5,
					right : 20,
					zIndex : 999,
					padding: '2px 3px 0',
					backgroundColor: 'red',
					borderRadius: 20,
					color : '#FFF',
					fontSize: 10,
				});
				link.attr('href', 'javascript:void(0);');
				link.attr('data-id', data_id);
				link.attr('data-slug', file_name);
				if( apiUri !== false ) {
					link.click(function(){
						var data_id = $(this).attr('data-id');
						var data_slug = $(this).attr('data-slug');
						var postData = {
							id : data_id,
							slug : data_slug
						};
						if( confirm('Are you sure?') === false ) {
							return;
						}
						$.post( myBaseURL + '/api/' + apiUri, postData, function(response) {
							console.log( response );
							if( response.error == true ) {
								showAlert( 'danger', response.message );
							} else {
								showAlert( 'success', response.message );
								$('#' + prefix +response.postData.slug).fadeOut(function(){
									$(this).remove();
								});
							}
						}, 'json').fail(function(xhr, textStatus, errorThrown){
							console.log( xhr.responseText );
						});
					});
				}
				return link;
			},
			
			addFiles : function( files ) {
				if( files.length > 0 ) {
					for( var i in files ) {
						if( typeof files[i] === 'object') {
							this.file_list.push(files[i]); 
							var item = this.addItem( files[i].name, undefined, 'new_upload_');
							files[i].uploaded = 0;
							this.readFile( files[i], item );
							item.css('opacity', '0.4');
						}
					}
				}
				
				var self = this;
				setTimeout(function(){
					self.prepUpload();
				}, 2000);
			},
			
			__goodUpload : function(mediaData, returnData) {
				console.log( this.deleteApi );
				this.addDelete( mediaData.client_name, returnData.dir_med_id, this.deleteApi, 'new_upload_' );
			},
			
			__uploadFile : function(postData, progressBar){
				var progressBar = progressBar || false;
				var self = this;
				$.ajax({
					xhr: function()
					  {
						if( progressBar !== false ) {
							var xhr = new window.XMLHttpRequest();
							//Upload progress
							xhr.upload.addEventListener("progress", function(evt){
							  if (evt.lengthComputable) {
								var percentComplete = evt.loaded / evt.total;
								//Do something with upload progress
								//console.log(percentComplete);
								progressBar.attr('aria-valuenow', (percentComplete*100));
								progressBar.css('width', (percentComplete*100)+'%');
							  }
							}, false);
							//Download progress
							xhr.addEventListener("progress", function(evt){
							  if (evt.lengthComputable) {
								var percentComplete = evt.loaded / evt.total;
								//Do something with download progress
								//console.log(percentComplete);
								progressBar.attr('aria-valuenow', (percentComplete*100));
								progressBar.css('width', (percentComplete*100)+'%');
							  }
							}, false);
							return xhr;
						}
					},
					url: myBaseURL + '/api/' + this.uploadApi,
					type: 'POST',
					data:  postData ,
					cache: false,
					dataType: 'json',
					processData: false, // Don't process the files
					contentType: false, // Set content type to false as jQuery will tell the server its a query string request
					success: function(response, textStatus, jqXHR)
					{
						if( response.error == true ) {
							showAlert( 'danger', response.message );
						} else {
							showAlert( 'success', response.message );
							self.__goodUpload( response.media_data, response.returnData );
						}
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\\nError:" + err);
					}
				});
			},
			
			__addProgressBar : function( file_name ) {
				
				var column = $('#new_upload_'+file_name);
			
				// progress bar
				var progressContainer = this.__addElement('progress-container', column, 'upload_progressbar_container_'+file_name );
				progressContainer.css({
					width : '100px',
					position : 'absolute',
					margin : '25% 20%',
					top : 0,
				});
				var progress = this.__addElement('progress sm progress-striped active', progressContainer, 'upload_progress_'+file_name );
				var progressbar = this.__addElement('progress-bar progress-bar-success', progress, 'upload_progressbar_'+file_name );
				progressbar.attr('role', 'progressbar');
				progressbar.attr('aria-valuenow', '20');
				progressbar.attr('aria-valuemin', '0');
				progressbar.attr('aria-valuemax', '100');
				progressbar.css('width', '0');
				return progressbar;
				
			},
			
			__startUpload : function(){
				if( this.for_upload && typeof this.for_upload[0] == 'object' && this.uploadApi !== false ) {
					var iFile = this.for_upload[0];
					var pbar = this.__addProgressBar( this.__toSlug( iFile.name ) );
					var formData = new FormData();
					formData.append('userfile', iFile);
					this.__uploadFile( formData, pbar );
				}
				this.for_upload.splice(0,1);
				if( this.for_upload.length > 0 ) {
					this.__startUpload();
				}
			},
			
			prepUpload : function() {
				this.for_upload = [];
				for( var i in this.file_list ) {
					if( this.file_list[i].uploaded == 0 ) {
						this.for_upload.push( this.file_list[i] );
						this.file_list[i].uploaded = 1;
					}
				}
				this.__startUpload();
			},
		},
		
		ButtonDropdown : {
			__addElement : function( classname, parent, id, element) {
				var element = element || 'div';
				var classname = classname || '';
				var id = id || false;
				
				if( id && $('#'+id).length > 0) {
					return $('#'+id);
				}
				
				var newElement = $('<'+element+'/>').addClass( classname ).appendTo( parent );
				
				if( id ) {
					newElement.attr('id', id );
				}
				
				return newElement;
			},
			
			create : function( container, id, label ) {
				var label = label || 'Action';
				this.Container = this.__addElement('btn-group', container, id );
				this.Button = this.__addElement('btn btn-default dropdown-toggle', this.Container, false, 'button' );
				this.Button.attr('type', 'button');
				this.Button.attr('data-toggle', 'dropdown');
				this.Button.attr('data-id', id);
				this.Button.html(label + ' <span class="caret"></span>');
				this.DropdownMenu = this.__addElement('dropdown-menu', this.Container, false, 'ul' );
				this.DropdownMenu.attr('role', 'menu');
				$('<img/>').attr('src', '/assets/images/ajax-loader.gif').css('margin-left', '15px').appendTo( this.DropdownMenu );
				return this;
			},
						
			addItem : function( id, label, prefix, callback, parent) {
				var callback = callback || false;
				var prefix = prefix || '';
				var parent = parent || false;
				if( parent !== false ) {
					var parentMenu = $('#'+parent+' .dropdown-menu');
					this.newItem = this.__addElement('', parentMenu, 'li-'+prefix+id, 'li' );
					this.newItemLink = this.__addElement('', this.newItem, 'a-'+prefix+id, 'a' );
					this.newItemLink.text( label );
					this.newItemLink.attr('href', 'javascript:void(0);');
					this.newItemLink.attr('data-id', id);
					this.newItemLink.attr('data-parent', parent);
					if( callback !== false ) {
						this.newItemLink.click( callback );
					}
				}
				return this;
			},
		},
		
		ListGroup : {
			__addElement : function( classname, parent, id, element) {
				var element = element || 'ul';
				var classname = classname || 'list-group';
				var id = id || false;
				
				if( id && $('#'+id).length > 0) {
					return $('#'+id);
				}
				
				var newElement = $('<'+element+'/>').addClass( classname ).appendTo( parent );
				
				if( id ) {
					newElement.attr('id', id );
				}
				
				return newElement;
			},
			
			create : function( container, id, element ) {
				this.Container = this.__addElement('list-group', container, id, element );
				return this;
			},
			
			addDelete : function(callback) {
					var callback = callback || false;
					if ( callback !== false && typeof callback == 'function' ) {
						this.deleteCallback = callback;
					} else {
						this.deleteCallback = false;
					}
			},
			
			addItem : function( id, label, prefix, element, href, parent ) {
				var element = element || 'li';
				var href = href || false;
				var prefix = prefix || '';
				var parent = parent || '';
				this.newItem = this.__addElement('list-group-item', this.Container, prefix+id, element );
				this.newItem.text( label );
				if( href !== false ) {
					this.newItem.attr( 'href', href );
				}
				if( this.deleteCallback !== false ) {
					this.deleteButton = this.__addElement('btn btn-danger btn-xs pull-right', this.newItem, false, 'button' );
					this.deleteButton.attr( 'data-id', id );
					this.deleteButton.attr( 'data-parent', parent );
					this.deleteButton.click( this.deleteCallback );
					var deleteIcon = this.__addElement('fa fa-times', this.deleteButton, false, 'i' );
				}
				return this;
			},
		},
		
		Box : { 
								
			__addDiv : function( classname, parent, id, element) {
				var element = element || 'div';
				var id = id || false;
				
				if( id && $('#'+id).length > 0) {
					return $('#'+id);
				}
				
				var newDiv = $('<'+element+'/>').addClass( classname ).appendTo( parent );
				
				if( id ) {
					newDiv.attr('id', id );
				}
				
				return newDiv;
			},
			
			box_items : [],	
			
			pop_box : function() {
				if( typeof this.box_items[0] == 'undefined' ) {
					return false;
				} else {
					var first_queue = this.box_items[0];
					this.box_items.splice(0, 1);
					return first_queue;
				}
			},
			
			init : function( container, width, id ) {
				this.bRow = this.__addDiv('row current-box-display', container, id );
				this.width = width;
				return this;
			},
			
			addFooter : function( footer ) {
				this.footer = footer;
				this.add_footer = true;
				return this;
			},
			
			addBody : function( body ) {
				this.body = body;
				this.add_body = true;
				return this;
			},
			
			addHeader : function( title ) {
				this.title = title;
				this.add_header = true;
				return this;
			},
			
			setId : function( box_id ) {
				this.box_id = box_id;
				return this;
			},
			
			addLoadMore : function(api, id, start, limit) {
				this.add_load_more = true;
				this.loadmore_data_api = api;
				this.loadmore_data_id = id;
				this.loadmore_data_start = start;
				this.loadmore_data_limit = limit;
				return this;
			},
			
			createBox : function( hide ) {
				var hidden = hide || false;
				var bCol = this.__addDiv('col-md-'+this.width, this.bRow, 'outer-' + this.box_id );
				var bOut = this.__addDiv('box box-info', bCol, this.box_id );
				if(hidden) {
					bOut.css('display', 'none');
					this.box_items[this.box_items.length] = '#'+this.box_id;
				}
				var isHeader = this.add_header || false;
				if( isHeader ) {
					var bHead = this.__addDiv('box-header', bOut );
					var bTitle = this.__addDiv('box-title', bHead, false, 'h3' ).html( this.title );
				}
				var isBody = this.add_body || false;
				if( isBody ) {
					var bBody = this.__addDiv('box-body', bOut ).html( this.body );
				}
				var isFooter = this.add_footer || false;
				if( isFooter ) {
					var bFoot = this.__addDiv('box-footer', bOut ).html( this.footer );
				}
			},
			
			appendLoadMore : function() {
				var lmBox = this.__addDiv('col-md-12 text-center loadmore', this.bRow );
				var lmButton = this.__addDiv('btn btn-default btn-sm', lmBox, false, 'button' );
				lmButton.text('Load More');
				lmButton.attr('data-api', this.loadmore_data_api);
				lmButton.attr('data-id', this.loadmore_data_id);
				lmButton.attr('data-start', this.loadmore_data_start);
				lmButton.attr('data-limit', this.loadmore_data_limit);
				lmButton.click(inDavaoApp.__loadMore);
			},
			
			showBox : function(effect) {
				 var self = this, runInit = setInterval(function(){
					 var elm = self.pop_box();
					 
					 if( elm ) {
						 switch( effect ) {
							case 'fade':
								$( elm ).fadeIn();
								break;
							case 'slide':
								$( elm ).slideDown();
								break;
							case 'show':
								$( elm ).show();
								break;
							default:
								$( elm ).fadeIn();
								break;
						 }
					 } else {
						 self.appendLoadMore();
						 clearInterval( runInit );
					 }
				 }, 100);
			}
		},
		
		__loadMore : function() {
			var api = $(this).attr('data-api');
			var start = $(this).attr('data-start');
			var limit = $(this).attr('data-limit');
			inDavaoApp.__getData(api, {start : start, limit : limit}, inDavaoApp.__loadData);
			$('#'+$(this).attr('data-api')+'-list .loadmore').remove();
		},
		
		__loadData : function( data ) {
			var returnData = data.returnData || false;
			var returnAPI = data.api || false;
			if( returnAPI ) {
				switch( returnAPI ) {
					case 'properties':
						inDavaoApp.__myProperties( returnData, returnAPI );
						break;
					case 'business_directory':
						inDavaoApp.__businessDirectory( returnData, returnAPI );
						break;
					case 'business_directory_photos':
						inDavaoApp.__businessDirectoryPhotos( returnData, returnAPI );
						break;
					case 'business_directory_categories':
						inDavaoApp.__businessDirectoryCategories( returnData, returnAPI );
						break;
					case 'business_directory_location':
						inDavaoApp.__businessDirectoryLocation( returnData, returnAPI );
						break;
					case 'business_directory_tags':
						inDavaoApp.__businessDirectoryTags( returnData, returnAPI );
						break;
				}
			}
		},
		
		__getData : function( apiUri, postData, callback ) {
			$.post( myBaseURL + '/api/' + apiUri, postData, function(response) {
				console.log( response );
				if( response.error == true ) {
					showAlert( 'danger', response.message );
				} else {
					inDavaoApp.PostData = response.postData;
					inDavaoApp.API = response.api;
					callback( response );
				}
			}, 'json').fail(function(xhr, textStatus, errorThrown){
				console.log( xhr.responseText );
			});
		},
		
		__popQueue : function() {
			if( typeof window.inDavaoApp_Queue[0] == 'undefined' ) {
				return false;
			} else {
				var first_queue = window.inDavaoApp_Queue[0];
				window.inDavaoApp_Queue.splice(0, 1);
				return first_queue;
			}
		},
		
		__loadQueue : function() {
			var queue_id = this.__popQueue();
			if( queue_id === false ) return;
			this.__prepareData( queue_id );
			this.__loadQueue();
		},
		
		__preparePreview : function( id, container_id, files, uploadApi, deleteApi ) {
			var preview = inDavaoApp.ImagePreview.create( $('#'+id), container_id, uploadApi, deleteApi );
			preview.addFiles( files );
		},
		
		__prepareData : function(id) {
				this.currentQueue = $('#'+id);
				this.colWidth = this.currentQueue.attr('data-colwidth') || 12;
				this.output = this.currentQueue.attr('data-output') || false;
				var apiUri = this.currentQueue.attr('data-api') || false;
				var limit = this.currentQueue.attr('data-limit') || 10;
				var start = this.currentQueue.attr('data-start') || 0;
				var postData = { start : start , limit : limit };
				if( apiUri !== false ) {
					this.__getData( apiUri, postData, this.__loadData);
				}
		},
		
		Effects : {
			add_item : function(elm) {
				window.inDavaoApp_Effects_Queue = window.inDavaoApp_Effects_Queue || [];
				window.inDavaoApp_Effects_Queue[window.inDavaoApp_Effects_Queue.length] = elm;
			},
			pop : function() {
				if( typeof window.inDavaoApp_Effects_Queue[0] == 'undefined' ) {
					return false;
				} else {
					var first_queue = window.inDavaoApp_Effects_Queue[0];
					window.inDavaoApp_Effects_Queue.splice(0, 1);
					return first_queue;
				}
			},
			run : function(effect) {
				 var self = this, runInit = setInterval(function(){
					 var elm = self.pop();
					 if( elm ) {
						 switch( effect ) {
							case 'fade':
								$( elm ).fadeIn();
								break;
							case 'slide':
								$( elm ).slideDown();
								break;
							case 'show':
								$( elm ).show();
								break;
							default:
								$( elm ).fadeIn();
								break;
						 }
					 } else {
						 clearInterval( runInit );
					 }
				 }, 100);
			}
		},
		
		init : function(evt, selector) {
			switch(evt) {
				case 'click':
					var app = this;
					$(selector).on('click', function() {
						var loaded = $(this).attr('data-loaded') || 1,
						data_id = $(this).attr('data-id') || false;
						if( loaded == 0 && data_id !== false) {
							$(this).attr('data-loaded', 1);
							app.__prepareData( data_id );
						}
						var show_hide = $(this).attr('data-show') || false;
						if( show_hide !== false ) {
							$(show_hide).each(function(){
								if( $(this).css('display') === 'none' ) {
									$(this).css('display', 'block').removeClass('hidden');
								} else {
									$(this).css('display', 'none').addClass('hidden');
								}
							});
						}
					});
					break;
				case 'change':
					var app = this;
					$( selector ).on('change', function() {
						var action = $(this).attr('data-action') || false,
						data_id = $(this).attr('data-id') || false,
						container_id = $(this).attr('data-container_id') || false,
						upload_api = $(this).attr('data-api') || false,
						delete_api = $(this).attr('data-delete_api') || false,
						files = this.files || false;
						if( action !== false && data_id !== false) {
							switch( action ) {
								case 'image-upload':
									app.__preparePreview( data_id, container_id, files, upload_api, delete_api );
									break;
							}
						}						
					});
					break;
			}
		},
		
		load : function(identifier) {
			window.inDavaoApp_Queue = window.inDavaoApp_Queue || [];
			$(identifier).each(function() {
				window.inDavaoApp_Queue[window.inDavaoApp_Queue.length] = $(this).prop('id');
			});
			this.__loadQueue();
		}
	}
	
	inDavaoApp.init('click', '.click-load-box');
	inDavaoApp.init('change', '.image-upload-preview');
	
}(jQuery));

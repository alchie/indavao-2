<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
    
    function __construct() 
    {
        parent::__construct();
        
        // DEBUG
        //$this->output->enable_profiler(TRUE); 
        
		// check clear cache
        $this->_clear_cache();
		
        // helpers
        $this->load->helper( array('url', 'cookie', 'settings', 'pagination') );
        
        // libraries
        $this->load->library('template_data');
        	
    }
    
    public function __init() {
		
		
		if( $this->session->userdata('logged_in') )
        {
            $this->template_data->set('current_session', $this->session->all_userdata() );
            $this->template_data->set('current_user_id', $this->session->userdata('user_id') );
            $this->template_data->set('current_user_permissions',  $this->session->userdata('permissions') );
            
            $this->template_data->set('ajax_url', site_url("my/".$this->session->userdata('user_id')."/ajax") );
            
        } else {
			
            $this->template_data->set('current_session', array() );
            
        }
        
        $this->template_data->set('main_page', 'home' );
	    $this->template_data->set('sub_page', 'home' );

        $this->template_data->set('referrer', get_cookie( $this->config->item('cookie_prefix') . 'referrer') );
		
		$this->config->load('facebook', true);
		$this->template_data->set('facebook_app_id', $this->config->item('appId', 'facebook') );
		$this->template_data->set('facebook_permissions', $this->config->item('permissions', 'facebook') );
	
		
		$this->template_data->meta_tags(array(
			'description'=>get_settings_value('default_meta_description', $this->template_data->get('page_title')),
            'keywords'=>get_settings_value('default_meta_keywords', ''),
        ));
        
		$this->template_data->opengraph(array(
            'og:title'=> get_settings_value('default_og_title', $this->template_data->get('page_title')),
            'og:site_name'=>get_settings_value('default_og_site_name', ''),
            'og:description'=>get_settings_value('default_og_description', $this->template_data->get('page_title')),
            'og:url'=> site_url( trim( uri_string(), "/") ),
            'og:type'=>get_settings_value('default_og_type', 'article'),
            'og:image'=> get_settings_value('default_og_image', base_url('/assets/images/logo.png')),
            'og:image:width'=>get_settings_value('default_og_image_width', '289'),
            'og:image:height'=>get_settings_value('default_og_image_height', '102'),
            'fb:app_id'=>get_settings_value('facebook_app_id'),
        ));
        $this->template_data->itemprop(array(
            'name'=> get_settings_value('default_og_title', $this->template_data->get('page_title')),
            'description'=>get_settings_value('default_og_description', ''),
            'image'=> get_settings_value('default_og_image', base_url('/assets/images/logo.png')),
        ));
       
        $this->_init_nav();
	}
    
    public function _init_nav() {
		$this->load->model( array('Taxonomies_ancestors_model', 'Locations_ancestors_model') );
		
		$locations = new $this->Locations_ancestors_model;
		$locations->setJoin('locations', 'locations.loc_id = locations_ancestors.loc_id');
		$locations->setWhere('locations.loc_active', 1);
		$locations->setOrder('locations.loc_name', 'ASC');
		$locations->setLimit(0);
		$locations->cache_on();
		$this->template_data->set("locations", $locations->recursive('loc_parent', get_settings_value('main_location_id'), 'loc_id', 2));
		
		$job_functions = new $this->Taxonomies_ancestors_model;
		$job_functions->setJoin('taxonomies', 'taxonomies.tax_id = taxonomies_ancestors.tax_id');
		$job_functions->setWhere('taxonomies.tax_type', get_settings_value('jobs_tax_name', 'job_function'));
		$job_functions->setWhere('taxonomies.tax_active', 1);
		$job_functions->setOrder('taxonomies.tax_name', 'ASC');
		$job_functions->setLimit(0);
		$job_functions->cache_on();
		$this->template_data->set("job_functions", $job_functions->recursive('tax_parent', get_settings_value('main_job_function_id'), 'tax_id', 2));
		
		$business_categories = new $this->Taxonomies_ancestors_model;
		$business_categories->setJoin('taxonomies', 'taxonomies.tax_id = taxonomies_ancestors.tax_id');
		$business_categories->setWhere('taxonomies.tax_type', get_settings_value('business_tax_name', 'business_category'));
		$business_categories->setWhere('taxonomies.tax_active', 1);
		$business_categories->setOrder('taxonomies.tax_name', 'ASC');
		$business_categories->setLimit(0);
		$business_categories->cache_on();
		$this->template_data->set("business_categories", $business_categories->recursive('tax_parent', get_settings_value('main_business_category_id'), 'tax_id', 2));
	}
	
	public function isPermited( $permission ) {
		if( in_array( $permission, $this->session->userdata('permissions') ) ) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
    public function _mustLogin($redirect='', $r=FALSE)
    {
        if ( !  $this->session->userdata('logged_in') ) {
            if( $redirect != '') {
				$this->output->set_header("Location: " . site_url('login') . "?redirect=" . $redirect);
				exit;
			}
            if ($r===TRUE) {
                redirect('login' , 'refresh');
                exit;
            } else {
                redirect('login' , 'location', 301);
                exit;
            }
            
        }
    }
    
    public function _notLoggedIn() {
        if( $this->session->userdata('logged_in') === TRUE ) {
            redirect("my/{$this->session->userdata('user_id')}/account", 'location', 301);
            exit;
        } 
        return true;
    }
    
    public function _addReferrer($id)
    {
        $cookie = array(
			'name'   => 'referrer',
			'value'  => $id,
			'expire' => '86500',
			'domain' => $this->config->item('cookie_domain'),
			'path'   => $this->config->item('cookie_path'),
			'prefix' => $this->config->item('cookie_prefix'),
			'secure' => $this->config->item('cookie_secure')
		);

		$this->input->set_cookie($cookie);
    }
    
    private function _clear_cache() {
		if( $this->input->get("clear_cache") !== FALSE ) {
			
			 if(( $this->session->userdata('logged_in') === TRUE ) && (in_array('clear_cache', $this->session->userdata('permissions')))) {
				 $this->db->cache_delete();
			 }
		}
	}
	
}

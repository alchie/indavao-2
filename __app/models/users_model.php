<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users_model Class
 *
 * Manipulates `users` table on database

CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `api` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `verified` int(1) DEFAULT '0',
  `link` varchar(255) DEFAULT NULL,
  `referrer` bigint(20) DEFAULT NULL,
  `add_points` int(1) DEFAULT '0',
  `date_joined` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NULL DEFAULT NULL,
  `manager` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
);

 * @package			Model
 * @project			InDavao Network
 * @project_link	http://www.indavao.net
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com
 */
 
class Users_model extends CI_Model {

	protected $user_id;
	protected $api;
	protected $email;
	protected $first_name;
	protected $last_name;
	protected $gender;
	protected $name;
	protected $verified = '0';
	protected $link;
	protected $referrer;
	protected $add_points = '0';
	protected $date_joined;
	protected $last_login;
	protected $manager;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array();
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;
	protected $batch = array();

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: user_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_id = ( ($value == '') && ($this->user_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.user_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'user_id';
		}
		return $this;
	}

	/** 
	* Get the value of `user_id` variable
	* @access public
	* @return String;
	*/

	public function getUserId() {
		return $this->user_id;
	}

	/**
	* Get row by `user_id`
	* @param user_id
	* @return QueryResult
	**/

	public function getByUserId() {
		if($this->user_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.user_id' => $this->user_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_id`
	**/

	public function updateByUserId() {
		if($this->user_id != '') {
			$this->setExclude('user_id');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.user_id' => $this->user_id ) );
			}
		}
	}


	/**
	* Delete row by `user_id`
	**/

	public function deleteByUserId() {
		if($this->user_id != '') {
			return $this->db->delete('users', array('users.user_id' => $this->user_id ) );
		}
	}

	/**
	* Increment row by `user_id`
	**/

	public function incrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `user_id`
	**/

	public function decrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: api
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `api` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setApi($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->api = ( ($value == '') && ($this->api != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.api';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'api';
		}
		return $this;
	}

	/** 
	* Get the value of `api` variable
	* @access public
	* @return String;
	*/

	public function getApi() {
		return $this->api;
	}

	/**
	* Get row by `api`
	* @param api
	* @return QueryResult
	**/

	public function getByApi() {
		if($this->api != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.api' => $this->api), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `api`
	**/

	public function updateByApi() {
		if($this->api != '') {
			$this->setExclude('api');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.api' => $this->api ) );
			}
		}
	}


	/**
	* Delete row by `api`
	**/

	public function deleteByApi() {
		if($this->api != '') {
			return $this->db->delete('users', array('users.api' => $this->api ) );
		}
	}

	/**
	* Increment row by `api`
	**/

	public function incrementByApi() {
		if($this->api != '' && $this->countField != '') {
			$this->db->where('api', $this->api);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `api`
	**/

	public function decrementByApi() {
		if($this->api != '' && $this->countField != '') {
			$this->db->where('api', $this->api);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: api
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: email
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->email = ( ($value == '') && ($this->email != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.email';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'email';
		}
		return $this;
	}

	/** 
	* Get the value of `email` variable
	* @access public
	* @return String;
	*/

	public function getEmail() {
		return $this->email;
	}

	/**
	* Get row by `email`
	* @param email
	* @return QueryResult
	**/

	public function getByEmail() {
		if($this->email != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.email' => $this->email), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `email`
	**/

	public function updateByEmail() {
		if($this->email != '') {
			$this->setExclude('email');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.email' => $this->email ) );
			}
		}
	}


	/**
	* Delete row by `email`
	**/

	public function deleteByEmail() {
		if($this->email != '') {
			return $this->db->delete('users', array('users.email' => $this->email ) );
		}
	}

	/**
	* Increment row by `email`
	**/

	public function incrementByEmail() {
		if($this->email != '' && $this->countField != '') {
			$this->db->where('email', $this->email);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `email`
	**/

	public function decrementByEmail() {
		if($this->email != '' && $this->countField != '') {
			$this->db->where('email', $this->email);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: email
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: first_name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `first_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setFirstName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->first_name = ( ($value == '') && ($this->first_name != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.first_name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'first_name';
		}
		return $this;
	}

	/** 
	* Get the value of `first_name` variable
	* @access public
	* @return String;
	*/

	public function getFirstName() {
		return $this->first_name;
	}

	/**
	* Get row by `first_name`
	* @param first_name
	* @return QueryResult
	**/

	public function getByFirstName() {
		if($this->first_name != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.first_name' => $this->first_name), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `first_name`
	**/

	public function updateByFirstName() {
		if($this->first_name != '') {
			$this->setExclude('first_name');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.first_name' => $this->first_name ) );
			}
		}
	}


	/**
	* Delete row by `first_name`
	**/

	public function deleteByFirstName() {
		if($this->first_name != '') {
			return $this->db->delete('users', array('users.first_name' => $this->first_name ) );
		}
	}

	/**
	* Increment row by `first_name`
	**/

	public function incrementByFirstName() {
		if($this->first_name != '' && $this->countField != '') {
			$this->db->where('first_name', $this->first_name);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `first_name`
	**/

	public function decrementByFirstName() {
		if($this->first_name != '' && $this->countField != '') {
			$this->db->where('first_name', $this->first_name);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: first_name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: last_name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `last_name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->last_name = ( ($value == '') && ($this->last_name != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.last_name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'last_name';
		}
		return $this;
	}

	/** 
	* Get the value of `last_name` variable
	* @access public
	* @return String;
	*/

	public function getLastName() {
		return $this->last_name;
	}

	/**
	* Get row by `last_name`
	* @param last_name
	* @return QueryResult
	**/

	public function getByLastName() {
		if($this->last_name != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.last_name' => $this->last_name), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `last_name`
	**/

	public function updateByLastName() {
		if($this->last_name != '') {
			$this->setExclude('last_name');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.last_name' => $this->last_name ) );
			}
		}
	}


	/**
	* Delete row by `last_name`
	**/

	public function deleteByLastName() {
		if($this->last_name != '') {
			return $this->db->delete('users', array('users.last_name' => $this->last_name ) );
		}
	}

	/**
	* Increment row by `last_name`
	**/

	public function incrementByLastName() {
		if($this->last_name != '' && $this->countField != '') {
			$this->db->where('last_name', $this->last_name);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `last_name`
	**/

	public function decrementByLastName() {
		if($this->last_name != '' && $this->countField != '') {
			$this->db->where('last_name', $this->last_name);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: last_name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: gender
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `gender` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setGender($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->gender = ( ($value == '') && ($this->gender != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.gender';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'gender';
		}
		return $this;
	}

	/** 
	* Get the value of `gender` variable
	* @access public
	* @return String;
	*/

	public function getGender() {
		return $this->gender;
	}

	/**
	* Get row by `gender`
	* @param gender
	* @return QueryResult
	**/

	public function getByGender() {
		if($this->gender != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.gender' => $this->gender), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `gender`
	**/

	public function updateByGender() {
		if($this->gender != '') {
			$this->setExclude('gender');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.gender' => $this->gender ) );
			}
		}
	}


	/**
	* Delete row by `gender`
	**/

	public function deleteByGender() {
		if($this->gender != '') {
			return $this->db->delete('users', array('users.gender' => $this->gender ) );
		}
	}

	/**
	* Increment row by `gender`
	**/

	public function incrementByGender() {
		if($this->gender != '' && $this->countField != '') {
			$this->db->where('gender', $this->gender);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `gender`
	**/

	public function decrementByGender() {
		if($this->gender != '' && $this->countField != '') {
			$this->db->where('gender', $this->gender);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: gender
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->name = ( ($value == '') && ($this->name != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'name';
		}
		return $this;
	}

	/** 
	* Get the value of `name` variable
	* @access public
	* @return String;
	*/

	public function getName() {
		return $this->name;
	}

	/**
	* Get row by `name`
	* @param name
	* @return QueryResult
	**/

	public function getByName() {
		if($this->name != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.name' => $this->name), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `name`
	**/

	public function updateByName() {
		if($this->name != '') {
			$this->setExclude('name');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.name' => $this->name ) );
			}
		}
	}


	/**
	* Delete row by `name`
	**/

	public function deleteByName() {
		if($this->name != '') {
			return $this->db->delete('users', array('users.name' => $this->name ) );
		}
	}

	/**
	* Increment row by `name`
	**/

	public function incrementByName() {
		if($this->name != '' && $this->countField != '') {
			$this->db->where('name', $this->name);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `name`
	**/

	public function decrementByName() {
		if($this->name != '' && $this->countField != '') {
			$this->db->where('name', $this->name);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: verified
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `verified` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setVerified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->verified = ( ($value == '') && ($this->verified != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.verified';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'verified';
		}
		return $this;
	}

	/** 
	* Get the value of `verified` variable
	* @access public
	* @return String;
	*/

	public function getVerified() {
		return $this->verified;
	}

	/**
	* Get row by `verified`
	* @param verified
	* @return QueryResult
	**/

	public function getByVerified() {
		if($this->verified != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.verified' => $this->verified), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `verified`
	**/

	public function updateByVerified() {
		if($this->verified != '') {
			$this->setExclude('verified');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.verified' => $this->verified ) );
			}
		}
	}


	/**
	* Delete row by `verified`
	**/

	public function deleteByVerified() {
		if($this->verified != '') {
			return $this->db->delete('users', array('users.verified' => $this->verified ) );
		}
	}

	/**
	* Increment row by `verified`
	**/

	public function incrementByVerified() {
		if($this->verified != '' && $this->countField != '') {
			$this->db->where('verified', $this->verified);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `verified`
	**/

	public function decrementByVerified() {
		if($this->verified != '' && $this->countField != '') {
			$this->db->where('verified', $this->verified);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: verified
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: link
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `link` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLink($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->link = ( ($value == '') && ($this->link != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.link';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'link';
		}
		return $this;
	}

	/** 
	* Get the value of `link` variable
	* @access public
	* @return String;
	*/

	public function getLink() {
		return $this->link;
	}

	/**
	* Get row by `link`
	* @param link
	* @return QueryResult
	**/

	public function getByLink() {
		if($this->link != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.link' => $this->link), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `link`
	**/

	public function updateByLink() {
		if($this->link != '') {
			$this->setExclude('link');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.link' => $this->link ) );
			}
		}
	}


	/**
	* Delete row by `link`
	**/

	public function deleteByLink() {
		if($this->link != '') {
			return $this->db->delete('users', array('users.link' => $this->link ) );
		}
	}

	/**
	* Increment row by `link`
	**/

	public function incrementByLink() {
		if($this->link != '' && $this->countField != '') {
			$this->db->where('link', $this->link);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `link`
	**/

	public function decrementByLink() {
		if($this->link != '' && $this->countField != '') {
			$this->db->where('link', $this->link);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: link
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: referrer
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `referrer` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReferrer($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->referrer = ( ($value == '') && ($this->referrer != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.referrer';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'referrer';
		}
		return $this;
	}

	/** 
	* Get the value of `referrer` variable
	* @access public
	* @return String;
	*/

	public function getReferrer() {
		return $this->referrer;
	}

	/**
	* Get row by `referrer`
	* @param referrer
	* @return QueryResult
	**/

	public function getByReferrer() {
		if($this->referrer != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.referrer' => $this->referrer), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `referrer`
	**/

	public function updateByReferrer() {
		if($this->referrer != '') {
			$this->setExclude('referrer');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.referrer' => $this->referrer ) );
			}
		}
	}


	/**
	* Delete row by `referrer`
	**/

	public function deleteByReferrer() {
		if($this->referrer != '') {
			return $this->db->delete('users', array('users.referrer' => $this->referrer ) );
		}
	}

	/**
	* Increment row by `referrer`
	**/

	public function incrementByReferrer() {
		if($this->referrer != '' && $this->countField != '') {
			$this->db->where('referrer', $this->referrer);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `referrer`
	**/

	public function decrementByReferrer() {
		if($this->referrer != '' && $this->countField != '') {
			$this->db->where('referrer', $this->referrer);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: referrer
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: add_points
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `add_points` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setAddPoints($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->add_points = ( ($value == '') && ($this->add_points != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.add_points';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'add_points';
		}
		return $this;
	}

	/** 
	* Get the value of `add_points` variable
	* @access public
	* @return String;
	*/

	public function getAddPoints() {
		return $this->add_points;
	}

	/**
	* Get row by `add_points`
	* @param add_points
	* @return QueryResult
	**/

	public function getByAddPoints() {
		if($this->add_points != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.add_points' => $this->add_points), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `add_points`
	**/

	public function updateByAddPoints() {
		if($this->add_points != '') {
			$this->setExclude('add_points');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.add_points' => $this->add_points ) );
			}
		}
	}


	/**
	* Delete row by `add_points`
	**/

	public function deleteByAddPoints() {
		if($this->add_points != '') {
			return $this->db->delete('users', array('users.add_points' => $this->add_points ) );
		}
	}

	/**
	* Increment row by `add_points`
	**/

	public function incrementByAddPoints() {
		if($this->add_points != '' && $this->countField != '') {
			$this->db->where('add_points', $this->add_points);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `add_points`
	**/

	public function decrementByAddPoints() {
		if($this->add_points != '' && $this->countField != '') {
			$this->db->where('add_points', $this->add_points);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: add_points
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: date_joined
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `date_joined` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDateJoined($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->date_joined = ( ($value == '') && ($this->date_joined != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.date_joined';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'date_joined';
		}
		return $this;
	}

	/** 
	* Get the value of `date_joined` variable
	* @access public
	* @return String;
	*/

	public function getDateJoined() {
		return $this->date_joined;
	}

	/**
	* Get row by `date_joined`
	* @param date_joined
	* @return QueryResult
	**/

	public function getByDateJoined() {
		if($this->date_joined != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.date_joined' => $this->date_joined), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `date_joined`
	**/

	public function updateByDateJoined() {
		if($this->date_joined != '') {
			$this->setExclude('date_joined');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.date_joined' => $this->date_joined ) );
			}
		}
	}


	/**
	* Delete row by `date_joined`
	**/

	public function deleteByDateJoined() {
		if($this->date_joined != '') {
			return $this->db->delete('users', array('users.date_joined' => $this->date_joined ) );
		}
	}

	/**
	* Increment row by `date_joined`
	**/

	public function incrementByDateJoined() {
		if($this->date_joined != '' && $this->countField != '') {
			$this->db->where('date_joined', $this->date_joined);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `date_joined`
	**/

	public function decrementByDateJoined() {
		if($this->date_joined != '' && $this->countField != '') {
			$this->db->where('date_joined', $this->date_joined);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: date_joined
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: last_login
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `last_login` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setLastLogin($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->last_login = ( ($value == '') && ($this->last_login != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.last_login';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'last_login';
		}
		return $this;
	}

	/** 
	* Get the value of `last_login` variable
	* @access public
	* @return String;
	*/

	public function getLastLogin() {
		return $this->last_login;
	}

	/**
	* Get row by `last_login`
	* @param last_login
	* @return QueryResult
	**/

	public function getByLastLogin() {
		if($this->last_login != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.last_login' => $this->last_login), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `last_login`
	**/

	public function updateByLastLogin() {
		if($this->last_login != '') {
			$this->setExclude('last_login');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.last_login' => $this->last_login ) );
			}
		}
	}


	/**
	* Delete row by `last_login`
	**/

	public function deleteByLastLogin() {
		if($this->last_login != '') {
			return $this->db->delete('users', array('users.last_login' => $this->last_login ) );
		}
	}

	/**
	* Increment row by `last_login`
	**/

	public function incrementByLastLogin() {
		if($this->last_login != '' && $this->countField != '') {
			$this->db->where('last_login', $this->last_login);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `last_login`
	**/

	public function decrementByLastLogin() {
		if($this->last_login != '' && $this->countField != '') {
			$this->db->where('last_login', $this->last_login);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: last_login
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: manager
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `manager` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setManager($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->manager = ( ($value == '') && ($this->manager != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'users.manager';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'manager';
		}
		return $this;
	}

	/** 
	* Get the value of `manager` variable
	* @access public
	* @return String;
	*/

	public function getManager() {
		return $this->manager;
	}

	/**
	* Get row by `manager`
	* @param manager
	* @return QueryResult
	**/

	public function getByManager() {
		if($this->manager != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('users', array('users.manager' => $this->manager), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `manager`
	**/

	public function updateByManager() {
		if($this->manager != '') {
			$this->setExclude('manager');
			if( $this->getData() ) {
				return $this->db->update('users', $this->getData(), array('users.manager' => $this->manager ) );
			}
		}
	}


	/**
	* Delete row by `manager`
	**/

	public function deleteByManager() {
		if($this->manager != '') {
			return $this->db->delete('users', array('users.manager' => $this->manager ) );
		}
	}

	/**
	* Increment row by `manager`
	**/

	public function incrementByManager() {
		if($this->manager != '' && $this->countField != '') {
			$this->db->where('manager', $this->manager);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('users');
		}
	}

	/**
	* Decrement row by `manager`
	**/

	public function decrementByManager() {
		if($this->manager != '' && $this->countField != '') {
			$this->db->where('manager', $this->manager);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('users');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: manager
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('users', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('users');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('users', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('users', $this->getData() ) === TRUE ) {
				$this->user_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('users', $this->getData() ) === TRUE ) {
				$this->user_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('users');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('users');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='user_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('users');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'users.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("user_id","api","email","first_name","last_name","gender","name","verified","link","referrer","add_points","date_joined","last_login","manager");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/

        protected function __setCondition($condition, $key, $value, $priority) {
		switch( $condition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or
                                );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		
		$return = FALSE;
		
		$conditions = array_merge( $this->having, $this->having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or,
                                $this->having,
                                $this->having_or
                        );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'users'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_null( $index ) ) {
			$this->select[] = $select;
		} else {
			$this->select[$index] = $select;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('users');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->db->from('users');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
	
	/**
	* Batch Insert
	* @access public
	*/
	public function updateBatch() {
		$this->batch = array_merge( $this->batch, array($this->getData()) );
	}
	
	public function insert_batch() {
		if( is_array( $this->batch ) && count( $this->batch ) > 0 ) {
			if( $this->db->insert_batch('users', $this->batch ) === TRUE ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

/* End of file users_model.php */
/* Location: ./application/models/users_model.php */

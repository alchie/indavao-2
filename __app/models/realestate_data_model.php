<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Realestate_data_model Class
 *
 * Manipulates `realestate_data` table on database

CREATE TABLE `realestate_data` (
  `re_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `re_title` varchar(255) NOT NULL,
  `re_slug` varchar(255) DEFAULT NULL,
  `re_type` varchar(10) DEFAULT 'IND',
  `re_active` int(1) DEFAULT '0',
  `re_added` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `re_parent` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `re_status` varchar(20) NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`re_id`)
);

 * @package			Model
 * @project			InDavao Network
 * @project_link	http://www.indavao.net
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com
 */
 
class Realestate_data_model extends CI_Model {

	protected $re_id;
	protected $re_title;
	protected $re_slug;
	protected $re_type;
	protected $re_active = '0';
	protected $re_added;
	protected $re_parent;
	protected $user_id;
	protected $re_status = 'draft';
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array();
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;
	protected $batch = array();

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: re_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_id = ( ($value == '') && ($this->re_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_data.re_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_id';
		}
		return $this;
	}

	/** 
	* Get the value of `re_id` variable
	* @access public
	* @return String;
	*/

	public function getReId() {
		return $this->re_id;
	}

	/**
	* Get row by `re_id`
	* @param re_id
	* @return QueryResult
	**/

	public function getByReId() {
		if($this->re_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_data', array('realestate_data.re_id' => $this->re_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_id`
	**/

	public function updateByReId() {
		if($this->re_id != '') {
			$this->setExclude('re_id');
			if( $this->getData() ) {
				return $this->db->update('realestate_data', $this->getData(), array('realestate_data.re_id' => $this->re_id ) );
			}
		}
	}


	/**
	* Delete row by `re_id`
	**/

	public function deleteByReId() {
		if($this->re_id != '') {
			return $this->db->delete('realestate_data', array('realestate_data.re_id' => $this->re_id ) );
		}
	}

	/**
	* Increment row by `re_id`
	**/

	public function incrementByReId() {
		if($this->re_id != '' && $this->countField != '') {
			$this->db->where('re_id', $this->re_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_data');
		}
	}

	/**
	* Decrement row by `re_id`
	**/

	public function decrementByReId() {
		if($this->re_id != '' && $this->countField != '') {
			$this->db->where('re_id', $this->re_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_data');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_title
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_title` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReTitle($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_title = ( ($value == '') && ($this->re_title != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_data.re_title';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_title';
		}
		return $this;
	}

	/** 
	* Get the value of `re_title` variable
	* @access public
	* @return String;
	*/

	public function getReTitle() {
		return $this->re_title;
	}

	/**
	* Get row by `re_title`
	* @param re_title
	* @return QueryResult
	**/

	public function getByReTitle() {
		if($this->re_title != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_data', array('realestate_data.re_title' => $this->re_title), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_title`
	**/

	public function updateByReTitle() {
		if($this->re_title != '') {
			$this->setExclude('re_title');
			if( $this->getData() ) {
				return $this->db->update('realestate_data', $this->getData(), array('realestate_data.re_title' => $this->re_title ) );
			}
		}
	}


	/**
	* Delete row by `re_title`
	**/

	public function deleteByReTitle() {
		if($this->re_title != '') {
			return $this->db->delete('realestate_data', array('realestate_data.re_title' => $this->re_title ) );
		}
	}

	/**
	* Increment row by `re_title`
	**/

	public function incrementByReTitle() {
		if($this->re_title != '' && $this->countField != '') {
			$this->db->where('re_title', $this->re_title);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_data');
		}
	}

	/**
	* Decrement row by `re_title`
	**/

	public function decrementByReTitle() {
		if($this->re_title != '' && $this->countField != '') {
			$this->db->where('re_title', $this->re_title);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_data');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_title
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_slug
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_slug` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_slug = ( ($value == '') && ($this->re_slug != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_data.re_slug';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_slug';
		}
		return $this;
	}

	/** 
	* Get the value of `re_slug` variable
	* @access public
	* @return String;
	*/

	public function getReSlug() {
		return $this->re_slug;
	}

	/**
	* Get row by `re_slug`
	* @param re_slug
	* @return QueryResult
	**/

	public function getByReSlug() {
		if($this->re_slug != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_data', array('realestate_data.re_slug' => $this->re_slug), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_slug`
	**/

	public function updateByReSlug() {
		if($this->re_slug != '') {
			$this->setExclude('re_slug');
			if( $this->getData() ) {
				return $this->db->update('realestate_data', $this->getData(), array('realestate_data.re_slug' => $this->re_slug ) );
			}
		}
	}


	/**
	* Delete row by `re_slug`
	**/

	public function deleteByReSlug() {
		if($this->re_slug != '') {
			return $this->db->delete('realestate_data', array('realestate_data.re_slug' => $this->re_slug ) );
		}
	}

	/**
	* Increment row by `re_slug`
	**/

	public function incrementByReSlug() {
		if($this->re_slug != '' && $this->countField != '') {
			$this->db->where('re_slug', $this->re_slug);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_data');
		}
	}

	/**
	* Decrement row by `re_slug`
	**/

	public function decrementByReSlug() {
		if($this->re_slug != '' && $this->countField != '') {
			$this->db->where('re_slug', $this->re_slug);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_data');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_slug
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_type
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_type = ( ($value == '') && ($this->re_type != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_data.re_type';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_type';
		}
		return $this;
	}

	/** 
	* Get the value of `re_type` variable
	* @access public
	* @return String;
	*/

	public function getReType() {
		return $this->re_type;
	}

	/**
	* Get row by `re_type`
	* @param re_type
	* @return QueryResult
	**/

	public function getByReType() {
		if($this->re_type != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_data', array('realestate_data.re_type' => $this->re_type), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_type`
	**/

	public function updateByReType() {
		if($this->re_type != '') {
			$this->setExclude('re_type');
			if( $this->getData() ) {
				return $this->db->update('realestate_data', $this->getData(), array('realestate_data.re_type' => $this->re_type ) );
			}
		}
	}


	/**
	* Delete row by `re_type`
	**/

	public function deleteByReType() {
		if($this->re_type != '') {
			return $this->db->delete('realestate_data', array('realestate_data.re_type' => $this->re_type ) );
		}
	}

	/**
	* Increment row by `re_type`
	**/

	public function incrementByReType() {
		if($this->re_type != '' && $this->countField != '') {
			$this->db->where('re_type', $this->re_type);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_data');
		}
	}

	/**
	* Decrement row by `re_type`
	**/

	public function decrementByReType() {
		if($this->re_type != '' && $this->countField != '') {
			$this->db->where('re_type', $this->re_type);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_data');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_type
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_active
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_active = ( ($value == '') && ($this->re_active != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_data.re_active';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_active';
		}
		return $this;
	}

	/** 
	* Get the value of `re_active` variable
	* @access public
	* @return String;
	*/

	public function getReActive() {
		return $this->re_active;
	}

	/**
	* Get row by `re_active`
	* @param re_active
	* @return QueryResult
	**/

	public function getByReActive() {
		if($this->re_active != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_data', array('realestate_data.re_active' => $this->re_active), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_active`
	**/

	public function updateByReActive() {
		if($this->re_active != '') {
			$this->setExclude('re_active');
			if( $this->getData() ) {
				return $this->db->update('realestate_data', $this->getData(), array('realestate_data.re_active' => $this->re_active ) );
			}
		}
	}


	/**
	* Delete row by `re_active`
	**/

	public function deleteByReActive() {
		if($this->re_active != '') {
			return $this->db->delete('realestate_data', array('realestate_data.re_active' => $this->re_active ) );
		}
	}

	/**
	* Increment row by `re_active`
	**/

	public function incrementByReActive() {
		if($this->re_active != '' && $this->countField != '') {
			$this->db->where('re_active', $this->re_active);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_data');
		}
	}

	/**
	* Decrement row by `re_active`
	**/

	public function decrementByReActive() {
		if($this->re_active != '' && $this->countField != '') {
			$this->db->where('re_active', $this->re_active);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_data');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_active
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_added
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_added` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReAdded($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_added = ( ($value == '') && ($this->re_added != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_data.re_added';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_added';
		}
		return $this;
	}

	/** 
	* Get the value of `re_added` variable
	* @access public
	* @return String;
	*/

	public function getReAdded() {
		return $this->re_added;
	}

	/**
	* Get row by `re_added`
	* @param re_added
	* @return QueryResult
	**/

	public function getByReAdded() {
		if($this->re_added != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_data', array('realestate_data.re_added' => $this->re_added), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_added`
	**/

	public function updateByReAdded() {
		if($this->re_added != '') {
			$this->setExclude('re_added');
			if( $this->getData() ) {
				return $this->db->update('realestate_data', $this->getData(), array('realestate_data.re_added' => $this->re_added ) );
			}
		}
	}


	/**
	* Delete row by `re_added`
	**/

	public function deleteByReAdded() {
		if($this->re_added != '') {
			return $this->db->delete('realestate_data', array('realestate_data.re_added' => $this->re_added ) );
		}
	}

	/**
	* Increment row by `re_added`
	**/

	public function incrementByReAdded() {
		if($this->re_added != '' && $this->countField != '') {
			$this->db->where('re_added', $this->re_added);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_data');
		}
	}

	/**
	* Decrement row by `re_added`
	**/

	public function decrementByReAdded() {
		if($this->re_added != '' && $this->countField != '') {
			$this->db->where('re_added', $this->re_added);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_data');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_added
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_parent
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_parent` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReParent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_parent = ( ($value == '') && ($this->re_parent != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_data.re_parent';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_parent';
		}
		return $this;
	}

	/** 
	* Get the value of `re_parent` variable
	* @access public
	* @return String;
	*/

	public function getReParent() {
		return $this->re_parent;
	}

	/**
	* Get row by `re_parent`
	* @param re_parent
	* @return QueryResult
	**/

	public function getByReParent() {
		if($this->re_parent != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_data', array('realestate_data.re_parent' => $this->re_parent), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_parent`
	**/

	public function updateByReParent() {
		if($this->re_parent != '') {
			$this->setExclude('re_parent');
			if( $this->getData() ) {
				return $this->db->update('realestate_data', $this->getData(), array('realestate_data.re_parent' => $this->re_parent ) );
			}
		}
	}


	/**
	* Delete row by `re_parent`
	**/

	public function deleteByReParent() {
		if($this->re_parent != '') {
			return $this->db->delete('realestate_data', array('realestate_data.re_parent' => $this->re_parent ) );
		}
	}

	/**
	* Increment row by `re_parent`
	**/

	public function incrementByReParent() {
		if($this->re_parent != '' && $this->countField != '') {
			$this->db->where('re_parent', $this->re_parent);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_data');
		}
	}

	/**
	* Decrement row by `re_parent`
	**/

	public function decrementByReParent() {
		if($this->re_parent != '' && $this->countField != '') {
			$this->db->where('re_parent', $this->re_parent);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_data');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_parent
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_id = ( ($value == '') && ($this->user_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_data.user_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'user_id';
		}
		return $this;
	}

	/** 
	* Get the value of `user_id` variable
	* @access public
	* @return String;
	*/

	public function getUserId() {
		return $this->user_id;
	}

	/**
	* Get row by `user_id`
	* @param user_id
	* @return QueryResult
	**/

	public function getByUserId() {
		if($this->user_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_data', array('realestate_data.user_id' => $this->user_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_id`
	**/

	public function updateByUserId() {
		if($this->user_id != '') {
			$this->setExclude('user_id');
			if( $this->getData() ) {
				return $this->db->update('realestate_data', $this->getData(), array('realestate_data.user_id' => $this->user_id ) );
			}
		}
	}


	/**
	* Delete row by `user_id`
	**/

	public function deleteByUserId() {
		if($this->user_id != '') {
			return $this->db->delete('realestate_data', array('realestate_data.user_id' => $this->user_id ) );
		}
	}

	/**
	* Increment row by `user_id`
	**/

	public function incrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_data');
		}
	}

	/**
	* Decrement row by `user_id`
	**/

	public function decrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_data');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: re_status
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `re_status` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setReStatus($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->re_status = ( ($value == '') && ($this->re_status != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'realestate_data.re_status';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 're_status';
		}
		return $this;
	}

	/** 
	* Get the value of `re_status` variable
	* @access public
	* @return String;
	*/

	public function getReStatus() {
		return $this->re_status;
	}

	/**
	* Get row by `re_status`
	* @param re_status
	* @return QueryResult
	**/

	public function getByReStatus() {
		if($this->re_status != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('realestate_data', array('realestate_data.re_status' => $this->re_status), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `re_status`
	**/

	public function updateByReStatus() {
		if($this->re_status != '') {
			$this->setExclude('re_status');
			if( $this->getData() ) {
				return $this->db->update('realestate_data', $this->getData(), array('realestate_data.re_status' => $this->re_status ) );
			}
		}
	}


	/**
	* Delete row by `re_status`
	**/

	public function deleteByReStatus() {
		if($this->re_status != '') {
			return $this->db->delete('realestate_data', array('realestate_data.re_status' => $this->re_status ) );
		}
	}

	/**
	* Increment row by `re_status`
	**/

	public function incrementByReStatus() {
		if($this->re_status != '' && $this->countField != '') {
			$this->db->where('re_status', $this->re_status);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('realestate_data');
		}
	}

	/**
	* Decrement row by `re_status`
	**/

	public function decrementByReStatus() {
		if($this->re_status != '' && $this->countField != '') {
			$this->db->where('re_status', $this->re_status);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('realestate_data');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: re_status
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('realestate_data', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('realestate_data');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('realestate_data', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('realestate_data', $this->getData() ) === TRUE ) {
				$this->re_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('realestate_data', $this->getData() ) === TRUE ) {
				$this->re_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('realestate_data');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('realestate_data');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='re_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('realestate_data');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'realestate_data.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("re_id","re_title","re_slug","re_type","re_active","re_added","re_parent","user_id","re_status");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/

        protected function __setCondition($condition, $key, $value, $priority) {
		switch( $condition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or
                                );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		
		$return = FALSE;
		
		$conditions = array_merge( $this->having, $this->having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or,
                                $this->having,
                                $this->having_or
                        );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'realestate_data'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_null( $index ) ) {
			$this->select[] = $select;
		} else {
			$this->select[$index] = $select;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('realestate_data');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->db->from('realestate_data');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
	
	/**
	* Batch Insert
	* @access public
	*/
	public function updateBatch() {
		$this->batch = array_merge( $this->batch, array($this->getData()) );
	}
	
	public function insert_batch() {
		if( is_array( $this->batch ) && count( $this->batch ) > 0 ) {
			if( $this->db->insert_batch('realestate_data', $this->batch ) === TRUE ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

/* End of file realestate_data_model.php */
/* Location: ./application/models/realestate_data_model.php */

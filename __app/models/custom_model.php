<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_model extends CI_Model {

	function __construct() {
		parent::__construct();
	}
	
	function get_my_tasks( $user_id ) {
		$sql = "SELECT 
		re_id as object_id, 
		re_title as title, 
		re_slug as slug, 
		'realestate' as type, 
		tu.page_id, 
		tu.page_name, 
		tu.page_url, 
		tu.active as active, 
		tu.object_type as tu_type, 
		tu.tasks_url_id as tu_task_id,
		(SELECT post_url 
		FROM tasks_verified tv 
		WHERE tv.tasks_url_id = tu.tasks_url_id AND re.re_id = tv.object_id AND tv.object_type = type
		LIMIT 1) as verified_post_id,
		(SELECT post_url 
		FROM tasks_verified tv 
		WHERE tv.tasks_url_id = '{$user_id}' AND re.re_id = tv.object_id AND tv.object_type = type
		LIMIT 1) as own_profile_post_id,
		(SELECT meta_value FROM realestate_meta rm WHERE rm.re_id = re.re_id AND meta_key='task_message' AND re_m_active=1 LIMIT 1) as task_message,
		(SELECT meta_value FROM realestate_meta rm WHERE rm.re_id = re.re_id AND meta_key='is_task' AND re_m_active=1 LIMIT 1) as is_task
		FROM 
		`realestate_data` re, 
		`tasks_urls` tu 

		HAVING type = tu_type 
		AND verified_post_id IS NULL
		AND is_task = 1
		AND active = 1";
		
		//$this->db->cache_on();
		$dbres = $this->db->query($sql);
		
		return $dbres->first_row();
		
	}
	
	function get_my_shares( $user_id, $start=0, $limit=10 ) {
		$sql = "SELECT 
		*,
		'realestate' as type,
		DATEDIFF(tv.date_verified, NOW()) as days,
		tv.tasks_url_id as tv_tasks_url_id,
		tv.object_id as tv_object_id,
		tv.object_type as tv_object_type,
		tv.post_url as tv_post_id
		FROM 
		`tasks_verified` tv 
		
		LEFT OUTER JOIN `tasks_urls` tu 
		ON (tv.tasks_url_id = tu.tasks_url_id)
		
		LEFT OUTER JOIN `realestate_data` re 
		ON (tv.object_id = re.re_id AND tv.object_type = 'realestate')
		
		LEFT OUTER JOIN `users_points` up 
		ON (tv.tasks_ver_id = up.object_id AND up.object_type = 'tasks' AND up.user_id = '{$user_id}')
		
		WHERE 
		tv.user_id = '{$user_id}'
		
		ORDER BY points_credited ASC, days ASC
		
		LIMIT {$start}, {$limit}
		;
		";
		//$this->db->cache_on();
		$dbres = $this->db->query($sql);
		
		return $dbres->result();
	}
}



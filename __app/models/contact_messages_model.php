<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Contact_messages_model Class
 *
 * Manipulates `contact_messages` table on database

CREATE TABLE `contact_messages` (
  `msg_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_id` bigint(20) DEFAULT NULL,
  `object_type` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `message` text,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` bigint(20) DEFAULT NULL,
  `msg_type` varchar(100) NOT NULL,
  PRIMARY KEY (`msg_id`)
);

 * @package			Model
 * @project			InDavao Network
 * @project_link	http://www.indavao.net
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com
 */
 
class Contact_messages_model extends CI_Model {

	protected $msg_id;
	protected $object_id;
	protected $object_type;
	protected $name;
	protected $phone;
	protected $email;
	protected $message;
	protected $date_sent;
	protected $user_id;
	protected $msg_type;
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array();
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;
	protected $batch = array();

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: msg_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `msg_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMsgId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->msg_id = ( ($value == '') && ($this->msg_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.msg_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'msg_id';
		}
		return $this;
	}

	/** 
	* Get the value of `msg_id` variable
	* @access public
	* @return String;
	*/

	public function getMsgId() {
		return $this->msg_id;
	}

	/**
	* Get row by `msg_id`
	* @param msg_id
	* @return QueryResult
	**/

	public function getByMsgId() {
		if($this->msg_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.msg_id' => $this->msg_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `msg_id`
	**/

	public function updateByMsgId() {
		if($this->msg_id != '') {
			$this->setExclude('msg_id');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.msg_id' => $this->msg_id ) );
			}
		}
	}


	/**
	* Delete row by `msg_id`
	**/

	public function deleteByMsgId() {
		if($this->msg_id != '') {
			return $this->db->delete('contact_messages', array('contact_messages.msg_id' => $this->msg_id ) );
		}
	}

	/**
	* Increment row by `msg_id`
	**/

	public function incrementByMsgId() {
		if($this->msg_id != '' && $this->countField != '') {
			$this->db->where('msg_id', $this->msg_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `msg_id`
	**/

	public function decrementByMsgId() {
		if($this->msg_id != '' && $this->countField != '') {
			$this->db->where('msg_id', $this->msg_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: msg_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: object_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `object_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setObjectId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->object_id = ( ($value == '') && ($this->object_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.object_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'object_id';
		}
		return $this;
	}

	/** 
	* Get the value of `object_id` variable
	* @access public
	* @return String;
	*/

	public function getObjectId() {
		return $this->object_id;
	}

	/**
	* Get row by `object_id`
	* @param object_id
	* @return QueryResult
	**/

	public function getByObjectId() {
		if($this->object_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.object_id' => $this->object_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `object_id`
	**/

	public function updateByObjectId() {
		if($this->object_id != '') {
			$this->setExclude('object_id');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.object_id' => $this->object_id ) );
			}
		}
	}


	/**
	* Delete row by `object_id`
	**/

	public function deleteByObjectId() {
		if($this->object_id != '') {
			return $this->db->delete('contact_messages', array('contact_messages.object_id' => $this->object_id ) );
		}
	}

	/**
	* Increment row by `object_id`
	**/

	public function incrementByObjectId() {
		if($this->object_id != '' && $this->countField != '') {
			$this->db->where('object_id', $this->object_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `object_id`
	**/

	public function decrementByObjectId() {
		if($this->object_id != '' && $this->countField != '') {
			$this->db->where('object_id', $this->object_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: object_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: object_type
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `object_type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setObjectType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->object_type = ( ($value == '') && ($this->object_type != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.object_type';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'object_type';
		}
		return $this;
	}

	/** 
	* Get the value of `object_type` variable
	* @access public
	* @return String;
	*/

	public function getObjectType() {
		return $this->object_type;
	}

	/**
	* Get row by `object_type`
	* @param object_type
	* @return QueryResult
	**/

	public function getByObjectType() {
		if($this->object_type != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.object_type' => $this->object_type), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `object_type`
	**/

	public function updateByObjectType() {
		if($this->object_type != '') {
			$this->setExclude('object_type');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.object_type' => $this->object_type ) );
			}
		}
	}


	/**
	* Delete row by `object_type`
	**/

	public function deleteByObjectType() {
		if($this->object_type != '') {
			return $this->db->delete('contact_messages', array('contact_messages.object_type' => $this->object_type ) );
		}
	}

	/**
	* Increment row by `object_type`
	**/

	public function incrementByObjectType() {
		if($this->object_type != '' && $this->countField != '') {
			$this->db->where('object_type', $this->object_type);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `object_type`
	**/

	public function decrementByObjectType() {
		if($this->object_type != '' && $this->countField != '') {
			$this->db->where('object_type', $this->object_type);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: object_type
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: name
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->name = ( ($value == '') && ($this->name != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.name';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'name';
		}
		return $this;
	}

	/** 
	* Get the value of `name` variable
	* @access public
	* @return String;
	*/

	public function getName() {
		return $this->name;
	}

	/**
	* Get row by `name`
	* @param name
	* @return QueryResult
	**/

	public function getByName() {
		if($this->name != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.name' => $this->name), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `name`
	**/

	public function updateByName() {
		if($this->name != '') {
			$this->setExclude('name');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.name' => $this->name ) );
			}
		}
	}


	/**
	* Delete row by `name`
	**/

	public function deleteByName() {
		if($this->name != '') {
			return $this->db->delete('contact_messages', array('contact_messages.name' => $this->name ) );
		}
	}

	/**
	* Increment row by `name`
	**/

	public function incrementByName() {
		if($this->name != '' && $this->countField != '') {
			$this->db->where('name', $this->name);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `name`
	**/

	public function decrementByName() {
		if($this->name != '' && $this->countField != '') {
			$this->db->where('name', $this->name);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: name
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: phone
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `phone` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setPhone($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->phone = ( ($value == '') && ($this->phone != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.phone';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'phone';
		}
		return $this;
	}

	/** 
	* Get the value of `phone` variable
	* @access public
	* @return String;
	*/

	public function getPhone() {
		return $this->phone;
	}

	/**
	* Get row by `phone`
	* @param phone
	* @return QueryResult
	**/

	public function getByPhone() {
		if($this->phone != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.phone' => $this->phone), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `phone`
	**/

	public function updateByPhone() {
		if($this->phone != '') {
			$this->setExclude('phone');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.phone' => $this->phone ) );
			}
		}
	}


	/**
	* Delete row by `phone`
	**/

	public function deleteByPhone() {
		if($this->phone != '') {
			return $this->db->delete('contact_messages', array('contact_messages.phone' => $this->phone ) );
		}
	}

	/**
	* Increment row by `phone`
	**/

	public function incrementByPhone() {
		if($this->phone != '' && $this->countField != '') {
			$this->db->where('phone', $this->phone);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `phone`
	**/

	public function decrementByPhone() {
		if($this->phone != '' && $this->countField != '') {
			$this->db->where('phone', $this->phone);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: phone
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: email
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->email = ( ($value == '') && ($this->email != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.email';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'email';
		}
		return $this;
	}

	/** 
	* Get the value of `email` variable
	* @access public
	* @return String;
	*/

	public function getEmail() {
		return $this->email;
	}

	/**
	* Get row by `email`
	* @param email
	* @return QueryResult
	**/

	public function getByEmail() {
		if($this->email != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.email' => $this->email), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `email`
	**/

	public function updateByEmail() {
		if($this->email != '') {
			$this->setExclude('email');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.email' => $this->email ) );
			}
		}
	}


	/**
	* Delete row by `email`
	**/

	public function deleteByEmail() {
		if($this->email != '') {
			return $this->db->delete('contact_messages', array('contact_messages.email' => $this->email ) );
		}
	}

	/**
	* Increment row by `email`
	**/

	public function incrementByEmail() {
		if($this->email != '' && $this->countField != '') {
			$this->db->where('email', $this->email);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `email`
	**/

	public function decrementByEmail() {
		if($this->email != '' && $this->countField != '') {
			$this->db->where('email', $this->email);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: email
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: message
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `message` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMessage($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->message = ( ($value == '') && ($this->message != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.message';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'message';
		}
		return $this;
	}

	/** 
	* Get the value of `message` variable
	* @access public
	* @return String;
	*/

	public function getMessage() {
		return $this->message;
	}

	/**
	* Get row by `message`
	* @param message
	* @return QueryResult
	**/

	public function getByMessage() {
		if($this->message != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.message' => $this->message), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `message`
	**/

	public function updateByMessage() {
		if($this->message != '') {
			$this->setExclude('message');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.message' => $this->message ) );
			}
		}
	}


	/**
	* Delete row by `message`
	**/

	public function deleteByMessage() {
		if($this->message != '') {
			return $this->db->delete('contact_messages', array('contact_messages.message' => $this->message ) );
		}
	}

	/**
	* Increment row by `message`
	**/

	public function incrementByMessage() {
		if($this->message != '' && $this->countField != '') {
			$this->db->where('message', $this->message);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `message`
	**/

	public function decrementByMessage() {
		if($this->message != '' && $this->countField != '') {
			$this->db->where('message', $this->message);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: message
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: date_sent
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `date_sent` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDateSent($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->date_sent = ( ($value == '') && ($this->date_sent != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.date_sent';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'date_sent';
		}
		return $this;
	}

	/** 
	* Get the value of `date_sent` variable
	* @access public
	* @return String;
	*/

	public function getDateSent() {
		return $this->date_sent;
	}

	/**
	* Get row by `date_sent`
	* @param date_sent
	* @return QueryResult
	**/

	public function getByDateSent() {
		if($this->date_sent != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.date_sent' => $this->date_sent), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `date_sent`
	**/

	public function updateByDateSent() {
		if($this->date_sent != '') {
			$this->setExclude('date_sent');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.date_sent' => $this->date_sent ) );
			}
		}
	}


	/**
	* Delete row by `date_sent`
	**/

	public function deleteByDateSent() {
		if($this->date_sent != '') {
			return $this->db->delete('contact_messages', array('contact_messages.date_sent' => $this->date_sent ) );
		}
	}

	/**
	* Increment row by `date_sent`
	**/

	public function incrementByDateSent() {
		if($this->date_sent != '' && $this->countField != '') {
			$this->db->where('date_sent', $this->date_sent);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `date_sent`
	**/

	public function decrementByDateSent() {
		if($this->date_sent != '' && $this->countField != '') {
			$this->db->where('date_sent', $this->date_sent);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: date_sent
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: user_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->user_id = ( ($value == '') && ($this->user_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.user_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'user_id';
		}
		return $this;
	}

	/** 
	* Get the value of `user_id` variable
	* @access public
	* @return String;
	*/

	public function getUserId() {
		return $this->user_id;
	}

	/**
	* Get row by `user_id`
	* @param user_id
	* @return QueryResult
	**/

	public function getByUserId() {
		if($this->user_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.user_id' => $this->user_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `user_id`
	**/

	public function updateByUserId() {
		if($this->user_id != '') {
			$this->setExclude('user_id');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.user_id' => $this->user_id ) );
			}
		}
	}


	/**
	* Delete row by `user_id`
	**/

	public function deleteByUserId() {
		if($this->user_id != '') {
			return $this->db->delete('contact_messages', array('contact_messages.user_id' => $this->user_id ) );
		}
	}

	/**
	* Increment row by `user_id`
	**/

	public function incrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `user_id`
	**/

	public function decrementByUserId() {
		if($this->user_id != '' && $this->countField != '') {
			$this->db->where('user_id', $this->user_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: user_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: msg_type
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `msg_type` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setMsgType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->msg_type = ( ($value == '') && ($this->msg_type != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'contact_messages.msg_type';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'msg_type';
		}
		return $this;
	}

	/** 
	* Get the value of `msg_type` variable
	* @access public
	* @return String;
	*/

	public function getMsgType() {
		return $this->msg_type;
	}

	/**
	* Get row by `msg_type`
	* @param msg_type
	* @return QueryResult
	**/

	public function getByMsgType() {
		if($this->msg_type != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('contact_messages', array('contact_messages.msg_type' => $this->msg_type), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `msg_type`
	**/

	public function updateByMsgType() {
		if($this->msg_type != '') {
			$this->setExclude('msg_type');
			if( $this->getData() ) {
				return $this->db->update('contact_messages', $this->getData(), array('contact_messages.msg_type' => $this->msg_type ) );
			}
		}
	}


	/**
	* Delete row by `msg_type`
	**/

	public function deleteByMsgType() {
		if($this->msg_type != '') {
			return $this->db->delete('contact_messages', array('contact_messages.msg_type' => $this->msg_type ) );
		}
	}

	/**
	* Increment row by `msg_type`
	**/

	public function incrementByMsgType() {
		if($this->msg_type != '' && $this->countField != '') {
			$this->db->where('msg_type', $this->msg_type);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('contact_messages');
		}
	}

	/**
	* Decrement row by `msg_type`
	**/

	public function decrementByMsgType() {
		if($this->msg_type != '' && $this->countField != '') {
			$this->db->where('msg_type', $this->msg_type);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('contact_messages');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: msg_type
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('contact_messages', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('contact_messages');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('contact_messages', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('contact_messages', $this->getData() ) === TRUE ) {
				$this->msg_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('contact_messages', $this->getData() ) === TRUE ) {
				$this->msg_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('contact_messages');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('contact_messages');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='msg_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('contact_messages');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'contact_messages.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("msg_id","object_id","object_type","name","phone","email","message","date_sent","user_id","msg_type");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/

        protected function __setCondition($condition, $key, $value, $priority) {
		switch( $condition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or
                                );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		
		$return = FALSE;
		
		$conditions = array_merge( $this->having, $this->having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or,
                                $this->having,
                                $this->having_or
                        );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'contact_messages'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_null( $index ) ) {
			$this->select[] = $select;
		} else {
			$this->select[$index] = $select;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('contact_messages');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->db->from('contact_messages');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
	
	/**
	* Batch Insert
	* @access public
	*/
	public function updateBatch() {
		$this->batch = array_merge( $this->batch, array($this->getData()) );
	}
	
	public function insert_batch() {
		if( is_array( $this->batch ) && count( $this->batch ) > 0 ) {
			if( $this->db->insert_batch('contact_messages', $this->batch ) === TRUE ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

/* End of file contact_messages_model.php */
/* Location: ./application/models/contact_messages_model.php */

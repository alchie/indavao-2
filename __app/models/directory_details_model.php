<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Directory_details_model Class
 *
 * Manipulates `directory_details` table on database

CREATE TABLE `directory_details` (
  `dir_d_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dir_id` bigint(20) NOT NULL,
  `dir_d_key` varchar(100) NOT NULL,
  `dir_d_value` longtext NOT NULL,
  `dir_d_label` varchar(100) DEFAULT NULL,
  `dir_d_active` int(1) DEFAULT '0',
  `dir_d_order` int(4) DEFAULT '0',
  PRIMARY KEY (`dir_d_id`),
  KEY `dir_id` (`dir_id`),
  CONSTRAINT `DIR_ID` FOREIGN KEY (`dir_id`) REFERENCES `directory_data` (`dir_id`) ON DELETE CASCADE ON UPDATE CASCADE
);

 * @package			Model
 * @project			InDavao Network
 * @project_link	http://www.indavao.net
 * @author			Chester Alan Tagudin
 * @author_link		http://www.chesteralan.com
 */
 
class Directory_details_model extends CI_Model {

	protected $dir_d_id;
	protected $dir_id;
	protected $dir_d_key;
	protected $dir_d_value;
	protected $dir_d_label;
	protected $dir_d_active = '0';
	protected $dir_d_order = '0';
	protected $dataFields = array();
	protected $select = array();
	protected $join = array();
	protected $where = array();
	protected $where_or = array();
	protected $where_in = array();
	protected $where_in_or = array();
	protected $where_not_in = array();
	protected $where_not_in_or = array();
	protected $like = array();
	protected $like_or = array();
	protected $like_not = array();
	protected $like_not_or = array();
	protected $having = array();
	protected $having_or = array();
	protected $group_by = array();
	protected $filter = array();
	protected $order = array();
	protected $exclude = array();
	protected $required = array();
	protected $countField = '';
	protected $start = 0;
	protected $limit = 10;
	protected $results = FALSE;
	protected $distinct = FALSE;
	protected $cache_on = FALSE;
	protected $batch = array();

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct() {
		parent::__construct();
	}

	// --------------------------------------------------------------------


	// --------------------------------------------------------------------
	// Start Field: dir_d_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `dir_d_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDirDId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->dir_d_id = ( ($value == '') && ($this->dir_d_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'directory_details.dir_d_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'dir_d_id';
		}
		return $this;
	}

	/** 
	* Get the value of `dir_d_id` variable
	* @access public
	* @return String;
	*/

	public function getDirDId() {
		return $this->dir_d_id;
	}

	/**
	* Get row by `dir_d_id`
	* @param dir_d_id
	* @return QueryResult
	**/

	public function getByDirDId() {
		if($this->dir_d_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('directory_details', array('directory_details.dir_d_id' => $this->dir_d_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `dir_d_id`
	**/

	public function updateByDirDId() {
		if($this->dir_d_id != '') {
			$this->setExclude('dir_d_id');
			if( $this->getData() ) {
				return $this->db->update('directory_details', $this->getData(), array('directory_details.dir_d_id' => $this->dir_d_id ) );
			}
		}
	}


	/**
	* Delete row by `dir_d_id`
	**/

	public function deleteByDirDId() {
		if($this->dir_d_id != '') {
			return $this->db->delete('directory_details', array('directory_details.dir_d_id' => $this->dir_d_id ) );
		}
	}

	/**
	* Increment row by `dir_d_id`
	**/

	public function incrementByDirDId() {
		if($this->dir_d_id != '' && $this->countField != '') {
			$this->db->where('dir_d_id', $this->dir_d_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('directory_details');
		}
	}

	/**
	* Decrement row by `dir_d_id`
	**/

	public function decrementByDirDId() {
		if($this->dir_d_id != '' && $this->countField != '') {
			$this->db->where('dir_d_id', $this->dir_d_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('directory_details');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: dir_d_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: dir_id
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `dir_id` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDirId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->dir_id = ( ($value == '') && ($this->dir_id != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'directory_details.dir_id';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'dir_id';
		}
		return $this;
	}

	/** 
	* Get the value of `dir_id` variable
	* @access public
	* @return String;
	*/

	public function getDirId() {
		return $this->dir_id;
	}

	/**
	* Get row by `dir_id`
	* @param dir_id
	* @return QueryResult
	**/

	public function getByDirId() {
		if($this->dir_id != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('directory_details', array('directory_details.dir_id' => $this->dir_id), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `dir_id`
	**/

	public function updateByDirId() {
		if($this->dir_id != '') {
			$this->setExclude('dir_id');
			if( $this->getData() ) {
				return $this->db->update('directory_details', $this->getData(), array('directory_details.dir_id' => $this->dir_id ) );
			}
		}
	}


	/**
	* Delete row by `dir_id`
	**/

	public function deleteByDirId() {
		if($this->dir_id != '') {
			return $this->db->delete('directory_details', array('directory_details.dir_id' => $this->dir_id ) );
		}
	}

	/**
	* Increment row by `dir_id`
	**/

	public function incrementByDirId() {
		if($this->dir_id != '' && $this->countField != '') {
			$this->db->where('dir_id', $this->dir_id);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('directory_details');
		}
	}

	/**
	* Decrement row by `dir_id`
	**/

	public function decrementByDirId() {
		if($this->dir_id != '' && $this->countField != '') {
			$this->db->where('dir_id', $this->dir_id);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('directory_details');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: dir_id
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: dir_d_key
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `dir_d_key` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDirDKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->dir_d_key = ( ($value == '') && ($this->dir_d_key != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'directory_details.dir_d_key';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'dir_d_key';
		}
		return $this;
	}

	/** 
	* Get the value of `dir_d_key` variable
	* @access public
	* @return String;
	*/

	public function getDirDKey() {
		return $this->dir_d_key;
	}

	/**
	* Get row by `dir_d_key`
	* @param dir_d_key
	* @return QueryResult
	**/

	public function getByDirDKey() {
		if($this->dir_d_key != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('directory_details', array('directory_details.dir_d_key' => $this->dir_d_key), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `dir_d_key`
	**/

	public function updateByDirDKey() {
		if($this->dir_d_key != '') {
			$this->setExclude('dir_d_key');
			if( $this->getData() ) {
				return $this->db->update('directory_details', $this->getData(), array('directory_details.dir_d_key' => $this->dir_d_key ) );
			}
		}
	}


	/**
	* Delete row by `dir_d_key`
	**/

	public function deleteByDirDKey() {
		if($this->dir_d_key != '') {
			return $this->db->delete('directory_details', array('directory_details.dir_d_key' => $this->dir_d_key ) );
		}
	}

	/**
	* Increment row by `dir_d_key`
	**/

	public function incrementByDirDKey() {
		if($this->dir_d_key != '' && $this->countField != '') {
			$this->db->where('dir_d_key', $this->dir_d_key);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('directory_details');
		}
	}

	/**
	* Decrement row by `dir_d_key`
	**/

	public function decrementByDirDKey() {
		if($this->dir_d_key != '' && $this->countField != '') {
			$this->db->where('dir_d_key', $this->dir_d_key);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('directory_details');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: dir_d_key
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: dir_d_value
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `dir_d_value` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDirDValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->dir_d_value = ( ($value == '') && ($this->dir_d_value != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'directory_details.dir_d_value';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'dir_d_value';
		}
		return $this;
	}

	/** 
	* Get the value of `dir_d_value` variable
	* @access public
	* @return String;
	*/

	public function getDirDValue() {
		return $this->dir_d_value;
	}

	/**
	* Get row by `dir_d_value`
	* @param dir_d_value
	* @return QueryResult
	**/

	public function getByDirDValue() {
		if($this->dir_d_value != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('directory_details', array('directory_details.dir_d_value' => $this->dir_d_value), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `dir_d_value`
	**/

	public function updateByDirDValue() {
		if($this->dir_d_value != '') {
			$this->setExclude('dir_d_value');
			if( $this->getData() ) {
				return $this->db->update('directory_details', $this->getData(), array('directory_details.dir_d_value' => $this->dir_d_value ) );
			}
		}
	}


	/**
	* Delete row by `dir_d_value`
	**/

	public function deleteByDirDValue() {
		if($this->dir_d_value != '') {
			return $this->db->delete('directory_details', array('directory_details.dir_d_value' => $this->dir_d_value ) );
		}
	}

	/**
	* Increment row by `dir_d_value`
	**/

	public function incrementByDirDValue() {
		if($this->dir_d_value != '' && $this->countField != '') {
			$this->db->where('dir_d_value', $this->dir_d_value);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('directory_details');
		}
	}

	/**
	* Decrement row by `dir_d_value`
	**/

	public function decrementByDirDValue() {
		if($this->dir_d_value != '' && $this->countField != '') {
			$this->db->where('dir_d_value', $this->dir_d_value);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('directory_details');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: dir_d_value
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: dir_d_label
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `dir_d_label` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDirDLabel($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->dir_d_label = ( ($value == '') && ($this->dir_d_label != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'directory_details.dir_d_label';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'dir_d_label';
		}
		return $this;
	}

	/** 
	* Get the value of `dir_d_label` variable
	* @access public
	* @return String;
	*/

	public function getDirDLabel() {
		return $this->dir_d_label;
	}

	/**
	* Get row by `dir_d_label`
	* @param dir_d_label
	* @return QueryResult
	**/

	public function getByDirDLabel() {
		if($this->dir_d_label != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('directory_details', array('directory_details.dir_d_label' => $this->dir_d_label), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `dir_d_label`
	**/

	public function updateByDirDLabel() {
		if($this->dir_d_label != '') {
			$this->setExclude('dir_d_label');
			if( $this->getData() ) {
				return $this->db->update('directory_details', $this->getData(), array('directory_details.dir_d_label' => $this->dir_d_label ) );
			}
		}
	}


	/**
	* Delete row by `dir_d_label`
	**/

	public function deleteByDirDLabel() {
		if($this->dir_d_label != '') {
			return $this->db->delete('directory_details', array('directory_details.dir_d_label' => $this->dir_d_label ) );
		}
	}

	/**
	* Increment row by `dir_d_label`
	**/

	public function incrementByDirDLabel() {
		if($this->dir_d_label != '' && $this->countField != '') {
			$this->db->where('dir_d_label', $this->dir_d_label);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('directory_details');
		}
	}

	/**
	* Decrement row by `dir_d_label`
	**/

	public function decrementByDirDLabel() {
		if($this->dir_d_label != '' && $this->countField != '') {
			$this->db->where('dir_d_label', $this->dir_d_label);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('directory_details');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: dir_d_label
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: dir_d_active
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `dir_d_active` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDirDActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->dir_d_active = ( ($value == '') && ($this->dir_d_active != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'directory_details.dir_d_active';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'dir_d_active';
		}
		return $this;
	}

	/** 
	* Get the value of `dir_d_active` variable
	* @access public
	* @return String;
	*/

	public function getDirDActive() {
		return $this->dir_d_active;
	}

	/**
	* Get row by `dir_d_active`
	* @param dir_d_active
	* @return QueryResult
	**/

	public function getByDirDActive() {
		if($this->dir_d_active != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('directory_details', array('directory_details.dir_d_active' => $this->dir_d_active), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `dir_d_active`
	**/

	public function updateByDirDActive() {
		if($this->dir_d_active != '') {
			$this->setExclude('dir_d_active');
			if( $this->getData() ) {
				return $this->db->update('directory_details', $this->getData(), array('directory_details.dir_d_active' => $this->dir_d_active ) );
			}
		}
	}


	/**
	* Delete row by `dir_d_active`
	**/

	public function deleteByDirDActive() {
		if($this->dir_d_active != '') {
			return $this->db->delete('directory_details', array('directory_details.dir_d_active' => $this->dir_d_active ) );
		}
	}

	/**
	* Increment row by `dir_d_active`
	**/

	public function incrementByDirDActive() {
		if($this->dir_d_active != '' && $this->countField != '') {
			$this->db->where('dir_d_active', $this->dir_d_active);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('directory_details');
		}
	}

	/**
	* Decrement row by `dir_d_active`
	**/

	public function decrementByDirDActive() {
		if($this->dir_d_active != '' && $this->countField != '') {
			$this->db->where('dir_d_active', $this->dir_d_active);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('directory_details');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: dir_d_active
	// --------------------------------------------------------------------

	// --------------------------------------------------------------------
	// Start Field: dir_d_order
	// -------------------------------------------------------------------- 

	/** 
	* Sets a value to `dir_d_order` variable
	* @access public
	* @param  String
	* @return $this;
	*/

	public function setDirDOrder($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		$this->dir_d_order = ( ($value == '') && ($this->dir_d_order != '') ) ? '' : $value;

		if( $setWhere ) {
			$key = 'directory_details.dir_d_order';
			if ( $whereOperator != NULL && $whereOperator != '' ) {
				$key = $key . ' ' . $whereOperator;
			}

			$this->__setCondition($underCondition, $key, $value, $priority);
			
		}
		
		if( $set_data_field ) {
			$this->dataFields[] = 'dir_d_order';
		}
		return $this;
	}

	/** 
	* Get the value of `dir_d_order` variable
	* @access public
	* @return String;
	*/

	public function getDirDOrder() {
		return $this->dir_d_order;
	}

	/**
	* Get row by `dir_d_order`
	* @param dir_d_order
	* @return QueryResult
	**/

	public function getByDirDOrder() {
		if($this->dir_d_order != '') {
			if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
			}

			$this->setupJoin();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get_where('directory_details', array('directory_details.dir_d_order' => $this->dir_d_order), 1, 0);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			if( $query->num_rows > 0 ) {
				$result = $query->result();
				if( isset( $result[0] ) ) {
					$this->results = $result[0];
					return  $result[0];
				}
			}
		}
	}


	/**
	* Update table by `dir_d_order`
	**/

	public function updateByDirDOrder() {
		if($this->dir_d_order != '') {
			$this->setExclude('dir_d_order');
			if( $this->getData() ) {
				return $this->db->update('directory_details', $this->getData(), array('directory_details.dir_d_order' => $this->dir_d_order ) );
			}
		}
	}


	/**
	* Delete row by `dir_d_order`
	**/

	public function deleteByDirDOrder() {
		if($this->dir_d_order != '') {
			return $this->db->delete('directory_details', array('directory_details.dir_d_order' => $this->dir_d_order ) );
		}
	}

	/**
	* Increment row by `dir_d_order`
	**/

	public function incrementByDirDOrder() {
		if($this->dir_d_order != '' && $this->countField != '') {
			$this->db->where('dir_d_order', $this->dir_d_order);
			$this->db->set($this->countField, $this->countField.'+1', FALSE);
			$this->db->update('directory_details');
		}
	}

	/**
	* Decrement row by `dir_d_order`
	**/

	public function decrementByDirDOrder() {
		if($this->dir_d_order != '' && $this->countField != '') {
			$this->db->where('dir_d_order', $this->dir_d_order);
			$this->db->set($this->countField, $this->countField.'-1', FALSE);
			$this->db->update('directory_details');
		}
	}
	
	// --------------------------------------------------------------------
	// End Field: dir_d_order
	// --------------------------------------------------------------------


	// --------------------------------------------------------------------

	/**
	* Checks if result not empty 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function nonEmpty() {
		if ( $this->hasConditions() ) {
			
			$this->setupJoin();
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('directory_details', 1);
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$result = $query->result();
			if ( isset( $result[0] ) === true ) {
				$this->results = $result[0];
				return true;
			} else {
				return false;
			}
		}
	}


	/**
	* Get Results 
	* @return Mixed;
	*/

	public function getResults() {
		return $this->results;
	}


	// --------------------------------------------------------------------

	/**
	* Delete 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function delete() {
		if ( $this->hasConditions() ) {
				$this->setupConditions();
				return $this->db->delete('directory_details');
		}
	}


	// --------------------------------------------------------------------

	/**
	* Limit Data Fields
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function limitDataFields($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->dataFields = array($fields);
			} else {
				$this->dataFields = $fields;
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	* Update 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function update() {
		if ( ( $this->getData() ) && ( $this->hasConditions() ) ) {
				$this->setupConditions();
				return $this->db->update('directory_details', $this->getData() );
		}
	}


	// --------------------------------------------------------------------

	/**
	* Insert new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function insert() {
		if( $this->getData() ) {
			if( $this->db->insert('directory_details', $this->getData() ) === TRUE ) {
				$this->dir_d_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}


	// --------------------------------------------------------------------

	/**
	* Replace new row 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	public function replace() {
		if( $this->getData() ) {
			if( $this->db->replace('directory_details', $this->getData() ) === TRUE ) {
				$this->dir_d_id = $this->db->insert_id();
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Get First Data 
	* @access public
	* @return Object / False;
	*/

	public function get() {
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		$this->db->limit( 1, $this->start);
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('directory_details');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
		
		$result = $query->result();
		
		if( isset($result[0]) ) {
			
			$this->results = $result[0];
			
			return $this->results;
			
		}
		
		return false;
	}
	// --------------------------------------------------------------------

	/**
	* Populate Data 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function populate() {
	
		if( $this->distinct ) {
			$this->db->distinct();
		}
		
		if( $this->select ) {
				$this->db->select( implode(',' , $this->select ) );
		}
		
		$this->setupJoin();
		$this->setupConditions();
		$this->setupHaving();
		$this->setupGroupBy();
		
		if( $this->order ) {
			foreach( $this->order as $field=>$orientation ) {
				$this->db->order_by( $field, $orientation );
			}
		}
		
		if( $this->limit > 0 ) {
			$this->db->limit( $this->limit,$this->start);
		}
		
		if( $this->cache_on ) {
			$this->db->cache_on();
		}
		
		$query = $this->db->get('directory_details');
		
		if( $this->cache_on ) {
			$this->db->cache_off();
		}
			
		$this->results = $query->result();
		
		return $this->results;
	}
	// --------------------------------------------------------------------
	/**
	* Recursive 
	* @access public
	* @param  String
	* @return Array;
	*/

	public function recursive($match, $find, $child='dir_d_id', $level=10, $conn='children') {
			if( $level == 0 ) return;
			if( $this->limit > 0 ) {
				$this->db->limit( $this->limit,$this->start);
			}
			if( $this->select ) {
					$this->db->select( implode(',' , $this->select) );
			}
			
			$this->setupJoin();
			$this->setWhere($match, $find);
			$this->setupConditions();
			$this->setupHaving();
			$this->setupGroupBy();
			$this->clearWhere( $match );
			
			if( $this->order ) {
				foreach( $this->order as $field=>$orientation ) {
					$this->db->order_by( $field, $orientation );
				}
			}
			
			if( $this->cache_on ) {
				$this->db->cache_on();
			}
			
			$query = $this->db->get('directory_details');
			
			if( $this->cache_on ) {
				$this->db->cache_off();
			}
			
			$results=array();
			if( $query->result() ) {
				foreach( $query->result() as $qr ) {
					$children = $this->recursive($match, $qr->$child, $child, ($level-1), $conn ) ;
					$results[] = (object) array_merge( (array) $qr, array($conn=>$children) );
				}
			}
		return $results;
	}

// --------------------------------------------------------------------

	/**
	* Set Field Where Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setFieldWhere($fields) {
		if($fields != '') {
			if( ! is_array($fields) ) {
				$this->where[] = array($fields => $this->$fields);
			} else {
				foreach($fields as $field) {
					if($w != '') {
						$this->where[] = array($field => $this->$field);
					}
				}
			}
		}
	}

// --------------------------------------------------------------------

	/**
	* Set Field Value Manually
	* @access public
	* @param Field Key ; Value
	* @return self;
	*/
	public function setFieldValue($field, $value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL) {
		if( isset( $this->$field ) ) {
			$this->$field = ( ($value == '') && ($this->$field != '') ) ? '' : $value;
			if( $setWhere ) {
				$key = 'directory_details.'.$field;
				if ( $whereOperator != NULL && $whereOperator != '' ) {
					$key = $key . ' ' . $whereOperator;
				}
				//$this->where[$key] = $this->$field;
				$this->where[] = array( $key => $this->$field );
			}
			if( $set_data_field ) {
				$this->dataFields[] = $field;
			}
		}
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Prepares data 
	* @access private
	* @return Array;
	*/
	public function getData($exclude=NULL) {
		$data = array();

		$fields = array("dir_d_id","dir_id","dir_d_key","dir_d_value","dir_d_label","dir_d_active","dir_d_order");
		if( count( $this->dataFields ) > 0 ) {
    		            $fields = $this->dataFields;
		}
		foreach( $fields as $field ) {
		    if( ( in_array( $field, $this->required ) ) 
		    && ($this->$field == '') 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		return false;
    		}
    		if( ( in_array( $field, $this->required ) ) 
    		&& ($this->$field != '') 
    		&& ( ! in_array( $field, $this->exclude ) ) 
    		) {
		        $data[$field] = $this->$field;
		    }
		    if( ( ! in_array( $field, $this->required ) ) 
		    && ( ! in_array( $field, $this->exclude ) ) 
		    ) {
        		$data[$field] = $this->$field;
    		}  
		}
		return $data;   
	}


	// --------------------------------------------------------------------

	/**
	* Setup Conditional Clauses 
	* @access public
	* @return Null;
	*/

        protected function __setCondition($condition, $key, $value, $priority) {
		switch( $condition ) {
			case 'where_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereOr($key, $value, $priority);
			break;
			case 'where_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereIn($key, $value, $priority);
			break;
			case 'where_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereInOr($key, $value, $priority);
			break;
			case 'where_not_in':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotIn($key, $value, $priority);
			break;
			case 'where_not_in_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setWhereNotInOr($key, $value, $priority);
			break;
			case 'like':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLike($key, $value, $priority);
			break;
			case 'like_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeOr($key, $value, $priority);
			break;
			case 'like_not':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNot($key, $value, $priority);
			break;
			case 'like_not_or':
				if( ! is_array( $value ) ) {
					$value = explode(',', $value );
				}
				$this->setLikeNotOr($key, $value, $priority);
			break;
			default:
				$this->setWhere($key, $value, $priority);
			break;
		}
	}
	
	protected function __apply_condition($what, $condition) {
		if( is_array( $condition ) ) {
			foreach( $condition as $key => $value ) {
				$this->db->$what( $key , $value );
			}
		} else {
			$this->db->$what( $condition );
		}
	}
	
	private function setupConditions() {
		$return = FALSE;
		
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or
                                );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'where_or':
							$this->__apply_condition('or_where', $sortd_value[0]);
						break;
						case 'where_in':
							$this->__apply_condition('where_in', $sortd_value[0]);
						break;
						case 'where_in_or':
							$this->__apply_condition('or_where_in', $sortd_value[0]);
						break;
						case 'where_not_in':
							$this->__apply_condition('where_not_in', $sortd_value[0]);
						break;
						case 'where_not_in_or':
							$this->__apply_condition('or_where_not_in', $sortd_value[0]);
						break;
						case 'like':
							$this->__apply_condition('like', $sortd_value[0]);
						break;
						case 'like_or':
							$this->__apply_condition('or_like', $sortd_value[0]);
						break;
						case 'like_not':
							$this->__apply_condition('not_like', $sortd_value[0]);
						break;
						case 'like_not_or':
							$this->__apply_condition('or_not_like', $sortd_value[0]);
						break;
						default:
							$this->__apply_condition('where', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	private function setupHaving() {
		
		$return = FALSE;
		
		$conditions = array_merge( $this->having, $this->having_or );
		
		if( count( $conditions ) > 0 ) {
			$i = 0;
			$sorted = array();
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( ! is_null( $priority ) ) {
						while( isset($sorted[$priority]) ) {
							$i++;
							$priority += $i;
						}
						$sorted[ $priority ] = $options;
					}
				}
			}
			
			foreach( $conditions as $condition ) {
				foreach( $condition as $field=>$options ) {
					$priority = $options['priority'];
					unset( $options['priority'] );
					if( is_null( $priority )  ) {
						while( isset($sorted[$i]) ) {
							$i++;
						}
						$sorted[$i] = $options;
					} 
				}
			}
			
			ksort( $sorted );
		
			foreach( $sorted as $sortd ) {
				$sortd_key = array_keys( $sortd );
				$sortd_value = array_values( $sortd );
				
				if( isset( $sortd_key[0] ) && $sortd_value[0] ) {
					switch( $sortd_key[0] ) {
						case 'having':
							$this->__apply_condition('having', $sortd_value[0]);
						break;
						case 'having_or':
							$this->__apply_condition('or_having', $sortd_value[0]);
						break;
					}
				}
			}
			$return = TRUE;
		}
		
		return $return;
	}
	
	/**
	* Check Conditions Availability
	* @access public
	* @return Array;
	*/
	private function hasConditions() {
		$conditions = array_merge(
                                $this->where,
                                $this->filter,
                                $this->where_or,
                                $this->where_in,
                                $this->where_in_or,
                                $this->where_not_in,
                                $this->where_not_in_or,
                                $this->like,
                                $this->like_or,
                                $this->like_not,
                                $this->like_not_or,
                                $this->having,
                                $this->having_or
                        );
		if( count( $conditions ) > 0 ) {
			return true;
		}
		return false;
	}
	

	// --------------------------------------------------------------------
	/**
	* Set Join Clause 
	* @access public
	* @param String / Array
	* @return Array;
	*/
	public function setJoin($table, $connection, $option='left') {
		$this->join[] = array('table'=>$table, 'connection'=>$connection, 'option'=>$option );
	}

	private function setupJoin() {
		if( $this->join ) {
			foreach( $this->join as $join ) {
				$this->db->join( $join['table'], $join['connection'], $join['option'] );
			}
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Filter 
	* @access public
	* @return Array;
	*/
	public function setFilter($field, $value, $table=NULL, $priority=NULL, $underCondition='where') {
		$key = array();
		if( $table == NULL ) { 
			$table = 'directory_details'; 
		} 
		if( $table != '' ) {
			$key[] = $table;
		}
		$key[] = $field;
		
		$newField = implode('.', $key);
		
		if( is_null( $value ) ) {
			$where = $newField;
		} else {
			$where = array( $newField => $value );
		}
		
		$this->filter[] = array( $newField => array( $underCondition => $where, 'priority'=>$priority ) );
	}


	public function clearFilter($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->filter);
			$this->filter = array();
		} else {
			$newfilter = array();
			foreach($this->filter as $filter ) {
				if( ! isset( $filter[$field] ) ) {
					$newfilter[] = $filter;
				}
			}
			$this->filter = $newfilter;
		}
	}

	// --------------------------------------------------------------------

	/**
	* Set Where 
	* @access public
	*/
	public function setWhere($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where[] = array( $field => array( 'where' => $where, 'priority'=>$priority )); 
	}


	public function clearWhere($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where);
			$this->where = array();
		} else {
			$newwhere = array();
			foreach($this->where as $where ) {
				if( ! isset( $where[$field] ) ) {
					$newwhere[] = $where;
				}
			}
			$this->where = $newwhere;
		}
	}
	
	/**
	* Set Or Where 
	* @access public
	*/
	public function setWhereOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_or[] = array( $field => array( 'where_or' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_or);
			$this->where_or = array();
		} else {
			$newwhere_or = array();
			foreach($this->where_or as $where_or ) {
				if( ! isset( $where_or[$field] ) ) {
					$newwhere_or[] = $where_or;
				}
			}
			$this->where_or = $newwhere_or;
		}
	}
	
	/**
	* Set Where In
	* @access public
	*/
	public function setWhereIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in[] = array( $field => array( 'where_in' => $where, 'priority'=>$priority ));  
	}
	
	public function clearWhereIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in);
			$this->where_in = array();
		} else {
			$newwhere_in = array();
			foreach($this->where_in as $where_in ) {
				if( ! isset( $where_in[$field] ) ) {
					$newwhere_in[] = $where_in;
				}
			}
			$this->where_in = $newwhere_in;
		}
	}
	
	/**
	* Set Or Where In
	* @access public
	*/
	public function setWhereInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_in_or[] = array( $field => array( 'where_in_or' => $where, 'priority'=>$priority ));  
	}

	
	public function clearWhereInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_in_or);
			$this->where_in_or = array();
		} else {
			$newwhere_in_or = array();
			foreach($this->where_in_or as $where_in_or ) {
				if( ! isset( $where_in_or[$field] ) ) {
					$newwhere_in_or[] = $where_in_or;
				}
			}
			$this->where_in_or = $newwhere_in_or;
		}
	}
	
	/**
	* Set Where Not In
	* @access public
	*/
	public function setWhereNotIn($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in[] = array( $field => array( 'where_not_in' => $where, 'priority'=>$priority )); 
	}
	
	public function clearWhereNotIn($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in);
			$this->where_not_in = array();
		} else {
			$newwhere_not_in = array();
			foreach($this->where_not_in as $where_not_in ) {
				if( ! isset( $where_not_in[$field] ) ) {
					$newwhere_not_in[] = $where_not_in;
				}
			}
			$this->where_not_in = $newwhere_not_in;
		}
	}
	
	/**
	* Set Or Where Not In
	* @access public
	*/
	public function setWhereNotInOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->where_not_in_or[] = array( $field => array( 'where_not_in_or' => $where, 'priority'=>$priority )); 
	}

	public function clearWhereNotInOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->where_not_in_or);
			$this->where_not_in_or = array();
		} else {
			$newwhere_not_in_or = array();
			foreach($this->where_not_in_or as $where_not_in_or ) {
				if( ! isset( $where_not_in_or[$field] ) ) {
					$newwhere_not_in_or[] = $where_not_in_or;
				}
			}
			$this->where_not_in_or = $newwhere_not_in_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Like
	* @access public
	*/
	public function setLike($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like[] = array( $field => array( 'like' => $where, 'priority'=>$priority )); 
	}

	public function clearLike($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like);
			$this->like = array();
		} else {
			$newlike = array();
			foreach($this->like as $like ) {
				if( ! isset( $like[$field] ) ) {
					$newlike[] = $like;
				}
			}
			$this->like = $newlike;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_or[] = array( $field => array( 'like_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_or);
			$this->like_or = array();
		} else {
			$newlike_or = array();
			foreach($this->like_or as $like_or ) {
				if( ! isset( $like_or[$field] ) ) {
					$newlike_or[] = $like_or;
				}
			}
			$this->like_or = $newlike_or;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNot($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not[] = array( $field => array( 'like_not' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNot($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not);
			$this->like_not = array();
		} else {
			$newlike_not = array();
			foreach($this->like_not as $like_not ) {
				if( ! isset( $like_not[$field] ) ) {
					$newlike_not[] = $like_not;
				}
			}
			$this->like_not = $newlike_not;
		}
	}
	
	/**
	* Set Like
	* @access public
	*/
	public function setLikeNotOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$where = $field;
		} else {
			$where = array( $field => $value );
		}
		$this->like_not_or[] = array( $field => array( 'like_not_or' => $where, 'priority'=>$priority ));  
	}

	public function clearLikeNotOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->like_not_or);
			$this->like_not_or = array();
		} else {
			$newlike_not_or = array();
			foreach($this->like_not_or as $like_not_or ) {
				if( ! isset( $like_not_or[$field] ) ) {
					$newlike_not_or[] = $like_not_or;
				}
			}
			$this->like_not_or = $newlike_not_or;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Having 
	* @access public
	*/
	public function setHaving($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having[] = array( $field => array( 'having' => $having, 'priority'=>$priority )); 
	}
	
	public function clearHaving($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having);
			$this->having = array();
		} else {
			$newhaving = array();
			foreach($this->having as $having ) {
				if( ! isset( $having[$field] ) ) {
					$newhaving[] = $having;
				}
			}
			$this->having = $newhaving;
		}
	}
	
	/**
	* Set Or Having 
	* @access public
	*/
	public function setHavingOr($field, $value=NULL, $priority=NULL) {
		if( is_null( $value ) ) {
			$having = $field;
		} else {
			$having = array( $field => $value );
		}
		$this->having_or[] = array( $field => array( 'having_or' => $having, 'priority'=>$priority ));  
	}

	public function clearHavingOr($field=NULL) {
		if( is_null( $field ) ) {
			unset($this->having_or);
			$this->having_or = array();
		} else {
			$newhaving_or = array();
			foreach($this->having_or as $having_or ) {
				if( ! isset( $having_or[$field] ) ) {
					$newhaving_or[] = $having_or;
				}
			}
			$this->having_or = $newhaving_or;
		}
	}
	
	// --------------------------------------------------------------------

	/**
	* Set Group By
	* @access public
	*/
	public function setGroupBy($fields) {
		if( is_array( $fields ) ) { 
			$this->group_by = array_merge( $this->group_by, $fields );
		} else {
			$this->group_by[] = $fields;
		}
	}

	private function setupGroupBy() {
		if( $this->group_by ) {
			$this->db->group_by( $this->group_by ); 
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	* Set Start  
	* @access public
	*/
	public function setStart($value) {
		$this->start = $value;
		return $this;
	}

	/**
	* Set Limit  
	* @access public
	*/
	public function setLimit($value) {
		$this->limit = $value;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Order  
	* @access public
	*/

	public function setOrder($field, $orientation='asc') {
		$this->order[$field] = $orientation;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Select  
	* @access public
	*/

	public function setSelect($select, $index=NULL) {
		if( is_null( $index ) ) {
			$this->select[] = $select;
		} else {
			$this->select[$index] = $select;
		}
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function setExclude($exclude) {
		$this->exclude[] = $exclude;
		return $this;
	}

	// --------------------------------------------------------------------

	/**
	* Set Exclude  
	* @access public
	*/

	public function isDistinct($distinct=TRUE) {
		$this->distinct = $distinct;
		return $this;
	}
	
	// --------------------------------------------------------------------

	/**
	* Count All  
	* @access public
	*/

	public function count_all() {
		return  $this->db->count_all('directory_details');
	}

	// --------------------------------------------------------------------

	/**
	* Count All Results 
	* @access public
	*/

	public function count_all_results() {
		$this->setupConditions();
		$this->db->from('directory_details');
		return  $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	/**
	* Cache Control
	* @access public
	*/
	public function cache_on() {
		$this->cache_on = TRUE;
	}
	
	/**
	* Batch Insert
	* @access public
	*/
	public function updateBatch() {
		$this->batch = array_merge( $this->batch, array($this->getData()) );
	}
	
	public function insert_batch() {
		if( is_array( $this->batch ) && count( $this->batch ) > 0 ) {
			if( $this->db->insert_batch('directory_details', $this->batch ) === TRUE ) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}
}

/* End of file directory_details_model.php */
/* Location: ./application/models/directory_details_model.php */

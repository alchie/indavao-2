<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Realestate extends MY_Controller {
    function __construct() 
    {
        parent::__construct();

    }
    
	function __init() {
		
		parent::__init();
		
		// model 
        $this->load->model(array('Realestate_data_model','Realestate_types_model'));
        
        // default title
        $this->template_data->page_title("Properties In Davao City - For Sale, For Rent / Lease, For Assume");
        
        $this->template_data->set('itemscope', 'Residence' );
        
        if( $this->session->userdata('logged_in') ) {
			$this->template_data->add_to_array('footer_js', base_url('assets/js/jquery.useractions.js'), 201 );
		}
		
	}
	
    public function index()
	{
		$this->__init();
		$this->browse();
	}
	
	public function search() {
		$url = "realestate/browse";

		if( $this->input->post() ) {
			$keys = array();
			if( ($this->input->post('status') !== FALSE) && ($this->input->post('status') != '')) {
				$keys[] = $this->input->post('status');
			} else {
				$keys[] = 'sale';
			}
			if( ($this->input->post('beds') !== FALSE) && ($this->input->post('beds') != '')) {
				$keys[] = $this->input->post('beds');
			} else {
				$keys[] = 0;
			}
			if( ($this->input->post('baths') !== FALSE) && ($this->input->post('baths') != '')) {
				$keys[] = $this->input->post('baths');
			} else {
				$keys[] = 0;
			}
			if( ($this->input->post('property_type') !== FALSE) && ($this->input->post('property_type') != '')) {
				$keys[] = $this->input->post('property_type');
			} else {
				$keys[] = "";
			}
			if( ($this->input->post('price_range') !== FALSE) && ($this->input->post('price_range') != '')) {
				$keys[] = url_title( $this->input->post('price_range'), -"-", TRUE );
			} else {
				$keys[] = "0-0";
			}
			if( ($this->input->post('lot_area') !== FALSE) && ($this->input->post('lot_area') != '')) {
				$keys[] = url_title( $this->input->post('lot_area'), -"-", TRUE );
			} else {
				$keys[] = "0-0";
			}
			if( ($this->input->post('location') !== FALSE) && (count(array_filter($this->input->post('location'))) > 0)) {
				$keys[] = implode("_", $this->input->post('location'));
			} else {
				$keys[] = "";
			}
			$url = "realestate/search-" . urlencode( base64_encode( implode('%7C', $keys) ) );

		}
		redirect( $url, 'location');
		exit;
	}
	
	private function __decode_search( $code ) {
		$keys = explode('%7C',  base64_decode( urldecode( $code ) ) );
		
		$search_keys = array();
		$search_keys['status'] = (isset($keys[0]) && $keys[0] != '') ? $keys[0] : 'sale';
		$search_keys['beds'] = (isset($keys[1]) && $keys[1] != '') ? (int) $keys[1] : 0;
		$search_keys['baths'] = (isset($keys[2]) && $keys[2] != '') ? (int) $keys[2] : 0;
		$search_keys['property_type'] = (isset($keys[3]) && $keys[3] != '') ? $keys[3] : 'HOUSE';
		
		if(isset($keys[4]) && $keys[4] != '') {
			$price_range = explode('-', $keys[4]);
			if(count($price_range) >= 2) {
				$search_keys['price_range_min'] = (isset($price_range[0])) ? (int) $price_range[0] : 0;
				$search_keys['price_range_max'] = (isset($price_range[1])) ? (int) $price_range[1] : 0;
			} elseif(count($price_range) == 1) {
				$search_keys['price_range_min'] = 0;
				$search_keys['price_range_max'] = (isset($price_range[0])) ? (int) $price_range[0] : 0;
			}
		}
		if(isset($keys[5]) && $keys[5] != '') {
			$lot_area = explode('-', $keys[5]);
			if(count($lot_area) >= 2) {
				$search_keys['lot_area_min'] = (isset($lot_area[0])) ? (int) $lot_area[0] : 0;
				$search_keys['lot_area_max'] = (isset($lot_area[1])) ? (int) $lot_area[1] : 0;
			} elseif(count($lot_area) == 1) {
				$search_keys['lot_area_min'] = 0;
				$search_keys['lot_area_max'] = (isset($lot_area[0])) ? (int) $lot_area[0] : 0;
			}
		}
		
		$search_keys['location'] = (isset($keys[6]) && $keys[6] != '') ? explode("_", $keys[6]) : array();
		
		return (object) $search_keys;
	}
	
	public function browse( $code=NULL ) {
		
		$this->__init();
		$title = "Real Estate Listings In Davao City";
		$this->template_data->page_title( $title );
		
		$keys = (object) array();
		
		if( !is_null( $code ) ) {
			$keys = $this->__decode_search( $code );
			$this->template_data->page_title( "Search Properties In Davao City" );
		}
		
		$this->__headerTags( $title, $title, "Real Estate Listings, Development Projects, Open Lots, House and Lot, Condominiums, Residential, Commercial");
		$this->__display($keys);
		
	}
	
	private function __headerTags($title, $description, $keywords) {
		$description = $title;
		$this->template_data->meta_tags(array(
				'description'=> $description,
				'keywords'=> $keywords,
			));
			
		$this->template_data->itemprop(array(
			'name'=> $description,
			'image'=> base_url('/assets/images/logo.png'),
			'description'=> $description,
			));
			
		$this->template_data->opengraph(array(
			'name'=> $description,
			'image'=> base_url('/assets/images/logo.png'),
			'description'=> $description,
			'og:title'=> $description,
			'og:site_name'=>"Online Listings in Davao City",
			'og:description'=> $description,
			'og:url'=> site_url( trim( uri_string(), "/") ),
			'og:type'=>'indavao:property',
			'og:image'=>  base_url('/assets/images/logo.png'),
			'og:image:width'=>'289',
			'og:image:height'=>'102',
			'fb:app_id'=>get_settings_value('facebook_app_id'),
		));
	}
	
	public function projects( ) {
		$this->__init();
		$title = "Development Projects In Davao City";
		$this->template_data->page_title( $title );
		
		$this->__headerTags( $title, $title, "Development Projects, Subdivisions, Davao City");
		$this->__display( ((object) array('property_type'=>'PROJ')), 'type');
	}
	
	public function residential( ) {
		$this->__init();
		$title = "Residential Properties In Davao City";
		$this->template_data->page_title( $title );
		
		$this->__headerTags( $title, $title, "Residential, Residential Properties, Davao City");
		$this->__display( ((object) array('property_type'=>'RESID')), 'type');
	}
	
	public function commercial( ) {
		$this->__init();
		$title = "Commercial Properties In Davao City";
		$this->template_data->page_title( $title );
		
		$this->__headerTags( $title, $title, "Commercial, Commercial Properties, Davao City");
		
		$this->__display( ((object) array('property_type'=>'COMM')), 'type');
	}
	
	public function foreclosures() {
		$this->__init();
		$title = "Foreclosed Properties In Davao City";
		$this->template_data->page_title( $title );
		
		$this->__headerTags( $title, $title, "Foreclosures, Foreclosed Properties, Davao City");
		
		$this->__display( ((object) array('property_type'=>'FCL')), 'type');
	}
	
	public function open_lots() {
		$this->__init();
		$title = "Open Lot / Land Properties In Davao City";
		$this->template_data->page_title( $title );
		
		$this->__headerTags( $title, $title, "Open Lot, Land, Open Lot or Land Properties, Davao City");
		
		$this->__display( ((object) array('property_type'=>'LOT')), 'type');
	}
	
	public function condo() {
		$this->__init();
		$title = "Condominium Properties In Davao City";
		$this->template_data->page_title( $title );
		
		$this->__headerTags( $title, $title, "Condominium, Condominium Properties, Davao City");
		
		$this->__display( ((object) array('property_type'=>'CONDO')), 'type');
	}
	
	public function apartments() {
		$this->__init();
		$title = "Apartments Properties In Davao City";
		$this->template_data->page_title( $title );
		
		$this->__headerTags( $title, $title, "Apartments, Apartment Properties, Davao City");
		
		$this->__display( ((object) array('property_type'=>'APART')), 'type');
	}
	
	public function house_and_lot() {
		$this->__init();
		$title = "House and Lot Properties In Davao City";
		$this->template_data->page_title( $title );
		
		$this->__headerTags( $title, $title, "House and Lot, House and Lot Properties, Davao City");
		
		$this->__display( ((object) array('property_type'=>'HOUSE')), 'type');
	}
	
	protected function __display( $keys, $by=NULL ) {
				
		$this->template_data->set('search_keys', $keys );
				 
		$current_page = 1;
		if( $this->input->get('page') != '') {
			 $current_page = $this->input->get('page');
			 $this->template_data->page_title( "Page " . $current_page . " | " . $this->template_data->get('page_title') );
		}
		$this->template_data->set('current_page', $current_page );

		$list_limit = 10;
		$list_start = ( $current_page - 1) * $list_limit;
		
		switch( $by ) {
			case 'type':
				$properties = $this->__browseByType( $keys, $list_start, $list_limit );
			break;
			default:
				$properties = $this->__browse( $keys, $list_start, $list_limit );
			break;
		}
		
		
		$this->template_data->set('properties', $properties->results );
		$this->template_data->set('total_items',  $properties->total_items );
		$this->template_data->set('pages',  ceil( $properties->total_items / $list_limit ) );
		
		$this->load->view('realestate/index', $this->template_data->get() );
	}
	
	protected function __browseByType( $keys, $list_start, $list_limit ) {
		
		$type = ($keys->property_type != '') ? $keys->property_type : 'HOUSE';
		
		$properties = new $this->Realestate_types_model;
		$properties->setTypeName( $type, TRUE );
		$properties->setReTypeActive( 1, TRUE );
		$properties->setWhere('realestate_data.re_status', 'publish');
		$properties->setWhere('realestate_data.re_parent IS NULL');
		$properties->setOrder('realestate_data.re_added', 'DESC');
		$properties->setStart($list_start);
		$properties->setLimit($list_limit);
		//$properties->setGroupBy('realestate_data.re_id');
		$properties->isDistinct(TRUE);
		$properties->cache_on();
		
		$properties->setSelect("realestate_types.*");
		$properties->setSelect("realestate_data.*");
		$properties->setJoin( 'realestate_data', 'realestate_data.re_id = realestate_types.re_id' );
		
		$properties = $this->__addPropertyDetails( $properties );
		
		$pagination = new $this->Realestate_types_model;
		$pagination->setSelect("COUNT( DISTINCT realestate_data.re_id ) as total_items");
		$pagination->setTypeName( $type, TRUE );
		$pagination->setReTypeActive( 1, TRUE );
		$pagination->setWhere('realestate_data.re_parent IS NULL');
		$pagination->setWhere('realestate_data.re_status', 'publish');
		$pagination->setJoin( 'realestate_data', 'realestate_data.re_id = realestate_types.re_id' );
		
		$total_items = $pagination->get();
		$properties_result = $properties->populate();
		
		$properties_result_new = array();
		foreach( $properties_result as $property ) {
			$property = (array) $property;
			$property['children'] = $this->__getChildren( $property['re_id'], $property['re_id'], 0, 5 );
			
			$properties_result_new[] = (object) $property;
		}
		
		return (object) array(
			'results' => $properties_result_new,
			'total_items' => $total_items->total_items,
		);
	}
	
	protected function __browse( $keys, $list_start, $list_limit ) {
		
		$properties = new $this->Realestate_data_model;
		$properties->setSelect("realestate_data.*");
		$properties->setReActive(1, TRUE);
		$properties->setReStatus('publish', TRUE);
		$properties->setWhere('realestate_data.re_parent IS NULL', NULL, 999);
		$properties->setOrder('realestate_data.re_added', 'DESC');
		$properties->setStart($list_start);
		$properties->setLimit($list_limit);
		$properties->setGroupBy('realestate_data.re_id');
		$properties->isDistinct(TRUE);
		$properties->cache_on();
		
		$pagination = new $this->Realestate_data_model;
		$pagination->setSelect("COUNT(DISTINCT realestate_data.re_id) as total_items");
		$pagination->setReActive(1, TRUE);
		$pagination->setReStatus('publish', TRUE);
		$pagination->setWhere('realestate_data.re_parent IS NULL', NULL, 999);
		$pagination->cache_on();
		
		// exclude models
		$properties->setFilter('re_type !=', 'MODEL', 'realestate_data');
		$pagination->setFilter('re_type !=', 'MODEL', 'realestate_data');
		
		$properties = $this->__addPropertyDetails( $properties );
		
		// set conditions
		if( isset( $keys->status ) && ($keys->status != '')) {
			$properties->setWhere('rd_listing_type.re_d_value',  $keys->status);
			
			$pagination->setJoin('realestate_details rd_listing_type', 'rd_listing_type.re_id = realestate_data.re_id AND rd_listing_type.re_d_active = 1 AND rd_listing_type.re_d_key = \'listing_type\'', 'LEFT OUTER');
			$pagination->setWhere('rd_listing_type.re_d_value',  $keys->status);
		}
		
		if( isset( $keys->beds ) && ($keys->beds != '') && ($keys->beds > 0) ) {
			$properties->setWhere("(rd_beds.re_d_value = {$keys->beds} OR pri_beds.re_d_value = {$keys->beds})");
			
			$pagination->setJoin('realestate_details rd_beds', 'rd_beds.re_id = realestate_data.re_id AND rd_beds.re_d_active = 1 AND rd_beds.re_d_key = \'beds\'', 'LEFT OUTER');

			$pagination->setJoin('realestate_details pri_beds', 'pri_beds.re_id = realestate_data.re_parent AND pri_beds.re_d_active = 1 AND pri_beds.re_d_key = \'beds\'', 'LEFT OUTER');
			
			$pagination->setWhere("(rd_beds.re_d_value = {$keys->beds} OR pri_beds.re_d_value = {$keys->beds})");
		}
		
		if( isset( $keys->baths ) && ($keys->baths != '') && ($keys->baths > 0)) {
			$properties->setWhere("(rd_baths.re_d_value = {$keys->baths} OR pri_baths.re_d_value = {$keys->baths})");
			//$properties->setWhereOr('pri_baths.re_d_value', $keys->baths );
			
			$pagination->setJoin('realestate_details rd_baths', 'rd_baths.re_id = realestate_data.re_id AND rd_baths.re_d_active = 1 AND rd_baths.re_d_key = \'baths\'', 'LEFT OUTER');
			
			$pagination->setJoin('realestate_details pri_baths', 'pri_baths.re_id = realestate_data.re_parent AND pri_baths.re_d_active = 1 AND pri_baths.re_d_key = \'baths\'', 'LEFT OUTER');
			
			$pagination->setWhere("(rd_baths.re_d_value = {$keys->baths} OR pri_baths.re_d_value = {$keys->baths})");
		}
		
		if( isset( $keys->property_type ) && ( $keys->property_type != '' ) ) {
			$properties->setJoin('realestate_types rd_property_type', 'rd_property_type.re_id = realestate_data.re_id AND rd_property_type.re_type_active = 1', 'LEFT OUTER');
			$properties->setWhereIn('rd_property_type.type_name',  $keys->property_type);
			
			$pagination->setJoin('realestate_types rd_property_type', 'rd_property_type.re_id = realestate_data.re_id AND rd_property_type.re_type_active = 1', 'LEFT OUTER');
			$pagination->setWhereIn('rd_property_type.type_name',  $keys->property_type);
			
			if( (count( $keys->property_type ) > 1) || ( (count( $keys->property_type ) == 1) && ($keys->property_type[0] != 'PROJ')) ) {
				$properties->setJoin('realestate_types pri_property_type', 'pri_property_type.re_id = realestate_data.re_parent AND pri_property_type.re_type_active = 1', 'LEFT OUTER');
				$properties->setWhereInOr('pri_property_type.type_name',  $keys->property_type);
				
				$pagination->setJoin('realestate_types pri_property_type', 'pri_property_type.re_id = realestate_data.re_parent AND pri_property_type.re_type_active = 1', 'LEFT OUTER');
				$pagination->setWhereInOr('pri_property_type.type_name',  $keys->property_type);
			}
		}
		
		if( isset( $keys->price_range_min ) && isset( $keys->price_range_max ) ) {
			$price_range_min = ($keys->price_range_min == '') ? 0 : $keys->price_range_min;
			$price_range_max = ($keys->price_range_max == '') ? 0 : $keys->price_range_max;
			
			if( $price_range_max > 0 ) {
				if( isset( $keys->status ) && ($keys->status == 'rent')) { 
					
					$pagination->setJoin('realestate_details rd_rent', 'rd_rent.re_id = realestate_data.re_id AND rd_rent.re_d_active = 1 AND rd_rent.re_d_key = \'rent\'', 'LEFT OUTER');
					
					$properties->setWhere('rd_rent.re_d_value >=',  $price_range_min);
					$properties->setWhere('rd_rent.re_d_value <=',  $price_range_max);
					
					$pagination->setWhere('rd_rent.re_d_value >=',  $price_range_min);
					$pagination->setWhere('rd_rent.re_d_value <=',  $price_range_max);

				} else {
					
					$pagination->setJoin('realestate_details rd_price', 'rd_price.re_id = realestate_data.re_id AND rd_price.re_d_active = 1 AND rd_price.re_d_key = \'price\'', 'LEFT OUTER');
					
					$pagination->setJoin('realestate_details pri_price', 'pri_price.re_id = realestate_data.re_parent AND pri_price.re_d_active = 1 AND pri_price.re_d_key = \'price\'', 'LEFT OUTER');
			
					$properties->setWhere("(rd_price.re_d_value BETWEEN {$price_range_min} AND {$price_range_max})");
					$properties->setWhereOr("(pri_price.re_d_value BETWEEN {$price_range_min} AND {$price_range_max})");
					
					$pagination->setWhere("(rd_price.re_d_value BETWEEN {$price_range_min} AND {$price_range_max})");
					$pagination->setWhereOr("(pri_price.re_d_value BETWEEN {$price_range_min} AND {$price_range_max})");
				}
			}
		}
		
		if( isset( $keys->lot_area_min ) && isset( $keys->lot_area_max ) ) {
			$lot_area_min = ($keys->lot_area_min == '') ? 0 : $keys->lot_area_min;
			$lot_area_max = ($keys->lot_area_max == '') ? 0 : $keys->lot_area_max;
			
			if( $lot_area_max > 0 ) {
				$properties->setWhere("(rd_lot_area.re_d_value BETWEEN {$lot_area_min} AND $lot_area_max)");
				$properties->setWhereOr("(pri_lot_area.re_d_value BETWEEN {$lot_area_min} AND $lot_area_max)");
				
				$pagination->setJoin('realestate_details rd_lot_area', 'rd_lot_area.re_id = realestate_data.re_id AND rd_lot_area.re_d_active = 1 AND rd_lot_area.re_d_key = \'lot_area\'', 'LEFT OUTER');
			
				$pagination->setJoin('realestate_details pri_lot_area', 'pri_lot_area.re_id = realestate_data.re_parent AND pri_lot_area.re_d_active = 1 AND pri_lot_area.re_d_key = \'lot_area\'', 'LEFT OUTER');
				
				$pagination->setWhere("(rd_lot_area.re_d_value BETWEEN {$lot_area_min} AND $lot_area_max)");
				$pagination->setWhereOr("(pri_lot_area.re_d_value BETWEEN {$lot_area_min} AND $lot_area_max)");
			}
		}
		
		if( isset( $keys->location ) && count(array_filter($keys->location)) > 0 ) {
			$properties->setWhereIn('locations.loc_slug',  $keys->location);
			$properties->setWhereInOr('pri_locations.loc_slug',  $keys->location);
			
			$pagination->setJoin('realestate_location rd_location', 'rd_location.re_id = realestate_data.re_id AND rd_location.re_loc_active = 1', 'LEFT OUTER');
			$pagination->setJoin('realestate_location pri_location', 'pri_location.re_id = realestate_data.re_parent AND pri_location.re_loc_active = 1', 'LEFT OUTER');
		
			$pagination->setJoin('locations', 'locations.loc_id = rd_location.loc_id', 'LEFT OUTER');
			$pagination->setJoin('locations pri_locations', 'pri_locations.loc_id = pri_location.loc_id', 'LEFT OUTER');
			
			$pagination->setWhereIn('locations.loc_slug',  $keys->location);
			$pagination->setWhereInOr('pri_locations.loc_slug',  $keys->location);
		}
		
		// pagination
		$total_items = $pagination->get();
		$properties_result = $properties->populate();
		
		$properties_result_new = array();
		foreach( $properties_result as $property ) {
			$property = (array) $property;
			$property['children'] = $this->__getChildren( $property['re_id'], $property['re_id'], 0, 5 );
			
			$properties_result_new[] = (object) $property;
		}
		
		return (object) array(
			'results' => $properties_result_new,
			'total_items' => $total_items->total_items,
		);
		
	}
	
	protected function __addPropertyDetails( $properties ) {
		
		// set developer
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'developer' LIMIT 1) as developer");
		
		// set address
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'address' LIMIT 1) as address");
		
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'address' LIMIT 1) as pri_address");
		
		// set abstract
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'abstract' LIMIT 1) as abstract");
		
		// set lot_area
		$properties->setSelect('rd_lot_area.re_d_value as lot_area');
		$properties->setJoin('realestate_details rd_lot_area', 'rd_lot_area.re_id = realestate_data.re_id AND rd_lot_area.re_d_active = 1 AND rd_lot_area.re_d_key = \'lot_area\'', 'LEFT OUTER');
		
		$properties->setSelect('pri_lot_area.re_d_value as pri_lot_area');
		$properties->setJoin('realestate_details pri_lot_area', 'pri_lot_area.re_id = realestate_data.re_parent AND pri_lot_area.re_d_active = 1 AND pri_lot_area.re_d_key = \'lot_area\'', 'LEFT OUTER');
		
		// set floor area
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'floor_area' LIMIT 1) as floor_area");
		
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'floor_area' LIMIT 1) as pri_floor_area");
		
		// set beds
		$properties->setSelect('rd_beds.re_d_value as beds');
		$properties->setJoin('realestate_details rd_beds', 'rd_beds.re_id = realestate_data.re_id AND rd_beds.re_d_active = 1 AND rd_beds.re_d_key = \'beds\'', 'LEFT OUTER');
		
		$properties->setSelect('pri_beds.re_d_value as pri_beds');
		$properties->setJoin('realestate_details pri_beds', 'pri_beds.re_id = realestate_data.re_parent AND pri_beds.re_d_active = 1 AND pri_beds.re_d_key = \'beds\'', 'LEFT OUTER');
		
		// set baths
		$properties->setSelect('rd_baths.re_d_value as baths');
		$properties->setJoin('realestate_details rd_baths', 'rd_baths.re_id = realestate_data.re_id AND rd_baths.re_d_active = 1 AND rd_baths.re_d_key = \'baths\'', 'LEFT OUTER');
		
		$properties->setSelect('pri_baths.re_d_value as pri_baths');
		$properties->setJoin('realestate_details pri_baths', 'pri_baths.re_id = realestate_data.re_parent AND pri_baths.re_d_active = 1 AND pri_baths.re_d_key = \'baths\'', 'LEFT OUTER');
		
		// set price
		$properties->setSelect('rd_price.re_d_value as price');
		$properties->setJoin('realestate_details rd_price', 'rd_price.re_id = realestate_data.re_id AND rd_price.re_d_active = 1 AND rd_price.re_d_key = \'price\'', 'LEFT OUTER');
		
		$properties->setSelect('pri_price.re_d_value as pri_price');
		$properties->setJoin('realestate_details pri_price', 'pri_price.re_id = realestate_data.re_parent AND pri_price.re_d_active = 1 AND pri_price.re_d_key = \'price\'', 'LEFT OUTER');
		
		// set rent
		$properties->setSelect('rd_rent.re_d_value as rent');
		$properties->setJoin('realestate_details rd_rent', 'rd_rent.re_id = realestate_data.re_id AND rd_rent.re_d_active = 1 AND rd_rent.re_d_key = \'rent\'', 'LEFT OUTER');
		
		// set thumbnail
		$properties->setSelect("(SELECT mu.file_name FROM realestate_media rmed LEFT JOIN media_uploads mu ON mu.media_id = rmed.media_id WHERE rmed.re_id = realestate_data.re_id AND rmed.re_med_active = 1 AND rmed.media_type = 'image' AND rmed.media_name = 'logo' ORDER BY RAND() LIMIT 1) as thumbnail");
		
		$properties->setSelect("(SELECT mu.file_name FROM realestate_media rmed LEFT JOIN media_uploads mu ON mu.media_id = rmed.media_id WHERE rmed.re_id = realestate_data.re_parent AND rmed.re_med_active = 1 AND rmed.media_type = 'image' AND rmed.media_name = 'logo' ORDER BY RAND() LIMIT 1) as pri_thumbnail");
		
		// set listing_type
		$properties->setSelect('rd_listing_type.re_d_value as listing_type');
		$properties->setJoin('realestate_details rd_listing_type', 'rd_listing_type.re_id = realestate_data.re_id AND rd_listing_type.re_d_active = 1 AND rd_listing_type.re_d_key = \'listing_type\'', 'LEFT OUTER');
		
		// set is_rent
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'listing_type' AND rd.re_d_value = 'rent' LIMIT 1) as is_rent");
		
		// set is_sale
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'listing_type' AND rd.re_d_value = 'sale' LIMIT 1) as is_sale");
		
		// set project_status
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'project_status' LIMIT 1) as project_status");
		
		// set available_slots
		$properties->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'available_slots' LIMIT 1) as available_slots");
		
		// set location
		$properties->setJoin('realestate_location rd_location', 'rd_location.re_id = realestate_data.re_id AND rd_location.re_loc_active = 1', 'LEFT OUTER');
		$properties->setJoin('realestate_location pri_location', 'pri_location.re_id = realestate_data.re_parent AND pri_location.re_loc_active = 1', 'LEFT OUTER');
		
		// set location slug
		$properties->setJoin('locations', 'locations.loc_id = rd_location.loc_id', 'LEFT OUTER');
		$properties->setJoin('locations pri_locations', 'pri_locations.loc_id = pri_location.loc_id', 'LEFT OUTER');
		
		return $properties;
	}
	
	private function __getChildren( $current, $ids, $start=0, $limit=10, $show_pages=FALSE ) {
		
		$this->load->model('Realestate_ancestors_model');
		$children = new $this->Realestate_ancestors_model;
		$children->cache_on();
		$children->setWhere( 'realestate_ancestors.re_id !=', $current);
		$children->setWhereIn( 'realestate_ancestors.ancestor_id', $ids);
		$children->setOrder('realestate_data.re_added', 'DESC');
		$children->setStart($start);
		$children->setLimit($limit);
		
		$children->setJoin('realestate_data', 'realestate_data.re_id = realestate_ancestors.re_id');
		$children->setSelect('realestate_data.*');
		$children->setFilter('re_type !=', 'MODEL', 'realestate_data');
		
		$pagination = new $this->Realestate_ancestors_model;
		$pagination->cache_on();
		$pagination->setSelect('COUNT(*) as total_items');
		$pagination->setWhere( 'realestate_ancestors.re_id !=', $current);
		$pagination->setWhereIn( 'realestate_ancestors.ancestor_id', $ids);
		$pagination->setJoin('realestate_data', 'realestate_data.re_id = realestate_ancestors.re_id');
		$pagination->setFilter('re_type !=', 'MODEL', 'realestate_data');
		
				// set developer
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'developer' LIMIT 1) as developer");
				
				// set listing_type
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'listing_type' LIMIT 1) as listing_type");
				
				// set address
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'address' LIMIT 1) as address");
				
				// set abstract
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'abstract' LIMIT 1) as abstract");
				
				// set description
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'description' LIMIT 1) as description");
				
				// set lot area
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'lot_area' LIMIT 1) as lot_area");
				
				// set floor area
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'floor_area' LIMIT 1) as floor_area");
				
				// set beds
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'beds' LIMIT 1) as beds");
				
				// set baths
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'baths' LIMIT 1) as baths");
				
				// set price
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'price' LIMIT 1) as price");
				
				// set rent
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'rent' LIMIT 1) as rent");
				
				// set map_long
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'map_long' LIMIT 1) as map_long");
				
				// set map_lat
				$children->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'map_lat' LIMIT 1) as map_lat");
				
				// set logo_image
				$children->setSelect("(SELECT mu.file_name FROM realestate_media rmed LEFT JOIN media_uploads mu ON mu.media_id = rmed.media_id WHERE rmed.re_id = realestate_data.re_id AND rmed.re_med_active = 1 AND rmed.media_name = 'logo' AND rmed.media_type = 'image' ORDER BY rmed.re_med_order DESC LIMIT 1) as logo_image");
				
				// set logo_image
				$children->setSelect("(SELECT red.re_title FROM realestate_ancestors re_anc LEFT JOIN realestate_data red ON red.re_id = re_anc.ancestor_id WHERE re_anc.re_anc_active = 1 AND red.re_type = 'PROJ' AND re_anc.re_id = realestate_data.re_id LIMIT 1) as project_name");
		
		if( $show_pages === TRUE ) {
			$total_items = $pagination->get();
			return (object) array(
				'total_items' => $total_items->total_items,
				'results' => $children->populate(),
			);
		} else {
			return $children->populate();
		}
	}
	
	private function __getProperty($slug='', $id='') {
		
			$property = new $this->Realestate_data_model;
			$property->setReActive(1, TRUE);
			if( $id != '') { 
				$property->setReId($id, TRUE);
			}
			if( $slug != '') { 
				$property->setReSlug($slug, TRUE);
			}
			$property->setSelect("realestate_data.*");
			$property->cache_on();
			
			if ($property->nonEmpty() === TRUE) {
				$initial = $property->getResults();
				
				// set developer
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'developer' LIMIT 1) as developer");
				
				// set listing_type
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'listing_type' LIMIT 1) as listing_type");
				
				// set address
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'address' LIMIT 1) as address");
				
				// set abstract
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'abstract' LIMIT 1) as abstract");
				
				// set description
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'description' LIMIT 1) as description");
				
				// set lot area
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'lot_area' LIMIT 1) as lot_area");
				
				// set floor area
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'floor_area' LIMIT 1) as floor_area");
				
				// set beds
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'beds' LIMIT 1) as beds");
				
				// set baths
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'baths' LIMIT 1) as baths");
				
				// set price
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'price' LIMIT 1) as price");
				
				// set rent
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'rent' LIMIT 1) as rent");
				
				// set map_long
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'map_long' LIMIT 1) as map_long");
				
				// set map_lat
				$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'map_lat' LIMIT 1) as map_lat");
				
				// set logo_image
				$property->setSelect("(SELECT mu.file_name FROM realestate_media rmed LEFT JOIN media_uploads mu ON mu.media_id = rmed.media_id WHERE rmed.re_id = realestate_data.re_id AND rmed.re_med_active = 1 AND rmed.media_name = 'logo' AND rmed.media_type = 'image' ORDER BY rmed.re_med_order DESC LIMIT 1) as logo_image");
				
				// set logo_image
				$property->setSelect("(SELECT red.re_title FROM realestate_ancestors re_anc LEFT JOIN realestate_data red ON red.re_id = re_anc.ancestor_id WHERE re_anc.re_anc_active = 1 AND red.re_type = 'PROJ' AND re_anc.re_id = realestate_data.re_id LIMIT 1) as project_name");
				
				
				if( $initial->re_parent != '' ) {
					// set developer
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'developer' LIMIT 1) as pri_developer");
					
					// set address
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'address' LIMIT 1) as pri_address");
					
					// set abstract
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'abstract' LIMIT 1) as pri_abstract");
					
					// set description
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'description' LIMIT 1) as pri_description");
					
					// set lot area
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'lot_area' LIMIT 1) as pri_lot_area");
					
					// set floor area
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'floor_area' LIMIT 1) as pri_floor_area");
					
					// set beds
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'beds' LIMIT 1) as pri_beds");
					
					// set baths
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'baths' LIMIT 1) as pri_baths");
					
					// set price
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'price' LIMIT 1) as pri_price");
					
					// set rent
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'rent' LIMIT 1) as pri_rent");
					
					// set map_long
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'map_long' LIMIT 1) as pri_map_long");
					
					// set map_lat
					$property->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_parent AND rd.re_d_active = 1 AND rd.re_d_key = 'map_lat' LIMIT 1) as pri_map_lat");
					
					// set logo_image
					$property->setSelect("(SELECT mu.file_name FROM realestate_media rmed LEFT JOIN media_uploads mu ON mu.media_id = rmed.media_id WHERE rmed.re_id = realestate_data.re_parent AND rmed.re_med_active = 1 AND rmed.media_name = 'logo' AND rmed.media_type = 'image' ORDER BY rmed.re_med_order DESC LIMIT 1) as pri_logo_image");
					
					// set logo_image
					$property->setSelect("(SELECT red.re_title FROM realestate_ancestors re_anc LEFT JOIN realestate_data red ON red.re_id = re_anc.ancestor_id WHERE re_anc.re_anc_active = 1 AND red.re_type = 'PROJ' AND re_anc.re_id = realestate_data.re_parent LIMIT 1) as pri_project_name");
				}
				
			return $property->getByReSlug(); 
		} 
		return NULL;
	}
	
	private function __propertyHeaderTags( $result ) {
		$re_title = $result->re_title;
		if( $result->listing_type == 'sale' ) {
			$re_title = "FOR SALE: " . $re_title;
		} elseif( $result->listing_type == 'rent' ) {
			$re_title = "FOR RENT: " . $re_title;
		} elseif( $result->listing_type == 'assume' ) {
			$re_title = "FOR ASSUME: " . $re_title;
		}
		
		$re_logo = $result->logo_image;
		if( $result->logo_image == '' && ( isset( $result->pri_logo_image ) && $result->pri_logo_image != '' ) ) {
			$re_logo = $result->pri_logo_image;
		}
		
		$re_abstract = substr( $result->abstract, 0, 150 );
		if( $result->abstract == '' && ( isset( $result->pri_abstract ) && $result->pri_abstract != '' ))  {
			$re_abstract = substr( $result->pri_abstract, 0, 150 );
		}
		
		$this->template_data->meta_tags(array(
			'description'=> substr( $re_title . " - " . $re_abstract, 0, 150 ),
			'keywords'=>get_settings_value('default_meta_keywords', ''),
		));
		
		$this->template_data->itemprop(array(
			'name'=> $re_title,
			'image'=> (($re_logo != '') ? get_settings_value('upload_url') . $re_logo : base_url('/assets/images/logo.png')),
			'description'=> substr( $re_title . " - " . $re_abstract, 0, 150 ),
		));
		
		$this->template_data->opengraph(array(
			'name'=> $re_title,
			'image'=> (($re_logo != '') ? get_settings_value('upload_url') . $re_logo : base_url('/assets/images/logo.png')),
			'description'=>$re_abstract,
			'og:title'=> $re_title,
			'og:site_name'=>"Online Listings in Davao City",
			'og:description'=>$re_abstract,
			'og:url'=> site_url( trim( uri_string(), "/") ),
			'og:type'=>'indavao:property',
			'og:image'=> (($re_logo != '') ? get_settings_value('upload_url') . $re_logo : base_url('/assets/images/logo.png')),
			'og:image:width'=>'289',
			'og:image:height'=>'102',
			'fb:app_id'=>get_settings_value('facebook_app_id'),
		));
	}
	
	public function property($slug='', $referrer_id='', $id='')
	{
			$this->__init();
			if( $referrer_id != '' ) {
				$this->_addReferrer( $referrer_id );
			}
			
			$view_file = '404';
			$result = $this->__getProperty($slug, $id);
			
			if( $result != NULL ) {
				
				$this->template_data->page_title($result->re_title . " - Properties in Davao City");
				$this->template_data->set('property', $result );
				
				$ids = array($result->re_id);
				if( $result->re_parent != '' ) {
					$ids[] = $result->re_parent;
				}
				
				$children = $this->__getChildren( $result->re_id, $ids );
				
				$this->template_data->set('children', $children );
				
				
				$this->load->model('Realestate_media_model');
				$gallery = new $this->Realestate_media_model;
				
				//$gallery->setReId( $result->re_id, TRUE );
				
				$gallery->setWhereIn('realestate_media.re_id', $ids);
				
				$gallery->setReMedActive( 1, TRUE );
				$gallery->setWhere('re_med_group', 'gallery' );
				$gallery->setJoin('media_uploads img', 'img.media_id = realestate_media.media_id');
				$gallery->setJoin('media_uploads thumb', 'thumb.media_id = realestate_media.media_thumb');
				$gallery->setOrder('re_med_order', 'ASC');
				$gallery->setLimit(0);
				$gallery->setGroupBy("realestate_media.media_id");
				$gallery->setSelect("realestate_media.*");
				$gallery->setSelect("img.file_name as img_file");
				$gallery->setSelect("thumb.file_name as img_thumb");
				
				$gallery->cache_on();
				$this->template_data->set('gallery', $gallery->populate() );
				
				$this->load->model('Realestate_types_model');
				$property_types = new $this->Realestate_types_model;
				//$property_types->setReId($result->re_id, TRUE);
				$property_types->setWhereIn('realestate_types.re_id', $ids);
				$property_types->setReTypeActive( 1, TRUE );
				$property_types->setGroupBy("realestate_types.type_name");
				$property_types->cache_on();
				
				$property_types->setSelect("realestate_types.*");
				$property_types->setSelect("(SELECT attr_label FROM `attributes` attr WHERE attr.attr_name = realestate_types.type_name LIMIT 1) as label");
				$property_types->setSelect("(SELECT attr_m.meta_value FROM `attributes` attr LEFT OUTER JOIN attributes_meta attr_m ON attr.attr_id = attr_m.attr_id WHERE attr.attr_name = realestate_types.type_name AND attr_m.meta_key = 'link' AND attr_m.active = 1 LIMIT 1) as link");
				
				$this->template_data->set('property_types', $property_types->populate() );
				
				switch( $result->re_type ) {
					case 'PROJ':
						$view_file = 'realestate/project';
						
						$models = new $this->Realestate_ancestors_model;
						$models->setAncestorId($result->re_id, TRUE);
						$models->setReAncActive(1, TRUE);
						$models->setJoin('realestate_data', 'realestate_data.re_id = realestate_ancestors.re_id');
						$models->setSelect('realestate_data.*');
						$models->setFilter('re_type', 'MODEL', 'realestate_data');
						
						// set beds
						$models->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'beds' LIMIT 1) as beds");
						
						// set baths
						$models->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'baths' LIMIT 1) as baths");
						
						// set price
						$models->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'price' LIMIT 1) as price");
						
						// set lot_area
						$models->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'lot_area' LIMIT 1) as lot_area");
						
						// set floor_area
						$models->setSelect("(SELECT rd.re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_active = 1 AND rd.re_d_key = 'floor_area' LIMIT 1) as floor_area");
						
						// set logo_image
						$models->setSelect("(SELECT mu.file_name FROM realestate_media rmed LEFT JOIN media_uploads mu ON mu.media_id = rmed.media_id WHERE rmed.re_id = realestate_data.re_id AND rmed.re_med_active = 1 AND rmed.media_name = 'logo' AND rmed.media_type = 'image' ORDER BY rmed.re_med_order DESC LIMIT 1) as logo_image");
				
						$this->template_data->set('model_houses', $models->populate() );
						
					break;
					case 'MODEL':
						$view_file = 'realestate/model_house';
					break;
					default:
						$view_file = 'realestate/property';
					break;
				}
				
				$this->__propertyHeaderTags( $result );
				$this->template_data->set('manage_page_url', site_url( array("my", $this->session->userdata('user_id'), "properties", "update", $result->re_id)) );
			}
		
		$this->load->view($view_file, $this->template_data->get() );
		
	}
	
	public function contact($slug='', $id='') {
		
		$this->__init();
		$view_file = '404';
		
		$result = $this->__getProperty( $slug, $id );
		
		if( $result != NULL ) {
			$view_file = 'realestate/contact';
			
			$this->template_data->set('property',  $result );
			
			$ids = array($result->re_id);
			if( $result->re_parent != '' ) {
				$ids[] = $result->re_parent;
			}
			$this->template_data->set('children',  $this->__getChildren( $result->re_id, $ids ) );
			
			$this->template_data->page_title("Ask About " . $result->re_title . " - Properties in Davao City");

			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			//$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			
			$this->template_data->set('message_sent', FALSE);
			
			if ($this->form_validation->run() == FALSE)
			{
				if( $this->input->post() ) {
					$this->template_data->alert( validation_errors(), 'danger');
				}
			} 
			else 
			{
				$this->load->model('Contact_messages_model');
				$msg = new $this->Contact_messages_model;
				$msg->setObjectId( $result->re_id );
				$msg->setObjectType('realestate');
				$msg->setName( $this->input->post('name') );
				$msg->setPhone( $this->input->post('phone') );
				$msg->setEmail( $this->input->post('email') );
				$msg->setMessage( $this->input->post('message') );
				$msg->setMsgType('contact');
				if( $this->session->userdata('user_id') ) {
					$msg->setUserId( $this->session->userdata('user_id') );
				}
				
				if( $msg->insert() ) {
					$this->template_data->set('message_sent', TRUE);
				}
			}
			
			$this->__propertyHeaderTags( $result );
			$this->template_data->set('manage_page_url', site_url( array("my", $this->session->userdata('user_id'), "properties", "update", $result->re_id)) );
		}
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function show_map($slug='', $id='') {
		$this->__init();
		$view_file = '404';
		$result = $this->__getProperty( $slug, $id );

		if( $result != NULL ) {
			$view_file = 'realestate/map';
			
			$this->template_data->set('property',  $result );
			
			$this->template_data->page_title( "Map - " . $result->re_title . " | " . $this->template_data->get('page_title') );
			
			$this->__propertyHeaderTags( $result );
			$this->template_data->set('manage_page_url', site_url( array("my", $this->session->userdata('user_id'), "properties", "update", $result->re_id)) );
		}
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function related($slug='', $id='') {
		$this->__init();
		$view_file = '404';
		
		$result = $this->__getProperty( $slug, $id );
		
		if( $result != NULL ) {
			$this->template_data->page_title( "Related Properties - " . $result->re_title . " | " . $this->template_data->get('page_title') );
			
			$current_page = 1;
			if( $this->input->get('page') != '') {
				 $current_page = $this->input->get('page');
				 $this->template_data->page_title( "Page " . $current_page . " | " . $this->template_data->get('page_title') );
			}
			$this->template_data->set('current_page', $current_page );

			$list_limit = 10;
			$list_start = ( $current_page - 1) * $list_limit;
		
			$view_file = 'realestate/related';
			
			$this->template_data->set('property',  $result );
			
			$ids = array($result->re_id);
			if( $result->re_parent != '' ) {
				$ids[] = $result->re_parent;
			}
			$children = $this->__getChildren( $result->re_id, $ids, $list_start, $list_limit, TRUE );
			$this->template_data->set('children',  $children->results );
			$this->template_data->set('total_items',  $children->total_items );
			$this->template_data->set('pages',  ceil( $children->total_items / $list_limit ) );
		
			$this->__propertyHeaderTags( $result );
			$this->template_data->set('manage_page_url', site_url( array("my", $this->session->userdata('user_id'), "properties", "update", $result->re_id)) );
		}
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function redirect( $slug='browse' ) {
		redirect( "realestate/" . $slug );
		exit;
	}
}

/* End of file login.php */
/* Location: ./application/controllers/signup.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Business extends MY_Controller {

	function __construct() 
    {
        parent::__construct();
        
    }
    
	function __init() {
		
		parent::__init();
		
		// model 
        $this->load->model(array('Directory_data_model'));
        
        // default title
        $this->template_data->page_title("Local Businesses In Davao City");
        
        $this->template_data->set('itemscope', 'LocalBusiness' );
        
        if( $this->session->userdata('logged_in') ) {
			$this->template_data->add_to_array('footer_js', base_url('assets/js/jquery.useractions.js'), 201 );
		}
		
	}
	
	public function index()
	{
		$this->__init();
		$this->load->view('business/categories', $this->template_data->get() );
	}
	
	public function search() {
		$url = "local_business/browse";
		if( $this->input->post() ) {
			$keys = array();
			if( ($this->input->post('category') !== FALSE) && ($this->input->post('category') != '')) {
				$keys[] = $this->input->post('category');
			} else {
				$keys[] = 'any';
			}
			if( ($this->input->post('verified') !== FALSE) && ($this->input->post('verified') != '')) {
				$keys[] = $this->input->post('verified');
			} else {
				$keys[] = 0;
			}
			if( ($this->input->post('main') !== FALSE) && ($this->input->post('main') != '')) {
				$keys[] = $this->input->post('main');
			} else {
				$keys[] = 0;
			}
			if( ($this->input->post('phone') !== FALSE) && ($this->input->post('phone') != '')) {
				$keys[] = $this->input->post('phone');
			} else {
				$keys[] = 0;
			}
			if( ($this->input->post('email') !== FALSE) && ($this->input->post('email') != '')) {
				$keys[] = $this->input->post('email');
			} else {
				$keys[] = 0;
			}
			if( ($this->input->post('address') !== FALSE) && ($this->input->post('address') != '')) {
				$keys[] = $this->input->post('address');
			} else {
				$keys[] = 0;
			}
			if( ($this->input->post('website') !== FALSE) && ($this->input->post('website') != '')) {
				$keys[] = $this->input->post('website');
			} else {
				$keys[] = 0;
			}
			if( ($this->input->post('location') !== FALSE) && (count(array_filter($this->input->post('location'))) > 0)) {
				$keys[] = implode("_", $this->input->post('location'));
			} else {
				$keys[] = "";
			}
			$url = "local_business/search-" . urlencode( base64_encode( implode('%7C', $keys) ) );

		}
		redirect( $url, 'location');
		exit;
	}
	
	private function __decode_search( $code ) {
		$keys = explode('%7C',  base64_decode( urldecode( $code ) ) );
		
		$search_keys = array();
		$search_keys['category'] = (isset($keys[0]) && $keys[0] != '' && $keys[0] != 'any') ? $keys[0] : '';
		$search_keys['verified'] = (isset($keys[1]) && $keys[1] != '') ? $keys[1] : 0;
		$search_keys['main'] = (isset($keys[2]) && $keys[2] != '') ? $keys[2] : 0;
		$search_keys['phone'] = (isset($keys[3]) && $keys[3] != '') ? $keys[3] : 0;
		$search_keys['email'] = (isset($keys[4]) && $keys[4] != '') ? $keys[4] : 0;
		$search_keys['address'] = (isset($keys[5]) && $keys[5] != '') ? $keys[5] : 0;
		$search_keys['website'] = (isset($keys[6]) && $keys[6] != '') ? $keys[6] : 0;
		$search_keys['location'] = (isset($keys[7]) && $keys[7] != '') ? explode("_", $keys[7]) : array();
		
		return (object) $search_keys;
	}
	
	public function category( $slug ) {
		
		$this->__init();
		
		$keys = (object) array(
			'category' => $slug
		);
		
		$this->load->model(array('Taxonomies_model'));
		$tax = new $this->Taxonomies_model;
		$tax->setTaxName( $slug );
		$tax->setSelect("taxonomies.*");
		
		$tax->setSelect("(SELECT tax_meta.meta_value FROM `taxonomies_meta` tax_meta WHERE tax_meta.tax_id = taxonomies.tax_id AND tax_meta.meta_key = 'ItemScope' LIMIT 1) as ItemScope");
		
		$tax->setSelect("(SELECT tax_meta.meta_value FROM `taxonomies_meta` tax_meta WHERE tax_meta.tax_id = taxonomies.tax_id AND tax_meta.meta_key = 'thumbnail' LIMIT 1) as thumbnail");
		
		$tax->cache_on();
		$result = $tax->getByTaxName();
		if( $result != NULL ) {
			
			if( $result->ItemScope != '' ) {
				$this->template_data->set('itemscope', $result->ItemScope );
			}
		
			$this->template_data->page_title( $result->tax_label . " in Davao City" );
			$description = "List of all " . $result->tax_label . " in Davao City";
			$this->template_data->meta_tags(array(
					'description'=> $description,
					'keywords'=> $result->tax_label,
				));
				
			$this->template_data->itemprop(array(
				'name'=> $result->tax_label . " in Davao City",
				'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> $description,
				));
				
			$this->template_data->opengraph(array(
				'name'=> $result->tax_label . " in Davao City",
				'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> $description,
				'og:title'=> $result->tax_label . " in Davao City",
				'og:site_name'=>"Properties in Davao City",
				'og:description'=> $description,
				'og:url'=> site_url( trim( uri_string(), "/") ),
				'og:type'=>'indavao:business',
				'og:image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'og:image:width'=>'289',
				'og:image:height'=>'102',
				'fb:app_id'=>get_settings_value('facebook_app_id'),
			));
					
			$this->__display($keys);
		} else {
			$this->template_data->page_title( "Page Not Found!" );
			$this->load->view('404', $this->template_data->get() );
		}
	}

	public function tag( $slug ) {
		
		$this->__init();
				
		$this->load->model(array('Tags_model'));
		$tag = new $this->Tags_model;
		$tag->setTagSlug( $slug );
		$tag->cache_on();
		$result = $tag->getByTagSlug();
		
		if( $result != NULL ) {
			
			// if( $result->ItemScope != '' ) {
				// $this->template_data->set('itemscope', $result->ItemScope );
			// }
		
			$this->template_data->page_title( ucwords($result->tag_name) . " in Davao City" );
			$description = "List of all " . $result->tag_name . " in Davao City";
			$this->template_data->meta_tags(array(
					'description'=> $description,
					'keywords'=> $result->tag_name,
				));
				
			$this->template_data->itemprop(array(
				'name'=> $result->tag_name . " in Davao City",
				//'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> $description,
				));
				
			$this->template_data->opengraph(array(
				'name'=> $result->tag_name . " in Davao City",
				//'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> $description,
				'og:title'=> $result->tag_name . " in Davao City",
				'og:site_name'=>"Properties in Davao City",
				'og:description'=> $description,
				'og:url'=> site_url( trim( uri_string(), "/") ),
				'og:type'=>'indavao:business',
				//'og:image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'og:image:width'=>'289',
				'og:image:height'=>'102',
				'fb:app_id'=>get_settings_value('facebook_app_id'),
			));
			
			$this->__display($result->tag_id, 'tag');
			
		} else {
			
			$this->template_data->page_title( "Page Not Found!" );
			$this->load->view('404', $this->template_data->get() );
			
		}
	}
	
	public function browse( $code=NULL ) {
		
		$this->__init();
		
		$this->template_data->page_title( "Browse All Businesses In Davao City" );
		
		$keys = (object) array();
		
		if( !is_null( $code ) ) {
			$keys = $this->__decode_search( $code );
			$this->template_data->page_title( "Search Businesses In Davao City" );
		}
		 
		$this->__display($keys);
		
	}
	
	protected function __display( $keys, $by=NULL ) {
		
		$this->template_data->set('search_keys', $keys );
				 
		$current_page = 1;
		if( $this->input->get('page') != '') {
			 $current_page = $this->input->get('page');
			 $this->template_data->page_title( "Page " . $current_page . " | " . $this->template_data->get('page_title') );
		}
		$this->template_data->set('current_page', $current_page );

		$list_limit = 10;
		$list_start = ( $current_page - 1) * $list_limit;
		
		$businesses = $this->__browse( $keys, $list_start, $list_limit, $by );
		
		$this->template_data->set('businesses', $businesses->results );
		$this->template_data->set('total_items',  $businesses->total_items );
		$this->template_data->set('pages',  ceil( $businesses->total_items / $list_limit ) );
		
		$this->load->view('business/index', $this->template_data->get() );
	}
	
	protected function __browse( $keys, $list_start, $list_limit, $by=NULL ) {
		switch( $by ) {
			case 'tag':
				return $this->__browseByTag( $keys, $list_start, $list_limit );
			break;
			default:
				return $this->__browseDefault( $keys, $list_start, $list_limit );
			break;
		}
	}
	protected function __browseByTag( $tag_id, $list_start, $list_limit ) {
		
		$this->load->model(array('Directory_tags_model'));
		$directory = new $this->Directory_tags_model;
		$directory->setTagId( $tag_id, true );
		$directory->setStart($list_start);
		$directory->setLimit($list_limit);
		$directory->cache_on();
		
		$pagination = new $this->Directory_tags_model;
		$pagination->setSelect("COUNT(*) as total_items");
		$pagination->setTagId( $tag_id, true );
		$pagination->setStart(0);
		$pagination->setLimit(0);
		$pagination->cache_on();
		
		$directory->setSelect("directory_data.*");
		$directory->setJoin("directory_data","directory_data.dir_id = directory_tags.dir_id");
		
		// email
		$directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'email' LIMIT 1) as email");
		// phone_label
		$directory->setSelect("(SELECT dd.dir_d_label FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'phone' LIMIT 1) as phone_label");
		// phone
		$directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'phone' LIMIT 1) as phone");
		// address
		$directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'address' LIMIT 1) as address");
		// thumbnail
		$directory->setSelect("(SELECT mu.file_name FROM directory_media dir_med LEFT JOIN media_uploads mu ON mu.media_id = dir_med.media_id WHERE dir_med.dir_id = directory_data.dir_id AND dir_med.dir_med_active = 1 AND dir_med.media_name = 'logo' AND dir_med.media_type = 'image' ORDER BY dir_med.dir_med_order DESC LIMIT 1) as thumbnail");
		
		$total_items = $pagination->get();
		
		return (object) array(
			'results' => $directory->populate(),
			'total_items' => $total_items->total_items,
		);
	}
	
	protected function __browseDefault( $keys, $list_start, $list_limit ) {
		
		$directory = new $this->Directory_data_model;
		$directory->setSelect("directory_data.*");
		$directory->setDirActive(1, TRUE);
		$directory->setDirStatus('publish', TRUE);
		$directory->setOrder('dir_added', 'DESC');
		$directory->setStart($list_start);
		$directory->setLimit($list_limit);
		$directory->cache_on();

		$pagination = new $this->Directory_data_model;
		$pagination->setSelect("COUNT(*) as total_items");
		$pagination->setDirActive(1, TRUE);
		$pagination->setDirStatus('publish', TRUE);
		$pagination->setStart(0);
		$pagination->setLimit(0);
		$pagination->cache_on();
		
		// set thumbnail
		$directory->setSelect("(SELECT mu.file_name FROM directory_media dir_med LEFT JOIN media_uploads mu ON mu.media_id = dir_med.media_id WHERE dir_med.dir_id = directory_data.dir_id AND dir_med.dir_med_active = 1 AND dir_med.media_name = 'logo' AND dir_med.media_type = 'image' ORDER BY dir_med.dir_med_order DESC LIMIT 1) as thumbnail");
		
		// set category
		$directory->setSelect("taxonomies.tax_id,taxonomies.tax_name,taxonomies.tax_label");
		$directory->setJoin('directory_category dir_cat', 'dir_cat.dir_id = directory_data.dir_id AND dir_cat.dir_cat_active = 1', 'LEFT OUTER');
		$directory->setJoin('taxonomies', 'dir_cat.tax_id = taxonomies.tax_id AND taxonomies.tax_active = 1', 'LEFT OUTER');
		
		// set location
		$directory->setSelect("locations.loc_id,locations.loc_name,locations.loc_slug");
		$directory->setJoin('directory_location dir_location', 'dir_location.dir_id = directory_data.dir_id AND dir_location.dir_loc_active = 1 AND dir_location.dir_loc_primary = 1', 'LEFT OUTER');
		$directory->setJoin('locations', 'dir_location.loc_id = locations.loc_id AND locations.loc_active = 1', 'LEFT OUTER');
		
		$pagination->setJoin('directory_location dir_location', 'dir_location.dir_id = directory_data.dir_id AND dir_location.dir_loc_active = 1 AND dir_location.dir_loc_primary = 1', 'LEFT OUTER');
		$pagination->setJoin('locations', 'dir_location.loc_id = locations.loc_id AND locations.loc_active = 1', 'LEFT OUTER');
		
		// set phone
		$directory->setSelect("dir_phone.dir_d_value as phone, dir_phone.dir_d_label as phone_label");
		$directory->setJoin('directory_details dir_phone', 'dir_phone.dir_id = directory_data.dir_id AND dir_phone.dir_d_active = 1 AND dir_phone.dir_d_key = "phone"', 'LEFT OUTER');
		$pagination->setJoin('directory_details dir_phone', 'dir_phone.dir_id = directory_data.dir_id AND dir_phone.dir_d_active = 1 AND dir_phone.dir_d_key = "phone"', 'LEFT OUTER');
		
		// set email
		$directory->setSelect("dir_email.dir_d_value as email");
		$directory->setJoin('directory_details dir_email', 'dir_email.dir_id = directory_data.dir_id AND dir_email.dir_d_active = 1 AND dir_email.dir_d_key = "email"', 'LEFT OUTER');
		$pagination->setJoin('directory_details dir_email', 'dir_email.dir_id = directory_data.dir_id AND dir_email.dir_d_active = 1 AND dir_email.dir_d_key = "email"', 'LEFT OUTER');
		
		// set address
		$directory->setSelect("dir_address.dir_d_value as address");
		$directory->setJoin('directory_details dir_address', 'dir_address.dir_id = directory_data.dir_id AND dir_address.dir_d_active = 1 AND dir_address.dir_d_key = "address"', 'LEFT OUTER');
		$pagination->setJoin('directory_details dir_address', 'dir_address.dir_id = directory_data.dir_id AND dir_address.dir_d_active = 1 AND dir_address.dir_d_key = "address"', 'LEFT OUTER');
		
		// set website
		$directory->setSelect("dir_website.dir_d_value as website");
		$directory->setJoin('directory_details dir_website', 'dir_website.dir_id = directory_data.dir_id AND dir_website.dir_d_active = 1 AND dir_website.dir_d_key = "website"', 'LEFT OUTER');
		$pagination->setJoin('directory_details dir_website', 'dir_website.dir_id = directory_data.dir_id AND dir_website.dir_d_active = 1 AND dir_website.dir_d_key = "website"', 'LEFT OUTER');
		
		// set abstract
		$directory->setSelect("dir_abstract.dir_d_value as abstract");
		$directory->setJoin('directory_details dir_abstract', 'dir_abstract.dir_id = directory_data.dir_id AND dir_abstract.dir_d_active = 1 AND dir_abstract.dir_d_key = "abstract"', 'LEFT OUTER');
		$pagination->setJoin('directory_details dir_abstract', 'dir_abstract.dir_id = directory_data.dir_id AND dir_abstract.dir_d_active = 1 AND dir_abstract.dir_d_key = "abstract"', 'LEFT OUTER');
		
		$directory->setGroupBy('directory_data.dir_id');
		
		if( isset( $keys->category ) && ($keys->category != '') ) {
			$directory->setWhere('taxonomies.tax_name',  $keys->category);
			
			$pagination->setJoin('directory_category dir_cat', 'dir_cat.dir_id = directory_data.dir_id AND dir_cat.dir_cat_active = 1', 'LEFT OUTER');
			$pagination->setJoin('taxonomies', 'dir_cat.tax_id = taxonomies.tax_id AND taxonomies.tax_active = 1', 'LEFT OUTER');
		
			$pagination->setWhere('taxonomies.tax_name',  $keys->category);
		}
		
		if( isset( $keys->verified ) && ($keys->verified == 1) ) {
			$directory->setWhere('directory_data.dir_verified', 1 );
			$pagination->setWhere('directory_data.dir_verified', 1 );
		}
		if( isset( $keys->main ) && ($keys->main == 1) ) {
			$directory->setWhere('directory_data.dir_branch', 0 );
			$pagination->setWhere('directory_data.dir_branch', 0 );
		}
		if( isset( $keys->phone ) && ($keys->phone == 1) ) {
			$directory->setWhere('dir_phone.dir_d_value !=', "" );
			$pagination->setWhere('dir_phone.dir_d_value !=', "" );
		}
		if( isset( $keys->email ) && ($keys->email == 1) ) {
			$directory->setWhere('dir_email.dir_d_value !=', "" );
			$pagination->setWhere('dir_email.dir_d_value !=', "" );
		}
		if( isset( $keys->address ) && ($keys->address == 1) ) {
			$directory->setWhere('dir_address.dir_d_value !=', "" );
			$pagination->setWhere('dir_address.dir_d_value !=', "" );
		}
		if( isset( $keys->website ) && ($keys->website == 1) ) {
			$directory->setWhere('dir_website.dir_d_value !=', "" );
			$pagination->setWhere('dir_website.dir_d_value !=', "" );
		}
		
		if( isset( $keys->location ) && count(array_filter($keys->location)) > 0 ) {
			$directory->setWhereIn('locations.loc_slug',  $keys->location);
			$pagination->setWhereIn('locations.loc_slug',  $keys->location);
		}
		
		$total_items = $pagination->get();
		
		return (object) array(
			'results' => $directory->populate(),
			'total_items' => $total_items->total_items,
		);
	}
	
	private function __company($id, $slug) {
		
		$directory = new $this->Directory_data_model;
		$directory->setSelect("directory_data.*");
		
		if( $this->session->userdata('manager') == FALSE ) {
			$directory->setDirActive(1, TRUE);
		}
		
		$directory->setDirId( $id, TRUE );
		$directory->setDirSlug( $slug, TRUE );
		$directory->cache_on();
		
		if ($directory->nonEmpty() === TRUE) {
			$view_file = "business/company";
			
			// set thumbnail
			$directory->setSelect("(SELECT mu.file_name FROM directory_media dir_med LEFT JOIN media_uploads mu ON mu.media_id = dir_med.media_id WHERE dir_med.dir_id = directory_data.dir_id AND dir_med.dir_med_active = 1 AND dir_med.media_name = 'logo' AND dir_med.media_type = 'image' ORDER BY dir_med.dir_med_order DESC LIMIT 1) as thumbnail");

			// set category
			$directory->setSelect("taxonomies.tax_id,taxonomies.tax_name,taxonomies.tax_label");
			$directory->setJoin('directory_category dir_cat', 'dir_cat.dir_id = directory_data.dir_id AND dir_cat.dir_cat_active = 1', 'LEFT OUTER');
			$directory->setJoin('taxonomies', 'dir_cat.tax_id = taxonomies.tax_id AND taxonomies.tax_active = 1', 'LEFT OUTER');
			
			// set location
			$directory->setSelect("locations.loc_id,locations.loc_name,locations.loc_slug");
			$directory->setJoin('directory_location dir_location', 'dir_location.dir_id = directory_data.dir_id AND dir_location.dir_loc_active = 1 AND dir_location.dir_loc_primary = 1', 'LEFT OUTER');
			$directory->setJoin('locations', 'dir_location.loc_id = locations.loc_id AND locations.loc_active = 1', 'LEFT OUTER');
			
			// set phone
			$directory->setSelect("dir_phone.dir_d_value as phone, dir_phone.dir_d_label as phone_label");
			$directory->setJoin('directory_details dir_phone', 'dir_phone.dir_id = directory_data.dir_id AND dir_phone.dir_d_active = 1 AND dir_phone.dir_d_key = "phone"', 'LEFT OUTER');
			
			// set map
			$directory->setSelect("dir_map.dir_d_value as map");
			$directory->setJoin('directory_details dir_map', 'dir_map.dir_id = directory_data.dir_id AND dir_map.dir_d_active = 1 AND dir_map.dir_d_key = "map"', 'LEFT OUTER');
			
			// set email
			$directory->setSelect("dir_email.dir_d_value as email");
			$directory->setJoin('directory_details dir_email', 'dir_email.dir_id = directory_data.dir_id AND dir_email.dir_d_active = 1 AND dir_email.dir_d_key = "email"', 'LEFT OUTER');
			
			
			// set address
			$directory->setSelect("dir_address.dir_d_value as address");
			$directory->setJoin('directory_details dir_address', 'dir_address.dir_id = directory_data.dir_id AND dir_address.dir_d_active = 1 AND dir_address.dir_d_key = "address"', 'LEFT OUTER');
			
			// set website
			$directory->setSelect("dir_website.dir_d_value as website");
			$directory->setJoin('directory_details dir_website', 'dir_website.dir_id = directory_data.dir_id AND dir_website.dir_d_active = 1 AND dir_website.dir_d_key = "website"', 'LEFT OUTER');
			
			// set abstract
			$directory->setSelect("dir_abstract.dir_d_value as abstract");
			$directory->setJoin('directory_details dir_abstract', 'dir_abstract.dir_id = directory_data.dir_id AND dir_abstract.dir_d_active = 1 AND dir_abstract.dir_d_key = "abstract"', 'LEFT OUTER');

			// set map_lat
			$directory->setSelect("dir_map_lat.dir_d_value as map_lat");
			$directory->setJoin('directory_details dir_map_lat', 'dir_map_lat.dir_id = directory_data.dir_id AND dir_map_lat.dir_d_active = 1 AND dir_map_lat.dir_d_key = "map_lat"', 'LEFT OUTER');
			
			// set map_long
			$directory->setSelect("dir_map_long.dir_d_value as map_long");
			$directory->setJoin('directory_details dir_map_long', 'dir_map_long.dir_id = directory_data.dir_id AND dir_map_long.dir_d_active = 1 AND dir_map_long.dir_d_key = "map_long"', 'LEFT OUTER');
			
			return $directory->get();
		}
		return false;
		
	}
	
	public function company($id, $slug, $referrer_id='') {
		
		$this->__init();
		
		if( $referrer_id != '' ) {
			$this->_addReferrer( $referrer_id );
		}

		$view_file = '404';
		$result = $this->__company($id, $slug);
		if( $result  ) {
			
			$view_file = 'business/company';
			
			$this->template_data->page_title($result->dir_name . " - Local Business in Davao City");
			$this->template_data->set('directory', $result );
			
			// categories belong to
			$this->load->model("Directory_category_model");
			$categories = new $this->Directory_category_model;
			$categories->setDirId( $result->dir_id, TRUE );
			$categories->setDirCatActive(1, TRUE);
			$categories->setJoin('taxonomies', 'taxonomies.tax_id = directory_category.tax_id');
			$categories->cache_on();
			$directory_categories = $categories->populate();
			$this->template_data->set('categories', $directory_categories ); 
			
			// tags belong to
			$this->load->model("Directory_tags_model");
			$tags = new $this->Directory_tags_model;
			$tags->setDirId( $result->dir_id, TRUE );
			$tags->setDirTagActive(1, TRUE);
			$tags->setJoin('tags', 'tags.tag_id = directory_tags.tag_id');
			$tags->cache_on();
			$this->template_data->set('tags', $tags->populate() ); 
			
			
			$related_businesses = array();
			if (count( $directory_categories ) > 0 ) {
				$tax_ids = array();
				foreach( $directory_categories as $dircat ) {
					$tax_ids[] = $dircat->tax_id;
				}
				
				// related Businesses
				$related = new $this->Directory_category_model;
				$related->setDirCatActive(1, TRUE);
				$related->setWhere('directory_category.dir_id !=', $result->dir_id);
				$related->setWhere('directory_data.dir_active', 1);
				$related->setWhere('directory_data.dir_status', 'publish');
				$related->setWhereIn('directory_category.tax_id', $tax_ids);
				$related->setSelect("directory_data.*");
				$related->setJoin('directory_data', 'directory_data.dir_id = directory_category.dir_id');

				$related->setSelect("dir_abstract.dir_d_value as abstract");
				$related->setJoin('directory_details dir_abstract', 'dir_abstract.dir_id = directory_data.dir_id AND dir_abstract.dir_d_active = 1 AND dir_abstract.dir_d_key = "abstract"', 'LEFT OUTER');

				$related->setSelect("dir_address.dir_d_value as address");
				$related->setJoin('directory_details dir_address', 'dir_address.dir_id = directory_data.dir_id AND dir_address.dir_d_active = 1 AND dir_address.dir_d_key = "address"', 'LEFT OUTER');

				$related->setSelect("dir_phone.dir_d_value as phone, dir_phone.dir_d_label as phone_label");
				$related->setJoin('directory_details dir_phone', 'dir_phone.dir_id = directory_data.dir_id AND dir_phone.dir_d_active = 1 AND dir_phone.dir_d_key = "phone"', 'LEFT OUTER');

				$related->setSelect("(SELECT mu.file_name FROM directory_media dir_med LEFT JOIN media_uploads mu ON mu.media_id = dir_med.media_id WHERE dir_med.dir_id = directory_data.dir_id AND dir_med.dir_med_active = 1 AND dir_med.media_name = 'logo' AND dir_med.media_type = 'image' ORDER BY dir_med.dir_med_order DESC LIMIT 1) as thumbnail");

				$related->setLimit( 5 );
				$related->isDistinct();
				$related->setGroupBy('dir_id');
				$related->cache_on();
				$related_businesses = $related->populate();
			}
			
			$this->template_data->set('related', $related_businesses ); 
			
			$this->template_data->meta_tags(array(
					'description'=> substr( $result->abstract, 0, 150 ),
					'keywords'=>get_settings_value('default_meta_keywords', ''),
				));
				
			$this->template_data->itemprop(array(
				'name'=> $result->dir_name . " - Local Business in Davao City",
				'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> substr( $result->abstract, 0, 150 ),
				));
				
			$this->template_data->opengraph(array(
				'name'=> $result->dir_name . " - Local Business in Davao City",
				'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> substr( $result->abstract, 0, 150 ),
				'og:title'=> $result->dir_name . " - Local Business in Davao City",
				'og:site_name'=>"Online Listings in Davao City",
				'og:description'=> substr( $result->abstract, 0, 150 ),
				'og:url'=> site_url( trim( uri_string(), "/") ),
				'og:type'=>'business.business',
				'place:location:latitude'=> (($result->map_lat != '') ? $result->map_lat : '7.25307765679945'),
				'place:location:longitude'=> (($result->map_long != '') ? $result->map_long : '125.45107234999999'),
				'business:contact_data:street_address'=>$result->address,
				'business:contact_data:locality'=>'Davao City',
				'business:contact_data:country_name'=>'PHILIPPINES',
				'business:contact_data:postal_code'=>'8000',
				'og:image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'og:image:width'=>'289',
				'og:image:height'=>'102',
				'fb:app_id'=>get_settings_value('facebook_app_id'),
				));
			$this->template_data->set('head_attr', 'prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# business: http://ogp.me/ns/business#"');
		
			$this->template_data->set('manage_page_url', site_url( array("my", $this->session->userdata('user_id'), "manage", "business")) . "?view=update&id=" . $result->dir_id );
		}
		
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function related($id, $slug, $referrer_id='') {
		
		$this->__init();
		
		if( $referrer_id != '' ) {
			$this->_addReferrer( $referrer_id );
		}

		$current_page = 1;
		if( $this->input->get('page') != '') {
			 $current_page = $this->input->get('page');
		}
		$this->template_data->set('current_page', $current_page );

		$list_limit = 10;
		$list_start = ( $current_page - 1) * $list_limit;
		
		$view_file = '404';
		$result = $this->__company($id, $slug);
		if( $result  ) {
			
			$view_file = 'business/related';
			
			$title = $result->dir_name . " Related Local Business in Davao City";
			
			if( $this->input->get('page') != '') {
				$title = "Page {$current_page} - " . $title;
			}

			$this->template_data->page_title( $title );
			$this->template_data->set('directory', $result );
			
			// categories belong to
			$this->load->model("Directory_category_model");
			$categories = new $this->Directory_category_model;
			$categories->setDirId( $result->dir_id, TRUE );
			$categories->setDirCatActive(1, TRUE);
			$categories->setJoin('taxonomies', 'taxonomies.tax_id = directory_category.tax_id');
			$categories->cache_on();
			$directory_categories = $categories->populate();
			$this->template_data->set('categories', $directory_categories ); 
			
			$related_businesses = array();
			$pages=0;
			if (count( $directory_categories ) > 0 ) {
				$tax_ids = array();
				foreach( $directory_categories as $dircat ) {
					$tax_ids[] = $dircat->tax_id;
				}
				
				// related Businesses
				$related = new $this->Directory_category_model;
				$related->setDirCatActive(1, TRUE);
				$related->setWhere('directory_category.dir_id !=', $result->dir_id);
				$related->setWhere('directory_data.dir_active', 1);
				$related->setWhere('directory_data.dir_status', 'publish');
				$related->setWhereIn('directory_category.tax_id', $tax_ids);
				$related->setSelect("directory_data.*");
				$related->setJoin('directory_data', 'directory_data.dir_id = directory_category.dir_id');
				
				$related->setSelect("dir_abstract.dir_d_value as abstract");
				$related->setJoin('directory_details dir_abstract', 'dir_abstract.dir_id = directory_data.dir_id AND dir_abstract.dir_d_active = 1 AND dir_abstract.dir_d_key = "abstract"', 'LEFT OUTER');
				
				$related->setSelect("dir_address.dir_d_value as address");
				$related->setJoin('directory_details dir_address', 'dir_address.dir_id = directory_data.dir_id AND dir_address.dir_d_active = 1 AND dir_address.dir_d_key = "address"', 'LEFT OUTER');

				$related->setSelect("dir_phone.dir_d_value as phone, dir_phone.dir_d_label as phone_label");
				$related->setJoin('directory_details dir_phone', 'dir_phone.dir_id = directory_data.dir_id AND dir_phone.dir_d_active = 1 AND dir_phone.dir_d_key = "phone"', 'LEFT OUTER');

				$related->setSelect("(SELECT mu.file_name FROM directory_media dir_med LEFT JOIN media_uploads mu ON mu.media_id = dir_med.media_id WHERE dir_med.dir_id = directory_data.dir_id AND dir_med.dir_med_active = 1 AND dir_med.media_name = 'logo' AND dir_med.media_type = 'image' ORDER BY dir_med.dir_med_order DESC LIMIT 1) as thumbnail");

				//$related->isDistinct();
				$related->setGroupBy('directory_data.dir_id');
				
				$related->setStart( $list_start );
				$related->setLimit( $list_limit );
				
				$related->cache_on();
				$related_businesses = $related->populate();
				
				$pagination = new $this->Directory_category_model;
				$pagination->setDirCatActive(1, TRUE);
				$pagination->setWhere('directory_category.dir_id !=', $result->dir_id);
				$pagination->setWhere('directory_data.dir_active', 1);
				$pagination->setWhere('directory_data.dir_status', 'publish');
				$pagination->setWhereIn('directory_category.tax_id', $tax_ids);
				$pagination->setSelect("COUNT( DISTINCT directory_category.dir_id ) as total_items");
				$pagination->setJoin('directory_data', 'directory_data.dir_id = directory_category.dir_id');
				$pages = ceil( $pagination->get()->total_items / $list_limit );
			}
			
			$this->template_data->set('related', $related_businesses ); 
			$this->template_data->set('pages', $pages ); 
			
			$this->template_data->meta_tags(array(
					'description'=> substr( $result->abstract, 0, 150 ),
					'keywords'=>get_settings_value('default_meta_keywords', ''),
				));
				
			$this->template_data->itemprop(array(
				'name'=> $title,
				'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> substr( $result->abstract, 0, 150 ),
				));
				
			$this->template_data->opengraph(array(
				'name'=> $title,
				'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> substr( $result->abstract, 0, 150 ),
				'og:title'=> $result->dir_name . " - Local Business in Davao City",
				'og:site_name'=>"Online Listings in Davao City",
				'og:description'=> substr( $result->abstract, 0, 150 ),
				'og:url'=> site_url( trim( uri_string(), "/") ),
				'og:type'=>'business.business',
				'place:location:latitude'=> (($result->map_lat != '') ? $result->map_lat : '7.25307765679945'),
				'place:location:longitude'=> (($result->map_long != '') ? $result->map_long : '125.45107234999999'),
				'business:contact_data:street_address'=>$result->address,
				'business:contact_data:locality'=>'Davao City',
				'business:contact_data:country_name'=>'PHILIPPINES',
				'business:contact_data:postal_code'=>'8000',
				'og:image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'og:image:width'=>'289',
				'og:image:height'=>'102',
				'fb:app_id'=>get_settings_value('facebook_app_id'),
				));
			$this->template_data->set('head_attr', 'prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# business: http://ogp.me/ns/business#"');
		}
		
		$this->load->view($view_file, $this->template_data->get() );
	}

	public function map($id, $slug, $referrer_id='') {
		
		$this->__init();
		
		if( $referrer_id != '' ) {
			$this->_addReferrer( $referrer_id );
		}

		$view_file = '404';
		$result = $this->__company($id, $slug);
		
		if( $result  ) {
			
			$view_file = 'business/map';
			
			$title = $result->dir_name . " Map - Local Business in Davao City";
			
			$this->template_data->page_title( $title );
			$this->template_data->set('directory', $result );
			
			$this->template_data->meta_tags(array(
					'description'=> substr( $result->abstract, 0, 150 ),
					'keywords'=>get_settings_value('default_meta_keywords', ''),
				));
				
			$this->template_data->itemprop(array(
				'name'=> $title,
				'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> substr( $result->abstract, 0, 150 ),
				));
				
			$this->template_data->opengraph(array(
				'name'=> $title,
				'image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'description'=> substr( $result->abstract, 0, 150 ),
				'og:title'=> $result->dir_name . " - Local Business in Davao City",
				'og:site_name'=>"Online Listings in Davao City",
				'og:description'=> substr( $result->abstract, 0, 150 ),
				'og:url'=> site_url( trim( uri_string(), "/") ),
				'og:type'=>'business.business',
				'place:location:latitude'=> (($result->map_lat != '') ? $result->map_lat : '7.25307765679945'),
				'place:location:longitude'=> (($result->map_long != '') ? $result->map_long : '125.45107234999999'),
				'business:contact_data:street_address'=>$result->address,
				'business:contact_data:locality'=>'Davao City',
				'business:contact_data:country_name'=>'PHILIPPINES',
				'business:contact_data:postal_code'=>'8000',
				'og:image'=> (($result->thumbnail != '') ? get_settings_value('upload_url') . $result->thumbnail : base_url('/assets/images/logo.png')),
				'og:image:width'=>'289',
				'og:image:height'=>'102',
				'fb:app_id'=>get_settings_value('facebook_app_id'),
				));
			$this->template_data->set('head_attr', 'prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# business: http://ogp.me/ns/business#"');
		}
		
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function request($slug, $key) {
		
		$this->__init();
		
		$view_file = '404';
		
		$directory = new $this->Directory_data_model;
		$directory->setSelect("directory_data.*");
		$directory->setDirActive(1, TRUE);
		$directory->setDirSlug( $slug, TRUE );
		$directory->cache_on();
		
		if ($directory->nonEmpty() === TRUE) {
			//$view_file = "business/requestform";
			$result = $directory->getResults();
			$this->template_data->page_title( $result->dir_name . " - " . $this->template_data->get('page_title') );
			switch( $key ) {
				case 'address':
					$this->template_data->page_title( "Add Address - " . $this->template_data->get('page_title') );
				break;
				case 'info':
					$this->template_data->page_title( "Add Description - " . $this->template_data->get('page_title') );
				break;
				case 'logo':
					$this->template_data->page_title( "Add Logo - " . $this->template_data->get('page_title') );
				break;
				case 'phone':
					$this->template_data->page_title( "Add Phone - " . $this->template_data->get('page_title') );
				break;
				case 'map':
					$this->template_data->page_title( "Add Map - " . $this->template_data->get('page_title') );
				break;
				case 'website':
					$this->template_data->page_title( "Add Website - " . $this->template_data->get('page_title') );
				break;
			}
		
		}
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function redirect( $slug='browse' ) {
		redirect( "local_business/" . $slug );
		exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/business.php */

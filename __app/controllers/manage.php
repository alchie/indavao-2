<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends MY_Controller {
	
	function __construct() 
    {
        parent::__construct();
        
        $this->_mustLogin();
        
        $this->template_data->page_title("Manage Site - In Davao City");
		
		$this->template_data->set('main_page', 'manage' ); 
        $this->template_data->set('sub_page', 'manage' );
		
		$this->load->helper( array('pagination') );
    }
    
    function __init() {
		parent::__init();
		
		$this->template_data->add_to_array('footer_js', base_url('assets/js/jquery.manager.js'), 201 );
		
		$this->template_data->set('ajax_url', site_url("my/".$this->session->userdata('user_id')."/manage/ajax") );
	}
	
	
	private function _check_id( $account_id, $page='account' ) 
	{
		if( $account_id != $this->session->userdata('user_id') ) {
			redirect('my/' . $this->session->userdata('user_id') . '/' . $page , 'location');
			exit;
		}
	}
	
	private function _is_manager() 
	{
		if( $this->session->userdata('manager') === FALSE ) {
			redirect('my/' . $this->session->userdata('user_id') . '/account' , 'location');
			exit;
		}
	}
	
	private function _is_permited( $permission ) {
		if( ! in_array( $permission, $this->session->userdata('permissions') ) ) {
			redirect('my/' . $this->session->userdata('user_id') . '/account' , 'location');
			exit;
		}
	}
	
	public function index()
	{
		redirect('my/' . $this->session->userdata('user_id') . '/account' , 'location');
		exit;
	}
	
	public function users( $account_id ) {
		$this->_check_id( $account_id, 'account' );
		$this->_is_manager();
		$this->__init();
		
		$this->_is_permited( 'manage_users' );
		$this->template_data->set('main_page', 'manage_users' ); 
		
		$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
		$this->template_data->set('current_page', $current_page );
		
		$page_limit = 20;
		$page_start = ( $current_page - 1) * $page_limit;
					
		if( $this->input->get('view') !== FALSE ) {
			
			$this->load->model(array('Users_model', 'Attributes_model', 'Users_points_model', 'Users_points_withdrawals_model'));
			
			switch( $this->input->get('view') ) {
				case 'all':
						
					$users = new $this->Users_model;
					$users->setManager(0, TRUE);
					$users->setOrder('name', 'ASC');
					$users->setStart($page_start);
					$users->setLimit($page_limit);
					$users->cache_on();
					$this->template_data->set('users', $users->populate() );
					
					$pagination = new $this->Users_model;
					$pagination->setSelect("COUNT(*) as total_items");
					$pagination->setManager(0, TRUE);
					$pagination->setOrder('name', 'ASC');
					$pagination->cache_on();
					
					$this->template_data->set('pages', ceil($pagination->get()->total_items / $page_limit) );
					
				break;
				case 'search':
				
					$order = ($this->input->get('by')!==FALSE) ? $this->input->get('by') : 'name';
					$users = new $this->Users_model;
					$users->setManager(0, TRUE);
					$users->setOrder($order, 'ASC');
					$users->setStart($page_start);
					$users->setLimit($page_limit);
					$users->cache_on();
					
					$pagination = new $this->Users_model;
					$pagination->setSelect("COUNT(*) as total_items");
					$pagination->setManager(0, TRUE);
					$pagination->setOrder('name', 'ASC');
					$pagination->cache_on();
					
					switch( $this->input->get('by') ) {
						case 'name':
							$users->setLike('name', $this->input->get('keyword') );
							$pagination->setLike('name', $this->input->get('keyword') );
						break;
						case 'email':
							$users->setLike('email', $this->input->get('keyword') );
							$pagination->setLike('email', $this->input->get('keyword') );
						break;
						case 'first_name':
							$users->setLike('first_name', $this->input->get('keyword') );
							$pagination->setLike('first_name', $this->input->get('keyword') );
						break;
						case 'last_name':
							$users->setLike('last_name', $this->input->get('keyword') );
							$pagination->setLike('last_name', $this->input->get('keyword') );
						break;
					}
					
					
					$this->template_data->set('users', $users->populate() );
					$this->template_data->set('pages', ceil($pagination->get()->total_items / $page_limit) );
					
				break;
				case 'summary':
				
					if( $this->input->get('user_id') === FALSE || $this->input->get('user_id') == '') {
						redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
						exit;
					}
					
						$user = new $this->Users_model;
						$user->setManager(0, TRUE);
						$user->setUserId( $this->input->get('user_id'), TRUE);
						
						if( $user->nonEmpty() === FALSE ) {
							redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
							exit;
						}
						
						$selected = $user->getResults();
						
						if( $selected->add_points == 1 ) {
							
							$user->setSelect("users.*");
							$user->setSelect("(SELECT SUM(points_credited) FROM users_points pnts WHERE pnts.user_id = ".$this->input->get('user_id')." AND pnts.points_claimed = 1 LIMIT 1) as overall_points");
							
							$user->setSelect("(SELECT SUM(points_withdrawn) FROM users_points_withdrawals wdrw WHERE wdrw.user_id = '".$this->input->get('user_id')."' LIMIT 1) as overall_withdrawals");
							
							$user->setSelect("(SELECT um.meta_value FROM users_meta um WHERE um.user_id = '".$this->input->get('user_id')."' AND um.user_meta_active = 1 AND um.meta_key = 'alchienetcafe_username' LIMIT 1) as alchienetcafe_username");
							
							//$user->cache_on();
							
						}
						
						$this->template_data->set('user_selected', $user->get() );
						$this->template_data->set('sub_page', 'summary' );
						
						$redeemable_items = new $this->Attributes_model;
						$redeemable_items->setAttrGroup('points_redeemable_items', TRUE);
						$redeemable_items->setAttrActive(1, TRUE);
						$redeemable_items->setSelect('attributes.*');
						$redeemable_items->setSelect("(SELECT attr_meta.meta_value FROM attributes_meta attr_meta WHERE attr_meta.attr_id = attributes.attr_id AND attr_meta.meta_key = 'points' LIMIT 1) as points");
						
						$redeemable_items->cache_on();
						$this->template_data->set('redeemable_items', $redeemable_items->populate() );
						
						$withdrawal = new $this->Users_points_withdrawals_model;
						$withdrawal->setUserId( $this->input->get('user_id'), TRUE );
						$withdrawal->setLimit(3);
						$withdrawal->setOrder('date_withdrawn', 'DESC');
						$this->template_data->set('withdrawals', $withdrawal->populate() );
						
				break;
				case 'points':
					
					if( $this->input->get('user_id') === FALSE || $this->input->get('user_id') == '') {
						redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
						exit;
					}
					
					
						$user = new $this->Users_model;
						$user->setManager(0, TRUE);
						$user->setAddPoints(1, TRUE);
						$user->setUserId( $this->input->get('user_id'), TRUE);
						
						if( $user->nonEmpty() === FALSE ) {
							redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
							exit;
						}
						
						$user->cache_on();
						$this->template_data->set('user_selected', $user->get() );
					
						$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
						$this->template_data->set('current_page', $current_page );
						
						$page_limit = 10;
						$page_start = ( $current_page - 1) * $page_limit;
								
						$this->load->model('Users_points_model');
						$points = new $this->Users_points_model;
						$points->setSelect("users_points.*");
						$points->setUserId( $this->input->get('user_id'), true );
						$points->setOrder('date_added', 'DESC');
						$points->setStart($page_start);
						$points->setLimit($page_limit);
						
						$points->cache_on();
						$this->template_data->set('user_points', $points->populate() );
						$this->template_data->set('sub_page', 'points' );
					
					
				break;
				case 'withdrawals':
					
					if( $this->input->get('user_id') === FALSE || $this->input->get('user_id') == '') {
						redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
						exit;
					}
					
					
						$user = new $this->Users_model;
						$user->setManager(0, TRUE);
						$user->setAddPoints(1, TRUE);
						$user->setUserId( $this->input->get('user_id'), TRUE);
						
						if( $user->nonEmpty() === FALSE ) {
							redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
							exit;
						}
						
						$user->cache_on();
						$this->template_data->set('user_selected', $user->get() );
						$this->template_data->set('sub_page', 'withdrawals' );
						
						$withdrawal = new $this->Users_points_withdrawals_model;
						$withdrawal->setUserId( $this->input->get('user_id'), TRUE );
						$withdrawal->setLimit(20);
						$withdrawal->setOrder('date_withdrawn', 'DESC');
						$withdrawal->cache_on();
						$this->template_data->set('withdrawals', $withdrawal->populate() );
						
					
				break;
				case 'referrals':
					
					if( $this->input->get('user_id') === FALSE || $this->input->get('user_id') == '') {
						redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
						exit;
					}
					
					
						$user = new $this->Users_model;
						$user->setManager(0, TRUE);
						$user->setUserId( $this->input->get('user_id'), TRUE);
						
						if( $user->nonEmpty() === FALSE ) {
							redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
							exit;
						}
						
						$user->cache_on();
						$this->template_data->set('user_selected', $user->get() );
						$this->template_data->set('sub_page', 'referrals' );
						
						$referrals = new $this->Users_model;
						$referrals->setReferrer( $this->input->get('user_id'), TRUE);
						$this->template_data->set('referrals', $referrals->populate() );
					
				break;
				case 'services':
					
					if( $this->input->get('user_id') === FALSE || $this->input->get('user_id') == '') {
						redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
						exit;
					}
					
					
						$user = new $this->Users_model;
						$user->setManager(0, TRUE);
						$user->setUserId( $this->input->get('user_id'), TRUE);
						
						if( $user->nonEmpty() === FALSE ) {
							redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
							exit;
						}
						
						$user->cache_on();
						$this->template_data->set('user_selected', $user->get() );
						$this->template_data->set('sub_page', 'services' );
						
						$this->load->model(array('Users_permissions_model'));
						$permission = $this->Users_permissions_model;
						$permission->setUserId( $this->input->get('user_id'), true );
						
						$this->template_data->set('permissions', $permission->populate() );
					
				break;
				case 'redeem':
					
					if( $this->input->get('user_id') === FALSE || $this->input->get('user_id') == '') {
						redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
						exit;
					}
					
						$user = new $this->Users_model;
						$user->setManager(0, TRUE);
						$user->setAddPoints(1, TRUE);
						$user->setUserId( $this->input->get('user_id'), TRUE);
						
						if( $user->nonEmpty() === FALSE ) {
							redirect('my/' . $this->session->userdata('user_id') . '/manage/users' , 'location');
							exit;
						}
						
						$user->cache_on();
						$this->template_data->set('user_selected', $user->get() );
						$this->template_data->set('sub_page', 'redeem' );
											
				break;
			}
		}
		
		$this->load->view('manage/users/users', $this->template_data->get() );
	}
	
	public function business( $account_id ) {
		$this->_check_id( $account_id, 'account' );
		$this->_is_manager();
		$this->__init();
		
		$this->_is_permited( 'manage_business' );
		$this->template_data->set('main_page', 'manage_business' ); 
		
		$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
		$this->template_data->set('current_page', $current_page );
		
		$page_limit = 20;
		$page_start = ( $current_page - 1) * $page_limit;
		
		$view_file = 'manage/business/business';
		$this->load->model( array(
			'Users_points_model', 
			'Directory_data_model', 
			'Directory_category_model', 
			'Directory_location_model',
			'Directory_tags_model'
		));
		
		$business_directory = new $this->Directory_data_model;
		$business_directory->setOrder( 'directory_data.dir_name', 'ASC' );
		$business_directory->setSelect('directory_data.*');
		
		$business_directory->setStart($page_start);
		$business_directory->setLimit($page_limit);
		
		$business_directory->setSelect('users.name as users_name');
		$business_directory->setJoin('users', 'users.user_id = directory_data.user_id');
		
		$pagination = new $this->Directory_data_model;
		$pagination->setSelect('COUNT(*) as total_items');
		
		switch( $this->input->get('view') ) {
			
			case 'published':
				$business_directory->setDirActive( 1, TRUE );
				$business_directory->setDirStatus( 'publish', TRUE );

				if( $this->input->get('user_id') != FALSE ) {
					$business_directory->setUserId( $this->input->get('user_id'), TRUE );
				}

				$business_directory_result = $business_directory->populate();
				$this->template_data->set('sub_page', 'published' );
				
				$pagination->setDirActive( 1, TRUE );
				$pagination->setDirStatus( 'publish', TRUE );
				if( $this->input->get('user_id') != FALSE ) {
					$pagination->setUserId( $this->input->get('user_id'), TRUE );
				}
				$this->template_data->set('pages', ceil( $pagination->get()->total_items / $page_limit ) );
				
			break;
			case 'draft':
				$business_directory->setDirActive( 1, TRUE );
				$business_directory->setDirStatus( 'draft', TRUE );
				$business_directory_result = $business_directory->populate();
				$this->template_data->set('sub_page', 'draft' );
				
				$pagination->setDirActive( 1, TRUE );
				$pagination->setDirStatus( 'draft', TRUE );
				$this->template_data->set('pages', ceil( $pagination->get()->total_items / $page_limit ) );
			break;
			case 'deleted':
				$business_directory->setDirActive( 0, TRUE );
				$business_directory_result = $business_directory->populate();
				$this->template_data->set('sub_page', 'deleted' );
				
				$pagination->setDirActive( 0, TRUE );
				$this->template_data->set('pages', ceil( $pagination->get()->total_items / $page_limit ) );
				
			break;
			case 'reviewer':
			case 'review':
			case 'update':
	
				if( $this->input->post('action') == 'business-reject' ) {
						$dir = new $this->Directory_data_model;
						$dir->setDirId( $this->input->post('business_id'), TRUE );
						$dir->setDirStatus('draft', FALSE, TRUE);
						$dir->update();
				}
				
				if( $this->input->post('action') == 'business-approve' ) {
						$dir = new $this->Directory_data_model;
						$dir->setDirId( $this->input->post('business_id'), TRUE );
						$dir->setDirStatus('publish', FALSE, TRUE);
						if( $dir->update() ) {
							
							if( $this->input->post('points') != FALSE && $this->input->post('user_id') != FALSE ) {
								
								$points = new $this->Users_points_model;
								$points->setUserId( $this->input->post('user_id'), TRUE, TRUE);
								$points->setObjectId( $this->input->post('business_id'), TRUE, TRUE);
								$points->setObjectType( 'business', TRUE, TRUE );
								$points->setPointsType( 'DIR_APRVD', TRUE, TRUE );
								$points->setPointsCredited( $this->input->post('points'), FALSE, TRUE);
								$points->setPointsClaimed( 0, FALSE, TRUE );
								
								if( $this->input->post('points_id') != FALSE ) {
									$points->setPointsId( $this->input->post('points_id'), TRUE, TRUE);
									$points->limitDataFields('points_credited');
									$points->update();
								} else {
									if( $points->nonEmpty() == FALSE ) {
										$points->insert();
									} else {
										$points->limitDataFields('points_credited');
										$points->update();
									}
								}
								
							}
							
						}
				}
				
				// abstract
				$business_directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'abstract' LIMIT 1) as abstract");
				// address
				$business_directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'address' LIMIT 1) as address");
				// phone
				$business_directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'phone' LIMIT 1) as phone");
				// website
				$business_directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'website' LIMIT 1) as website");
				// email
				$business_directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'email' LIMIT 1) as email");
				// map_lat
				$business_directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'map_lat' LIMIT 1) as map_lat");
				// map_long
				$business_directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'map_long' LIMIT 1) as map_long");
				// logo
				$business_directory->setSelect("(SELECT mu.file_name FROM directory_media dir_med LEFT JOIN media_uploads mu ON mu.media_id = dir_med.media_id WHERE dir_med.dir_id = directory_data.dir_id AND dir_med.dir_med_active = 1 AND dir_med.media_name = 'logo' AND dir_med.media_type = 'image' ORDER BY dir_med.dir_med_order DESC LIMIT 1) as logo");
				// is_task
				$business_directory->setSelect("(SELECT dm.meta_value FROM directory_meta dm WHERE dm.dir_id = directory_data.dir_id AND dm.meta_key = 'is_task' LIMIT 1) as is_task");
				// task_message
				$business_directory->setSelect("(SELECT dm.meta_value FROM directory_meta dm WHERE dm.dir_id = directory_data.dir_id AND dm.meta_key = 'task_message' LIMIT 1) as task_message");
				
				// points credited
				$business_directory->setSelect("(SELECT up.points_claimed FROM users_points up WHERE up.object_id = directory_data.dir_id AND up.object_type = 'business' LIMIT 1) as points_claimed");
				
				// points id
				$business_directory->setSelect("(SELECT up.points_id FROM users_points up WHERE up.object_id = directory_data.dir_id AND up.object_type = 'business' LIMIT 1) as points_id");
				
				// add_points
				$business_directory->setSelect("(SELECT users.add_points FROM users WHERE users.user_id = directory_data.user_id LIMIT 1) as add_points");
				
				if( $this->input->get('id') != FALSE ) {
					$business_directory->setDirId( $this->input->get('id') , TRUE );
				} else {
					if( $this->input->get('view') == 'reviewer' ) {
						$dir_status = ($this->input->get('status')!=FALSE) ? $this->input->get('status') : 'pending';
						$business_directory->setDirStatus( $dir_status , TRUE );
					}
					$start = ( $this->input->get('start') != FALSE ) ? $this->input->get('start') : 0;
					$business_directory->setStart($start);
					$business_directory->setLimit(1);
				}
				
				$business_directory_result = $business_directory->get();
				$this->template_data->set('sub_page', 'update' );
				
				if( $business_directory_result ) {
					
					$words = explode(' ',trim( $business_directory_result->dir_name ) );
					
					$dir_match = new $this->Directory_data_model;
					$dir_match->setLike(  'dir_name' , $words[0] );
					$dir_match->setWhere( 'dir_id !=', $business_directory_result->dir_id );
					$dir_match->setOrder('dir_name', 'ASC');
					$dir_match->setOrder('dir_added', 'ASC');
					$dir_match->setLimit(0);
					
					$this->template_data->set('matches', $dir_match->populate() );
				
					$dir_categories = new $this->Directory_category_model;
					$dir_categories->setDirId(  $business_directory_result->dir_id , TRUE);
					$dir_categories->setDirCatActive(1, TRUE);
					$this->template_data->set('old_categories', $dir_categories->populate() );
					
					$dir_locations = new $this->Directory_location_model;
					$dir_locations->setDirId(  $business_directory_result->dir_id , TRUE);
					$dir_locations->setDirLocActive(1, TRUE);
					$this->template_data->set('old_locations', $dir_locations->populate() );
					
					$dir_tags = new $this->Directory_tags_model;
					$dir_tags->setDirId( $business_directory_result->dir_id , TRUE);
					$dir_tags->setJoin('tags', 'tags.tag_id = directory_tags.tag_id');
					$dir_tags->setLimit(0);
					$this->template_data->set('tags', $dir_tags->populate() );
				
				}
				
			break;
			case 'promote':
				$business_directory->setDirId( $this->input->get('id') , TRUE );
				$business_directory_result = $business_directory->get();
				
				if( $business_directory_result ) {
					$this->load->model(array('Directory_meta_model'));
					$meta = new $this->Directory_meta_model;
					$meta->setMetaGroup('promotion', true );
					$meta->setDirId( $business_directory_result->dir_id, true );
					
					$promotions = array();
					foreach( $meta->populate() as $promo ) {
						$promotions[$promo->meta_key] = $promo->meta_value;
					}
					$this->template_data->set('promotions', $promotions );
				}
			break;
			default:
				$business_directory->setDirActive( 1, TRUE );
				$business_directory->setDirStatus( 'pending', TRUE );
				
				$business_directory->setOrder('dir_name', 'ASC');
				$business_directory->setOrder('dir_added', 'DESC');
				$business_directory_result = $business_directory->populate();
				$this->template_data->set('sub_page', 'pending' );
				
				$pagination->setDirActive( 1, TRUE );
				$pagination->setDirStatus( 'pending', TRUE );
				$this->template_data->set('pages', ceil( $pagination->get()->total_items / $page_limit ) );
				
			break;
		} 
		
		$this->template_data->set('business_directory', $business_directory_result ); 
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function taxonomies( $account_id ) {
		$this->_check_id( $account_id, 'account' );
		$this->_is_manager();
		$this->__init();
		
		$this->_is_permited( 'manage_taxonomies' );
		$this->template_data->set('main_page', 'manage_taxonomies' ); 
		
		$view_file = 'manage/taxonomies/taxonomies';
		
		if( $this->input->get('view') == FALSE ) {
			$this->load->model(array('Taxonomies_meta_model', 'Taxonomies_model'));
			$meta_keys = new $this->Taxonomies_meta_model;
			$meta_keys->isDistinct();
			$meta_keys->setSelect('taxonomies_meta.meta_key');
			$meta_keys->setLimit(0);
			$meta_keys->cache_on();
			$this->template_data->set('meta_keys', $meta_keys->populate() );
			
			$taxs = new $this->Taxonomies_model;
			$taxs->setOrder('tax_label', 'ASC');
			$taxs->setLimit(0);
			$taxs->cache_on();
			$this->template_data->set('taxs', $taxs->populate() );
			
		} else {
			
			switch( $this->input->get('view') ) {
				case 'update_meta':
					$meta_key = ($this->input->get('meta_key')=='NEW_META_KEY') ? $this->input->get('new_meta_key') : $this->input->get('meta_key');
					if( $this->input->post('meta_value') !== FALSE ) {
						$this->load->model(array('Taxonomies_meta_model'));
						foreach( $this->input->post('meta_value') as $tax_id => $meta_value ) {
							if( $meta_value == '' ) {
								continue;
							}
							$tax_meta = new $this->Taxonomies_meta_model;
							$tax_meta->setTaxId($tax_id, TRUE);
							$tax_meta->setMetaKey( $meta_key, TRUE );
							if( $tax_meta->nonEmpty() == TRUE ) {
								$tax_meta->setMetaValue( $meta_value, FALSE, TRUE );
								$tax_meta->update();
							} else {
								$tax_meta->setTaxId($tax_id, FALSE, TRUE);
								$tax_meta->setMetaKey( $meta_key, FALSE, TRUE );
								$tax_meta->setMetaValue( $meta_value, FALSE, TRUE );
								$tax_meta->insert();
							}
						}
					}
					if( $this->input->get('group') !== FALSE ) {
						$this->load->model(array('Taxonomies_ancestors_model'));
						$taxonomies = new $this->Taxonomies_ancestors_model;
						$taxonomies->setTaxParent( $this->input->get('group'), TRUE );
						$taxonomies->setJoin('taxonomies', 'taxonomies.tax_id = taxonomies_ancestors.tax_id');
						$taxonomies->setSelect('taxonomies.*');
						$taxonomies->setSelect("(SELECT meta_value FROM taxonomies_meta WHERE meta_key = '{$meta_key}' AND taxonomies_meta.tax_id = taxonomies_ancestors.tax_id LIMIT 1) as meta_value");
						$taxonomies->setLimit(0);
						$taxonomies->setOrder('taxonomies.tax_label');
						$this->template_data->set('taxonomies', $taxonomies->populate() ); 
					}
				break;
				case 'update_children':
					$this->load->model(array('Taxonomies_model', 'Taxonomies_ancestors_model'));
					
					if( $this->input->post('add_child') !== FALSE ) {
						foreach( $this->input->post('add_child') as $tax_id => $value) {
							$tax_anc_add = new $this->Taxonomies_ancestors_model;
							$tax_anc_add->setTaxId( $tax_id );
							$tax_anc_add->setTaxParent( $this->input->get('tax_id') );
							$tax_anc_add->setTaxAncActive( 1 );
							$tax_anc_add->insert();
						}
					}
					
					if( $this->input->post('remove_child') !== FALSE ) {
						foreach( $this->input->post('remove_child') as $tax_id => $value) {
							$tax_anc_add = new $this->Taxonomies_ancestors_model;
							$tax_anc_add->setTaxId( $tax_id, TRUE );
							$tax_anc_add->setTaxParent( $this->input->get('tax_id'), TRUE );
							$tax_anc_add->delete();
						}
					}
					
					$current_tax = new $this->Taxonomies_model;
					$current_tax->setTaxId( $this->input->get('tax_id'), true);
					$current_tax->cache_on();
					$this->template_data->set('current_tax', $current_tax->get() );
					
					$taxs = new $this->Taxonomies_model;
					$taxs->setOrder('tax_label', 'ASC');
					$taxs->setSelect("taxonomies.*");
					$taxs->setSelect("(SELECT 1 FROM taxonomies_ancestors ta WHERE ta.tax_id = taxonomies.tax_id AND ta.tax_parent = {$this->input->get('tax_id')} LIMIT 1) as child");
					$taxs->setLimit(0);
					$this->template_data->set('taxs', $taxs->populate() );
					
				break;
			}
		}
		
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function realestate( $account_id ) {
		$this->_check_id( $account_id, 'realestate' );
		$this->_is_manager();
		$this->__init();
		
		$this->_is_permited( 'manage_realestate' );
		$this->template_data->set('main_page', 'manage_realestate' );
		
		$view_file = 'manage/realestate/realestate';
		
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function tasks( $account_id ) {
		$this->_check_id( $account_id, 'tasks' );
		$this->_is_manager();
		$this->__init();
		
		//$this->_is_permited( 'manage_taxonomies' );
		$this->template_data->set('main_page', 'manage_tasks' );
		
		$view_file = 'manage/tasks/tasks';
		
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function ajax( $account_id ) {
		$this->_check_id( $account_id, 'account' );
		$this->_is_manager();
		$this->__init();
		
		$results = FALSE;
		
		$this->load->library('form_validation');
		if( $this->input->post('action') !== FALSE ) {
			switch( $this->input->post('action') ) {
				
				case 'register-points-program':
				
						if( ! $this->isPermited( 'manage_users' ) ) {
							$results = FALSE;
							break;
						}
					
						$this->load->model(array('Users_model', 'Users_meta_model'));
						
						$user = new $this->Users_model;
						$user->setManager(0, TRUE);
						$user->setUserId( $this->input->post('user_id'), TRUE);
						$user->setAddPoints( 1, FALSE, TRUE );
						
						$users_meta = new $this->Users_meta_model;
						$users_meta->setUserId( $this->input->post('user_id') );
						$users_meta->setMetaKey( 'alchienetcafe_username' );
						$users_meta->setMetaValue( $this->input->post('username') );
						$users_meta->setUserMetaActive( 1 );
						if( $users_meta->replace() ) {
							$results = $user->update();
						}
						
				break;
				case 'redeem-points':
				
						if( ! $this->isPermited( 'manage_users' ) ) {
							$results = FALSE;
							break;
						}
					
						$this->load->model(array('Users_model', 'Users_points_withdrawals_model'));
						
						$user = new $this->Users_model;
						$user->setUserId( $this->input->post('user_id'), TRUE);
						$user->setAddPoints( 1, TRUE );
						
						$user->setSelect("(SELECT SUM(points_credited) FROM users_points pnts WHERE pnts.user_id = '".$this->input->post('user_id')."' LIMIT 1) as overall_points");
							
						$current_user = $user->get();
						
						if ($current_user->overall_points >= $this->input->post('points')) {
							$withdrawal = new $this->Users_points_withdrawals_model;
							$withdrawal->setUserId( $this->input->post('user_id') );
							$withdrawal->setWithdrawalReason( $this->input->post('item') );
							$withdrawal->setPointsWithdrawn( $this->input->post('points') );
							$results = $withdrawal->insert();
						}

				break;
				case 'remove-users-points':
						
						if( ! $this->isPermited( 'manage_users' ) ) {
							$results = FALSE;
							break;
						}
					
						$this->load->model(array('Users_points_model'));
						
						$points = new $this->Users_points_model;
						$points->setUserId( $this->input->post('user_id'), TRUE);
						$points->setPointsId( $this->input->post('points_id'), TRUE);
						$results['deleted'] = $points->delete();
						$results['id'] = $this->input->post('points_id');
						
				break;
				
				case 'toggle-permission':
					
					if( ! $this->isPermited( 'manage_users' ) ) {
						$results = FALSE;
						break;
					}
					
					$results['permission'] = $this->input->post('permission');
					
					$this->load->model(array('Users_permissions_model'));
					$permission = $this->Users_permissions_model;
					$permission->setUserId( $this->input->post('user_id'), true );
					$permission->setPermission( $this->input->post('permission'), true );
					
					if($this->input->post('status')=='true') {
						$status = true;
						$active = 1;
						
					} else {
						$status = false;
						$active = 0;
					}
					
					if( $permission->nonEmpty() !== FALSE ) {
						$permission->setActive($active, FALSE, TRUE);
						if( $permission->update() ) {
							$results['status'] = $status;
						}
					} else {
						$permission->setActive($active);
						if( $permission->insert() ) {
							$results['status'] = $status;
						}
					}
					
					
					
				break;
				
				// clear cache
				case 'clear_cache':
					$results['action'] = 'clear_cache';
					$this->form_validation->set_rules('uri1', 'class_name', 'required');
					$this->form_validation->set_rules('uri2', 'method_name', 'required');
					if ($this->form_validation->run() !== FALSE)
					{
						$results['error'] = false;
						$this->db->cache_delete($this->input->post('uri1'), $this->input->post('uri2'));
					}
				break;
				
				case 'business-approve':
					$results['action'] = 'business-approve';
					$results['error'] = true;
					$results['dir_id'] = $this->input->post('dir_id');
					
					$this->load->model(array('Directory_data_model', 'Users_points_model'));
					$this->form_validation->set_rules('dir_id', 'Business ID', 'required');
					if ($this->form_validation->run() !== FALSE)
					{
						$points_credited = ($this->input->post('points') !== FALSE) ? $this->input->post('points') : 1;
						$dir = new $this->Directory_data_model;
						$dir->setDirId( $this->input->post('dir_id'), TRUE );
						$dir->setDirStatus('publish', FALSE, TRUE);
						if( $dir->update() ) {
							$results['error'] = false;
							$dir_current = $dir->get(); 
							$points = new $this->Users_points_model;
							$points->setUserId($dir_current->user_id, TRUE, TRUE);
							$points->setObjectId( $dir_current->dir_id, TRUE, TRUE);
							$points->setObjectType( 'business', TRUE, TRUE );
							$points->setPointsType( 'DIR_APRVD', TRUE, TRUE );
							$points->setPointsCredited( $points_credited, FALSE, TRUE);
							$points->setPointsClaimed( 0, FALSE, TRUE );
							if( $points->nonEmpty() == FALSE ) {
								$points->insert();
							}
						}
					}
				break;
				
				case 'business-publish':
					$results['action'] = 'business-approve';
					$results['error'] = true;
					$results['dir_id'] = $this->input->post('dir_id');
					
					$this->load->model(array('Directory_data_model'));
					$this->form_validation->set_rules('dir_id', 'Business ID', 'required');
					if ($this->form_validation->run() !== FALSE)
					{
						$dir = new $this->Directory_data_model;
						$dir->setDirId( $this->input->post('dir_id'), TRUE );
						$dir->setDirStatus('publish', FALSE, TRUE);
						if( $dir->update() ) {
							$results['error'] = false;
						}
					}
				break;
				
				case 'business-reject':
					$results['action'] = 'business-reject';
					$results['error'] = true;
					$results['dir_id'] = $this->input->post('dir_id');
					
					$this->load->model(array('Directory_data_model'));
					$this->form_validation->set_rules('dir_id', 'Business ID', 'required');
					if ($this->form_validation->run() !== FALSE)
					{
						$dir = new $this->Directory_data_model;
						$dir->setDirId( $this->input->post('dir_id'), TRUE );
						$dir->setDirStatus('draft', FALSE, TRUE);
						$dir->setDirActive(1, FALSE, TRUE);
						if( $dir->update() ) {
							$results['error'] = false;
						}
					}
				break;
				
				case 'business-delete':
					$results['action'] = 'business-delete';
					$results['error'] = true;
					$results['dir_id'] = $this->input->post('dir_id');
					
					$this->load->model(array('Directory_data_model'));
					$this->form_validation->set_rules('dir_id', 'Business ID', 'required');
					if ($this->form_validation->run() !== FALSE)
					{
						$dir = new $this->Directory_data_model;
						$dir->setDirId( $this->input->post('dir_id'), TRUE );
						$dir->setDirActive(0, FALSE, TRUE);
						if( $dir->update() ) {
							$results['error'] = false;
						}
					}
				break;
				
				case 'business-delete-permanently':
					$results['action'] = 'business-delete-permanently';
					$results['error'] = true;
					$results['dir_id'] = $this->input->post('dir_id');
					
					$this->load->model(array('Directory_data_model'));
					$this->form_validation->set_rules('dir_id', 'Business ID', 'required');
					if ($this->form_validation->run() !== FALSE)
					{
						$dir = new $this->Directory_data_model;
						$dir->setDirId( $this->input->post('dir_id'), TRUE );
						if( $dir->delete() ) {
							$results['error'] = false;
						}
					}
				break;
				
				case 'business-reclaim':
					$results['action'] = 'business-reclaim';
					$results['error'] = true;
					$results['dir_id'] = $this->input->post('dir_id');
					
					$this->load->model(array('Directory_data_model'));
					$this->form_validation->set_rules('dir_id', 'Business ID', 'required');
					if ($this->form_validation->run() !== FALSE)
					{
						$dir = new $this->Directory_data_model;
						$dir->setDirId( $this->input->post('dir_id'), TRUE );
						$dir->setDirStatus('publish', FALSE, TRUE);
						$dir->setUserId(NULL, FALSE, TRUE);
						$dir->setDirActive(1, FALSE, TRUE);
						if( $dir->update() ) {
							$results['error'] = false;
						}
					}
				break;
				
				case 'business-update-details':
							
							$results['action'] = 'business-update-details';
							$results['error'] = false;
							$results['dir_id'] = $this->input->post('business_id');
					
							$this->load->model(array('Directory_data_model', 'Directory_details_model'));
							$directory_current = new $this->Directory_data_model;
							$directory_current->setDirId( $this->input->post('business_id'), TRUE );
						
							$results['msg'] = 'Business Details Updated!';
							
							if( $this->input->post('business_name') != FALSE ) {
								$directory_current->setDirName( $this->input->post('business_name'), FALSE, TRUE);
								
							}
							
							if( $this->input->post('business_slug') != FALSE ) {
								$directory_current->setDirSlug( url_title( $this->input->post('business_slug'), '-', TRUE ), FALSE, TRUE);
							} else {
								$directory_current->setDirSlug( url_title( $this->input->post('business_name'), '-', TRUE ), FALSE, TRUE);
							}
							
							$directory_current->update();
							function update_detail($self, $id, $dKey, $value) {
								$directory_details = new $self->Directory_details_model;
								$directory_details->setDirId( $id, TRUE, TRUE );
								$directory_details->setDirDKey($dKey, TRUE, TRUE );
								$directory_details->setDirDActive(1, TRUE, TRUE);
								$directory_details->setDirDValue( $value, FALSE, TRUE );
								if( $directory_details->nonEmpty() === TRUE ) {
									$directory_details->limitDataFields('dir_d_value');
									return $directory_details->update();
								} else {
									return $directory_details->insert();
								}
							}
							
							function delete_detail($self, $id, $dKey) {
								$directory_details = new $self->Directory_details_model;
								$directory_details->setDirId( $id, TRUE );
								$directory_details->setDirDKey($dKey, TRUE );
								$directory_details->delete();
							}
							
							if( $this->input->post('business_description') != FALSE ) {
								if( ! update_detail( $this, $this->input->post('business_id'), 'abstract', $this->input->post('business_description') ) ) {
									$results['error'] = true;
									$results['msg'] = 'Error Updating Description!';
								}
							} else {
								delete_detail( $this, $this->input->post('business_id'), 'abstract' );
							}
							
							if( $this->input->post('business_address') != FALSE ) {
								if( ! update_detail( $this, $this->input->post('business_id'), 'address', $this->input->post('business_address') ) ) {
									$results['error'] = true;
									$results['msg'] = 'Error Updating Address!';
								}
							} else {
								delete_detail( $this, $this->input->post('business_id'), 'address' );
							}
							
							if( $this->input->post('business_phone') != FALSE ) {
								if( ! update_detail( $this, $this->input->post('business_id'), 'phone', $this->input->post('business_phone') ) ) {
									$results['error'] = true;
									$results['msg'] = 'Error Updating Phone!';
								}
							} else {
								delete_detail( $this, $this->input->post('business_id'), 'phone' );
							}
							
							if( $this->input->post('business_website') != FALSE ) {
								if (! update_detail( $this, $this->input->post('business_id'), 'website', $this->input->post('business_website') ) ) {
									$results['error'] = true;
									$results['msg'] = 'Error Updating Website!';
								}
							} else {
								delete_detail( $this, $this->input->post('business_id'), 'website' );
							}
							
							if( $this->input->post('business_email') != FALSE ) {
								if (! update_detail( $this, $this->input->post('business_id'), 'email', $this->input->post('business_email') ) ) {
									$results['error'] = true;
									$results['msg'] = 'Error Updating Website!';
								}
							} else {
								delete_detail( $this, $this->input->post('business_id'), 'email' );
							}
							
							if( $this->input->post('business_map_lat') != FALSE ) {
								if (! update_detail( $this, $this->input->post('business_id'), 'map_lat', $this->input->post('business_map_lat') ) ) {
									$results['error'] = true;
									$results['msg'] = 'Error Updating Map Latitude!';
								}
							} else {
								delete_detail( $this, $this->input->post('business_id'), 'map_lat' );
							}
							
							if( $this->input->post('business_map_long') != FALSE ) {
								if (! update_detail( $this, $this->input->post('business_id'), 'map_long', $this->input->post('business_map_long') ) ) {
									$results['error'] = true;
									$results['msg'] = 'Error Updating Map Longitude!';
								}
							} else {
								delete_detail( $this, $this->input->post('business_id'), 'map_long' );
							}
							
				break;
				case 'business-update-categories':
					
					$results['action'] = 'business-update-categories';
					$results['error'] = false;
					$results['dir_id'] = $this->input->post('business_id');
					$results['msg'] = 'Business Categories Updated!';
					
					$this->load->model(array('Directory_category_model'));
					$dir_cats = new $this->Directory_category_model;
					$dir_cats->setDirId($this->input->post('business_id'), TRUE, TRUE);
					
					$old_categories = ( $this->input->post('old_categories') !== FALSE ) ? $this->input->post('old_categories') : array();
					
					$new_categories = ( $this->input->post('new_categories') !== FALSE ) ? $this->input->post('new_categories') : array();
					
					$remove_categories = ( $this->input->post('remove_categories') !== FALSE ) ? $this->input->post('remove_categories') : array();
					
					$new_cats = array_merge( $old_categories, $new_categories );
					
					// add new categories
					$error = false;
					if( count( $new_categories ) > 0 ) {
						foreach( $new_categories as $new_cat ) {
							if( ! in_array($new_cat, $old_categories ) ) {
								$dir_cats->setTaxId( $new_cat, FALSE, TRUE );
								$dir_cats->setDirCatActive( 1, FALSE, TRUE );
								if( ! $dir_cats->insert() ) {
									$error = true;
								}
							}
						}
					}
					
					
					if( count( $remove_categories ) > 0 ) {
						$dir_cats->setWhereIn('tax_id', $remove_categories );
						if( ! $dir_cats->delete() ) {
							$error = true;
						}
						foreach( $remove_categories as $rem_cat ) {
							if(($key = array_search($rem_cat, (array) $new_cats)) !== false) {
								unset($new_cats[$key]);
							}
						}
					}
					
					
					$results['new_categories'] = implode( ',', $new_cats );
					
					if( $error ) {
						$results['error'] = true;
						$results['msg'] = 'Error updating business categories!';
						$results['new_categories'] = $this->input->post('old_categories'); 
					}
							
				break;
				
				case 'business-update-locations':
					$results['action'] = 'business-update-categories';
					$results['error'] = false;
					$results['dir_id'] = $this->input->post('business_id');
					$results['msg'] = 'Business Location Updated!';
					
					$this->load->model(array('Directory_location_model'));
					$dir_locs = new $this->Directory_location_model;
					$dir_locs->setDirId($this->input->post('business_id'), TRUE, TRUE);
					
					$old_locations = ( $this->input->post('old_locations') !== FALSE ) ? $this->input->post('old_locations') : array();
					
					$new_locations = ( $this->input->post('new_locations') !== FALSE ) ? $this->input->post('new_locations') : array();
					
					$remove_locations = ( $this->input->post('remove_locations') !== FALSE ) ? $this->input->post('remove_locations') : array();
					
					$new_locs = array_merge( $old_locations, $new_locations );
					
					// add new location
					$error = false;
					if( count( $new_locations ) > 0 ) {
						foreach( $new_locations as $new_loc ) {
							if( ! in_array($new_loc, $old_locations ) ) {
								$dir_locs->setLocId( $new_loc, FALSE, TRUE );
								$dir_locs->setDirLocActive( 1, FALSE, TRUE );
								if( ! $dir_locs->insert() ) {
									$error = true;
								}
							}
						}
					}
					
					
					if( count( $remove_locations ) > 0 ) {
						$dir_locs->setWhereIn('loc_id', $remove_locations );
						if( ! $dir_locs->delete() ) {
							$error = true;
						}
						foreach( $remove_locations as $rem_loc ) {
							if(($key = array_search($rem_loc, (array) $new_locs)) !== false) {
								unset($new_locs[$key]);
							}
						}
					}
					
					
					$results['new_locations'] = implode( ',', $new_locs );
						
					if( $error ) {
						$results['error'] = true;
						$results['msg'] = 'Error updating business location!';
					}
				break;
				
				case 'business-update-tasks':
					$this->load->model(array('Directory_meta_model'));
					$is_task = new $this->Directory_meta_model;
					$is_task->setDirId( $this->input->post('business_id'), TRUE );
					$is_task->setMetaKey( 'is_task', TRUE );
					
					if( $is_task->nonEmpty() != FALSE ) {
						$is_task->setMetaValue( (($this->input->post('is_task')=='true') ? 1 : 0), FALSE, TRUE );
						$is_task->update();
					} else {
						$is_task->setDirId( $this->input->post('business_id'), TRUE, TRUE );
						$is_task->setMetaKey( 'is_task', TRUE, TRUE );
						$is_task->setMetaValue( (($this->input->post('is_task')) ? 1 : 0), FALSE, TRUE );
						$is_task->setActive(1, FALSE, TRUE);
						$is_task->insert();
					}
					
					$task_message = new $this->Directory_meta_model;
					$task_message->setDirId( $this->input->post('business_id'), TRUE );
					$task_message->setMetaKey( 'task_message', TRUE );
					
					if( $task_message->nonEmpty() != FALSE ) {
						$task_message->setMetaValue( $this->input->post('task_message'), FALSE, TRUE );
						$task_message->update();
					} else {
						$task_message->setDirId( $this->input->post('business_id'), TRUE, TRUE );
						$task_message->setMetaKey( 'task_message', TRUE, TRUE );
						$task_message->setMetaValue( $this->input->post('task_message'), FALSE, TRUE );
						$task_message->setActive(1, FALSE, TRUE);
						$task_message->insert();
					}
					
							$results['error'] = false;
							$results['msg'] = 'Updated!';
								
				break;
				
				case 'business-update-logo':
							
							$config['upload_path'] = '../indavao-uploads/';
							$config['allowed_types'] = 'jpg|jpeg|gif|png';
							$config['max_size']	= '100000';
							$config['max_width']  = '1024';
							$config['max_height']  = '768';

							$this->load->library('upload', $config);

							if ( ! $this->upload->do_upload())
							{
								$results['error'] = true;
								$results['message'] = $this->upload->display_errors();
								
							}
							else
							{
								$results['error'] = false;
								$results['message'] = "Success!";
								$upload_data = $this->upload->data();
								$results['upload_data'] = $upload_data;
								
								$this->load->model(array('Media_uploads_model', 'Directory_media_model'));
								$container = new $this->Media_uploads_model;
								$container->setFileName( $upload_data['file_name'], FALSE, TRUE );
								$container->setFileType( $upload_data['file_type'], FALSE, TRUE );
								$container->setFilePath( $upload_data['file_path'], FALSE, TRUE );
								$container->setFullPath( $upload_data['full_path'], FALSE, TRUE );
								$container->setRawName( $upload_data['raw_name'], FALSE, TRUE );
								$container->setOrigName( $upload_data['orig_name'], FALSE, TRUE );
								$container->setClientName( $upload_data['client_name'], FALSE, TRUE );
								$container->setFileExt( $upload_data['file_ext'], FALSE, TRUE );
								$container->setFileSize( $upload_data['file_size'], FALSE, TRUE );
								$container->setIsImage( $upload_data['is_image'], FALSE, TRUE );
								$container->setImageWidth( $upload_data['image_width'], FALSE, TRUE );
								$container->setImageHeight( $upload_data['image_height'], FALSE, TRUE );
								$container->setImageType( $upload_data['image_type'], FALSE, TRUE );
								$container->setImageSizeStr( $upload_data['image_size_str'], FALSE, TRUE );

								if( $container->insert() ) {
									$results['id'] = $container->getMediaId();
									$upload_results = $container->getByMediaId();
									$results['results'] = $upload_results;
									
									$media = new $this->Directory_media_model;
									$media->setDirId(  $this->input->post('business_id'), TRUE, TRUE );
									$media->setMediaId( $upload_results->media_id, FALSE, TRUE );
									$media->setMediaThumb( $upload_results->media_id, FALSE, TRUE );
									$media->setMediaName( 'logo', TRUE, TRUE );
									$media->setDirMedGroup( 'general', TRUE, TRUE );
									$media->setMediaType( 'image', TRUE, TRUE );
									$media->setDirMedActive( 1, FALSE, TRUE );
									$media->setDirMedOrder( 0, FALSE, TRUE );
									$media->delete();
									$media->insert();
								}
							}
							
				break;
				case 'business-remove-logo':
					$this->load->model(array('Directory_media_model'));
					$media = new $this->Directory_media_model;
					$media->setDirId(  $this->input->post('dir_id'), TRUE );
					$media->setMediaName( 'logo', TRUE );
					if( $media->delete() ) {
						$results['error'] = FALSE;
						$results['id'] = $this->input->post('dir_id');
					}
				break;
				
				case 'business_tags_add':
					if( $this->input->post('business_tag') != FALSE ) {
						$this->load->model(array('Tags_model'));
						
						foreach( explode(",", $this->input->post('business_tag')) as $tag_new) {
							$tag = new $this->Tags_model;
							$tag->setTagName( trim( strtolower( $tag_new ) ), true, true );
							$tag->setTagSlug( url_title( $tag_new, '-', true), false, true );
							if( $tag->nonEmpty() == FALSE ) {
								$tag->insert();
								$tagResult = $tag->get();
							} else {
								$tagResult = $tag->getResults();
							}
							
							$results['tag'] = $tagResult;
							
							$this->load->model(array('Directory_tags_model'));
							$dir_tag = new $this->Directory_tags_model;
							$dir_tag->setDirId( $this->input->post('business_id'), TRUE, TRUE);
							$dir_tag->setTagId( $tagResult->tag_id, true, true );
							$dir_tag->setDirTagActive( 1, false, true );
							
							if( $dir_tag->nonEmpty() == FALSE ) {
								if( $dir_tag->insert() ) {
									$results['error'] = false;
									$results['dir_tag'] =  $dir_tag->get();
								}
							} else {
								$results['dir_tag'] =  $dir_tag->getResults();
							}
						}
					}
				break;
				case 'business_tags_delete':
					if( $this->input->post('dir_tag_id') != FALSE ) {
						$this->load->model(array('Directory_tags_model'));
						$dir_tag = new $this->Directory_tags_model;
						$dir_tag->setDirTagId($this->input->post('dir_tag_id'), true);
						if( $dir_tag->delete() ) {
							$results['error'] = false;
							$results['dir_tag_id'] = $this->input->post('dir_tag_id');
						}
					}
				break;
				case 'business_promote':
					
					if( $this->input->post('dir_id') != FALSE ) {
						if(( $this->input->post('key') != FALSE ) && ( $this->input->post('value') != FALSE ) ) {
							$this->load->model(array('Directory_meta_model'));
							$meta = new $this->Directory_meta_model;
							$meta->setDirId( $this->input->post('dir_id'), true, true );
							$meta->setMetaKey( $this->input->post('key'), true, true );
							$meta->setMetaValue( $this->input->post('value'), false, true );
							$meta->setActive( 1, false, true );
							$meta->setMetaGroup('promotion', true, true );
							if( $meta->nonEmpty() !== FALSE ) {
								$meta->limitDataFields( 'meta_value' );
								$meta->update();
							} else {
								$meta->insert();
							}
						}
					}
				break;
			}
		}
		
		echo json_encode( $results );
		exit;
	}
	
	public function logout() {
		$this->session->sess_destroy();
		redirect('login', 'location');
		exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

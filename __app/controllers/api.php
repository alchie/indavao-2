<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MY_Controller {
	
	var $response = array(
			'error' => true,
			'message' => 'Empty Result',
			'api' => 'error',
			);
			
	public function __construct() {
		parent::__construct();
		$this->_mustLogin();
		
		$this->load->library(array(
			'user_agent',
			'form_validation',
		));
		
		$base = parse_url( base_url() );
		$referrer = parse_url( $this->agent->referrer() );
		
		if( ( isset( $referrer['host'] ) && $base['host'] !== $referrer['host'] ) || 
		( ! isset( $referrer['host'] ) )) {
			echo json_encode(0);
			exit;
		}
		
		$this->response['postData'] = $this->input->post();
		
	}
	
	public function properties($action=NULL) {
		$response = $this->response;
		
		$returnData = array();
		
		$this->load->model('Realestate_data_model');
		
		switch( $action ) {
			default:
				$limit = ($this->input->post('limit')!==FALSE) ? $this->input->post('limit') : 10;
				$start = ($this->input->post('start')!==FALSE) ? $this->input->post('start') : 0;
				$my_properties = new $this->Realestate_data_model;
				$my_properties->setUserId( $this->session->userdata('user_id'), TRUE );
				$my_properties->setReActive( 1, TRUE );
				$my_properties->setOrder('re_added', 'DESC');
				$my_properties->setLimit($limit);
				$my_properties->setStart($start);
				$returnData = $my_properties->populate();
				break;
		}
		
		$response['api'] = 'properties';
		$response['error'] = false;
		$response['message'] = 'Success';
		$response['returnData'] = $returnData;
		
		echo json_encode( $response );
		exit;
	}
	
	public function business_directory( $action=NULL, $business_id=NULL ) {
		$response = $this->response;
		$response['api'] = 'business_directory';
		$returnData = array();
		
		$this->load->model(array(
			'Directory_data_model',
			'Directory_details_model',
			'Directory_category_model',
			'Directory_tags_model',
			'Directory_location_model',
			'Directory_media_model',
			));
		
		function __update_detail($self, $id, $dKey, $value) {
			$directory_details = new $self->Directory_details_model;
			$directory_details->setDirId( $id, TRUE, TRUE );
			$directory_details->setDirDKey($dKey, TRUE, TRUE );
			$directory_details->setDirDActive(1, TRUE, TRUE);
			$directory_details->setDirDValue( trim( $value ), FALSE, TRUE );
			if( $directory_details->nonEmpty() === TRUE ) {
				$directory_details->limitDataFields('dir_d_value');
				return $directory_details->update();
			} else {
				return $directory_details->insert();
			}
		}

		function __delete_detail($self, $id, $dKey) {
			$directory_details = new $self->Directory_details_model;
			$directory_details->setDirId( $id, TRUE );
			$directory_details->setDirDKey($dKey, TRUE );
			$directory_details->delete();
		}
						
		switch( $action ) {
			case 'add_new':
				$this->form_validation->set_rules('business_name', 'Business Name', 'required');
				
				if ($this->form_validation->run() !== FALSE) {
					
					$new_id = random_string('nozero', 10);
					$slug = url_title( $this->input->post('business_name'), '-', TRUE );
					
					$directory_match = new $this->Directory_data_model;
					$directory_match->setDirId( $new_id, FALSE, TRUE );
					$directory_match->setDirName( $this->input->post('business_name'), FALSE, TRUE );
					$directory_match->setDirSlug( $slug, FALSE, TRUE );
					$directory_match->setDirBranch( 0, FALSE, TRUE );
					$directory_match->setUserId( $this->session->userdata('user_id'), FALSE, TRUE );
					$directory_match->setDirStatus('pending', FALSE, TRUE);
					$directory_match->setDirActive(1, FALSE, TRUE);
					
					if( $directory_match->insert() ) {
						$response['error'] = false;
						$response['message'] = 'Business Successfully Added!';
						$returnData = $directory_match->getByDirId();
						$response['redirect'] = site_url(array('my', md5(sha1( $this->session->userdata('user_id') )),'business')) . '?id=' . $new_id;
					}
				} else {
					$response['message'] = validation_errors();
				}
				
				break;
			case 'delete':
				if( $business_id != NULL ) {
					$directory_match = new $this->Directory_data_model;
					$directory_match->setDirId( $business_id, TRUE );
					if( $directory_match->nonEmpty() === TRUE) {
						$returnData = $directory_match->getResults();
						if( $directory_match->delete() ) {
							$response['error'] = false;
							$response['message'] = 'Business `' . $returnData->dir_name . '` Successfully Deleted!';
						}
					}
				}
				break;
			case 'update_basic':
				if( $business_id != NULL ) {
					$this->form_validation->set_rules('business_name', 'Business Name', 'required');
					$this->form_validation->set_rules('business_address', 'Business Address', 'required');
					$this->form_validation->set_rules('business_phone', 'Business Phone', 'required');
					$this->form_validation->set_rules('business_email', 'Business Email', 'valid_email');
					
					if ($this->form_validation->run() !== FALSE)
					{
						$directory_current = new $this->Directory_data_model;
						$directory_current->setDirId( $business_id, TRUE);
						$directory_current->setUserId( $this->session->userdata('user_id'), TRUE );
						
						if( $this->input->post('business_name') != FALSE ) {
							$directory_current->setDirName($this->input->post('business_name'), FALSE, TRUE);
							$directory_current->setDirSlug( url_title( $this->input->post('business_name'), '-', TRUE ), FALSE, TRUE );
							$directory_current->update();
						}
						
						if( $this->input->post('business_address') != FALSE ) {
							 __update_detail( $this, $business_id, 'address', $this->input->post('business_address') ); 
						} else {
							__delete_detail( $this, $business_id, 'address' );
						}
						
						if( $this->input->post('business_phone') != FALSE ) {
							__update_detail( $this, $business_id, 'phone', $this->input->post('business_phone') );
						} else {
							__delete_detail( $this, $business_id, 'phone' );
						}
						
						if( $this->input->post('business_fax') != FALSE ) {
							__update_detail( $this, $business_id, 'fax', $this->input->post('business_fax') );
						} else {
							__delete_detail( $this, $business_id, 'fax' );
						}
						
						if( $this->input->post('business_description') != FALSE ) {
							__update_detail( $this, $business_id, 'abstract', $this->input->post('business_description') );
						} else {
							__delete_detail( $this, $business_id, 'abstract' );
						}
						
						if( $this->input->post('business_website') != FALSE ) {
							__update_detail( $this, $business_id, 'website', $this->input->post('business_website') );
						} else {
							__delete_detail( $this, $business_id, 'website' );
						}
						
						if( $this->input->post('business_email') != FALSE ) {
							__update_detail( $this, $business_id, 'email', $this->input->post('business_email') );
						} else {
							__delete_detail( $this, $business_id, 'email' );
						}
						
						$response['error'] = false;
						$response['message'] = 'Basic Information Successfully Updated!';
						
					} else {
						$response['message'] = validation_errors();
					}
								
				}
				break;
			case 'update_map':
				if( $business_id != NULL ) {
					$this->form_validation->set_rules('business_map_lat', 'Business Map Latitude', 'required');
					$this->form_validation->set_rules('business_map_lng', 'Business Map Longtitude', 'required');
					
					if ($this->form_validation->run() !== FALSE)
					{
						if( $this->input->post('business_map_lat') != FALSE ) {
							__update_detail( $this, $business_id, 'map_lat', $this->input->post('business_map_lat') );
						} else {
							__delete_detail( $this, $business_id, 'map_lat' );
						}
						
						if( $this->input->post('business_map_lng') != FALSE ) {
							__update_detail( $this, $business_id, 'map_long', $this->input->post('business_map_lng') );
						} else {
							__delete_detail( $this, $business_id, 'map_long' );
						}
						
						$response['error'] = false;
						$response['message'] = 'Map Coordinates Successfully Updated!';
					} else {
						$response['message'] = validation_errors();
					}
				}
				break;
			case 'get_tags':
				if( $business_id != NULL ) {
					$response['error'] = false;
					$response['api'] = 'business_directory_tags';
					
					$dir_tags = new $this->Directory_tags_model;
					$dir_tags->setDirId($business_id, TRUE);
					$dir_tags->setJoin('tags', 'tags.tag_id = directory_tags.tag_id');
					$dir_tags->setLimit(0);
					$returnData = $dir_tags->populate();
				
				}
				break;
			case 'add_tag':
				if( $business_id != NULL ) {
					$response['error'] = false;
					$response['api'] = 'business_directory_tags';
					
					$this->load->model(array('Tags_model'));
					$tag = new $this->Tags_model;
					$tag->setTagName( trim( strtolower( $this->input->post('tag') ) ), true, true );
					$tag->setTagSlug( url_title( $this->input->post('tag'), '-', true), false, true );
					if( $tag->nonEmpty() == FALSE ) {
						$tag->insert();
						$tagResult = $tag->get();
					} else {
						$tagResult = $tag->getResults();
					}
					
					$response['tag'] = $tagResult;
					
					$dir_tag = new $this->Directory_tags_model;
					$dir_tag->setDirId( $business_id, TRUE, TRUE);
					$dir_tag->setTagId( $tagResult->tag_id, true, true );
					$dir_tag->setDirTagActive( 1, false, true );
					
					if( $dir_tag->nonEmpty() == FALSE ) {
						if( $dir_tag->insert() ) {
							$response['error'] = false;
							$returnData =  $dir_tag->get();
							$response['message'] = "Added New Tag `".$tagResult->tag_name."`!";
						}
					} else {
						$response['error'] = true;
						$returnData =  $dir_tag->getResults();
						$response['message'] = "Tag `".$tagResult->tag_name."` Already Exist!";
					}
					
				}
				break;
			case 'delete_tag':
				if( $business_id != NULL ) {
					$response['api'] = 'business_directory_tags';
					
					if( $this->input->post('tag_id') !== FALSE ) {
						$dir_tag = new $this->Directory_tags_model;
						$dir_tag->setDirTagId( $this->input->post('tag_id'), true, true );
						if( $dir_tag->delete() ) {
							$response['error'] = false;
							$response['message'] = "Tag Deleted Successfully!";
							$returnData =  $this->input->post('tag_id');
						} else {
							$response['message'] = "Error Deleting Tag!";
						}
					}
				}
				break;
			case 'get_categories':
				if( $business_id != NULL ) {
					$response['error'] = false;
					$response['api'] = 'business_directory_categories';
					
					$dir_categories = new $this->Directory_category_model;
					$dir_categories->setDirId($business_id, TRUE);
					$dir_categories->setDirCatActive(1, TRUE);
					$dir_categories->setLimit(0);
					$dir_categories->setSelect('directory_category.*, taxonomies.*');
					$dir_categories->setJoin('taxonomies', 'taxonomies.tax_id = directory_category.tax_id');
					$returnData = $dir_categories->populate();
				
				}
				break;
			case 'add_category':
				if( $business_id != NULL ) {
					$response['api'] = 'business_directory_category';
					
					if( $this->input->post('tax_id') !== FALSE ) {
						$dir_cat = new $this->Directory_category_model;
						$dir_cat->setDirId( $business_id, TRUE, TRUE);
						$dir_cat->setTaxId( $this->input->post('tax_id'), true, true );
						$dir_cat->setDirCatActive( 1, false, true );
						
						if( $dir_cat->nonEmpty() == FALSE ) {
							if( $dir_cat->insert() ) {
								$response['error'] = false;
								$returnData =  $dir_cat->get();
								$response['message'] = "Added Category `".$this->input->post('tax_name')."`!";
							}
						} else {
							$response['error'] = true;
							$returnData =  $dir_cat->getResults();
							$response['message'] = "Tag `".$this->input->post('tax_name')."` Already Exist!";
						}
					}
				}
				break;
			case 'delete_category':
				if( $business_id != NULL ) {
					$response['api'] = 'business_directory_category';
					
					if( $this->input->post('tax_id') !== FALSE ) {
						$dir_cat = new $this->Directory_category_model;
						$dir_cat->setDirId( $business_id, TRUE, TRUE);
						$dir_cat->setTaxId( $this->input->post('tax_id'), true, true );
						if( $dir_cat->delete() ) {
							$response['error'] = false;
							$response['message'] = "Category `".$this->input->post('tax_name')."` Deleted Successfully!";
							$returnData =  $this->input->post('tax_id');
						} else {
							$response['message'] = "Error Deleting Category!";
						}
					}
				}
				break;
			case 'get_location':
				if( $business_id != NULL ) {
					$response['error'] = false;
					$response['api'] = 'business_directory_location';
					
					$dir_location = new $this->Directory_location_model;
					$dir_location->setDirId($business_id, TRUE);
					$dir_location->setDirLocActive(1, TRUE);
					$dir_location->setLimit(0);
					$dir_location->setSelect('directory_location.*, locations.*');
					$dir_location->setJoin('locations', 'locations.loc_id = directory_location.loc_id');
					$returnData = $dir_location->get();
				}
				break;
			case 'update_location':
				if( $business_id != NULL ) {
					$response['error'] = false;
					$response['api'] = 'business_directory_location';
					
					$dir_location = new $this->Directory_location_model;
					$dir_location->setDirId($business_id, TRUE, TRUE);
					$dir_location->setLocId($this->input->post('loc_id'), FALSE, TRUE);
					$dir_location->setDirLocActive(1, FALSE, TRUE);
					$dir_location->setDirLocPrimary(1, FALSE, TRUE);
					if( $dir_location->nonEmpty() !== FALSE ) {
						$dir_location->limitDataFields('loc_id');
						$dir_location->update();
					} else {
						$dir_location->insert();
					}
					
					$dir_location->setWhere('loc_id', $this->input->post('loc_id') );
					$returnData = $dir_location->get();
					$response['message'] = 'Location Changed to `'.$this->input->post('loc_name').'`';
				}
				break;
			case 'get_photos':
				if( $business_id != NULL ) {
					$response['error'] = false;
					$response['api'] = 'business_directory_photos';
					$media = new $this->Directory_media_model;
					$media->setDirId( $business_id, TRUE, TRUE );
					$media->setJoin('media_uploads', 'media_uploads.media_id = directory_media.media_id');
					$media->setLimit(0);
					$returnData = $media->populate();
				}
				break;
			case 'delete_photo':
				if( $business_id != NULL ) {
					
					$response['api'] = 'business_directory_photos';
					$media = new $this->Directory_media_model;
					$media->setDirId( $business_id, TRUE );
					$media->setDirMedId( $this->input->post('id'), TRUE );
					if( $media->delete() ) {
						$response['error'] = false;
						$response['message'] = "Photo Deleted Successfully!";
					}
				}
				break;
			case 'add_photos':
				if( $business_id != NULL ) {
					$config['upload_path'] = '../indavao-uploads/';
					$config['allowed_types'] = 'jpg|jpeg|gif|png';
					$config['max_size']	= '100000';
					$config['max_width']  = '1024';
					$config['max_height']  = '768';

					$this->load->library('upload', $config);

					if ( ! $this->upload->do_upload())
					{
						$response['error'] = true;
						$response['message'] = $this->upload->display_errors();
						
					}
					else
					{
						$upload_data = $this->upload->data();
						$response['error'] = false;
						$response['message'] = "Photo `{$upload_data['file_name']}` Successfully Uploaded!";
						$response['upload_data'] = $upload_data;
						
						$this->load->model(array('Directory_media_model','Media_uploads_model'));
						
						$container = new $this->Media_uploads_model;
						$container->setFileName( $upload_data['file_name'], FALSE, TRUE );
						$container->setFileType( $upload_data['file_type'], FALSE, TRUE );
						$container->setFilePath( $upload_data['file_path'], FALSE, TRUE );
						$container->setFullPath( $upload_data['full_path'], FALSE, TRUE );
						$container->setRawName( $upload_data['raw_name'], FALSE, TRUE );
						$container->setOrigName( $upload_data['orig_name'], FALSE, TRUE );
						$container->setClientName( $upload_data['client_name'], FALSE, TRUE );
						$container->setFileExt( $upload_data['file_ext'], FALSE, TRUE );
						$container->setFileSize( $upload_data['file_size'], FALSE, TRUE );
						$container->setIsImage( $upload_data['is_image'], FALSE, TRUE );
						$container->setImageWidth( $upload_data['image_width'], FALSE, TRUE );
						$container->setImageHeight( $upload_data['image_height'], FALSE, TRUE );
						$container->setImageType( $upload_data['image_type'], FALSE, TRUE );
						$container->setImageSizeStr( $upload_data['image_size_str'], FALSE, TRUE );

						if( $container->insert() ) {
							$upload_results = $container->getByMediaId();
							$response['media_data'] = $upload_results;
							
							$media = new $this->Directory_media_model;
							$media->setDirId( $business_id, TRUE, TRUE );
							$media->setMediaId( $upload_results->media_id, FALSE, TRUE );
							$media->setMediaThumb( $upload_results->media_id, FALSE, TRUE );
							$media->setMediaName( 'photo', TRUE, TRUE );
							$media->setDirMedGroup( 'general', TRUE, TRUE );
							$media->setMediaType( 'image', TRUE, TRUE );
							$media->setDirMedActive( 1, FALSE, TRUE );
							$media->setDirMedOrder( 0, FALSE, TRUE );
							if( $media->insert() ) {
								$returnData = $media->getByDirMedId();
							}
						}
					}
				}
				break;
			default:
				$limit = ($this->input->post('limit')!==FALSE) ? $this->input->post('limit') : 10;
				$start = ($this->input->post('start')!==FALSE) ? $this->input->post('start') : 0;
				$my_directory = new $this->Directory_data_model;
				$my_directory->setUserId( $this->session->userdata('user_id'), TRUE );
				$my_directory->setDirActive( 1, TRUE );
				$my_directory->setOrder('dir_added', 'DESC');
				$my_directory->setLimit($limit);
				$my_directory->setStart($start);
				
				if( $my_directory->nonEmpty() !== FALSE ) {
					
					$my_directory->setSelect("directory_data.*");
					// address
					$my_directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'address' LIMIT 1) as address");
					// phone
					$my_directory->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'phone' LIMIT 1) as phone");
					
					$returnData = $my_directory->populate();
					$response['error'] = false;
					$response['message'] = 'Success';
				}
				
				break;
		}
		
		$response['returnData'] = $returnData;
		
		echo json_encode( $response );
		exit;
	}

	public function account( $action=NULL ) {
		$response = $this->response;
		
		$returnData = array();
		
		switch( $action ) {
			case 'update':
				$this->form_validation->set_rules('name', 'Name', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required');
				$this->form_validation->set_rules('phone', 'Phone', 'required');
				
				if ($this->form_validation->run() !== FALSE)
				{
					$this->__user_meta_update('name', $this->input->post('name'));
					$this->__user_meta_update('email', $this->input->post('email'));
					$this->__user_meta_update('phone', $this->input->post('phone'));
					$response['error'] = false;
					$response['message'] = 'Personal Information Successfully Updated!';
					
				} else {
					$response['message'] = validation_errors();
				}
				break;
		}
		
		$response['api'] = 'business_directory';
		
		echo json_encode( $response );
		exit;
	}
	
	public function taxonomies( $action=NULL ) {
		$response = $this->response;
		$api = 'taxonomies';
		$returnData = array();
		
		switch( $action ) {
			case 'business_category':
				$response['error'] = false;
				$this->load->model('Taxonomies_ancestors_model');
				$business_categories = new $this->Taxonomies_ancestors_model;
				$business_categories->setJoin('taxonomies', 'taxonomies.tax_id = taxonomies_ancestors.tax_id');
				$business_categories->setWhere('taxonomies.tax_type', get_settings_value('business_tax_name', 'business_category'));
				$business_categories->setWhere('taxonomies.tax_active', 1);
				$business_categories->setOrder('taxonomies.tax_name', 'ASC');
				$business_categories->setLimit(0);
				$business_categories->cache_on();
				$returnData = $business_categories->recursive('tax_parent', get_settings_value('main_business_category_id'), 'tax_id', 2);
				break;
		}
		
		$response['api'] = $api;
		$response['returnData'] = $returnData;
		echo json_encode( $response );
		exit;
	}
	
	public function locations( $action=NULL ) {
		$response = $this->response;
		$api = 'locations';
		$returnData = array();
		
		switch( $action ) {
			case 'all':
				$response['error'] = false;
				$this->load->model('Locations_ancestors_model');
				$locations = new $this->Locations_ancestors_model;
				$locations->setJoin('locations', 'locations.loc_id = locations_ancestors.loc_id');
				$locations->setWhere('locations.loc_active', 1);
				$locations->setOrder('locations.loc_name', 'ASC');
				$locations->setLimit(0);
				//$locations->cache_on();
				$returnData = $locations->recursive('loc_parent', get_settings_value('main_location_id'), 'loc_id', 2);
				break;
		}
		
		$response['api'] = $api;
		$response['returnData'] = $returnData;
		echo json_encode( $response );
		exit;
	}
	
	private function __user_meta_update($key, $value) {
		$this->load->model('Users_meta_model');
		$user_meta = new $this->Users_meta_model;
		$user_meta->setUserId( $this->session->userdata('user_id'), true );
		$user_meta->setMetaKey($key, TRUE);
		$user_meta->setMetaValue( $value, FALSE, TRUE );
		if( $user_meta->nonEmpty() === FALSE ) {
			$user_meta->setMetaKey($key, FALSE, TRUE);
			$user_meta->setUserId( $this->session->userdata('user_id'), FALSE, TRUE );
			$user_meta->setUserMetaActive(1, FALSE, TRUE);
			$user_meta->replace();
		} else {
			$user_meta->update();
		}
		
		$cookie = array(
			'name'   => 'meta_updated_' . $key,
			'value'  =>  $value,
			'expire' => '86500',
			'domain' => $this->config->item('cookie_domain'),
			'path'   => $this->config->item('cookie_path'),
			'prefix' => $this->config->item('cookie_prefix'),
			'secure' => $this->config->item('cookie_secure')
		);

		$this->input->set_cookie( $cookie );
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends MY_Controller {

	function __construct() 
    {
        parent::__construct();
                
    }
    
	function __init() {
		
		parent::__init();
		
		// model 
        $this->load->model(array('Directory_data_model'));
        
        // default title
        $this->template_data->page_title("Job Postings In Davao City");
        
        $this->template_data->set('itemscope', 'JobPosting' );
        
        if( $this->session->userdata('logged_in') ) {
			$this->template_data->add_to_array('footer_js', base_url('assets/js/jquery.useractions.js'), 201 );
		}
		
	}
	
	public function index()
	{
		$this->__init();
		$this->browse();
	}
	
	public function search() {
		$url = "jobs/browse";
		if( $this->input->post() ) {
			$keys = array();
			if( ($this->input->post('job_function') !== FALSE) && (count(array_filter($this->input->post('job_function'))) > 0)) {
				$keys[] = implode("_", $this->input->post('job_function'));
			} else {
				$keys[] = "any";
			}
			if( ($this->input->post('job_status') !== FALSE) && (count(array_filter($this->input->post('job_status'))) > 0)) {
				$keys[] = implode("-", $this->input->post('job_status'));
			} else {
				$keys[] = "any";
			}
			if( ($this->input->post('location') !== FALSE) && (count(array_filter($this->input->post('location'))) > 0)) {
				$keys[] = implode("_", $this->input->post('location'));
			} else {
				$keys[] = "any";
			}
			$url = "jobs/search-" . implode('%7C', $keys);
			
		}
		redirect( $url, 'location');
		exit;
	}
	
	private function __decode_search( $code ) {
		$keys = explode('%7C',  $code );
		
		$search_keys = array();
		$search_keys['job_function'] = (isset($keys[0]) && $keys[0] != '' && $keys[0] != 'any') ? explode("_",$keys[0]) : array();
		$search_keys['job_status'] = (isset($keys[1]) && $keys[1] != '' && $keys[1] != 'any') ? explode("-", $keys[1]) : array();
		$search_keys['location'] = (isset($keys[2]) && $keys[2] != '' && $keys[2] != 'any') ? explode("_", $keys[2]) : array();
		
		return (object) $search_keys;
	}
	
	public function browse( $code=NULL ) {
		
		$this->__init();
		
		$this->template_data->page_title( "Browse All Jobs In Davao City" );
		
		$keys = (object) array();
		
		if( !is_null( $code ) ) {
			$keys = $this->__decode_search( $code );
			$this->template_data->page_title( "Search Jobs In Davao City" );
		}
		 
		$this->__display($keys);
		
	}
	
	protected function __display( $keys ) {
		
		$this->__init();
		
		$this->template_data->set('search_keys', $keys );
				 
		$current_page = 1;
		if( $this->input->get('page') != '') {
			 $current_page = $this->input->get('page');
			 $this->template_data->page_title( "Page " . $current_page . " | " . $this->template_data->get('page_title') );
		}
		$this->template_data->set('current_page', $current_page );

		$list_limit = 10;
		$list_start = ( $current_page - 1) * $list_limit;
		
		$jobs = $this->__browse( $keys, $list_start, $list_limit );
		
		$this->template_data->set('jobs', $jobs->results );
		$this->template_data->set('total_items',  $jobs->total_items );
		$this->template_data->set('pages',  ceil( $jobs->total_items / $list_limit ) );
		
		$this->load->view('jobs/index', $this->template_data->get() );
	}
	
	protected function __browse( $keys, $list_start, $list_limit ) {
		return (object) array(
			'results' => '',
			'total_items' => 0
		);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/jobs.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My extends MY_Controller {
	
	function __construct() 
    {
        parent::__construct();
        
        $this->_mustLogin();
        
        $this->template_data->page_title("My Account - In Davao City");
		
		$this->template_data->set('main_page', 'my' ); 
        $this->template_data->set('sub_page', 'account' );
    }
    
    function __init() {
		parent::__init();
		
		
		$this->template_data->add_to_array('footer_js', base_url('assets/js/jquery.useractions.js'), 201 );
		
		$activity_stats = $this->_user_stats();
		$this->template_data->set('activity_stats', $activity_stats );

	}
		
	protected function _check_id( $account_id, $page='dashboard' ) 
	{
		if( $account_id != md5(sha1($this->session->userdata('user_id'))) ) {
			redirect('my/' . md5(sha1($this->session->userdata('user_id'))) . '/' . $page , 'location');
			exit;
		}
	}
	
	private function _is_points() 
	{
		if( $this->session->userdata('isPoints') === FALSE ) {
			redirect('my/' . $this->session->userdata('user_id') . '/account' , 'location');
			exit;
		}
	}
	
	private function _user_stats() {
		
		$this->load->model('Users_model');
		$stats = new $this->Users_model;
		$stats->setSelect("COUNT(*) as total_referrals");
		$stats->setReferrer( $this->session->userdata('user_id'), true );
		$stats->setLimit(1);

		$stats->setSelect("(SELECT COUNT(*) FROM users_bookmarks bm WHERE bm.user_id = '".$this->session->userdata('user_id')."' LIMIT 1) as total_bookmarks");
		
		if( $this->session->userdata('isPoints') ) {
		
			$stats->setSelect("(SELECT COUNT(*) FROM tasks_verified tv WHERE tv.user_id = '".$this->session->userdata('user_id')."' LIMIT 1) as total_shares");
			
			$stats->setSelect("(SELECT SUM(points_credited) FROM users_points pnts WHERE pnts.user_id = '".$this->session->userdata('user_id')."' AND points_claimed = 1  LIMIT 1) as sum_points");
			
			$stats->setSelect("(SELECT COUNT(*) FROM users_points pnts WHERE pnts.user_id = '".$this->session->userdata('user_id')."' LIMIT 1) as total_points");
			
			$stats->setSelect("(SELECT SUM(points_withdrawn) FROM users_points_withdrawals wdrw WHERE wdrw.user_id = '".$this->session->userdata('user_id')."' LIMIT 1) as sum_withdrawals");
			
			$stats->setSelect("(SELECT COUNT(*) FROM users_points_withdrawals wdrw WHERE wdrw.user_id = '".$this->session->userdata('user_id')."' LIMIT 1) as total_withdrawals");
		}
		
		$stats->cache_on();
		return $stats->get();
	}
	
	private function _meta_update($key, $value) {
		$user_meta = new $this->Users_meta_model;
		$user_meta->setUserId( $this->session->userdata('user_id'), true );
		$user_meta->setMetaKey($key, TRUE);
		$user_meta->setMetaValue( $value, FALSE, TRUE );
		if( $user_meta->nonEmpty() === FALSE ) {
			$user_meta->setMetaKey($key, FALSE, TRUE);
			$user_meta->setUserId( $this->session->userdata('user_id'), FALSE, TRUE );
			$user_meta->setUserMetaActive(1, FALSE, TRUE);
			$user_meta->replace();
		} else {
			$user_meta->update();
		}
		
		$cookie = array(
			'name'   => 'meta_updated_' . $key,
			'value'  =>  $value,
			'expire' => '86500',
			'domain' => $this->config->item('cookie_domain'),
			'path'   => $this->config->item('cookie_path'),
			'prefix' => $this->config->item('cookie_prefix'),
			'secure' => $this->config->item('cookie_secure')
		);

		$this->input->set_cookie( $cookie );
	}
	
	public function index()
	{
		redirect('my/' . $this->session->userdata('user_id') . '/dashboard' , 'location');
		exit;
	}
	
	public function page_not_found( $account_id='' ) {
		$this->_check_id( $account_id, 'dashboard' );
		$this->__init();
		$this->load->view('my/404', $this->template_data->get() );
	}
	
	public function dashboard( $account_id='' )
	{
		$this->_check_id( $account_id, 'dashboard' );
		$this->__init();
		$this->template_data->set('sidebar_nav', 'dashboard' );
		$this->load->view('my/dashboard', $this->template_data->get() );
	}
	
	public function account( $account_id='' ) {
		
		$this->_check_id( $account_id, 'account' );
		
		$this->__init();
		 
		$this->template_data->set('sub_page', 'account' );
		$this->template_data->page_title("Account Settings - In Davao City");
		
		$this->load->model('Users_meta_model');
		if( $this->input->post() ) {
			if( $this->input->post('name') !== FALSE ) {
				$this->_meta_update( "name", $this->input->post('name'));
			}
			if( $this->input->post('email') !== FALSE ) {
				$this->_meta_update( "email", $this->input->post('email'));
			}
			if( $this->input->post('phone') !== FALSE ) {
				$this->_meta_update( "phone", $this->input->post('phone'));
			}
		} 
		
		
		$user_meta = new $this->Users_meta_model;
		$user_meta->setUserId( $this->session->userdata('user_id'), true );
		$user_meta->setMetaKey('name');
		$user_meta->setSelect('users_meta.meta_value as name');
		
		$user_meta->setSelect('(SELECT meta_value FROM users_meta ume WHERE ume.user_id = \''.$this->session->userdata('user_id').'\' AND ume.meta_key = \'email\' LIMIT 1) as email');
		
		$user_meta->setSelect('(SELECT meta_value FROM users_meta ume WHERE ume.user_id = \''.$this->session->userdata('user_id').'\' AND ume.meta_key = \'phone\' LIMIT 1) as phone');
		
		$user_meta->cache_on();
		$this->template_data->set('user_meta', $user_meta->get() );
		
		$this->template_data->set('sidebar_nav',  'account' );
		$this->load->view('my/account', $this->template_data->get() );
	}
	
	public function add_business( $account_id='' ) {
		
		$this->_check_id( $account_id, 'add_business' );
		
		$this->__init();
		 
		$this->template_data->set('sub_page', 'business' );
		$this->template_data->page_title("Add a Business - In Davao City");
		/*
		$this->load->model('Directory_data_model');
		
		if( $this->input->post('action') == 'post_a_business' ) {
		
				$this->load->library('form_validation');
				$this->form_validation->set_rules('business_name', 'Business Name', 'required');
				$this->form_validation->set_rules('business_address', 'Business Address', 'required');
				$this->form_validation->set_rules('business_phone', 'Business Phone', 'required');
				if ($this->form_validation->run() !== FALSE)
				{
					
					
					$new_id = random_string('nozero', 10);
					$slug = url_title( $this->input->post('business_name'), '-', TRUE );
					
					$directory_match = new $this->Directory_data_model;
					$directory_match->setDirId( $new_id, FALSE, TRUE );
					$directory_match->setDirName( $this->input->post('business_name'), FALSE, TRUE );
					$directory_match->setDirSlug( $slug, FALSE, TRUE );
					$directory_match->setDirBranch( 0, FALSE, TRUE );
					$directory_match->setUserId( $this->session->userdata('user_id'), FALSE, TRUE );
					$directory_match->setDirStatus('pending', FALSE, TRUE);
					$directory_match->setDirActive(1, FALSE, TRUE);
					
					if( $directory_match->insert() ) {
						header("Location: " . current_url() . "?new_business=" . $new_id);
						exit;
					}
				}
		} 
		
		if( $this->input->get('new_business') != FALSE ) {
		
			if( $this->input->post('action') == 'share_to_facebook' ) {
				$this->load->library('facebook');
				$this->facebook->retrieveJavaScriptSession();
				$this->facebook->addBusiness( $this->input->post('url') );
				$this->facebook->postLinkToFeed( $this->input->post('url'), $this->input->post('business_name') );
				header("Location: " . $this->input->post('url'));
				exit;
			}
		
			$directory = new $this->Directory_data_model;
			$directory->setDirId( $this->input->get('new_business'), TRUE );
			$this->template_data->set('directory', $directory->get() );
		}
		*/
		$this->template_data->set('sidebar_nav',  'business' );
		$this->load->view('my/business/add_business', $this->template_data->get() );
	}
	
	
	
	public function referrals( $account_id ) {
		
		$this->_check_id( $account_id, 'referrals' );
		
		$this->__init();
		
		$this->template_data->set('sub_page', 'referrals' );
		$this->template_data->page_title("My Referrals - In Davao City");
		
		$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
		$this->template_data->set('current_page', $current_page );
		
		$page_limit = 10;
		$page_start = ( $current_page - 1) * $page_limit;
		
		$this->load->model('Users_model');
		$referrals = new $this->Users_model;
		$referrals->setSelect("users.*");
		$referrals->setReferrer( $this->session->userdata('user_id'), true );
		$referrals->setOrder('date_joined', 'DESC');
		$referrals->setStart($page_start);
		$referrals->setLimit($page_limit);
		
		$referrals->setSelect("(SELECT COUNT(*) FROM users ur WHERE ur.referrer = users.user_id) as total_referrals");
		$referrals->setSelect("(SELECT COUNT(*) FROM users_shares us WHERE us.user_id = users.user_id) as total_shares");
		
		$referrals->cache_on();
		$this->template_data->set('referrals', $referrals->populate() );
		
		$activity_stats = $this->template_data->get('activity_stats');
		$this->template_data->set('pages',  ceil( $activity_stats->total_referrals / $page_limit ) );
		
		$this->load->view('my/referrals', $this->template_data->get() );
	}
	
	public function bookmarks( $account_id ) {
		
		$this->_check_id( $account_id, 'bookmarks');
		
		$this->__init();
		
		$this->template_data->set('sub_page', 'bookmarks' );
		$this->template_data->page_title("My Bookmarks - In Davao City");
		
		$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
		$this->template_data->set('current_page', $current_page );
		
		$page_limit = 10;
		$page_start = ( $current_page - 1) * $page_limit;
		
		$this->load->model('Users_bookmarks_model');
		$bookmarks = new $this->Users_bookmarks_model;
		$bookmarks->setSelect("users_bookmarks.*");
		$bookmarks->setUserId( $this->session->userdata('user_id'), true );
		$bookmarks->setOrder('date_added', 'DESC');
		$bookmarks->setStart($page_start);
		$bookmarks->setLimit($page_limit);
		
		//$bookmarks->setSelect("realestate_data.re_title as object_title, realestate_data.re_slug as object_slug");
		//$bookmarks->setJoin('realestate_data', 'realestate_data.re_id = users_bookmarks.object_id AND users_bookmarks.object_type = \'realestate\'', 'LEFT OUTER');
		
		//$bookmarks->setSelect("directory_data.dir_name as object_title, directory_data.dir_slug as object_slug");
		//$bookmarks->setJoin('directory_data', 'directory_data.dir_id = users_bookmarks.object_id AND users_bookmarks.object_type = \'business\'', 'LEFT OUTER');
		
		$bookmarks->setSelect("(SELECT re_title FROM realestate_data where realestate_data.re_id = users_bookmarks.object_id AND users_bookmarks.object_type = 'realestate' LIMIT 1) as re_title");
		$bookmarks->setSelect("(SELECT re_slug FROM realestate_data where realestate_data.re_id = users_bookmarks.object_id AND users_bookmarks.object_type = 'realestate' LIMIT 1) as re_slug");

		$bookmarks->setSelect("(SELECT dir_name FROM directory_data where directory_data.dir_id = users_bookmarks.object_id AND users_bookmarks.object_type = 'business' LIMIT 1) as dir_title");
		$bookmarks->setSelect("(SELECT dir_slug FROM directory_data where directory_data.dir_id = users_bookmarks.object_id AND users_bookmarks.object_type = 'business' LIMIT 1) as dir_slug");

		$bookmarks->cache_on();
		$this->template_data->set('bookmarks', $bookmarks->populate() );
		
		$activity_stats =$this->template_data->get('activity_stats');
		$this->template_data->set('pages',  ceil( $activity_stats->total_bookmarks / $page_limit ) );
		
		$this->load->view('my/bookmarks', $this->template_data->get() );
	}
	
	public function add_bookmark( $account_id ) {
		
		$this->_check_id( $account_id, 'bookmarks');
		
		$this->__init();
		
		$bookmark_title = NULL;
		$bookmark_url = NULL;
		
		if( $this->input->get('type') != FALSE && $this->input->get('id') != FALSE ) {
		
			if( $this->input->post('action') == 'share_to_facebook' ) {
				$this->load->library('facebook');
				
				if( $this->facebook->retrieveRedirectSession() ) {
				
					//$this->facebook->postLinkToFeed( $this->input->post('url'), $this->input->post('message') . " at InDavao.Net" );
				
					switch( $this->input->get('type') ) {
						case 'business':
							$this->facebook->bookmarkBusiness( $this->input->post('url') );
						break;
						case 'realestate':
							$this->facebook->bookmarkProperty( $this->input->post('url') );
						break;
					}
				}
				
				header("Location: " . $this->input->post('url'));
				exit;
			}
		
			$this->load->model('Users_bookmarks_model');
			$bookmark = new $this->Users_bookmarks_model;
			$bookmark->setUserId( $this->session->userdata('user_id'), TRUE, TRUE );
			$bookmark->setObjectId( $this->input->get('id'), TRUE, TRUE );
			$bookmark->setObjectType( $this->input->get('type'), TRUE, TRUE );
			$bookmark->setDateAdded( unix_to_human( time(), TRUE, 'eu' ), FALSE, TRUE );
			if( $bookmark->nonEmpty() == FALSE ) {
				$bookmark->insert();
			}
			
			switch( $this->input->get('type') ) {
				case 'business':
					$this->load->model('Directory_data_model');
					$dir = new $this->Directory_data_model;
					$dir->setDirId( $this->input->get('id'), TRUE );
					if( $dir->nonEmpty() != FALSE ) {
						$result = $dir->getResults();
						$bookmark_title = $result->dir_name;
						$bookmark_url = site_url(array(
							"company", 
							$result->dir_id,
							$result->dir_slug,
							$this->session->userdata('user_id')
						));
					}
				break;
				case 'realestate':
					$this->load->model('Realestate_data_model');
					$realestate = new $this->Realestate_data_model;
					$realestate->setReId( $this->input->get('id'), TRUE );
					if( $realestate->nonEmpty() != FALSE ) {
						$result = $realestate->getResults();
						$bookmark_title = $result->re_title;
						$bookmark_url = site_url(array(
							"property", 
							$result->re_id,
							$result->re_slug,
							$this->session->userdata('user_id')
						));
					}
				break;
			}
		}
		
		$this->template_data->set('bookmark_title', $bookmark_title );
		$this->template_data->set('bookmark_url', $bookmark_url );
		$this->template_data->set('sub_page', 'bookmarks' );
		$this->template_data->page_title("Add Bookmark - In Davao City");
		
		$this->load->view('my/add_to_bookmark', $this->template_data->get() );
	}
	
	public function facebook_friends( $account_id ) {
				
		$this->_check_id( $account_id, 'facebook_friends');
		
		$this->__init();
		
		$this->template_data->set('sub_page', 'facebook_friends' );
		$this->template_data->page_title("My Facebook Friends - In Davao City");
		
		$this->load->model('Users_fb_friends_model');
		$friends = new $this->Users_fb_friends_model;
		$friends->setSelect("users_fb_friends.*");
		$friends->setUserId( $this->session->userdata('user_id'), true );
		
		$friends->setSelect("users.name as friend_name");
		$friends->setJoin('users', 'users.user_id = users_fb_friends.friend_id', 'LEFT OUTER');
		
		$friends->setLimit(0);
		
		$this->template_data->set('friends', $friends->populate() );
		
		$this->load->view('my/facebook_friends', $this->template_data->get() );
	}
	
	public function tasks( $account_id ) {
		
		$this->_is_points();
		$this->_check_id( $account_id, 'tasks' );
		
		$this->__init();
		$post_type = 'realestate';
		switch( $this->input->get('type') ) {
			case 'business':
				$post_type = 'business';
			break;
			case 'realestate':
			default:
				$post_type = 'realestate';
				$this->load->model('Custom_model');
				$tasks = new $this->Custom_model;
				$this->template_data->set('current_task', $tasks->get_my_tasks( $this->session->userdata('user_id') ) );
			break;
		}
		
		$this->template_data->set('post_type', $post_type );
		$this->template_data->set('sub_page', 'tasks' );
		$this->template_data->page_title("My Tasks - In Davao City");
				
		$this->template_data->push('header_js', 'https://apis.google.com/js/client.js' );
		$this->load->view('my/tasks', $this->template_data->get() );
		
	}

	public function tasks_to_friends( $account_id ) {
		
		$this->_is_points();
		$this->_check_id( $account_id, 'tasks' );
		
		$this->__init();
		$tasks_list = array();
		$post_type = 'realestate';
		
		switch( $this->input->get('type') ) {
				case 'business':
					$post_type = 'business';
				break;
				case 'realestate':
				default:
					$post_type = 'realestate';
					if( $this->input->get('task_id') != FALSE && $this->input->get('friend_id') != FALSE ) {
						
						$this->load->model('Realestate_data_model');
						$current_task = new $this->Realestate_data_model;
						$current_task->setReId( $this->input->get('task_id'), TRUE );
						$current_task->setSelect("realestate_data.re_title as title");
						$current_task->setSelect("realestate_data.re_id as object_id");
						$current_task->setSelect("realestate_data.re_slug as slug");
						$current_task->setSelect("(SELECT meta_value FROM realestate_meta rm WHERE rm.re_id = realestate_data.re_id AND rm.meta_key='task_message' LIMIT 1) as task_message");
						
						$this->template_data->set('current_task', $current_task->get() );
						
					} elseif( $this->input->get('task_id') != FALSE ) {
						
						$this->load->model('Users_fb_friends_model');
						$friends = new $this->Users_fb_friends_model;
						$friends->setSelect("users_fb_friends.*");
						$friends->setUserId( $this->session->userdata('user_id'), true );
						
						$friends->setSelect("users.name as friend_name");
						$friends->setJoin('users', 'users.user_id = users_fb_friends.friend_id', 'LEFT OUTER');
						
						$friends->setSelect("tasks_verified.post_url as post_url");
						$friends->setJoin('tasks_verified', 'users_fb_friends.user_id = tasks_verified.user_id AND tasks_verified.tasks_url_id = users_fb_friends.friend_id AND tasks_verified.object_type = "realestate" AND tasks_verified.object_id = '.$this->input->get('task_id'), 'LEFT OUTER');
						
						$friends->setHaving('post_url IS NULL');
						
						$friends->setLimit(0);
						
						$this->template_data->set('friends', $friends->populate() );
					
					} else {
					
						$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
						$this->template_data->set('current_page', $current_page );
						
						$page_limit = 20;
						$page_start = ( $current_page - 1) * $page_limit;
						

							$this->load->model('Realestate_meta_model');
							$tasks = new $this->Realestate_meta_model;
							$tasks->setMetaKey('is_task', TRUE);
							$tasks->setStart($page_start);
							$tasks->setLimit($page_limit);
							$tasks->cache_on();
							$tasks->setJoin('realestate_data rd', 'rd.re_id = realestate_meta.re_id');
							$tasks_list = $tasks->populate();
						
					}
				break;
		}
		
		$this->template_data->set('tasks', $tasks_list );			
		$this->template_data->set('post_type', $post_type );
		
		$this->template_data->set('sub_page', 'tasks_to_friends' );
		$this->template_data->page_title("Tasks to Friends - In Davao City");
				
		$this->template_data->push('header_js', 'https://apis.google.com/js/client.js' );
		$this->load->view('my/tasks_to_friends', $this->template_data->get() );
		
	}
	
	public function shares(  $account_id ) {
		
		$this->_is_points();
		
		$this->_check_id( $account_id, 'shares' );
		
		$this->__init();
		
		$this->template_data->set('sub_page', 'shares' );
		$this->template_data->page_title("My Shares - In Davao City");
				
		$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
		$this->template_data->set('current_page', $current_page );
		
		$page_limit = 20;
		$page_start = ( $current_page - 1) * $page_limit;
		
		$this->db->cache_off();
		
		$this->load->model('Custom_model');
		$shares = new $this->Custom_model;
		
		$this->template_data->set('shares', $shares->get_my_shares( $this->session->userdata('user_id'), $page_start, $page_limit ) );
		
		$activity_stats = $this->template_data->get('activity_stats');
		$this->template_data->set('pages',  ceil( $activity_stats->total_shares / $page_limit ) );
		
		$this->load->view('my/shares', $this->template_data->get() );
	}
	
	
	public function points(  $account_id ) {
		
		$this->_is_points();
		
		$this->_check_id( $account_id, 'points' );
		
		$this->__init();
		
		$this->template_data->set('sub_page', 'points' );
		$this->template_data->page_title("My Points - In Davao City");
		
		$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
		$this->template_data->set('current_page', $current_page );
		
		$page_limit = 10;
		$page_start = ( $current_page - 1) * $page_limit;
				
		$this->load->model('Users_points_model');
		$points = new $this->Users_points_model;
		$points->setSelect("users_points.*");
		$points->setUserId( $this->session->userdata('user_id'), true );
		$points->setPointsClaimed( 1 , TRUE );
		$points->setOrder('date_added', 'DESC');
		$points->setStart($page_start);
		$points->setLimit($page_limit);
		
		$points->setSelect("tasks_verified.*");
		$points->setSelect("tasks_verified.object_type as tv_object_type");
		$points->setJoin('tasks_verified', 'tasks_verified.tasks_ver_id = users_points.object_id AND users_points.object_type = \'tasks\'', 'LEFT OUTER');
		
		$points->setSelect("users.*");
		$points->setJoin('users', 'users.user_id = users_points.object_id AND users_points.object_type = \'user\'', 'LEFT OUTER');
		
		$points->setSelect("realestate_data.*");
		$points->setJoin('realestate_data', 'realestate_data.re_id = users_points.object_id AND users_points.object_type = \'realestate\'', 'LEFT OUTER');
		
		$points->setSelect("directory_data.*");
		$points->setJoin('directory_data', 'directory_data.dir_id = users_points.object_id AND users_points.object_type = \'business\'', 'LEFT OUTER');
		
		$points->cache_on();
		$this->template_data->set('points', $points->populate() );
		
		$activity_stats = $this->template_data->get('activity_stats');
		$this->template_data->set('pages',  ceil( $activity_stats->total_points / $page_limit ) );
		
		$this->load->view('my/points', $this->template_data->get() );
	}
	
	public function redemptions(  $account_id ) {
		
		$this->_is_points();
		
		$this->_check_id( $account_id, 'redemptions' );
		
		$this->__init();
		
		$this->template_data->set('sub_page', 'redemptions' );
		$this->template_data->page_title("My Redemptions - In Davao City");

		$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
		$this->template_data->set('current_page', $current_page );
		
		$page_limit = 10;
		$page_start = ( $current_page - 1) * $page_limit;

		$this->load->model('Users_points_withdrawals_model');
		$withdrawals = new $this->Users_points_withdrawals_model;
		$withdrawals->setUserId( $this->session->userdata('user_id'), true );
		$withdrawals->setOrder('date_withdrawn', 'DESC');
		$withdrawals->setStart($page_start);
		$withdrawals->setLimit($page_limit);
		
		$withdrawals->cache_on();
		$this->template_data->set('redemptions', $withdrawals->populate() );
		
		$activity_stats = $this->template_data->get('activity_stats');
		$this->template_data->set('pages',  ceil( $activity_stats->total_withdrawals / $page_limit ) );
		
		$this->load->view('my/redemptions', $this->template_data->get() );
	}
	
	public function business( $account_id, $action=NULL, $slug=NULL ) {
		$this->_check_id( $account_id, 'business' );

		$this->__init();
		
		$this->template_data->set('sub_page', 'business' );
		if( $this->input->get('id') !== FALSE ) {
			
			$this->load->model('Directory_data_model');
			$directory_current = new $this->Directory_data_model;
			$directory_current->setDirId($this->input->get('id'), TRUE);
			$directory_current->setUserId( $this->session->userdata('user_id'), TRUE );
			
			if( $directory_current->nonEmpty() === FALSE ) {
				redirect( implode("/", array('my', md5(sha1($this->session->userdata('user_id'))), 'business')) );
				exit;
			}

			$directory_current->setSelect("directory_data.*");
			// abstract
			$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'abstract' LIMIT 1) as abstract");
			// address
			$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'address' LIMIT 1) as address");
			// phone
			$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'phone' LIMIT 1) as phone");
			// fax
			$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'fax' LIMIT 1) as fax");
			// website
			$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'website' LIMIT 1) as website");
			// email
			$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'email' LIMIT 1) as email");
			// map_lat
			$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'map_lat' LIMIT 1) as map_lat");
			// map_long
			$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'map_long' LIMIT 1) as map_long");
			// logo
			$directory_current->setSelect("(SELECT mu.file_name FROM directory_media dir_med LEFT JOIN media_uploads mu ON mu.media_id = dir_med.media_id WHERE dir_med.dir_id = directory_data.dir_id AND dir_med.dir_med_active = 1 AND dir_med.media_name = 'logo' AND dir_med.media_type = 'image' ORDER BY dir_med.dir_med_order DESC LIMIT 1) as logo");
			
			$dir_results = $directory_current->get();
			
			$this->template_data->page_title($dir_results->dir_name ." - In Davao City");
			
			$this->template_data->set('current_directory',  $dir_results );
		
			$view_file = 'my/business/update_business';
			
		} else {
		
			
			$this->template_data->page_title("My Business - In Davao City");

			$view_file = 'my/business/business';
		
		}
		/*
		$this->load->model(array(
			'Directory_data_model',
			'Directory_details_model',
			'Directory_category_model',
			'Directory_location_model',
			'Directory_media_model',
			'Directory_tags_model',
			'Users_meta_model'
		));

		if( $this->input->post('action') == 'delete' ) {
			$dir_delete = new $this->Directory_data_model;
			$dir_delete->setUserId( $this->session->userdata('user_id'), TRUE );
			$dir_delete->setDirId( $this->input->post('id'), TRUE );
			$dir_delete->setDirStatus('draft', TRUE);
			echo json_encode( array('error' => $dir_delete->delete(), 'id' =>  $this->input->post('id') ));
			exit;
		}

		switch( $action ) {
			case 'add':
				$view_file = 'my/business/add_business';
				
				$this->load->library('form_validation');
				switch( $this->input->post('action') ) {
					case 'check':
						$this->form_validation->set_rules('business_name', 'Business Name', 'required');
						if ($this->form_validation->run() !== FALSE)
						{
							$words = explode(' ',trim( $this->input->post('business_name') ) );
							$directory_match = new $this->Directory_data_model;
							$directory_match->setLike('dir_name',  $words[0] );
							$directory_match->setLikeOr('dir_slug', url_title($this->input->post('business_name')) );
							$directory_match->setOrder('dir_added', 'ASC');
							$directory_match->setLimit(0);
														
							$this->template_data->set('business_matches', $directory_match->populate() );
						}
					break;
					case 'add':
						$this->form_validation->set_rules('add_business_name', 'Business Name', 'required');
						$this->form_validation->set_rules('add_business_slug', 'Business Slug', 'required');
						if ($this->form_validation->run() !== FALSE)
						{
							
							$directory_match = new $this->Directory_data_model;
							$new_id = random_string('nozero', 10);
							$directory_match->setDirId( $new_id, FALSE, TRUE );
							$directory_match->setDirName( $this->input->post('add_business_name'), FALSE, TRUE );
							$directory_match->setDirSlug( $this->input->post('add_business_slug'), FALSE, TRUE );
							$directory_match->setDirBranch( $this->input->post('add_business_branch'), FALSE, TRUE );
							$directory_match->setUserId( $this->session->userdata('user_id'), FALSE, TRUE );
							$directory_match->setDirStatus('draft', FALSE, TRUE);
							$directory_match->setDirActive(1, FALSE, TRUE);
							
								if( $directory_match->insert() ) {
									redirect( implode("/", array( 'my', $this->session->userdata('user_id'), "business", "update", $new_id) ) );
									exit;
								}
							
						}
					break;
				}
			break;
			case 'update':
				
				if( $slug == NULL ) {
					redirect( implode("/", array('my', $this->session->userdata('user_id'), 'business') ) );
					exit;
				}
				
				$directory_current = new $this->Directory_data_model;
				$directory_current->setDirId($slug, TRUE);
				$directory_current->setUserId( $this->session->userdata('user_id'), TRUE );
				//$directory_current->setDirStatus('draft', TRUE );
				
				if( $directory_current->nonEmpty() !== TRUE ) {
					redirect( implode("/", array('my', $this->session->userdata('user_id'), 'business')) );
					exit;
				}
				
				$dir_results = $directory_current->getResults();
				
				if( $this->input->post('action') !== FALSE ) {
					$return = array(
						'error' => false
					);
					switch( $this->input->post('action') ) {
						case 'update_details':
							
							$return['msg'] = 'Business Details Updated!';
							$return['postData'] = $this->input->post();
							
							if( $this->input->post('business_name') != FALSE ) {
								$directory_current->setDirName($this->input->post('business_name'), FALSE, TRUE);
								$directory_current->update();
							}
							
							function update_detail($self, $id, $dKey, $value) {
								$directory_details = new $self->Directory_details_model;
								$directory_details->setDirId( $id, TRUE, TRUE );
								$directory_details->setDirDKey($dKey, TRUE, TRUE );
								$directory_details->setDirDActive(1, TRUE, TRUE);
								$directory_details->setDirDValue( trim( $value ), FALSE, TRUE );
								if( $directory_details->nonEmpty() === TRUE ) {
									$directory_details->limitDataFields('dir_d_value');
									return $directory_details->update();
								} else {
									return $directory_details->insert();
								}
							}
							
							function delete_detail($self, $id, $dKey) {
								$directory_details = new $self->Directory_details_model;
								$directory_details->setDirId( $id, TRUE );
								$directory_details->setDirDKey($dKey, TRUE );
								$directory_details->delete();
							}
							
							
							if( $this->input->post('business_address') != FALSE ) {
								if( ! update_detail( $this, $dir_results->dir_id, 'address', $this->input->post('business_address') ) ) {
									$return['error'] = true;
									$return['msg'] = 'Error Updating Address!';
								}
							} else {
								delete_detail( $this, $dir_results->dir_id, 'address' );
							}
							
							if( $this->input->post('business_phone') != FALSE ) {
								if( ! update_detail( $this, $dir_results->dir_id, 'phone', $this->input->post('business_phone') ) ) {
									$return['error'] = true;
									$return['msg'] = 'Error Updating Phone!';
								}
							} else {
								delete_detail( $this, $dir_results->dir_id, 'phone' );
							}
							
						break;
						case 'update_more_details':
							
							$return['msg'] = 'Business Details Updated!';
							$return['postData'] = $this->input->post();
							
							function update_detail($self, $id, $dKey, $value) {
								$directory_details = new $self->Directory_details_model;
								$directory_details->setDirId( $id, TRUE, TRUE );
								$directory_details->setDirDKey($dKey, TRUE, TRUE );
								$directory_details->setDirDActive(1, TRUE, TRUE);
								$directory_details->setDirDValue( trim($value), FALSE, TRUE );
								if( $directory_details->nonEmpty() === TRUE ) {
									$directory_details->limitDataFields('dir_d_value');
									return $directory_details->update();
								} else {
									return $directory_details->insert();
								}
							}
							
							function delete_detail($self, $id, $dKey) {
								$directory_details = new $self->Directory_details_model;
								$directory_details->setDirId( $id, TRUE );
								$directory_details->setDirDKey($dKey, TRUE );
								$directory_details->delete();
							}
							
							if( $this->input->post('business_description') != FALSE ) {
								if( ! update_detail( $this, $dir_results->dir_id, 'abstract', $this->input->post('business_description') ) ) {
									$return['error'] = true;
									$return['msg'] = 'Error Updating Description!';
								}
							} else {
								delete_detail( $this, $dir_results->dir_id, 'abstract' );
							}
							
							if( $this->input->post('business_website') != FALSE ) {
								if (! update_detail( $this, $dir_results->dir_id, 'website', $this->input->post('business_website') ) ) {
									$return['error'] = true;
									$return['msg'] = 'Error Updating Website!';
								}
							} else {
								delete_detail( $this, $dir_results->dir_id, 'website' );
							}
							
							if( $this->input->post('business_email') != FALSE ) {
								if (! update_detail( $this, $dir_results->dir_id, 'email', $this->input->post('business_email') ) ) {
									$return['error'] = true;
									$return['msg'] = 'Error Updating Website!';
								}
							} else {
								delete_detail( $this, $dir_results->dir_id, 'email' );
							}
							
							if( $this->input->post('business_map_lat') != FALSE ) {
								if (! update_detail( $this, $dir_results->dir_id, 'map_lat', $this->input->post('business_map_lat') ) ) {
									$return['error'] = true;
									$return['msg'] = 'Error Updating Map Latitude!';
								}
							} else {
								delete_detail( $this, $dir_results->dir_id, 'map_lat' );
							}
							
							if( $this->input->post('business_map_long') != FALSE ) {
								if (! update_detail( $this, $dir_results->dir_id, 'map_long', $this->input->post('business_map_long') ) ) {
									$return['error'] = true;
									$return['msg'] = 'Error Updating Map Longitude!';
								}
							} else {
								delete_detail( $this, $dir_results->dir_id, 'map_long' );
							}
							
							
						break;
						case 'update_categories':
							
							$return['msg'] = 'Business Categories Updated!';
							
							$old_categories = ( $this->input->post('old_categories') !== FALSE ) ? $this->input->post('old_categories') : array();
							
							if( !is_array( $old_categories ) ) {
								$old_categories = explode(",", $old_categories);
							}
							
							$new_categories = ( $this->input->post('new_categories') !== FALSE ) ? $this->input->post('new_categories') : array();
													
							// add new categories
							$error = false;
							if( count( $new_categories ) > 0 ) {
								foreach( $new_categories as $new_cat ) {
									if( ! in_array($new_cat, $old_categories ) ) {
										$dir_cats = new $this->Directory_category_model;
										$dir_cats->setDirId($dir_results->dir_id, TRUE, TRUE);
										$dir_cats->setTaxId( $new_cat, FALSE, TRUE );
										$dir_cats->setDirCatActive( 1, FALSE, TRUE );
										
										if( ! $dir_cats->insert() ) {
											$error = true;
										}
									}
								}
							}

							// remove unchecked categories
							if( count( $old_categories ) > 0 ) {
								foreach( $old_categories as $old_cat ) {
									if( ! in_array($old_cat, $new_categories ) ) {
										
										$dir_cats2 = new $this->Directory_category_model;
										$dir_cats2->setDirId($dir_results->dir_id, TRUE);
										$dir_cats2->setTaxId( $old_cat, TRUE );
										
										if( ! $dir_cats2->delete() ) {
											$error = true;
										}
									}
								}
							}
							
							if( $error ) {
								$return['error'] = true;
								$return['msg'] = 'Error updating business categories!';
								$return['new_categories'] = $this->input->post('old_categories'); 
							}
							
						break;
						case 'update_locations':
							
							$return['msg'] = 'Business Location Updated!';
							
							$old_locations = ( $this->input->post('old_locations') !== FALSE ) ? $this->input->post('old_locations') : array();
							
							if( !is_array( $old_locations ) ) {
								$old_locations = explode(",", $old_locations);
							}
							
							$new_locations = ( $this->input->post('new_locations') !== FALSE ) ? $this->input->post('new_locations') : array();
							
							
							// add new location
							$error = false;
							if( count( $new_locations ) > 0 ) {
								foreach( $new_locations as $new_loc ) {
									if( ! in_array($new_loc, $old_locations ) ) {
										$dir_locs = new $this->Directory_location_model;
										$dir_locs->setDirId($dir_results->dir_id, TRUE, TRUE);
										$dir_locs->setLocId( $new_loc, FALSE, TRUE );
										$dir_locs->setDirLocActive( 1, FALSE, TRUE );
										if( ! $dir_locs->insert() ) {
											$error = true;
										}
									}
								}
							}

							if( count( $old_locations ) > 0 ) {
								foreach( $old_locations as $old_loc ) {
									if( ! in_array($old_loc, $new_locations ) ) {
										$dir_locs2 = new $this->Directory_location_model;
										$dir_locs2->setDirId($dir_results->dir_id, TRUE);
										$dir_locs2->setLocId( $old_loc, TRUE );
										if( ! $dir_locs2->delete() ) {
											$error = true;
										}
									}
								}
							}
								
							if( $error ) {
								$return['error'] = true;
								$return['msg'] = 'Error updating business location!';
							}
							

						break;
						case 'update_logo':
							
							$config['upload_path'] = '../indavao-uploads/';
							$config['allowed_types'] = 'jpg|jpeg|gif|png';
							$config['max_size']	= '100000';
							$config['max_width']  = '1024';
							$config['max_height']  = '768';

							$this->load->library('upload', $config);

							if ( ! $this->upload->do_upload())
							{
								$return['error'] = true;
								$return['message'] = $this->upload->display_errors();
								
							}
							else
							{
								$return['error'] = false;
								$return['message'] = "Success!";
								$upload_data = $this->upload->data();
								$return['upload_data'] = $upload_data;
								
								$this->load->model('Media_uploads_model');
								$container = new $this->Media_uploads_model;
								$container->setFileName( $upload_data['file_name'], FALSE, TRUE );
								$container->setFileType( $upload_data['file_type'], FALSE, TRUE );
								$container->setFilePath( $upload_data['file_path'], FALSE, TRUE );
								$container->setFullPath( $upload_data['full_path'], FALSE, TRUE );
								$container->setRawName( $upload_data['raw_name'], FALSE, TRUE );
								$container->setOrigName( $upload_data['orig_name'], FALSE, TRUE );
								$container->setClientName( $upload_data['client_name'], FALSE, TRUE );
								$container->setFileExt( $upload_data['file_ext'], FALSE, TRUE );
								$container->setFileSize( $upload_data['file_size'], FALSE, TRUE );
								$container->setIsImage( $upload_data['is_image'], FALSE, TRUE );
								$container->setImageWidth( $upload_data['image_width'], FALSE, TRUE );
								$container->setImageHeight( $upload_data['image_height'], FALSE, TRUE );
								$container->setImageType( $upload_data['image_type'], FALSE, TRUE );
								$container->setImageSizeStr( $upload_data['image_size_str'], FALSE, TRUE );

								if( $container->insert() ) {
									$return['id'] = $container->getMediaId();
									$upload_results = $container->getByMediaId();
									$return['results'] = $upload_results;
									
									$media = new $this->Directory_media_model;
									$media->setDirId( $dir_results->dir_id, TRUE, TRUE );
									$media->setMediaId( $upload_results->media_id, FALSE, TRUE );
									$media->setMediaThumb( $upload_results->media_id, FALSE, TRUE );
									$media->setMediaName( 'logo', TRUE, TRUE );
									$media->setDirMedGroup( 'general', TRUE, TRUE );
									$media->setMediaType( 'image', TRUE, TRUE );
									$media->setDirMedActive( 1, FALSE, TRUE );
									$media->setDirMedOrder( 0, FALSE, TRUE );
									$media->delete();
									$media->insert();
								}
							}
							
						break;
						case 'update_tags':
							if( $this->input->post('business_tag') != FALSE ) {
								$this->load->model(array('Tags_model'));
								$tag = new $this->Tags_model;
								$tag->setTagName( trim( strtolower( $this->input->post('business_tag') ) ), true, true );
								$tag->setTagSlug( url_title( $this->input->post('business_tag'), '-', true), false, true );
								if( $tag->nonEmpty() == FALSE ) {
									$tag->insert();
									$tagResult = $tag->get();
								} else {
									$tagResult = $tag->getResults();
								}
								
								$return['tag'] = $tagResult;
								
								$dir_tag = new $this->Directory_tags_model;
								$dir_tag->setDirId($dir_results->dir_id, TRUE, TRUE);
								$dir_tag->setTagId( $tagResult->tag_id, true, true );
								$dir_tag->setDirTagActive( 1, false, true );
								
								if( $dir_tag->nonEmpty() == FALSE ) {
									if( $dir_tag->insert() ) {
										$return['error'] = false;
										$return['dir_tag'] =  $dir_tag->get();
									}
								} else {
									$return['dir_tag'] =  $dir_tag->getResults();
								}
							}
						break;
						case 'delete_tags':
							if( $this->input->post('dir_tag_id') != FALSE ) {
								$dir_tag = new $this->Directory_tags_model;
								$dir_tag->setDirTagId($this->input->post('dir_tag_id'), true);
								if( $dir_tag->delete() ) {
									$return['error'] = false;
									$return['dir_tag_id'] = $this->input->post('dir_tag_id');
								}
							}
						break;
						case 'request_approval':
							$directory_current->setDirStatus('pending', FALSE, TRUE );
							if( $directory_current->update() ) {
								redirect( implode("/", array('my', $this->session->userdata('user_id'), "business") ) );
								exit;
							}
						break;
					}
					
					if( $this->input->post('reload') != FALSE ) {
						redirect(uri_string());
						exit;
					}
							
					echo json_encode( $return );
					exit;
				}
				
				$directory_current->setSelect("directory_data.*");
				// abstract
				$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'abstract' LIMIT 1) as abstract");
				// address
				$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'address' LIMIT 1) as address");
				// phone
				$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'phone' LIMIT 1) as phone");
				// website
				$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'website' LIMIT 1) as website");
				// email
				$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'email' LIMIT 1) as email");
				// map_lat
				$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'map_lat' LIMIT 1) as map_lat");
				// map_long
				$directory_current->setSelect("(SELECT dd.dir_d_value FROM directory_details dd WHERE dd.dir_id = directory_data.dir_id AND dd.dir_d_key = 'map_long' LIMIT 1) as map_long");
				// logo
				$directory_current->setSelect("(SELECT mu.file_name FROM directory_media dir_med LEFT JOIN media_uploads mu ON mu.media_id = dir_med.media_id WHERE dir_med.dir_id = directory_data.dir_id AND dir_med.dir_med_active = 1 AND dir_med.media_name = 'logo' AND dir_med.media_type = 'image' ORDER BY dir_med.dir_med_order DESC LIMIT 1) as logo");
				
				$dir_categories = new $this->Directory_category_model;
				$dir_categories->setDirId($dir_results->dir_id, TRUE);
				$dir_categories->setDirCatActive(1, TRUE);
				$dir_categories->setLimit(0);
				$this->template_data->set('old_categories', $dir_categories->populate() );
				
				$dir_locations = new $this->Directory_location_model;
				$dir_locations->setDirId($dir_results->dir_id, TRUE);
				$dir_locations->setDirLocActive(1, TRUE);
				$dir_locations->setLimit(0);
				$this->template_data->set('old_locations', $dir_locations->populate() );
				
				$dir_tags = new $this->Directory_tags_model;
				$dir_tags->setDirId($dir_results->dir_id, TRUE);
				$dir_tags->setJoin('tags', 'tags.tag_id = directory_tags.tag_id');
				$dir_tags->setLimit(0);
				$this->template_data->set('tags', $dir_tags->populate() );
				
				$view_file = 'my/business/update_business';
				
				$current_dir = $directory_current->get();
				$this->template_data->set('current_directory', $current_dir );
				
			break;
			default:
				$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
				$this->template_data->set('current_page', $current_page );
		
				$page_limit = 15;
				$page_start = ( $current_page - 1) * $page_limit;
				
				$my_directory = new $this->Directory_data_model;
				$my_directory->setUserId( $this->session->userdata('user_id'), TRUE );
				$my_directory->setDirActive( 1, TRUE );
				$my_directory->setOrder('dir_added', 'DESC');
				$my_directory->setLimit($page_limit);
				$my_directory->setStart($page_start);
				
				$my_directory->setJoin('users_points', 'users_points.object_id = directory_data.dir_id', 'LEFT OUTER');
				
				$pagination = new $this->Directory_data_model;
				$pagination->setUserId( $this->session->userdata('user_id'), TRUE );
				$pagination->setDirActive( 1, TRUE );
				$pagination->setSelect("COUNT(*) as total_items");
				
				if( $this->input->get('search') != FALSE ) {
					$my_directory->setLike('dir_name', $this->input->get('search'));
					$pagination->setLike('dir_name', $this->input->get('search'));
				}
				
				$this->template_data->set('my_directories', $my_directory->populate() );
				$this->template_data->set('pages',  ceil( $pagination->get()->total_items / $page_limit ) );
				
				$last_claim = new $this->Users_meta_model;
				$last_claim->setUserId( $this->session->userdata('user_id'), TRUE );
				$last_claim->setMetaKey( 'last_claim_business_points', TRUE );
				
				$this->template_data->set('last_claim',  $last_claim->get() );
				
			break;
		}
		*/
		$this->template_data->set('sidebar_nav',  'business' );
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function realestate( $account_id, $action=NULL, $slug=NULL ) {
		$this->_check_id( $account_id, 'properties' );

		$this->__init();
		
		$this->template_data->set('sub_page', 'properties' );
		$this->template_data->set('current_tab', $this->input->get('tab') );
		$this->template_data->page_title("My Properties - In Davao City");
		
		$view_file = 'my/realestate/realestate';
		
		/*
		$this->load->model(array(
				'Realestate_data_model', 
				'Realestate_details_model', 
				'Realestate_ancestors_model', 
				'Realestate_meta_model', 
				'Realestate_types_model'
			));
		
		switch( $action ) {
			case 'add':
				$view_file = 'my/realestate/add_property';
				
				$this->load->library('form_validation');
				switch( $this->input->post('action') ) {
					case 'check':
						$this->form_validation->set_rules('property_title', 'Property Title', 'required');
						if ($this->form_validation->run() !== FALSE)
						{
							
							$property_match = new $this->Realestate_data_model;
							$property_match->setLike('re_title',  $this->input->post('property_title') );
							$property_match->setLikeOr('re_slug', url_title($this->input->post('property_title')) );
							$property_match->setOrder('re_title', 'ASC');
							
							$this->template_data->set('properties_matches', $property_match->populate() );
						}
					break;
					case 'add':
						$this->form_validation->set_rules('add_property_title', 'Property Title', 'required');
						$this->form_validation->set_rules('add_property_slug', 'Property Slug', 'required');
						if ($this->form_validation->run() !== FALSE)
						{
							
							$new_property = new $this->Realestate_data_model;
							$new_id = random_string('nozero', 10);
							$new_property->setReId( $new_id, TRUE, TRUE );
							$new_property->setReTitle( $this->input->post('add_property_title'), FALSE, TRUE );
							$new_property->setReSlug( $this->input->post('add_property_slug'), FALSE, TRUE );
							$new_property->setUserId( $this->session->userdata('user_id'), FALSE, TRUE );
							$new_property->setReStatus('draft', FALSE, TRUE);
							$new_property->setReActive(1, FALSE, TRUE);
							
								if( $new_property->replace() ) {
									redirect( implode("/", array( 'my', $this->session->userdata('user_id'), "properties", "update", $new_id ) ) );
									exit;
								}
							
						}
					break;
				}
				
			break;
			case 'update':
				$view_file = 'my/realestate/update_property';
				
				function update_detail($self, $id, $dKey, $value, $label=NULL) {
					$detail = new $self->Realestate_details_model;
					$detail->setReId( $id, TRUE, TRUE );
					$detail->setReDKey($dKey, TRUE, TRUE );
					$detail->setReDActive(1, TRUE, TRUE);
					$detail->setReDValue( trim($value), FALSE, TRUE );
					$detail->setReDLabel( $label, FALSE, TRUE );
					if( $detail->nonEmpty() === TRUE ) {
						$detail->limitDataFields('re_d_value');
						return $detail->update();
					} else {
						return $detail->insert();
					}
				}
				
				function delete_detail($self, $id, $dKey) {
					$detail = new $self->Realestate_details_model;
					$detail->setReId( $id, TRUE );
					$detail->setReDKey($dKey, TRUE );
					$detail->delete();
				}
				
				function update_meta($self, $id, $key, $value) {
					$meta = new $self->Realestate_meta_model;
					$meta->setReId( $id, TRUE, TRUE );
					$meta->setMetaKey($key, TRUE, TRUE );
					$meta->setReMActive(1, TRUE, TRUE);
					$meta->setMetaValue( $value, FALSE, TRUE );
					if( $meta->nonEmpty() === TRUE ) {
						$meta->limitDataFields('meta_value');
						return $meta->update();
					} else {
						return $meta->insert();
					}
				}
				
				function delete_meta($self, $id, $key) {
					$meta = new $self->Realestate_meta_model;
					$meta->setReId( $id, TRUE );
					$meta->setMetaKey($key, TRUE );
					$meta->delete();
				}
				
				function update_type($self, $id, $type_name, $active) {
					$type = new $self->Realestate_types_model;
					$type->setReId( $id, TRUE, TRUE );
					$type->setTypeName( $type_name, TRUE, TRUE );
					$type->setReTypeActive( $active, FALSE, TRUE );
					if( $type->nonEmpty() === TRUE ) {
						$type->limitDataFields('re_type_active');
						return $type->update();
					} else {
						return $type->insert();
					}
				}
				
				switch( $this->input->post('action') ) {
					case 'update_property':
						
						if( $this->input->post('property_title') != FALSE ) {
							$property_title = new $this->Realestate_data_model;
							$property_title->setReId( $slug, TRUE );
							$property_title->setReTitle( $this->input->post('property_title'), FALSE, TRUE );
							$property_title->update();
						}
						
						if( $this->input->post('price') != FALSE ) {
							update_detail( $this, $slug, 'price', $this->input->post('price') );
						} else {
							delete_detail( $this, $slug, 'price' );
						}
						
						if( $this->input->post('abstract') != FALSE ) {
							update_detail( $this, $slug, 'abstract', $this->input->post('abstract') );
						} else {
							delete_detail( $this, $slug, 'abstract' );
						}
						
						if( $this->input->post('description') != FALSE ) {
							update_detail( $this, $slug, 'description', $this->input->post('description') );
						} else {
							delete_detail( $this, $slug, 'description' );
						}
						
						if( $this->input->post('lot_area') != FALSE ) {
							update_detail( $this, $slug, 'lot_area', $this->input->post('lot_area') );
						} else {
							delete_detail( $this, $slug, 'lot_area' );
						}
						
					break;
					case 'update_house':
						
						if( $this->input->post('beds') != FALSE ) {
							update_detail( $this, $slug, 'beds', $this->input->post('beds') );
						} else {
							delete_detail( $this, $slug, 'beds' );
						}
						
						if( $this->input->post('baths') != FALSE ) {
							update_detail( $this, $slug, 'baths', $this->input->post('baths') );
						} else {
							delete_detail( $this, $slug, 'baths' );
						}
						
						if( $this->input->post('floor_area') != FALSE ) {
							update_detail( $this, $slug, 'floor_area', $this->input->post('floor_area') );
						} else {
							delete_detail( $this, $slug, 'floor_area' );
						}
						
					break;
					case 'update_location':
					
						if( $this->input->post('address') != FALSE ) {
							update_detail( $this, $slug, 'address', $this->input->post('address') );
						} else {
							delete_detail( $this, $slug, 'address' );
						}
						
						if( $this->input->post('address_lot') != FALSE ) {
							update_detail( $this, $slug, 'address_lot', $this->input->post('address_lot') );
						} else {
							delete_detail( $this, $slug, 'address_lot' );
						}
						
						if( $this->input->post('address_block') != FALSE ) {
							update_detail( $this, $slug, 'address_block', $this->input->post('address_block') );
						} else {
							delete_detail( $this, $slug, 'address_block' );
						}
						
						if( $this->input->post('map_lat') != FALSE ) {
							update_detail( $this, $slug, 'map_lat', $this->input->post('map_lat') );
						} else {
							delete_detail( $this, $slug, 'map_lat' );
						}
						
						if( $this->input->post('map_lng') != FALSE ) {
							update_detail( $this, $slug, 'map_lng', $this->input->post('map_lng') );
						} else {
							delete_detail( $this, $slug, 'map_lng' );
						}
						
					break;
					case 'update_options':
					
						if( $this->input->post('listing_type') != FALSE ) {
							update_detail( $this, $slug, 'listing_type', $this->input->post('listing_type') );
						} else {
							delete_detail( $this, $slug, 'listing_type' );
						}
						
						if( $this->input->post('property_type') != FALSE ) {
							$property_type = new $this->Realestate_data_model;
							$property_type->setReId( $slug, TRUE );
							$property_type->setReType( $this->input->post('property_type'), FALSE, TRUE );
							$property_type->update();
						}
						
						if( $this->input->post('property_status') != FALSE ) {
							$property_status = new $this->Realestate_data_model;
							$property_status->setReId( $slug, TRUE );
							$property_status->setReStatus( $this->input->post('property_status'), FALSE, TRUE );
							$property_status->update();
						}
						
						if( $this->input->post('property_parent') != FALSE ) {
							$property_parent = new $this->Realestate_data_model;
							$property_parent->setReId( $slug, TRUE );
							$property_parent->setReParent( $this->input->post('property_parent'), FALSE, TRUE );
							$property_parent->update();
							
							$property_ancestor = new $this->Realestate_ancestors_model();
							$property_ancestor->setReId($slug, TRUE, TRUE);
							$property_ancestor->setAncestorId($this->input->post('property_parent'), TRUE, TRUE);
							$property_ancestor->setReAncActive( 1, FALSE, TRUE );
							if( $property_ancestor->nonEmpty() == FALSE ) {
								$property_ancestor->insert();
							} else {
								$property_ancestor->limitDataFields('re_anc_active');
								$property_ancestor->update();
							}
							
						} else {
							$property_parent = new $this->Realestate_data_model;
							$property_parent->setReId( $slug, TRUE );
							$property_parent->setReParent( NULL, FALSE, TRUE );
							$property_parent->update();
							
							$property_ancestor = new $this->Realestate_ancestors_model();
							$property_ancestor->setReId($slug, TRUE, FALSE);
							$property_ancestor->setAncestorId($this->input->post('property_parent'), TRUE, FALSE);
							$property_ancestor->setReAncActive( 0, FALSE, TRUE );
							$property_ancestor->update();
						}
						
					break;
					
					case 'update_types':
						if( $this->session->userdata('manager') ) {
							
							if( $this->input->post('property_type') != FALSE ) {
								foreach( $this->input->post('property_type') as $key=>$value) {
									if( $key != '' ) {
										update_type($this, $slug, $key, $value);
									}
								}
							} 
							
						}
					break;
					
					case 'update_tasks':
						if( $this->session->userdata('manager') ) {
							
							if( $this->input->post('add_to_tasks') != FALSE ) {
								update_meta( $this, $slug, 'is_task', 1 );
							} else {
								delete_meta( $this, $slug, 'is_task' );
							}
							
							if( $this->input->post('task_message') != FALSE ) {
								update_meta( $this, $slug, 'task_message', $this->input->post('task_message') );
							} else {
								delete_meta( $this, $slug, 'task_message' );
							}
						}
					break;
				}
				
				$current_property = new $this->Realestate_data_model;
				$current_property->setReId( $slug, TRUE );
				
				$current_property->setSelect("realestate_data.*");
				
				// price
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'price' LIMIT 1) as price");
				
				// abstract
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'abstract' LIMIT 1) as abstract");
				
				// description
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'description' LIMIT 1) as description");
				
				// lot_area
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'lot_area' LIMIT 1) as lot_area");
				
				// house 
				// beds
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'beds' LIMIT 1) as beds");
				
				// baths
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'baths' LIMIT 1) as baths");
				
				// floor_area
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'floor_area' LIMIT 1) as floor_area");
				
				// location
				// address
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'address' LIMIT 1) as address");
				
				// address_lot
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'address_lot' LIMIT 1) as address_lot");
				
				// address_block
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'address_block' LIMIT 1) as address_block");
				
				// map_lat
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'map_lat' LIMIT 1) as map_lat");
				
				// map_lng
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'map_lng' LIMIT 1) as map_lng");
				
				// listing_type
				$current_property->setSelect("(SELECT re_d_value FROM realestate_details rd WHERE rd.re_id = realestate_data.re_id AND rd.re_d_key = 'listing_type' LIMIT 1) as listing_type");
				
				if( $this->session->userdata('manager') ) {
					$current_property->setSelect("(SELECT meta_value FROM realestate_meta rm WHERE rm.re_id = realestate_data.re_id AND rm.meta_key = 'is_task' LIMIT 1) as is_task");
					
					$current_property->setSelect("(SELECT meta_value FROM realestate_meta rm WHERE rm.re_id = realestate_data.re_id AND rm.meta_key = 'task_message' LIMIT 1) as task_message");
				}
				
				$this->template_data->set('current_property', $current_property->get() );
				
				$projects = new $this->Realestate_data_model;
				$projects->setReType('PROJ', TRUE);
				$projects->setReStatus('publish', TRUE);
				$projects->setLimit(0);
				$projects->setOrder('re_title', 'ASC');
				$projects->cache_on();
				
				$this->template_data->set('projects', $projects->populate() );
				
				$types = new $this->Realestate_types_model;
				$types->setReId( $slug, TRUE );
				$types->setReTypeActive( 1, TRUE  );
				$property_types = array();
				foreach( $types->populate() as $type_item ) {
					$property_types[] = $type_item->type_name;
				}
				$this->template_data->set('property_types', $property_types );
				
			break;
			default:
				$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
				$this->template_data->set('current_page', $current_page );
		
				$page_limit = 15;
				$page_start = ( $current_page - 1) * $page_limit;
				
				$my_properties = new $this->Realestate_data_model;
				$my_properties->setUserId( $this->session->userdata('user_id'), TRUE );
				$my_properties->setReActive( 1, TRUE );
				$my_properties->setOrder('re_added', 'DESC');
				$my_properties->setLimit($page_limit);
				$my_properties->setStart($page_start);
				

				$pagination = new $this->Realestate_data_model;
				$pagination->setSelect("COUNT(*) as total_items");
				$pagination->setReActive( 1, TRUE );
				$pagination->setUserId( $this->session->userdata('user_id'), TRUE );
				
				if( $this->input->get('search') != FALSE ) {
					$my_properties->setLike( 're_title', $this->input->get('search') );
					$pagination->setLike( 're_title', $this->input->get('search') );
				}
				
				$this->template_data->set('my_properties', $my_properties->populate() );			
				$this->template_data->set('pages',  ceil( $pagination->get()->total_items / $page_limit ) );
				
			break;
		}
		*/
		$this->template_data->set('sidebar_nav',  'realestate' );
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function jobs( $account_id, $action=NULL ) {
		$this->_check_id( $account_id, 'jobs' );

		$this->__init();
		
		$this->template_data->set('sub_page', 'jobs' );
		$this->template_data->page_title("My Job Posts - In Davao City");
		
		$view_file = 'my/jobs/jobs';
		
		switch( $action ) {
			case 'add':
				$view_file = 'my/jobs/add_job';
			break;
			default:
				$current_page = ( $this->input->get('page') != '') ? $this->input->get('page') : 1;
				$this->template_data->set('current_page', $current_page );
		
				$page_limit = 10;
				$page_start = ( $current_page - 1) * $page_limit;
			break;
		}
		
		$this->load->view($view_file, $this->template_data->get() );
	}
	
	public function ajax( $account_id ) {
		
		$this->_check_id( $account_id, 'ajax' );
		
		$return = array(
			'error' => true
		);
				
		$this->load->library('form_validation');
		
		switch($this->input->post('action')) {
			case 'share':
				$this->form_validation->set_rules('object_id', 'object_id', 'required');
				$this->form_validation->set_rules('object_type', 'object_type', 'required');
				$this->form_validation->set_rules('post_id', 'post_id', 'required');
				
				if ($this->form_validation->run() !== FALSE)
				{
					$return['error'] = false;
					$return['action'] = 'share';
					
					$this->load->model('Users_shares_model');
					$share = new $this->Users_shares_model;
					$share->setUserId( $this->session->userdata('user_id'), true );
					$share->setObjectId( $this->input->post('object_id'), true );
					$share->setObjectType( $this->input->post('object_type'), true );
					$share->setFbPostId( $this->input->post('post_id'), true );
					$share->cache_on();
					
					if ($share->nonEmpty() === TRUE) { 
						$return['results'] = $share->getResults();
					} else {
						if( $share->replace() ) {
							$return['results'] = $share->getByShareId();
						}
					}
				}
			break;
			case 'likes':
				$this->form_validation->set_rules('share_id', 'share_id', 'required');
				$this->form_validation->set_rules('liked', 'liked', 'required');
				
				if ($this->form_validation->run() !== FALSE)
				{
					$this->load->model('Users_shares_model');
					$share = new $this->Users_shares_model;
					$share->setUserId( $this->session->userdata('user_id'), true );
					$share->setShareId( $this->input->post('share_id'), true );
					$share->setLikes( $this->input->post('liked'), false, true );
					$share->cache_on();
					$share->update();
					
					$return['error'] = false;
					$return['action'] = 'likes';
					$return['results'] = $share->getByShareId();
				}
				
			break;
			case 'task_verified':
			
				$return['action'] = 'task_verified';
					
				$this->form_validation->set_rules('task_id', 'task_id', 'required');
				$this->form_validation->set_rules('object_id', 'object_id', 'required');
				$this->form_validation->set_rules('object_type', 'object_type', 'required');
				$this->form_validation->set_rules('post_url', 'post_url', 'required');
				if ($this->form_validation->run() !== FALSE)
				{
					$return['error'] = false;
					$this->load->model('Tasks_verified_model');
					$verified = new $this->Tasks_verified_model;
					$verified->setUserId( $this->session->userdata('user_id'), TRUE );
					$verified->setTasksUrlId( $this->input->post('task_id'), TRUE );
					$verified->setObjectId( $this->input->post('object_id'), TRUE );
					$verified->setObjectType( $this->input->post('object_type'), TRUE );
					$verified->setPostUrl( $this->input->post('post_url') );
					$verified->setDateVerified( unix_to_human( time(), TRUE, 'eu' ) );
					$verified->cache_on();
					
					if ($verified->nonEmpty() === TRUE) { 
						$return['results'] = $verified->getResults();
					} else {
						if( $verified->replace() ) {
							$return['results'] = $verified->getByTasksVerId();
						}
					}
				}
			break;
			case 'claim_task_points':
			
				$return['action'] = 'claim_task_points';
				
				$this->form_validation->set_rules('task_id', 'task_id', 'required');
				$this->form_validation->set_rules('tasks_url_id', 'tasks_url_id', 'required');
				$this->form_validation->set_rules('object_id', 'object_id', 'required');
				$this->form_validation->set_rules('object_type', 'object_type', 'required');
				$this->form_validation->set_rules('post_id', 'post_id', 'required');
				
				if ($this->form_validation->run() !== FALSE)
				{
					
					
					$this->load->model('Tasks_verified_model');
					$verified = new $this->Tasks_verified_model;
					$verified->setUserId( $this->session->userdata('user_id'), TRUE );
					$verified->setTasksVerId( $this->input->post('task_id'), TRUE );
					$verified->setTasksUrlId( $this->input->post('tasks_url_id'), TRUE );
					$verified->setObjectId( $this->input->post('object_id'), TRUE );
					$verified->setObjectType( $this->input->post('object_type'), TRUE );
					$verified->setPostId( $this->input->post('post_id'), TRUE );
					$verified->setLimit(1);
					$verified->cache_on();
					
					if ($verified->nonEmpty() === TRUE) { 
						$results = $verified->getResults();

						$days_elapsed = floor((time() - strtotime( $results ->date_verified ))/(60*60*24));
						
						if( $days_elapsed > 0 ) {
							$return['error'] = false;
							$this->load->model('Users_points_model');
							$points = new $this->Users_points_model;
							$points->setUserId( $this->session->userdata('user_id'), TRUE );
							$points->setObjectId( $this->input->post('task_id'), TRUE );
							$points->setObjectType('tasks', TRUE);
							$points->setPointsType('SHARE', TRUE);
							$points->cache_on();
							
							if ($points->nonEmpty() === TRUE) { 
								$return['results'] = $points->getResults();
							} else {
								$points->setPointsCredited( get_settings_value('points_tasks', 1) );
								$points->setDateAdded( unix_to_human( time(), TRUE, 'eu' ) );
								if( $points->replace() ) {
									
									$return['results'] = $points->getByPointsId();
								}
							}
							
						}
					}
				}
			break;
			case 'tasks_revert':
			
				$return['action'] = 'tasks_revert';
					
				$this->form_validation->set_rules('task_id', 'task_id', 'required');

				if ($this->form_validation->run() !== FALSE)
				{
					$this->load->model('Tasks_verified_model');
					$verified = new $this->Tasks_verified_model;
					$verified->setUserId( $this->session->userdata('user_id'), TRUE );
					$verified->setTasksVerId( $this->input->post('task_id'), TRUE );
					
					if ($verified->nonEmpty() === TRUE) { 
						$return['error'] = false;
						$verified->delete();
					} 
				}
			break;
			case 'save_bookmark':
			
				$return['action'] = 'save_bookmark';
					
				$this->form_validation->set_rules('object_id', 'object_id', 'required');
				$this->form_validation->set_rules('object_type', 'object_type', 'required');

				if ($this->form_validation->run() !== FALSE)
				{
					$this->load->model('Users_bookmarks_model');
					$bookmark = new $this->Users_bookmarks_model;
					$bookmark->setUserId( $this->session->userdata('user_id'), TRUE );
					$bookmark->setObjectId( $this->input->post('object_id'), TRUE );
					$bookmark->setObjectType( $this->input->post('object_type'), TRUE );
					
					$return['error'] = false;
					$return['object_id'] = $this->input->post('object_id');
					if ($bookmark->nonEmpty() === FALSE) { 
						$bookmark->replace();
					}  
				}
			break;
			
			case 'remove_bookmark':
			
				$return['action'] = 'remove_bookmark';
					
				$this->form_validation->set_rules('bookmark_id', 'bookmark_id', 'required');

				if ($this->form_validation->run() !== FALSE)
				{
					$this->load->model('Users_bookmarks_model');
					$bookmark = new $this->Users_bookmarks_model;
					$bookmark->setBookmarkId($this->input->post('bookmark_id'), TRUE);
					
					$return['error'] = false;
					$return['bookmark_id'] = $this->input->post('bookmark_id');
					$bookmark->delete();
				}
			break;
			
			// claim_business_points
			case 'claim_business_points':
			
				$return['action'] = 'claim_business_points';
				
				$this->load->model('Users_meta_model');
				$last_claim = new $this->Users_meta_model;
				$last_claim->setUserId( $this->session->userdata('user_id'), TRUE );
				$last_claim->setMetaKey( 'last_claim_business_points', TRUE );
				$last_claimed = $last_claim->get();
				
				if(($last_claimed == FALSE) || 
					( ($last_claimed != FALSE) && $last_claimed->meta_value == '') || 
					(($last_claimed != FALSE) && (time() - $last_claimed->meta_value) >= (60*60*3)) ) {
					$this->load->model(array('Users_points_model', 'Directory_meta_model'));
					
					if( $this->input->post('fb_obj_id') != FALSE ) {
						$meta = new $this->Directory_meta_model;
						$meta->setDirId($this->input->post('obj_id'), TRUE);
						$meta->setMetaKey('fb_obj_id', FALSE, TRUE);
						$meta->setMetaValue(  $this->input->post('fb_obj_id') , FALSE, TRUE);
						$meta->setActive(1, FALSE, TRUE);
						
						if( $meta->nonEmpty() === FALSE ) {
							$meta->setDirId($this->input->post('obj_id'), FALSE, TRUE);
							$meta->insert();
						} else {
							$meta->update();
						}
					}
					
					if( $this->input->post('obj_id') != FALSE ) {
						$points = new $this->Users_points_model;
						$points->setObjectId( $this->input->post('obj_id'), TRUE );
						$points->setObjectType('business', TRUE);
						$points->setUserId( $this->session->userdata('user_id') , TRUE);
						$points->setPointsClaimed( 1, FALSE, TRUE );
						
						if( $points->update() ) {
							
							$claim = new $this->Users_meta_model;
							$claim->setUserId( $this->session->userdata('user_id'), TRUE, TRUE );
							$claim->setMetaKey( 'last_claim_business_points', TRUE, TRUE );
							$claim->setUserMetaActive( 1, FALSE, TRUE );
							$claim->setMetaValue(time(), FALSE, TRUE);
							
							if( $claim->nonEmpty() === FALSE ) {
								$claim->insert();
							} else {
								$claim->limitDataFields('meta_value');
								$claim->update();
							}
						}
						
						$return['error'] = false;
						$return['user_id'] = $this->session->userdata('user_id');
					}
				}
				
			break;
			
			case 'add_new_friends':
				$this->load->model(array('Users_fb_friends_model'));
				$friends = new $this->Users_fb_friends_model;
				
				if( $this->input->post('friend_ids') != FALSE ) {
					foreach( $this->input->post('friend_ids') as $friend_id ) {
						$friends->setUserId( $this->session->userdata('user_id') );
						$friends->setFriendId( $friend_id );
						$friends->updateBatch();
					}
				}
				
				$return['error'] = ( $friends->insert_batch() !== TRUE );
								
			break;
			
			// manage site
			case 'clear_cache':
				$return['action'] = 'clear_cache';
				$this->form_validation->set_rules('uri1', 'class_name', 'required');
				$this->form_validation->set_rules('uri2', 'method_name', 'required');
				if ($this->form_validation->run() !== FALSE)
				{
					$return['error'] = false;
					$this->db->cache_delete($this->input->post('uri1'), $this->input->post('uri2'));
				}
			break;
		}
		
		echo json_encode( $return );
		exit;
	}
	
	public function logout() {
		$this->session->sess_destroy();
		$this->load->library('facebook');
		header("Location: " . $this->facebook->logout_url());
		exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends MY_Controller {
	
	function __construct() 
    {
        parent::__construct();
        
    }
    	
	public function index()
	{
		$this->_notLoggedIn();
		redirect('login', 'location');
		exit;
	}
	
	public function referrer($referrer_id='')
	{
		if( $referrer_id != '' ) {
			$this->_addReferrer( $referrer_id );
		}
	
		redirect('login', 'location');
		exit;
	}
	
	public function login()
	{
		$this->__init();
		$this->load->library('facebook');
		if( ! $this->facebook->retrieveRedirectSession() ) {
			header("Location: " . $this->facebook->login_url());
			exit;
		} 
		
		$this->_notLoggedIn();
		
		$this->__init();
		
		$this->template_data->page_title("Login - In Davao City");
		
		$this->load->view('login', $this->template_data->get() );
	}
	
	
	public function facebook() {
		$this->load->library('facebook');
		
		if( ! $this->facebook->retrieveRedirectSession() ) {
			header("Location: " . $this->facebook->login_url());
			exit;
		} else {
			$userData = (object) $this->facebook->get_user();
			if( $userData  ) {
				$exists = $this->__initAccount(array(
					'id' => $userData->id,
					'email' => $userData->email,
					'last_name' => $userData->last_name,
					'first_name' => $userData->first_name,
					'gender' => $userData->gender,
					'name' => $userData->name,
					'link' => $userData->link,
				));
				
				if( $exists ) {
					redirect("my/". $userData->id ."/dashboard");
					exit;
				}
			}
		}
		
		$this->_notLoggedIn();
		exit;
		
		
	}
	
	protected function __initAccount( $params ) 
	{
		$required = array(
			'id' => '',
			'email' => '',
			'last_name' => '',
			'first_name' => '',
			'gender' => '',
			'name' => '',
			'link' => '',
		);
		
		$userData = (object) array_merge( $required, $params );
			
		if ( $userData->id !== '' )
		{
			
			$this->load->helper( array('url', 'date') );
			$this->load->model( array('Users_model') );
			$user = new $this->Users_model;
			$user->setUserId( $userData->id, TRUE );
			$user->setApi( 'fb', TRUE );
			$user->setLimit(1);
			
			if ($user->nonEmpty() !== FALSE) { 
				
				$this->db->cache_delete('my',  $userData->id );
				
				$user->setSelect('users.*');
				$user->setSelect('(SELECT meta_value FROM users_meta WHERE users_meta.user_id = users.user_id AND users_meta.meta_key = \'name\' LIMIT 1) as uname');
				
				$user->setSelect('(SELECT meta_value FROM users_meta ume WHERE ume.user_id = users.user_id AND ume.meta_key = \'email\' LIMIT 1) as email');
				
				$user->setSelect('(SELECT meta_value FROM users_meta ume WHERE ume.user_id = users.user_id AND ume.meta_key = \'phone\' LIMIT 1) as phone');
				
				$result = $user->get();
				
				$last_login = unix_to_human( time(), TRUE, 'eu' );
								
				$this->session->set_userdata( array(
					'user_id' => $result->user_id,
					'user_name' => $result->uname,
					'logged_in' => true,
					'last_login' => $last_login,
					'date_joined' => $result->date_joined,
					'isPoints' => (( $result->add_points == 1 ) ? TRUE : FALSE),
					'manager' => (( $result->manager == 1 ) ? TRUE : FALSE),
					'permissions' => $this->__get_user_permissions( $result->user_id ),
					'meta_name' => $result->uname,
					'meta_email' => $result->email,
					'meta_phone' => $result->phone,
				));
				
				$user->setLastLogin( $last_login , false, true );
				if( $user->update() ) {
					return true;
					exit;
				}
				
			} else {
				
				$user->setApi( 'fb' );
				if( $userData->email !== FALSE ) {
					$user->setEmail( $userData->email );
				}
				$user->setFirstName( $userData->first_name );
				$user->setLastName( $userData->last_name );
				$user->setGender( $userData->gender );
				$user->setName( $userData->name );
				$user->setVerified(0);
				$user->setManager(0);
				$user->setLink( $userData->link );
				
				$today = unix_to_human( time(), TRUE, 'eu' );
				$user->setDateJoined( $today );
				$user->setLastLogin( $today );
				
				$ref_id = get_cookie( $this->config->item('cookie_prefix') . 'referrer');
				if( $userData->id != $ref_id ) {
					$user->setReferrer( $ref_id );
				}
				
				if( $user->insert() ) {
					
					$this->session->set_userdata( array(
						'user_id' => $userData->id,
						'user_name' => $userData->name,
						'logged_in' => TRUE,
						'last_login' => $today,
						'date_joined' => $today,
						'isPoints' => FALSE,
						'manager' => FALSE,
						'permissions' => $this->__get_user_permissions( $userData->id ),
						'meta_name' => $userData->name,
						'meta_email' => $userData->email,
						'meta_phone' => "",
					));
					
					$this->load->model( array('Users_meta_model') );
					
					$umeta_name = new $this->Users_meta_model;
					$umeta_name->setUserId( $userData->id );
					$umeta_name->setMetaKey('name');
					$umeta_name->setMetaValue( $userData->name );
					$umeta_name->setUserMetaActive( 1 );
					$umeta_name->replace();
					
					if( $userData->email !== FALSE ) {
						$umeta_email = new $this->Users_meta_model;
						$umeta_email->setUserId( $userData->id );
						$umeta_email->setMetaKey('email');
						$umeta_email->setMetaValue( $userData->email );
						$umeta_email->setUserMetaActive( 1 );
						$umeta_email->replace();
					}
					
					if( $ref_id != '') {
						$referrer = new $this->Users_model;
						$referrer->setUserId( $ref_id, TRUE);
						$refData = $referrer->getResults();
						
						if( ($referrer->nonEmpty() !== FALSE) && (isset($refData->add_points) && $refData->add_points == 1) ) {
							$this->load->model( array('Users_points_model') );
							$referral_points = new $this->Users_points_model;
							$referral_points->setUserId( $ref_id, TRUE );
							$referral_points->setObjectId( $userData->id, TRUE );
							$referral_points->setObjectType( 'user', TRUE );
							$referral_points->setPointsType('REFERRAL', TRUE);
							$referral_points->setPointsCredited( get_settings_value('points_referral', 1) );
							$referral_points->setDateAdded( $today );
							
							if( $referral_points->nonEmpty() === FALSE ) {
								$referral_points->replace();
							}
						}
					}
					return true;
					exit;
				}
			} 
			
			return false;
			exit;
		}
		
	}
	
	public function logout() {
		$this->session->sess_destroy();
		$this->load->library('facebook');
		header("Location: " . base_url());
		exit;
	}
	
	private function __get_user_permissions( $user_id ) {
		$this->load->model('Users_permissions_model');
		$permission = new $this->Users_permissions_model;
		$permission->setUserId( $user_id, TRUE);
		$permission->setActive( 1, TRUE );
		$permits=array();
		foreach( $permission->populate() as $permit) {
			$permits[] = $permit->permission;
		}
		return $permits;
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

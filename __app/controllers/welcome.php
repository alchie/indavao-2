<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function index()
	{
		$this->__init();
		
		$this->template_data->page_title( get_settings_value('site_slogan', "The #1 Online Listings In Davao City") );
		
		$this->load->model('Directory_data_model');
		$business = new $this->Directory_data_model;
		$business->setDirActive(1, TRUE);
		$business->setLimit(12);
		$business->setDirStatus('publish', TRUE);
		$business->setOrder('dir_added', 'DESC');
		$business->cache_on();
		$this->template_data->set( 'directories', $business->populate() );
		
		$this->load->view('homepage', $this->template_data->get() );
	}
	
	public function page($slug) {
		
		$this->__init();
		
		$this->template_data->page_title(get_settings_value($slug . "-title", "#1 Online Listings In Davao City") );
		
		$this->template_data->meta_tags(array(
			'description'=> substr( get_settings_value($slug . "-title"), 0, 150 ),
			'keywords'=>get_settings_value('default_meta_keywords', ''),
		));
		
		$this->template_data->itemprop(array(
			'name'=> get_settings_value($slug . "-title", "#1 Online Listings In Davao City"),
			'image'=> base_url('/assets/images/logo.png'),
			'description'=> substr( get_settings_value($slug . "-title"), 0, 150 ),
		));
		
		$this->template_data->opengraph(array(
			'name'=> get_settings_value($slug . "-title", "#1 Online Listings In Davao City"),
			'image'=> base_url('/assets/images/logo.png'),
			'description'=>substr( get_settings_value($slug . "-title"), 0, 150 ),
			'og:title'=> get_settings_value($slug . "-title", "#1 Online Listings In Davao City"),
			'og:site_name'=>"Online Listings in Davao City",
			'og:description'=>substr( get_settings_value($slug . "-title"), 0, 150 ),
			'og:url'=> site_url( trim( uri_string(), "/") ),
			'og:type'=>'article',
			'og:image'=> base_url('/assets/images/logo.png'),
			'og:image:width'=>'289',
			'og:image:height'=>'102',
			'fb:app_id'=>get_settings_value('facebook_app_id'),
		));
		
		$this->load->view('pages/' . $slug, $this->template_data->get() );
	}
	
	public function contact_us() {
		
		$this->__init();
		
			$this->load->library('form_validation');
			$this->form_validation->set_rules('firstname', 'Name', 'required');
			$this->form_validation->set_rules('phone', 'Phone', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			
			$this->template_data->set('message_sent', FALSE);
			
			if ($this->form_validation->run() == FALSE)
			{
				if( $this->input->post() ) {
					$this->template_data->alert( validation_errors(), 'danger');
				}
			} 
			else 
			{
				$this->load->model('Contact_messages_model');
				$msg = new $this->Contact_messages_model;
				$msg->setName( $this->input->post('firstname') );
				$msg->setPhone( $this->input->post('phone') );
				$msg->setEmail( $this->input->post('email') );
				$msg->setMessage( $this->input->post('message') );
				$msg->setMsgType('contact');
				
				if( $this->session->userdata('user_id') ) {
					$msg->setUserId( $this->session->userdata('user_id') );
				}
				
				if( $msg->replace() ) {
					$this->template_data->set('message_sent', TRUE);
				}
			}
			
		$this->template_data->page_title( get_settings_value("contact-us-title", "Contact Us | Indavao.Net") );
		$this->load->view('pages/contact-us', $this->template_data->get() );
	}
	
	public function page_not_found() {
		$this->__init();
		
		$this->template_data->page_title("404 Error - Page Not Found!");
		$this->load->view('404', $this->template_data->get() );
	}
	
	public function redirect() {
		if( $this->input->get('url') != '' ) {
			header("Location:" . prep_url( $this->input->get('url') ));
			exit;
		}
		header("Location: /");
		exit;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sitemap extends MY_Controller {

	function __construct() 
    {
        parent::__construct();
        
        $this->output->set_content_type('application/xml');
        
    }
    
	public function index()
	{
		$this->_init_nav();
		$this->load->view('sitemap/index', $this->template_data->get() );
	}
	
	public function tags()
	{
		$this->load->model(array('Tags_model'));
		$tags = new $this->Tags_model;
		$tags->setLimit(0);
		$tags->cache_on();
		$tags->setOrder('tag_name', 'ASC');
		$this->template_data->set('tags', $tags->populate() );
		$this->load->view('sitemap/tags', $this->template_data->get() );
	}

	public function realestate($slug=NULL)
	{
		$this->template_data->set('slug', $slug );
		$this->load->model(array('Realestate_data_model', 'Realestate_types_model'));
		switch( $slug ) {
			case 'projects':
				$re = new $this->Realestate_types_model;
				$re->setTypeName('PROJ', TRUE);
				$re->setReTypeActive( 1, TRUE );
				$re->isDistinct();
				$re->setLimit(0);
				$re->setWhere('realestate_data.re_active', 1);
				$re->setWhere('realestate_data.re_status', 'publish');
				$re->setJoin('realestate_data', 'realestate_data.re_id = realestate_types.re_id');
				$re->cache_on();
				$this->template_data->set('properties', $re->populate() );
			break;
			case 'residential':
				$re = new $this->Realestate_types_model;
				$re->setTypeName('RESID', TRUE);
				$re->setReTypeActive( 1, TRUE );
				$re->isDistinct();
				$re->setLimit(0);
				$re->setWhere('realestate_data.re_active', 1);
				$re->setWhere('realestate_data.re_status', 'publish');
				$re->setJoin('realestate_data', 'realestate_data.re_id = realestate_types.re_id');
				$re->cache_on();
				$this->template_data->set('properties', $re->populate() );
			break;
			case 'commercial':
				$re = new $this->Realestate_types_model;
				$re->setTypeName('COMM', TRUE);
				$re->setReTypeActive( 1, TRUE );
				$re->isDistinct();
				$re->setLimit(0);
				$re->setWhere('realestate_data.re_active', 1);
				$re->setWhere('realestate_data.re_status', 'publish');
				$re->setJoin('realestate_data', 'realestate_data.re_id = realestate_types.re_id');
				$re->cache_on();
				$this->template_data->set('properties', $re->populate() );
			break;
			case 'foreclosures':
				$re = new $this->Realestate_types_model;
				$re->setTypeName('FCL', TRUE);
				$re->setReTypeActive( 1, TRUE );
				$re->isDistinct();
				$re->setLimit(0);
				$re->setWhere('realestate_data.re_active', 1);
				$re->setWhere('realestate_data.re_status', 'publish');
				$re->setJoin('realestate_data', 'realestate_data.re_id = realestate_types.re_id');
				$re->cache_on();
				$this->template_data->set('properties', $re->populate() );
			break;
			case 'open_lots':
				$re = new $this->Realestate_types_model;
				$re->setTypeName('LOT', TRUE);
				$re->setReTypeActive( 1, TRUE );
				$re->isDistinct();
				$re->setLimit(0);
				$re->setWhere('realestate_data.re_active', 1);
				$re->setWhere('realestate_data.re_status', 'publish');
				$re->setJoin('realestate_data', 'realestate_data.re_id = realestate_types.re_id');
				$re->cache_on();
				$this->template_data->set('properties', $re->populate() );
			break;
			case 'condo':
				$re = new $this->Realestate_types_model;
				$re->setTypeName('CONDO', TRUE);
				$re->setReTypeActive( 1, TRUE );
				$re->isDistinct();
				$re->setLimit(0);
				$re->setWhere('realestate_data.re_active', 1);
				$re->setWhere('realestate_data.re_status', 'publish');
				$re->setJoin('realestate_data', 'realestate_data.re_id = realestate_types.re_id');
				$re->cache_on();
				$this->template_data->set('properties', $re->populate() );
			break;
			case 'apartments':
				$re = new $this->Realestate_types_model;
				$re->setTypeName('APART', TRUE);
				$re->setReTypeActive( 1, TRUE );
				$re->isDistinct();
				$re->setLimit(0);
				$re->setWhere('realestate_data.re_active', 1);
				$re->setWhere('realestate_data.re_status', 'publish');
				$re->setJoin('realestate_data', 'realestate_data.re_id = realestate_types.re_id');
				$re->cache_on();
				$this->template_data->set('properties', $re->populate() );
			break;
			case 'house_and_lot':
				$re = new $this->Realestate_types_model;
				$re->setTypeName('HOUSE', TRUE);
				$re->setReTypeActive( 1, TRUE );
				$re->isDistinct();
				$re->setLimit(0);
				$re->setWhere('realestate_data.re_active', 1);
				$re->setWhere('realestate_data.re_status', 'publish');
				$re->setJoin('realestate_data', 'realestate_data.re_id = realestate_types.re_id');
				$re->cache_on();
				$this->template_data->set('properties', $re->populate() );
			break;
			default:
				$re = new $this->Realestate_data_model;
				$re->setReActive( 1, TRUE );
				$re->setLimit(0);
				$re->setReStatus('publish', true);
				$re->cache_on();
				$this->template_data->set('properties', $re->populate() );
			break;
		}
		$this->load->view('sitemap/realestate', $this->template_data->get() );
	}
	
	public function local_business($slug=NULL)
	{
		$this->load->model(array('Directory_data_model'));
		$dirs = new $this->Directory_data_model;
		$dirs->setDirActive(1, TRUE);
		$dirs->setDirStatus('publish', TRUE );
		$dirs->setSelect("directory_data.*");
		$dirs->setLimit(0);
		$dirs->cache_on();
		$dirs->setOrder('directory_data.dir_added', 'DESC');
		
		if( ! is_null($slug) && $slug != '') {
			// set category
			$dirs->setSelect("taxonomies.tax_id,taxonomies.tax_name,taxonomies.tax_label");
			$dirs->setJoin('directory_category dir_cat', 'dir_cat.dir_id = directory_data.dir_id AND dir_cat.dir_cat_active = 1', 'LEFT OUTER');
			$dirs->setJoin('taxonomies', 'dir_cat.tax_id = taxonomies.tax_id AND taxonomies.tax_active = 1', 'LEFT OUTER');
			$dirs->setWhere('taxonomies.tax_name', $slug);
			
			$this->template_data->set('slug', $slug );
		}
		
		// set thumbnail
		$dirs->setSelect("(SELECT mu.file_name FROM directory_media dir_med LEFT JOIN media_uploads mu ON mu.media_id = dir_med.media_id WHERE dir_med.dir_id = directory_data.dir_id AND dir_med.dir_med_active = 1 AND dir_med.media_name = 'logo' AND dir_med.media_type = 'image' ORDER BY dir_med.dir_med_order DESC LIMIT 1) as thumbnail");
		
		$this->template_data->set('directories', $dirs->populate() );
		$this->load->view('sitemap/local_business', $this->template_data->get() );
	}
	
	public function multiple_sitemaps() {
		$this->_init_nav();
		$this->load->view('sitemap/sitemaps', $this->template_data->get() );
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/jobs.php */

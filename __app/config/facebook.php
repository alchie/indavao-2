<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['appId'] = '280451018797056';

$config['secret'] = '58d7d64c9b8d950ab5a7b9ea4dd7350a';

$config['permissions'] = array(
  'public_profile',
  'email',
  'user_friends',
  'publish_actions',
  //'user_actions:indavao',
);

$config['pageId'] = '1422670927991234'; 

$config['fileUpload'] = false; // optional

$config['allowSignedRequest'] = false; // optional, but should be set to false for non-canvas apps

$config['redirect_url'] = 'http://indavao.net/account/facebook.html'; 
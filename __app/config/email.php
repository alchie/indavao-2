<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Email Config
| -------------------------------------------------------------------------
|
*/
$config['useragent'] = 'CodeIgniter';
$config['protocol'] = 'mail'; // mail, sendmail, or smtp
$config['mailpath'] = '/usr/sbin/sendmail';
$config['smtp_host'] = '';
$config['smtp_user'] = '';
$config['smtp_pass'] = '';
$config['smtp_port'] = 25;
$config['smtp_timeout'] = 5;
$config['wordwrap'] = TRUE;
$config['wrapchars'] = 76;
$config['mailtype'] = 'text'; // text or html
$config['charset'] = 'iso-8859-1';
$config['validate'] = FALSE;
$config['priority'] = 3; // 1 - 5
$config['crlf'] = '\n';
$config['newline'] = '\n';
$config['bcc_batch_mode'] = FALSE;
$config['bcc_batch_size'] = 200;

/* End of file email.php */
/* Location: ./application/config/email.php */

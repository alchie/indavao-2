<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";
$route['404_override'] = 'welcome/page_not_found';

// pages
$route['privacy-policy'] = "welcome/page/privacy-policy";
$route['terms-and-conditions'] = "welcome/page/terms-and-conditions";
$route['about-us'] = "welcome/page/about-us";
$route['contact-us'] = "welcome/contact_us";

// account
$route['login/facebook'] = "account/facebook";
$route['login'] = "account/login";
$route['logout'] = "account/logout";
$route['r/(:num)'] = "account/referrer/$1";

// manage site
$route['my/(:any)/manage/ajax'] = "manage/ajax/$1";
$route['my/(:any)/manage/users'] = "manage/users/$1";
$route['my/(:any)/manage/business'] = "manage/business/$1";
$route['my/(:any)/manage/taxonomies'] = "manage/taxonomies/$1";
$route['my/(:any)/manage/realestate'] = "manage/realestate/$1";
$route['my/(:any)/manage/tasks'] = "manage/tasks/$1";
$route['my/(:any)/manage'] = "my/account/$1";

// my
$route['my/(:any)/api/(:any)'] = "api/$2";
$route['my/(:any)/ajax'] = "my/ajax/$1";
$route['my/(:any)/dashboard'] = "my/dashboard/$1";

$route['my/(:any)/bookmarks'] = "my/bookmarks/$1";
$route['my/(:any)/add_bookmark'] = "my/add_bookmark/$1";
$route['my/(:any)/facebook_friends'] = "my/facebook_friends/$1";
$route['my/(:any)/tasks'] = "my/tasks/$1";
$route['my/(:any)/tasks_to_friends'] = "my/tasks_to_friends/$1";
$route['my/(:any)/shares'] = "my/shares/$1";
$route['my/(:any)/points'] = "my/points/$1";
$route['my/(:any)/redemptions'] = "my/redemptions/$1";

$route['my/(:any)/properties/(:any)'] = "my/realestate/$1/$2";
$route['my/(:any)/properties'] = "my/realestate/$1";
$route['my/(:any)/search-properties'] = "my/page_not_found/$1";
$route['my/(:any)/sell-properties'] = "my/page_not_found/$1";
$route['my/(:any)/refer-property-buyer'] = "my/page_not_found/$1";

$route['my/(:any)/jobs/(:any)'] = "my/jobs/$1/$2";
$route['my/(:any)/jobs'] = "my/jobs/$1";
$route['my/(:any)/jobposts'] = "my/page_not_found/$1";
$route['my/(:any)/resume'] = "my/page_not_found/$1";
$route['my/(:any)/application-letters'] = "my/page_not_found/$1";
$route['my/(:any)/search-jobs'] = "my/page_not_found/$1";

$route['my/(:any)/add_business'] = "my/add_business/$1";
$route['my/(:any)/business/(:any)/(:any)'] = "my/business/$1/$2/$3";
$route['my/(:any)/business/(:any)'] = "my/business/$1/$2";
$route['my/(:any)/business'] = "my/business/$1";
$route['my/(:any)/products-services'] = "my/page_not_found/$1";

$route['my/(:any)/network'] = "my/page_not_found/$1";
$route['my/(:any)/friends'] = "my/page_not_found/$1";
$route['my/(:any)/referrals'] = "my/referrals/$1";

$route['my/(:any)/account'] = "my/account/$1";
$route['my/(:any)/profile'] = "my/page_not_found/$1";
$route['my/(:any)/messages'] = "my/page_not_found/$1";

$route['my/(:any)/logout'] = "my/logout";
$route['my/(:any)'] = "my/dashboard/$1";
$route['my/logout'] = "my/logout";

// realestate

$route['realestate/(:num)/(:any)/available-slots'] = "realestate/related/$2/$1";
$route['property/(:num)/(:any)/available-slots'] = "realestate/related/$2/$1";
$route['realestate/(:num)/(:any)/related'] = "realestate/related/$2/$1";
$route['property/(:num)/(:any)/related'] = "realestate/related/$2/$1";
$route['realestate/(:num)/(:any)/map'] = "realestate/show_map/$2/$1";
$route['property/(:num)/(:any)/map'] = "realestate/show_map/$2/$1";
$route['realestate/(:num)/(:any)/available-slots'] = "realestate/related/$2/$1";
$route['property/(:num)/(:any)/available-slots'] = "realestate/related/$2/$1";
$route['realestate/(:num)/(:any)/contact'] = "realestate/contact/$2/$1";
$route['property/(:num)/(:any)/contact'] = "realestate/contact/$2/$1";
$route['realestate/(:num)/(:any)/print'] = "realestate/print/$2/$1";
$route['property/(:num)/(:any)/print'] = "realestate/print/$2/$1";

$route['realestate/(:num)/(:any)/(:any)'] = "realestate/property/$2/$3/$1";
$route['property/(:num)/(:any)/(:any)'] = "realestate/property/$2/$3/$1";

// -- triple segment
$route['realestate/(:any)/available-slots'] = "realestate/related/$1";
$route['property/(:any)/available-slots'] = "realestate/related/$1";
$route['realestate/(:any)/related'] = "realestate/related/$1";
$route['property/(:any)/related'] = "realestate/related/$1";
$route['realestate/(:any)/map'] = "realestate/show_map/$1";
$route['property/(:any)/map'] = "realestate/show_map/$1";
$route['realestate/(:any)/available-slots'] = "realestate/available_slots/$1";
$route['realestate/(:any)/contact'] = "realestate/contact/$1";
$route['property/(:any)/contact'] = "realestate/contact/$1";
$route['realestate/(:any)/print'] = "realestate/print/$1";
$route['property/(:any)/print'] = "realestate/print/$1";
$route['model-house/(:any)_(:any)'] = "realestate/property/$1";

$route['realestate/(:any)/(:any)'] = "realestate/property/$2//$1";
$route['property/(:any)/(:any)'] = "realestate/property/$2//$1";

// -- double segment
$route['realestate/search-(:any)'] = "realestate/browse/$1";
$route['realestate/search'] = "realestate/search";
$route['realestate/projects'] = "realestate/projects";
$route['realestate/residential'] = "realestate/residential";
$route['realestate/commercial'] = "realestate/commercial";
$route['realestate/foreclosures'] = "realestate/foreclosures";
$route['realestate/open_lots'] = "realestate/open_lots";
$route['realestate/condo'] = "realestate/condo";
$route['realestate/apartments'] = "realestate/apartments";
$route['realestate/house_and_lot'] = "realestate/house_and_lot";
$route['realestate/browse'] = "realestate/browse";
$route['realestate/(:any)'] = "realestate/property/$1";
$route['property/(:any)'] = "realestate/property/$1";
$route['model-house/(:any)'] = "realestate/property/$1";

// -- single segment
$route['model-house'] = "realestate/redirect/browse";
$route['realestate'] = "realestate/redirect/browse";
$route['property'] = "realestate/redirect/browse";

// business
$route['business/search-(:any)'] = "business/browse/$1";
$route['business/search'] = "business/search";
$route['business/browse'] = "business/browse";
$route['business/(:any)'] = "business/category/$1";
$route['business'] = "business/index";

$route['local_business/search-(:any)'] = "business/browse/$1";
$route['local_business/search'] = "business/search";
$route['local_business/browse'] = "business/browse";
$route['local_business/tag/(:any)'] = "business/tag/$1";
$route['local_business/(:any)'] = "business/category/$1";
$route['local_business'] = "business/index";

$route['company/(:any)/(:any)/map/(:any)'] = "business/map/$1/$2/$3";
$route['company/(:any)/(:any)/map'] = "business/map/$1/$2";
$route['company/(:any)/(:any)/related/(:any)'] = "business/related/$1/$2/$3";
$route['company/(:any)/(:any)/related'] = "business/related/$1/$2";
$route['company/(:any)/(:any)/(:any)'] = "business/company/$1/$2/$3";
$route['company/(:any)/(:any)'] = "business/company/$1/$2";

// jobs
$route['jobs/search'] = "jobs/search";
$route['jobs/search-(:any)'] = "jobs/browse/$1";
$route['jobs/(:any)'] = "jobs/custom_search/$1";
$route['jobs/browse'] = "jobs/browse";
$route['jobs'] = "jobs/browse";

$route['sitemaps.xml'] = "sitemap/multiple_sitemaps";
$route['sitemap.xml'] = "sitemap/index";
$route['sitemap/tags.xml'] = "sitemap/tags";
$route['sitemap/realestate.xml'] = "sitemap/realestate";
$route['sitemap/realestate/(:any).xml'] = "sitemap/realestate/$1";
$route['sitemap/local_business.xml'] = "sitemap/local_business";
$route['sitemap/local_business/(:any).xml'] = "sitemap/local_business/$1";

$route['redirect'] = "welcome/redirect";

/* End of file routes.php */
/* Location: ./application/config/routes.php */

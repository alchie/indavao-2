<?php

if( !function_exists( 'encrypt_encode' ) ) {
    function encrypt_encode( $text ) {
        global $CI;
        $CI->load->library('encrypt');
         $encoded = $CI->encrypt->encode( $text );
         return $encoded;
         
    }
}

if( !function_exists( 'encrypt_decode' ) ) {
    function encrypt_decode( $text ) {
         global $CI;
        $CI->load->library('encrypt');
         $decoded = $CI->encrypt->decode( $text );
         return $decoded;
         
    }
}

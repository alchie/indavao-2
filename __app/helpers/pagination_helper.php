<?php

function bootstrap_pagination($pages, $current_page=1, $url='', $len=0, $class="pagination pagination-sm") {
 if( $pages > 1 ) { 
 $start = 1;
 $end = $pages;
 
 if( $len > 0 ) {
	$len_half = ceil( $len / 2 );
	$back_pages = ($current_page - $len_half);
	$start = $back_pages;
	
	$forward_pages = ($current_page + $len_half);
	$end = $forward_pages;
	
	if ($back_pages < 1 ) {
		$start = 1;
		if( ($end - $start) < $len ) {
			$end = $end + ($len - $end);
			if( $end > $pages ) {
				$end = $pages;
			}
		} 
	}
	
	if($forward_pages > $pages) {
		$end = $pages;
		
		if( ($end - $start) < $len ) {
			$start = $start - ($len - ($end - $start));
			if( $start < 1 ) {
				$start = 1;
			}
		}
		
	}
	
 }
 ?>
  <ul class="<?php echo $class; ?>">
	<li class="<?php echo ($back_pages < 1 ) ? 'disabled' : ''; ?>"><a href="<?php echo $url . 'page=1'; ?>">&laquo;</a></li>
  <?php for($i=$start;$i<=$end;$i++) { 
		if($current_page == $i) {
			echo '<li class="active"><a href="#current-page" DISABLED>'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.$url.'page='.$i.'">'.$i.'</a></li>';
		}
  }
  ?>
   <li class="<?php echo ($forward_pages > $pages) ? 'disabled' : ''; ?>"><a href="<?php echo $url . 'page=' . $pages; ?>">&raquo;</a></li>
  </ul>
<?php } 
}

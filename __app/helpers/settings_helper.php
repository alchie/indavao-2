<?php

function get_settings_value($name, $default=NULL, $group=NULL) {
	
	$CI =& get_instance();
	$CI->load->model('settings_model');
	
	$settings = new $CI->settings_model;
	$settings->cache_on();
	$settings->setSettingName($name, TRUE);
	
	if( ! is_null($group)  ) {
		$settings->setSettingGroup($group, TRUE);
	}
	
	$value = '';

	if( $settings->nonEmpty() === TRUE ) {
		$results = $settings->getResults();
		$value = $results->setting_value;
	} 
	
	if( ! is_null($default)  ) {
		if($value == '') {
			$value = $default;
		}
	}
	return $value;
}


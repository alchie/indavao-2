<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class CI_Template_data {
    
    var $CI;
    private $isLoggedIn = false;
    private $user_id = NULL;
    private $data = array();
    public function __construct()
    {
       $this->CI =& get_instance();
        $this->isLoggedIn = $this->CI->session->userdata('logged_in');
        $this->user_id = $this->CI->session->userdata('user_id');    
        $this->defaults();
    }
    
    function defaults() {
        $this->data = array(
                    /*******
                    * Header
                    ********/
                    'head_attr' => '',
                    'page_title' => get_settings_value('default_page_title', 'Home'),
                    'site_title' => get_settings_value('default_site_title', 'Online Listings In Davao City'),
                    'header_css' => array(),
                    'header_js' => array(),
                    'header' => '',
                    'header_top' => '',
                    'header_bottom' => '',
                    'meta_tags' => '',
                    /**** 
                    URL 
                    *****/
                     'base_url' => base_url(),
                     'site_url' => base_url(),
                     'home_url' => base_url(),
                     'redirect_url' => site_url('my/account'),
                     /*******
                     Session
                     ********/
                     'isLoggedIn' => $this->isLoggedIn(),
                    /******* 
                     Alert
                    *******/
                     'alert' => FALSE,
                     'alert_status' => 'default',
                     'alert_message' => '',
                    /*******
                    * Footer
                    ********/
                     'footer_js' => array(),
                     'footer' => '',
                     'footer_top' => '',
                     'footer_bottom' => '',
                     /* Custom */
                     'itemscope' => 'Article',
                     'itemprop' => '',
                     'opengraph' => '',
                     );
    }
    
    function isLoggedIn()
    {
        return $this->CI->session->userdata('logged_in');
    }
    
    function set($key, $value)
    {
        $this->data[$key] = $value; 
        return $this;
    }
    
    function get($key=false)
    {
        if($key) {
            return $this->data[$key];
        } else {
            return $this->data;
        }
    }
    
    function prepend($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $value . $current_value );
        return $this;
    }
    
    function append($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $current_value . $value );
        return $this;
    }
    
    function push( $key, $value, $priority=0 ) {
		if( is_array( $this->get( $key ) ) ) {
			$this->set( $key, array_merge( $this->get( $key ), array( $priority => $value ) ) );
		}
		return $this;
	} 
    
    function add_to_array( $key, $value, $priority=0 ) {
		if( is_array( $this->get( $key ) ) ) {
			$newvalue = ($this->get( $key ) + array( $priority => $value ));
			ksort( $newvalue );
			$this->set( $key, $newvalue );
		}
		return $this;
	}
	
    function alert($message='Alert!', $status='default') {
        $this->set('alert', TRUE );
        $this->set('alert_status', $status );       
        $this->set('alert_message', $message );
    }
    
    function alert_off()
    {
        $this->set('alert', FALSE );
    }
    
    function page_title($value='') {
        $this->set('page_title', $value );
        $this->opengraph(array('og:title' => $value));
        $this->itemprop(array('name' => $value));
        return $this;
    }
    
	
    function opengraph( $var=array() ) {
        $defaults = array(
			'name'=>'Online Listings in Davao City',
			'image'=>base_url('/assets/images/logo.png'),
			'description'=>'A Davao-based website for listing Local Business, Real Estate, Jobs Portal, Classified Ads, Videos, Tourist Spots, Hobbies, Food Trip.',
			'keywords'=>'Local Business, Real Estate, Jobs Portal, Classified Ads, Videos, Tourist Spots, Hobbies, Food Trip',
            'og:title'=> $this->get('page_title'),
            'og:site_name'=>'Online Listings in Davao City',
            'og:description'=>'A Davao-based website for listing Local Business, Real Estate, Jobs Portal, Classified Ads, Videos, Tourist Spots, Hobbies, Food Trip.',
            'og:url'=> site_url( trim( uri_string(), "/") ),
            'og:type'=>'article',
            'og:image'=> base_url('/assets/images/logo.png'),
            'og:image:width'=>'289',
            'og:image:height'=>'102',
            'fb:app_id'=>'',
        );
        
        $opengraph = array_merge( (array) $defaults, (array) $var );
        $meta = '';
        foreach( $opengraph as $tag=>$value ) {
            if( $value != '' ) {
                $meta .= "\n" . '<meta property="'.$tag.'" content="'.$value.'" />';               
            }
        }
        $this->set('opengraph', $meta );
        return $this;
    }
    
    function itemprop( $var=array() ) {
        $defaults = array(
			'name'=>'Online Listings in Davao City',
			'image'=>base_url('/assets/images/logo.png'),
			'description'=>'A Davao-based website for listing Local Business, Real Estate, Jobs Portal, Classified Ads, Videos, Tourist Spots, Hobbies, Food Trip.',
        );
        
        $itemprop = array_merge( (array) $defaults, (array) $var );
        $meta = '';
        foreach( $itemprop as $tag=>$value ) {
            if( $value != '' ) {
                $meta .= "\n" . '<meta itemprop="'.$tag.'" content="'.$value.'" />';               
            }
        }
        $this->set('itemprop', $meta );
        return $this;
    }
    
    function meta_tags( $var=array() ) {
        $defaults = array(
			'description'=>'A Davao-based website for listing Local Business, Real Estate, Jobs Portal, Classified Ads, Videos, Tourist Spots, Hobbies, Food Trip.',
			'keywords'=>'Local Business, Real Estate, Jobs Portal, Classified Ads, Videos, Tourist Spots, Hobbies, Food Trip',
        );
        
        $meta_tags = array_merge( (array) $defaults, (array) $var );
        $meta = '';
        foreach( $meta_tags as $tag=>$value ) {
            if( $value != '' ) {
                $meta .= "\n" . '<meta name="'.$tag.'" content="'.$value.'" />';               
            }
        }
        $this->set('meta_tags', $meta );
        return $this;
    }
    
}

/* End of file Global_variables.php */

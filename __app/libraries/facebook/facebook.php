<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( session_status() == PHP_SESSION_NONE ) {
  session_start();
}

// Autoload the required files
require_once( APPPATH . 'libraries/facebook/autoload.php' );

use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;


class Facebook {
  var $ci;
  var $helper;
  var $session;
  var $permissions;
  var $redirect_url;
  
  public function __construct($params) {
    
	if( isset( $params['redirect_url'] ) && $params['redirect_url'] != '' ) {
		$this->redirect_url = $params['redirect_url'];
	}
	if( isset( $params['permissions'] ) && $params['permissions'] != '' ) {
		$this->permissions = $params['permissions'];
	}
	
	$this->ci =& get_instance();
	$this->ci->config->load('facebook', true);
	
	if( $this->permissions == '' ) {
		$this->permissions = $this->ci->config->item('permissions', 'facebook');
	}
	if( $this->redirect_url == '' ) {
		$this->redirect_url = $this->ci->config->item('redirect_url', 'facebook');
	}
	
    // Initialize the SDK
    FacebookSession::setDefaultApplication( $this->ci->config->item('appId', 'facebook'), $this->ci->config->item('secret', 'facebook') );

 }
 
  public function retrieveRedirectSession() {
    $this->helper = new FacebookRedirectLoginHelper( $this->redirect_url );
	
    if ( $this->ci->session->userdata('fb_token') ) {
      $this->session = new FacebookSession( $this->ci->session->userdata('fb_token') );

      // Validate the access_token to make sure it's still valid
      try {
        if ( ! $this->session->validate() ) {
          $this->session = null;
        }
      } catch ( Exception $e ) {
        // Catch any exceptions
        $this->session = null;
      }
    } else {
      // No session exists
      try {
        $this->session = $this->helper->getSessionFromRedirect();
      } catch( FacebookRequestException $ex ) {
        // When Facebook returns an error
      } catch( Exception $ex ) {
        // When validation fails or other local issues
      }
    }

    if ( $this->session ) {
      $this->ci->session->set_userdata( 'fb_token', $this->session->getToken() );
      $this->session = new FacebookSession( $this->session->getToken() );
		return true;
    } else {
		return false;
	}
  }

  public function retrieveJavaScriptSession() {
    $this->helper = new FacebookJavaScriptLoginHelper();
	
    if ( $this->ci->session->userdata('fb_token') ) {
      $this->session = new FacebookSession( $this->ci->session->userdata('fb_token') );
      // Validate the access_token to make sure it's still valid
      try {
        if ( ! $this->session->validate() ) {
          $this->session = null;
        }
      } catch ( Exception $e ) {
        // Catch any exceptions
        $this->session = null;
      }
    } else {
      // No session exists
      try {
        $this->session = $this->helper->getSession();
      } catch( FacebookRequestException $ex ) {
        // When Facebook returns an error
      } catch( Exception $ex ) {
        // When validation fails or other local issues
      }
    }

    if ( $this->session ) {
      $this->ci->session->set_userdata( 'fb_token', $this->session->getToken() );

      $this->session = new FacebookSession( $this->session->getToken() );
    }
	
  }
  
  /**
   * Returns the login URL.
   */
  public function login_url() {
    return $this->helper->getLoginUrl( $this->permissions );
  }

  /**
   * Returns the logout URL.
   */
  public function logout_url() {
	if( $this->session ) {
		return $this->helper->getLogoutUrl( $this->session, base_url() );
	} else {
		return site_url("account/logout");
	}
  }
  
  /**
   * Returns the current user's info as an array.
   */
  public function get_user() {
    if ( $this->session ) {
      $request = ( new FacebookRequest( $this->session, 'GET', '/me' ) )->execute();
      $user = $request->getGraphObject()->asArray();
      return $user;
    }
    return false;
  }
  
  public function addBusiness( $url ) {
    if ( $this->session ) {
      $response = ( new FacebookRequest( $this->session, 'POST', '/me/indavao:add', array('business' => $url) ) )->execute();
      return $response->getGraphObject()->asArray();	  
    }
    return false;
  }
  
  public function bookmarkBusiness( $url ) {
    if ( $this->session ) {
      $response = ( new FacebookRequest( $this->session, 'POST', '/me/indavao:bookmark', array('business' => $url) ) )->execute();
      return $response->getGraphObject()->asArray();	  
    }
    return false;
  }
  
  public function bookmarkProperty( $url ) {
    if ( $this->session ) {
      $response = ( new FacebookRequest( $this->session, 'POST', '/me/indavao:bookmark', array('property' => $url) ) )->execute();
      return $response->getGraphObject()->asArray();	  
    }
    return false;
  }
  
  public function postLinkToFeed( $url, $message ) {
    if ( $this->session ) {
      $response = ( new FacebookRequest( $this->session, 'POST', '/me/feed', array('link' => $url, 'message'=>$message) ) )->execute();
      return $response->getGraphObject()->asArray();	  
    }
    return false;
  }
  
  public function get_user_friendlist() {
    if ( $this->session ) {
      /**
       * Retrieve User’s Profile Information
       */
      // Graph API to request user data
      $request = ( new FacebookRequest( $this->session, 'GET', '/me/friends' ) )->execute();

      // Get response as an array
      $friendlist = $request->getGraphObject()->asArray();

      return $friendlist;
    }
    return false;
  }
}
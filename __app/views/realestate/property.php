<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
       
       
<div class="row">
		<div class="col-md-12">

<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
	<ol class="breadcrumb">
	  <li>
		<a href="<?php echo base_url(); ?>" itemprop="url"><span itemprop="title">InDavao.Net</span></a>
	  </li>
	  <li>
		<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<a href="<?php echo site_url(array("realestate", "browse")); ?>" itemprop="url"><span itemprop="title">Real Estate Properties</span></a>
		</span>
	  </li>
	  <li class="active"><?php echo $property->re_title; ?></li>
	</ol>
</div>
		
<div class="panel panel-primary">
  <div class="panel-body">
	  
<?php if ( $property->logo_image != '' ) { ?>
	<div class="pull-right hidden-xs">
		<img itemprop="logo" width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url') . $property->logo_image; ?>" src="<?php echo get_settings_value('upload_url') . $property->logo_image; ?>">
	</div>
<?php } elseif ( isset( $property->pri_logo_image ) && $property->pri_logo_image != '' ) { ?>
	<div class="pull-right hidden-xs">
		<img itemprop="logo" width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url') . $property->pri_logo_image; ?>">
	</div>
<?php } else { ?>
	<div class="pull-right hidden-xs">
		<img width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo base_url() .'assets/images/photo-icon.png'; ?>">
	</div>
<?php } ?>
<h2 class="title"><span itemprop="name"><?php echo $property->re_title ; ?></span>

<?php if( $property->address != "") { ?>
<small><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress"><?php echo $property->address; ?></span>
</span>
</small>
<?php } elseif( isset($property->pri_address) && $property->pri_address != "") { ?>
<small><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress"><?php echo $property->pri_address; ?></span>
</span>
</small>
<?php } ?>
</h2>
<span class="abstract">
<?php if( $property->abstract != "") { 
		echo $property->abstract; 
	} elseif( isset($property->pri_abstract ) && $property->pri_abstract != "") { 
		echo $property->pri_abstract; 
	} ?>
</span>

<?php if( count($property_types) > 0 ) {
	echo "<p>";
	foreach( $property_types as $property_type ) {
		if ($property_type->link != '') {
			echo "<span itemscope itemtype=\"http://data-vocabulary.org/Breadcrumb\">";
			echo "<a href=\"{$property_type->link}\" class=\"label label-info\" itemprop=\"url\">";
			echo "<span itemprop=\"title\">{$property_type->label}</span>";
			echo "</a>";
			echo "</span>&nbsp;";
		} else {
			echo "<span class=\"label label-info\">{$property_type->label}</span> ";
		}
	}
	echo "</p>";
} ?>
		</div>
	 <div class="panel-footer">
		 <div class="btn-group btn-group-justified hidden-xs">
			 <?php if(count( $gallery ) > 0) { ?>
			 <a href="#photos" class="btn btn-primary smooth-scroll"><i class="glyphicon glyphicon-picture"></i> Photos</a>
			 <?php } ?>
			 <?php if($property->description != '' || ( isset($property->pri_description) && $property->pri_description != '') ) { ?>
			 <a href="#quick-facts" class="btn btn-warning smooth-scroll"><i class="glyphicon glyphicon-info-sign"></i> Quick Facts</a>
			 <?php } ?>
			 <?php if(count( $children ) > 0) { ?>
			 <a href="#available-slots" class="btn btn-success smooth-scroll"><i class="glyphicon glyphicon-ok"></i> Related Properties</a>
			 <?php } ?>
		
			<?php if( (($property->map_lat != '') && ($property->map_long != '')) 
			|| (( isset($property->pri_map_lat) && $property->pri_map_lat != '') && ( isset($property->pri_map_long) && $property->pri_map_long != '')) ) { ?>
			<a href="#map" class="btn btn-danger smooth-scroll"><i class="glyphicon glyphicon-map-marker"></i> Map</a>
			<?php } ?>
		</div>
		</div>
</div>
       
       </div>
</div>
       
      <div class="row">
		<div class="col-sm-12 col-md-8">
<?php if(count( $gallery ) > 0) { ?>
<div class="panel panel-primary" id="photos">
<div class="panel-heading">
	<h3 class="panel-title"><i class="glyphicon glyphicon-picture"></i> Photos</h3>
</div>
<div class="panel-body">
    <div class="row">
		<?php foreach( $gallery as $photo ) { ?>
		<div class="col-md-3">
			<a href="<?php echo get_settings_value('upload_url') . $photo->img_file; ?>" class="thumbnail fancybox" rel="gallery" title="<?php echo $photo->media_caption; ?>">
			  <?php 
				$thumbnail = get_settings_value('upload_url') . (($photo->img_thumb != '') ? $photo->img_thumb : $photo->img_file);
			  ?>
			  <img alt="image" class="img-responsive lazy" data-original="<?php echo $thumbnail; ?>">
			</a>
		</div>
		<?php } ?>
    </div>
</div>
</div>
<?php } // gallery check ?>

<?php if($property->description != '' || ( isset($property->pri_description) && $property->pri_description != '') ) { ?>
	
<div class="panel panel-warning" id="quick-facts">
<div class="panel-heading">
	<h3 class="panel-title"><i class="glyphicon glyphicon-info-sign"></i> Quick Facts</h3>
</div>
<div class="panel-body">
    <?php if($property->description != '') { 
			echo $property->description;
		} elseif($property->pri_description != '') {
			echo $property->pri_description ; 
		}
	?>
</div>
</div>
<?php } // description check ?>
<?php if(count( $children ) > 0) { ?>
<div class="panel panel-success" id="available-slots">
<div class="panel-heading">
	<a href="<?php echo site_url(array("property", $property->re_id, $property->re_slug, "related")); ?>" class="btn btn-default btn-xs pull-right">Show More</a>
	<h3 class="panel-title"><i class="glyphicon glyphicon-home"></i> Related Properties</h3>
</div>
<div class="list-group">
	<?php foreach( $children as $child ) { ?>
  <a href="<?php echo site_url(array("property", $child->re_id, $child->re_slug)); ?>" class="list-group-item">
    <h4 class="list-group-item-heading"><?php echo $child->re_title; ?></h4>
  </a>
  <?php } ?>
</div>
</div>
<?php } // Available Slots check ?>

<?php if( (get_settings_value('GOOGLE_API_KEY') != '') && (($property->map_lat != '') && ($property->map_long != '')) 
			|| (( isset($property->pri_map_lat) && $property->pri_map_lat != '') && ( isset($property->pri_map_long) && $property->pri_map_long != '')) ) { 
	
	$property_map_lat = '7.079733';
	$property_map_long = '125.506384';
	
	if( ($property->map_lat != '') && ($property->map_long != '') ) {
		$property_map_lat = $property->map_lat;
		$property_map_long = $property->map_long;
	} elseif( ($property->pri_map_lat != '') && ($property->pri_map_long != '') ) {
		$property_map_lat = $property->pri_map_lat;
		$property_map_long = $property->pri_map_long;
	}
?>
<div class="panel panel-danger" id="map">
<div class="panel-heading">
	<a href="<?php echo site_url(array("property", $property->re_id, $property->re_slug, "map")); ?>" class="btn btn-default btn-xs pull-right">View Map</a>
	<h3 class="panel-title"><i class="glyphicon glyphicon-map-marker"></i> Map</h3>
</div>
    <div id="map-canvas" style="height: 300px; margin: 0; padding: 0;"></div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo get_settings_value('GOOGLE_API_KEY'); ?>"></script>
<script type="text/javascript">
      function initialize() {
        var mapOptions = {
          center: { lat: <?php echo $property_map_lat; ?>, lng: <?php echo $property_map_long; ?> },
          zoom: 15
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        marker = new google.maps.Marker({
			map:map,
			draggable:false,
			animation: google.maps.Animation.DROP,
			position: { lat: <?php echo $property_map_lat; ?>, lng: <?php echo $property_map_long; ?> },
		});
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    
<?php } // map_lat, map_long check ?>
		</div>

        <div class="col-md-4 hidden-print">
			<?php $this->load->view('realestate/property-sidebar'); ?>
		</div>
      </div>  <!-- /row -->

    </div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

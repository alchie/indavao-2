<p>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- InDavao Front -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="4721203728"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</p>

<form method="POST" action="<?php echo site_url('realestate/search'); ?>" style="margin:0;">
<div class="panel panel-primary" id="refine-search">
	<div class="panel-heading">
		<h3 class="panel-title">Refine Search</h3>
	</div>
	<div class="panel-body">
		
		
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">Listing Status</label>
						<div class="btn-group btn-group-justified" role="group">
							<?php foreach(array('sale' => 'For Sale', 'rent' => 'For Rent', 'assume' => 'For Assume') as $key=>$label) { 
							if(isset($search_keys->status) && ($search_keys->status == $key)) { ?>
						<label class="btn btn-success btn-block btn-radio"><input name="status" type="radio" value="<?php echo $key; ?>" class="hidden" CHECKED><span><?php echo $label; ?></span></label>
						<?php } else { ?>
							<label class="btn btn-default btn-block btn-radio"><input name="status" type="radio" value="<?php echo $key; ?>" class="hidden"><span><?php echo $label; ?></span></label>
						<?php } ?>
					<?php } ?>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Bedrooms</label>
					<input name="beds" type="text" id="beds-output" readonly style="border:0; color:#f6931f; font-weight:bold;">
					<div>
						<div id="slider-beds" class="ui-slider normal" data-display="#beds-output" data-min="0" data-max="10" data-default="<?php echo (isset($search_keys->beds)) ? $search_keys->beds : 0; ?>" data-step="1" data-prefix=""></div>
					</div>
				  </div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Bathrooms</label>
					<input name="baths" type="text" id="baths-output" readonly style="border:0; color:#f6931f; font-weight:bold;">
					<div>
						<div id="slider-baths" class="ui-slider normal" data-display="#baths-output" data-min="0" data-max="10" data-default="<?php echo (isset($search_keys->baths)) ? $search_keys->baths : 0; ?>" data-step="1" data-prefix=""></div>
					</div>
				  </div>
				</div>
			</div>
			

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Price Range</label>
					<input name="price_range" type="text" id="price-range-output" readonly style="border:0; color:#f6931f; font-weight:bold;">
					<div>
						<div id="slider-price-range" class="ui-slider range" data-display="#price-range-output" data-min="0" data-max="20000000" data-min-default="<?php echo (isset($search_keys->price_range_min)) ? $search_keys->price_range_min : 0; ?>" data-max-default="<?php echo (isset($search_keys->price_range_max)) ? $search_keys->price_range_max : 0; ?>" data-prefix="&#8369" data-step="10000" ></div>
					</div>
				  </div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Lot Area (sqm)</label>
					<input name="lot_area" type="text" id="lot-area-output" readonly style="border:0; color:#f6931f; font-weight:bold;">
					<div>
						<div id="slider-lot-area" class="ui-slider range" data-display="#lot-area-output" data-min="0" data-max="2000" data-min-default="<?php echo (isset($search_keys->lot_area_min)) ? $search_keys->lot_area_min : 0; ?>" data-max-default="<?php echo (isset($search_keys->lot_area_max)) ? $search_keys->lot_area_max : 0; ?>" data-prefix="" data-step="10" ></div>
					</div>
				  </div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">Property Types</label>
					<select class="form-control selectpicker show-menu-arrow" title="Any Property Type" name="property_type">
						
						<?php foreach(array(
						'HOUSE' => 'House and Lot',
						'PROJ' => 'Development Projects', 
						'COMM' => 'Commercial Properties',
						'RESID' => 'Residential Properties',
						'LOT' => 'Open Lot / Land',
						'CONDO' => 'Condominiums',
						'APART' => 'Apartments',
						'FCL' => 'Foreclosures',
						) as $key=>$label) { 
							if( isset( $search_keys->property_type ) && $search_keys->property_type == $key ) {
								echo "<option value=\"{$key}\" SELECTED>{$label}</option>";
							 } else { 
								 echo "<option value=\"{$key}\">{$label}</option>"; 
							} 
						} ?>
					</select>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Locations</label>
					<div>
						<select class="form-control selectpicker show-menu-arrow" title="Any Location" name="location">
							<option value="">Any Location</option>
						<?php 
							function echo_locations_options($locations, $prefix='', $selected=array()) { 
								foreach($locations as $location) { 
									if(in_array($location->loc_slug, ((isset($selected)) ? $selected : array()))) { 
										echo "<option value=\"{$location->loc_slug}\" SELECTED>{$prefix}{$location->loc_name}</option>";
									} else {
										echo "<option value=\"{$location->loc_slug}\">{$prefix}{$location->loc_name}</option>";
									}
									if( isset($location->children) && (count($location->children) > 0) ) {
										echo_locations_options($location->children, $prefix." - ", $selected);
									}
								} 
							}
							if( $locations ) {
								echo_locations_options($locations, '', ((isset($search_keys->location)) ? $search_keys->location : array()));
							}
						   ?>
						   
						</select>
					</div>
				  </div>
				</div>
			</div>
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-warning btn-block"><i class="glyphicon glyphicon-search"></i> Refine Search</button>
	</div>
	
</div>
</form>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- InDavao Sidebar -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="4162800524"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
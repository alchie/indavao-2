<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
       
       
<div class="row">
		<div class="col-md-12">
<div class="panel panel-primary">
  <div class="panel-body">
	  
<?php if ( $property->thumbnail != '' ) { ?>
	<div class="pull-right hidden-xs">
		<img itemprop="logo" width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $property->thumbnail; ?>">
	</div>
<?php } else { ?>
	<div class="pull-right hidden-xs">
		<img width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo base_url() .'assets/images/photo-icon.png'; ?>">
	</div>
<?php } ?>
<h2 class="title"><span itemprop="name"><?php echo $property->re_title ; ?></span>

<?php if( $property->address != "") { ?>
<small><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress"><?php echo $property->address; ?></span>
</span>
</small>
<?php } ?>
</h2>
<span class="abstract">
<?php if( $property->abstract != "") { echo $property->abstract; } ?>
</span>
		</div>
	 <div class="panel-footer">
		 <div class="btn-group btn-group-justified hidden-xs">
			 <span class="btn btn-primary"><i class="glyphicon glyphicon-picture"></i> Photos</span>
			 <span class="btn btn-warning"><i class="glyphicon glyphicon-info-sign"></i> Quick Facts</span>
			 <span class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Available Slots</span>
			<span class="btn btn-info"><i class="glyphicon glyphicon-home"></i> Model Houses</span> 
			<span class="btn btn-danger"><i class="glyphicon glyphicon-map-marker"></i> Map</span>
		</div>
		</div>
</div>
       
       </div>
</div>
       
      <div class="row">

        <div class="col-sm-12 col-md-8">
		
		<?php if( $property->cover != '' ) { ?>
		<div class="project-cover" style="background: url(<?php echo get_settings_value('upload_url'); ?><?php echo $property->cover ; ?>);margin-bottom:20px;">
		</div>
		<?php } ?>

<?php $this->load->view('realestate/property-img-slider'); ?>

<div class="panel panel-default">
  <div class="panel-heading">
  
 <h3 class="panel-title"><?php echo $property->re_title ; ?> 
    
    </h3>
  </div>
  <div class="panel-body">
    
<?php if( $property->developer != '' ) { ?>
	<span class="detail">
		<label>Developer:</label>
		<span class="pull-right"><?php echo $property->developer ; ?></span>
	</span>
<?php } ?>

<?php if( $property->description != '' ) { ?>
	<?php echo $property->description ; ?>
<?php } ?>
  </div>
</div>

<?php if( count( $model_houses ) > 0 ) { ?>
<div class="page-header">
  <h1>Model Houses</h1>
</div>
<div class="row">
	<?php foreach($model_houses as $property) { ?>
<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12"> 
						<div class=" bgc-fff box-shad btm-mrg-20 property-listing">

                        <div class="row inner">
							<div class="col-md-4">
								<div class="thumb">
								<a href="<?php echo site_url('realestate/'. $property->re_slug ); ?>" target="_parent" title="<?php echo  $property->re_title; ?>">
<?php if ( $property->thumbnail == '' ) { ?>
	 <img alt="image" class="img-responsive lazy" data-original="<?php echo base_url() .'assets/images/photo-icon.png'; ?>">
<?php } else { ?>
	<img alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $property->thumbnail; ?>">
<?php } ?> </a>					</div>
							</div>
							<div class="col-md-8">
								<div class="info">
								<h4 class="title">
                                  <a href="<?php echo site_url('realestate/'. $property->re_slug ); ?>" target="_parent" title="<?php echo  $property->re_title; ?>"><?php echo  $property->re_title; ?></a>
                                </h4>

                                <?php if( $property->price != '' ) { ?><p class="attr"><i class="property-icon price"></i> ₱<?php echo number_format($property->price,0); ?></p><?php } ?>
                              
                                <?php if( $property->beds != '' ) { ?><p class="attr"><i class="property-icon beds"></i> <?php echo $property->beds; ?></p><?php } ?>
                                
                                <?php if( $property->baths != '' ) { ?><p class="attr"><i class="property-icon baths"></i> <?php echo $property->baths; ?></p><?php } ?>
								
                                </div>
							</div>
							
							<div class="footer col-md-8 hidden-xs hidden-sm">
										<a class="btn btn-primary btn-xs pull-right" href="<?php echo site_url('realestate/'. $property->re_slug ); ?>">Show Details <i class="glyphicon glyphicon-play"></i></a>
							</div>
                        </div>
                        
                    </div><!-- End Listing-->
				</div>
	
	<?php } ?>
</div>
 
<?php } ?>

<?php if( count( $children ) > 0 ) { ?>
<a href="" class="btn btn-primary btn-lg btn-block">Show Available Slots</a>
<?php } ?>



      </div>  <!-- /col-md-8 -->
		<div class="col-md-4 hidden-print">
			<?php $this->load->view('realestate/property-sidebar'); ?>
		</div>
      </div>  <!-- /row -->

    </div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

<div class="row">
<?php if($isLoggedIn) { ?>
	<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="glyphicon glyphicon-cog"></i> My Tools</h3>
  </div>
  <div class="list-group">
	  <a href="<?php echo site_url(array("my", $this->session->userdata('user_id'), "add_bookmark")); ?>?type=realestate&id=<?php echo $property->re_id; ?>" class="list-group-item" id="save-to-bookmarks-<?php echo $property->re_id; ?>"><i class="glyphicon glyphicon-bookmark"></i> Save to Bookmarks</a>
	   <a href="javascript:void(0);" class="list-group-item print-me"><i class="glyphicon glyphicon-print"></i> Print This Page</a>
	</div>
</div>
</div>
<?php } ?>

<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">

<p>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- InDavao Front -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="4721203728"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</p>


<div class="panel panel-info" id="seller-info">
   <div class="panel-heading">
    <h3 class="panel-title"><i class="glyphicon glyphicon-earphone"></i> Contact Us</h3>
    
  </div>
  <div class="panel-body">
	  <img class="lazy img-circle pull-right" data-original="<?php echo get_settings_value('default_seller_image'); ?>">
	  <p class="h3 name"><strong><?php echo get_settings_value('default_seller_name'); ?></strong></p>
	  <p class="h5"><?php echo get_settings_value('default_seller_info'); ?></p>
  </div>
  <div class="panel-footer">
	  <a href="mailto:<?php echo get_settings_value('default_seller_email', 'agent@indavao.net'); ?>" class="btn btn-success btn-xs pull-right">Email Agent</a>
	  <span class="phone"><i class="glyphicon glyphicon-earphone"></i> <?php echo get_settings_value('default_seller_phone'); ?></span>
	  </div>
</div>
</div>

<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
<!-- contact form -->
<form action="<?php echo site_url('realestate/' . $property->re_slug . '/contact'); ?>" method="POST">
				<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="glyphicon glyphicon-envelope"></i> Ask about this property</h3>
  </div>
  <div class="panel-body">
    
   <div class="form-group">
    <input name="name" type="text" class="form-control" placeholder="Name *" value="<?php echo $this->session->userdata('meta_name'); ?>">
  </div>
    
     <div class="form-group">
    <input name="phone" type="phone" class="form-control" placeholder="Phone *" value="<?php echo $this->session->userdata('meta_phone'); ?>">
  </div>
  
     <div class="form-group">
    <input name="email" type="email" class="form-control" placeholder="Email" value="<?php echo $this->session->userdata('meta_email'); ?>">
  </div>
  
  <div class="form-group">
    <textarea name="message" class="form-control" rows="5" placeholder="Message">Hello, I found your listing on InDavao.Net. Please send me more information about <?php echo $property->re_title; ?>. Thank you!</textarea>
  </div>
  
  <button type="submit" class="btn btn-warning btn-block">Submit</button>
  
  </div>
  </div>
</form>
</div>

<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
<?php $share_url = ( $isLoggedIn ) ? site_url( array('realestate', $property->re_id, $property->re_slug, $this->session->userdata('user_id') ) ) : current_url(); ?>
<div class="social-media-sidebar">

<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( $share_url ); ?>" class="btn btn-default btn-block btn-lg btn-facebook" >Share to Facebook</a>
	
<a target="_blank" href="https://twitter.com/home?status=<?php echo $property->re_title . ' - ' . urlencode( $share_url ); ?>" class="btn btn-default btn-block btn-lg btn-twitter">Tweet on Twitter</a>
	
	
<a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode( $share_url ); ?>" class="btn btn-default btn-block btn-lg btn-google">Share on Google+</a>
	
	
<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode( $share_url ); ?>&title=<?php echo $property->re_title; ?>" class="btn btn-default btn-block btn-lg btn-linkedin">Share on LinkedIn</a>
	
	
<?php if( $property->logo_image ) { ?>
	
<a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode( $share_url ); ?>&media=<?php echo urlencode( get_settings_value('upload_url') . $property->logo_image ); ?>&description=<?php echo $property->re_title; ?>" class="btn btn-default btn-block btn-lg btn-pinterest">Pin on Pinterest</a>
	
<?php } ?>
</div>
</div>



</div>

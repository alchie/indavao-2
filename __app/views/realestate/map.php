<?php $this->load->view('overall_header'); ?>
<?php 
	$property_map_lat = '7.079733';
	$property_map_long = '125.506384';
if( (($property->map_lat != '') && ($property->map_long != '')) 
			|| (( isset($property->pri_map_lat) && $property->pri_map_lat != '') && ( isset($property->pri_map_long) && $property->pri_map_long != '')) ) { 

	if( ($property->map_lat != '') && ($property->map_long != '') ) {
		$property_map_lat = $property->map_lat;
		$property_map_long = $property->map_long;
	} elseif( ($property->pri_map_lat != '') && ($property->pri_map_long != '') ) {
		$property_map_lat = $property->pri_map_lat;
		$property_map_long = $property->pri_map_long;
	}
}
?>
<div class="main-body" style="display:block;height: 800px;width:100%;padding:0;">
	<div id="map-canvas" style="height: 800px; margin: 0; padding: 0; width: 100%;"></div>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo get_settings_value('GOOGLE_API_KEY'); ?>"></script>
<script type="text/javascript">
      function initialize() {
        var mapOptions = {
          center: { lat: <?php echo $property_map_lat; ?>, lng: <?php echo $property_map_long; ?> },
          zoom: 15
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        marker = new google.maps.Marker({
			map:map,
			draggable:false,
			animation: google.maps.Animation.DROP,
			position: { lat: <?php echo $property_map_lat; ?>, lng: <?php echo $property_map_long; ?> },
		});
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
       
       
<div class="row">
		<div class="col-md-12">
<div class="panel panel-primary">
  <div class="panel-body">
	  
<?php if ( $property->logo_image != '' ) { ?>
	<div class="pull-right hidden-xs">
		<img itemprop="logo" width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url') . $property->logo_image; ?>">
	</div>
<?php } elseif ( isset( $property->pri_logo_image ) && $property->pri_logo_image != '' ) { ?>
	<div class="pull-right hidden-xs">
		<img itemprop="logo" width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url') . $property->pri_logo_image; ?>">
	</div>
<?php } else { ?>
	<div class="pull-right hidden-xs">
		<img width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo base_url() .'assets/images/photo-icon.png'; ?>">
	</div>
<?php } ?>
<h2 class="title"><span itemprop="name"><?php echo $property->re_title ; ?></span>

<?php if( $property->address != "") { ?>
<small><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress"><?php echo $property->address; ?></span>
</span>
</small>
<?php } elseif( isset($property->pri_address) && $property->pri_address != "") { ?>
<small><span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
<span itemprop="streetAddress"><?php echo $property->pri_address; ?></span>
</span>
</small>
<?php } ?>
</h2>
<span class="abstract">
<?php if( $property->abstract != "") { 
		echo $property->abstract; 
	} elseif( isset($property->pri_abstract ) && $property->pri_abstract != "") { 
		echo $property->pri_abstract; 
	} ?>
</span>

		</div>
	
</div>
       
       </div>
</div>
       
      <div class="row">
		<div class="col-sm-12 col-md-12">

<?php if(count( $children ) > 0) { ?>
<div class="panel panel-success" id="available-slots">
<div class="panel-heading">

	<h3 class="panel-title"><i class="glyphicon glyphicon-home"></i> Related Properties</h3>
</div>
<div class="list-group">
	<?php foreach( $children as $child ) { ?>
  <a href="<?php echo site_url(array("property", $child->re_id, $child->re_slug)); ?>" class="list-group-item">
    <h4 class="list-group-item-heading"><?php echo $child->re_title; ?></h4>
  </a>
  <?php } ?>
</div>
</div>
<?php } // Available Slots check ?>

<?php bootstrap_pagination( $pages, $current_page, current_url() . "?", 10 ); ?>

			</div>
      </div>  <!-- /row -->

    </div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
       
       
<div class="row">
		<div class="col-md-12">
<div class="panel panel-primary">
  <div class="panel-body">
	  
<?php if ( $property->logo_image != '' ) { ?>
	<div class="pull-right hidden-xs">
		<img itemprop="logo" width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url') . $property->logo_image; ?>">
	</div>
<?php } else { ?>
	<div class="pull-right hidden-xs">
		<img width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo base_url() .'assets/images/photo-icon.png'; ?>">
	</div>
<?php } ?>
<h2 class="title"><span itemprop="name"><?php echo $property->re_title ; ?></span>

<?php if( $property->project_name != "") { ?>
<small><?php echo $property->project_name; ?></small>
<?php } ?>

</h2>

<p>
<?php if( $property->price != '' ) { ?><span class="attr" title="Price"><i class="attr-icon price"></i> <?php echo "&#8369;" . number_format($property->price,0); ?></span><?php } ?>
		
		<?php if( $property->beds != '' ) { ?><span class="attr" title="Number of Bedrooms"><i class="attr-icon beds"></i> <?php echo $property->beds; ?></span><?php } ?>
		
		<?php if( $property->baths != '' ) { ?><span class="attr"  title="Number of Bathrooms"><i class="attr-icon baths"></i> <?php echo $property->baths; ?></span><?php } ?>
		
		<?php if( $property->lot_area != '' ) { ?><span class="attr"  title="Lot Area"><i class="attr-icon lot_area"></i> <?php echo $property->lot_area; ?></span><?php } ?>
		
         <?php if( $property->floor_area != '' ) { ?><span class="attr"  title="Floor Area"><i class="attr-icon floor_area"></i> <?php echo $property->floor_area; ?></span><?php } ?>
</p>
<span class="abstract">
<?php if( $property->abstract != "") { echo $property->abstract; } ?>
</span>

<?php if( count($property_types) > 0 ) {
	echo "<p>";
	foreach( $property_types as $property_type ) {
		if ($property_type->link != '') {
			echo "<a href=\"{$property_type->link}\" class=\"label label-info\">{$property_type->label}</a> ";
		} else {
			echo "<span class=\"label label-info\">{$property_type->label}</span> ";
		}
	}
	echo "</p>";
} ?>
		</div>
	 <div class="panel-footer">
		 <div class="btn-group btn-group-justified hidden-xs">
			 <?php if(count( $gallery ) > 0) { ?>
			 <a href="#photos" class="btn btn-primary smooth-scroll"><i class="glyphicon glyphicon-picture"></i> Photos</a>
			 <?php } ?>
			 <?php if($property->description != '') { ?>
			 <a href="#quick-facts" class="btn btn-warning smooth-scroll"><i class="glyphicon glyphicon-info-sign"></i> Quick Facts</a>
			 <?php } ?>
			 <?php if(count( $children ) > 0) { ?>
			 <a href="#available-slots" class="btn btn-success smooth-scroll"><i class="glyphicon glyphicon-ok"></i> Available Slots</a>
			 <?php } ?>
			<?php if( ($property->map_lat != '') && ($property->map_long != '') ) { ?>
			<a href="#map" class="btn btn-danger smooth-scroll"><i class="glyphicon glyphicon-map-marker"></i> Map</a>
			<?php } ?>
		</div>
		</div>
</div>
       
       </div>
</div>
       
      <div class="row">
		<div class="col-sm-12 col-md-8">
<?php if(count( $gallery ) > 0) { ?>
<div class="panel panel-primary" id="photos">
<div class="panel-heading">
	<h3 class="panel-title"><i class="glyphicon glyphicon-picture"></i> Photos</h3>
</div>
<div class="panel-body">
    <div class="row">
		<?php foreach( $gallery as $photo ) { ?>
		<div class="col-md-3">
			<a href="<?php echo get_settings_value('upload_url') . $photo->img_file; ?>" class="thumbnail fancybox" rel="gallery" title="<?php echo $photo->media_caption; ?>">
			  <?php 
				$thumbnail = get_settings_value('upload_url') . (($photo->img_thumb != '') ? $photo->img_thumb : $photo->img_file);
			  ?>
			  <img alt="image" class="img-responsive lazy" data-original="<?php echo $thumbnail; ?>">
			</a>
		</div>
		<?php } ?>
    </div>
</div>
</div>
<?php } // gallery check ?>

<?php if($property->description != '') { ?>
	
<div class="panel panel-warning" id="quick-facts">
<div class="panel-heading">
	<h3 class="panel-title"><i class="glyphicon glyphicon-info-sign"></i> Quick Facts</h3>
</div>
<div class="panel-body">
    <?php echo $property->description ; ?>
</div>
</div>
<?php } // description check ?>
<?php if(count( $children ) > 0) { ?>
<div class="panel panel-success" id="available-slots">
<div class="panel-heading">
	<a href="<?php echo site_url(array("property", $property->re_id, $property->re_slug, "related")); ?>" class="btn btn-default btn-xs pull-right">Show More</a>
	<h3 class="panel-title"><i class="glyphicon glyphicon-ok"></i> Available Slots</h3>
</div>
<div class="list-group">
	<?php foreach( $children as $child ) { ?>
  <a href="<?php echo site_url(array("property", $child->re_id, $child->re_slug)); ?>" class="list-group-item">
    <h4 class="list-group-item-heading"><?php echo $child->re_title; ?></h4>
  </a>
  <?php } ?>
</div>
</div>
<?php } // Available Slots check ?>



<?php if( (get_settings_value('GOOGLE_API_KEY') != '') && ($property->map_lat != '') && ($property->map_long != '') ) { ?>
<div class="panel panel-danger" id="map">
<div class="panel-heading">
	<h3 class="panel-title"><i class="glyphicon glyphicon-map-marker"></i> Map</h3>
</div>
    <div id="map-canvas" style="height: 300px; margin: 0; padding: 0;"></div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo get_settings_value('GOOGLE_API_KEY'); ?>"></script>
<script type="text/javascript">
      function initialize() {
        var mapOptions = {
          center: { lat: <?php echo $property->map_lat; ?>, lng: <?php echo $property->map_long; ?> },
          zoom: 15
        };
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        marker = new google.maps.Marker({
			map:map,
			draggable:false,
			animation: google.maps.Animation.DROP,
			position: { lat: <?php echo $property->map_lat; ?>, lng: <?php echo $property->map_long; ?> },
		});
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    
<?php } // map_lat, map_long check ?>
		</div>

        <div class="col-md-4 hidden-print">
			<?php $this->load->view('realestate/property-sidebar'); ?>
		</div>
      </div>  <!-- /row -->

    </div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

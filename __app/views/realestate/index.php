<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
	  
	  <div class="row">

		<div class="col-md-8">

		<a href="#refine-search" class="btn btn-warning btn-block visible-xs" style="margin-bottom:15px;">Refine Your List</a>

<div class="alert alert-success">
	<p><strong><?php echo ($total_items > 1) ? $total_items . " Properties Found!" : $total_items . " Property Found!"; ?></strong></p>
</div>
<div class="row">
				<?php foreach($properties as $property) { 
					
$property_url = site_url(array('property', $property->re_id, $property->re_slug) );
if ($property->re_type == 'PROJ') { 
	$property_url = site_url(array('realestate', $property->re_slug) );
}
					?>
                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12"> 
						<div class=" bgc-fff box-shad btm-mrg-20 property-listing">
						<?php if ($property->re_type == 'PROJ') { 
							if ($property->available_slots == 1) {
								echo '<span class="ribbon project_status">1 Slot Left</span>';
							} elseif ($property->available_slots > 1) {
								echo '<span class="ribbon project_status">'.$property->available_slots.' Slots</span>';
							} elseif ($property->available_slots == 0) {
								echo '<span class="ribbon project_status">Completed</span>';
							}
						} ?>
						<?php if( $property->listing_type == 'sale' ) { 
							echo '<span class="ribbon sale">For Sale</span>'; 
							if( $property->is_rent == 'rent' ) {
								echo '<span class="ribbon ribbon2 rent">For Rent</span>';
							}
						} ?>
						<?php if( $property->listing_type == 'rent' ) { 
							echo '<span class="ribbon rent">For Rent</span>'; 
							if( $property->is_sale == 'sale' ) {
								echo '<span class="ribbon ribbon2 sale">For Sale</span>';
							}
						} ?>
						
                        <?php if( $property->listing_type == 'assume' ) { echo '<span class="ribbon assume">For Assume</span>'; } ?>
                        
                        <div class="row inner">
							<div class="col-md-4">
								<div class="thumb">
								<a href="<?php echo $property_url; ?>" target="_parent" title="<?php echo  $property->re_title; ?>">
<?php if ( $property->thumbnail != '' ) { ?>
	 <img alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $property->thumbnail; ?>">
<?php } elseif ( $property->pri_thumbnail != '' ) { ?>
	 <img alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $property->pri_thumbnail; ?>">
<?php } else { ?>
	<img alt="image" class="img-responsive lazy no-image" data-original="<?php echo base_url() .'assets/images/photo-icon.png'; ?>">
<?php } ?> </a>					</div>
							</div>
							<div class="col-md-8">
								<div class="info">
								<h4 class="title">
                                  <a href="<?php echo $property_url; ?>" target="_parent" title="<?php echo  $property->re_title; ?>"><?php echo  $property->re_title; ?></a>
                                </h4>
                                <?php if( $property->address != '' ) { ?><p class="attr"  title="Location"><i class="property-icon map-marker"></i> <?php echo $property->address; ?></p>
								<?php } elseif( $property->pri_address != '' ) { ?><p class="attr"  title="Location"><i class="property-icon map-marker"></i> <?php echo $property->pri_address; ?></p>
								<?php } ?>
                                
                                <?php 
$rate = array();
if( $property->price != '' ) { 
	$rate[] = "&#8369;" . number_format($property->price,0);
} elseif( $property->pri_price != '' ) { 
	$rate[] = "&#8369;" . number_format($property->pri_price,0);
}
if( $property->rent != '' ) { 
	$rate[] = "&#8369;" . number_format($property->rent,0);
}

?>

<?php if( count($rate) > 0 ) { ?>
	<p class="attr"  title="Monthly Rent"><i class="property-icon price"></i> <?php echo implode(" / ", $rate); ?></p>
<?php } ?>
                                
                                <?php if( $property->beds != '' ) { ?>
									<p class="attr" title="Number of Bedrooms"><i class="property-icon beds"></i> <?php echo $property->beds; ?></p>
								<?php } elseif( $property->pri_beds != '' ) { ?>
									<p class="attr" title="Number of Bedrooms"><i class="property-icon beds"></i> <?php echo $property->pri_beds; ?></p>
                                <?php } ?>
                                
                                <?php if( $property->baths != '' ) { ?><p class="attr"  title="Number of Bathrooms"><i class="property-icon baths"></i> <?php echo $property->baths; ?></p>
								<?php } elseif( $property->pri_baths != '' ) { ?><p class="attr"  title="Number of Bathrooms"><i class="property-icon baths"></i> <?php echo $property->pri_baths; ?></p><?php } ?>
                                
                                <?php if( $property->lot_area != '' ) { ?><p class="attr"  title="Lot Area"><i class="property-icon lot_area"></i> <?php echo $property->lot_area; ?></p>
								<?php } elseif( $property->pri_lot_area != '' ) { ?><p class="attr"  title="Lot Area"><i class="property-icon lot_area"></i> <?php echo $property->pri_lot_area; ?></p>
								<?php } ?>
                                
                                <?php if( $property->floor_area != '' ) { ?><p class="attr"  title="Floor Area"><i class="property-icon floor_area"></i> <?php echo $property->floor_area; ?></p>
								<?php } elseif( $property->pri_floor_area != '' ) { ?><p class="attr"  title="Floor Area"><i class="property-icon floor_area"></i> <?php echo $property->pri_floor_area; ?></p>
								<?php } ?>
								
								<?php if( $property->abstract != '' ) { ?><p class="abstract small hidden-xs hidden-sm hidden-md hidden-lg"><?php echo substr($property->abstract, 0, 160); ?></p><?php } ?>
								
								<?php if( ($property->re_type == 'PROJ') && ($property->abstract != '') ) { ?><p class="abstract small hidden-xs"><?php echo substr($property->abstract, 0, 300); ?></p><?php } ?>
                                </div>
							
							<?php if( count( $property->children ) > 0 ) { ?>
							<ul class="list-unstyled children">
								<?php foreach( $property->children as $child ) { ?>
									<li><a href="<?php echo site_url(array("property", $child->re_id, $child->re_slug)); ?>"><?php echo $child->re_title; ?></a></li>
								<?php } ?>
							</ul>	
							<?php } ?>
								
							</div>
							<?php if( count( $property->children ) == 0 ) { ?>
							<div class="footer col-md-8 hidden-xs hidden-sm">
								
								<a class="btn btn-primary btn-xs pull-left" href="<?php echo site_url(array('realestate', $property->re_slug, "contact") ); ?>"><i class="glyphicon glyphicon-envelope"></i> Contact Seller</a>
								<a class="btn btn-primary btn-xs pull-right" href="<?php echo $property_url; ?>">Show Details <i class="glyphicon glyphicon-play"></i></a>
								
							</div>
							<?php } ?>
							
                        </div>
                        
                    </div><!-- End Listing-->
					
					
				</div>
				<?php } ?>
				
            </div><!-- End row -->


<?php bootstrap_pagination( $pages, $current_page, current_url() . "?", 10 ); ?>


		</div>
		
		<div class="col-sm-6 col-md-4">
			<?php $this->load->view('realestate/searchform'); ?>
		</div>
		
      </div>  <!-- /row -->
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

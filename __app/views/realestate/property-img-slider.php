<?php if(count($slider_images)>0) { ?>
<ul id="property-slider">
	<?php foreach( $slider_images as $slider_img ) { 
		if($slider_img->re_med_group == 'property_image_slider') { 
			?>
		<li><img src="<?php echo get_settings_value('upload_url') . $slider_img->file_name; ?>" /></li>
	<?php } 
	} ?>
</ul>

<div id="property-slider-thumbnails"  style="margin-top:-40px;margin-bottom:20px;">
	<?php $n=0;
	foreach( $slider_images as $slider_img ) { 
		if($slider_img->re_med_group == 'property_image_slider_thumb') { 
			?>
		<a data-slide-index="<?php echo $n; ?>" href="#"><img src="<?php echo get_settings_value('upload_url') . $slider_img->file_name; ?>" /></a>
	<?php $n++; } 
	} ?>
</div>

<?php } ?>

<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
       
      <div class="row">

        <div class="col-md-8">

		<h2><?php echo $property->re_title ; ?> </h2>


<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="glyphicon glyphicon-envelope"></i> Ask about this property</h3>
  </div>
  <div class="panel-body">
<?php if( $message_sent === TRUE ) { ?>

<div class="alert alert-success alert-dismissable">
  <strong>Your message has been sent to the seller!</strong> 
</div>

<?php } else { ?>
	
<?php if( $alert===TRUE ) { ?>
  <div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
</div>
<?php } ?>

<form action="<?php echo site_url('realestate/' . $property->re_slug . '/contact'); ?>" method="POST">
  <div class="form-group">
    <label>Name</label>
    <input name="name" type="text" class="form-control" placeholder="Enter Your Name Here" value="<?php echo ($this->input->post('name') != "") ? $this->input->post('name') : $this->session->userdata('meta_name');  ?>">
  </div>
  <div class="form-group">
    <label>Phone</label>
    <input name="phone" type="phone" class="form-control" placeholder="Enter Your Phone" value="<?php echo ($this->input->post('phone')) ? $this->input->post('phone') : $this->session->userdata('meta_phone'); ?>">
  </div>
  <div class="form-group">
    <label>Email</label>
    <input name="email" type="email" class="form-control" placeholder="Enter Your Email" value="<?php echo ($this->input->post('email')) ? $this->input->post('email') : $this->session->userdata('meta_email'); ?>">
  </div>
  <div class="form-group">
    <label>Message</label>
    <textarea name="message" class="form-control" rows="5" placeholder="What's Your Message"><?php echo ($this->input->post('message')!==FALSE) ? $this->input->post('message') : 'Hello, I found your listing on InDavao.Net. Please send me more information about '. $property->re_title.'. Thank you!'; ?></textarea>
  </div>
  <button type="submit" class="btn btn-warning btn-block btn-lg">Submit</button>
</form>
<?php } ?>
  </div>
</div>


<div class="panel panel-info" id="seller-info">
   <div class="panel-heading">
    <h3 class="panel-title"><i class="glyphicon glyphicon-earphone"></i> Contact Us</h3>
    
  </div>
  <div class="panel-body">
	  <img class="lazy img-circle pull-right" data-original="<?php echo get_settings_value('default_seller_image'); ?>">
	  <p class="h3 name"><strong><?php echo get_settings_value('default_seller_name'); ?></strong></p>
	  <p class="h5"><?php echo get_settings_value('default_seller_info'); ?></p>
  </div>
  <div class="panel-footer">
	  <a href="mailto:<?php echo get_settings_value('default_seller_email', 'agent@indavao.net'); ?>" class="btn btn-success btn-xs pull-right">Email Agent</a>
	  <span class="phone"><i class="glyphicon glyphicon-earphone"></i> <?php echo get_settings_value('default_seller_phone'); ?></span>
	  </div>
</div>


      </div>  <!-- /col-md-6 -->
		<div class="col-md-4">
<?php if(count( $children ) > 0) { ?>
<div class="panel panel-success" id="available-slots">
<div class="panel-heading">
	<h3 class="panel-title"><i class="glyphicon glyphicon-home"></i> Related Properties</h3>
</div>
<div class="list-group">
	<?php foreach( $children as $child ) { ?>
  <a href="<?php echo site_url(array("property", $child->re_id, $child->re_slug)); ?>" class="list-group-item">
    <h4 class="list-group-item-heading"><?php echo $child->re_title; ?></h4>
  </a>
  <?php } ?>
	<a href="<?php echo site_url(array("property", $property->re_id, $property->re_slug, "related")); ?>" class="list-group-item">
		Show More
	</a>
</div>
</div>
<?php } ?>

<?php if( (($property->map_lat != '') && ($property->map_long != '')) 
			|| (( isset($property->pri_map_lat) && $property->pri_map_lat != '') && ( isset($property->pri_map_long) && $property->pri_map_long != '')) ) { 
	
	$property_map_lat = '7.079733';
	$property_map_long = '125.506384';
	
	if( ($property->map_lat != '') && ($property->map_long != '') ) {
		$property_map_lat = $property->map_lat;
		$property_map_long = $property->map_long;
	} elseif( ($property->pri_map_lat != '') && ($property->pri_map_long != '') ) {
		$property_map_lat = $property->pri_map_lat;
		$property_map_long = $property->pri_map_long;
	}
?>
<div class="panel panel-danger" id="map">
<div class="panel-heading">
	<h3 class="panel-title"><i class="glyphicon glyphicon-map-marker"></i> Map</h3>
</div>
    <a href="<?php echo site_url(array("property", $property->re_id, $property->re_slug, "map")); ?>"><img src="https://maps.googleapis.com/maps/api/staticmap?center=<?php echo $property_map_lat; ?>,<?php echo $property_map_long; ?>&zoom=10&size=400x200&scale=2&markers=color:red%7C<?php echo $property_map_lat; ?>,<?php echo $property_map_long; ?>&key=<?php echo get_settings_value('GOOGLE_API_KEY'); ?>" class="img-responsive lazy" /></a>
</div>

<?php } // map_lat, map_long check ?>

		</div>
      </div>  <!-- /row -->

    </div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

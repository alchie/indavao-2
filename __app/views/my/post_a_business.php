<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
		
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#">Post A Business</a></li>
</ul>

<div class="brdr bgc-fff pad-10 box-shad" id="tasks">

<?php if( $this->input->get('new_business') == FALSE ) { ?>

<div class="page-header" style="margin-top:0;">
	<h1>Post A Business</h1>
</div>

<p class="text-right">*<small> Required</small></p>

<form method="post" action="<?php echo current_url(); ?>">
	<input type="hidden" name="action" value="post_a_business">
<div class="panel panel-default">
   
<div class="panel-body">
	
<div class="form-group">
    <label for="business_name">Business Name *</label>
    <input type="text" class="form-control" id="business_name" placeholder="Enter Business Name" name="business_name" value="">
</div>

<div class="form-group">
    <label for="business_address">Address *</label>
    <input type="text" class="form-control" id="business_address" placeholder="Enter Address" name="business_address" value="">
</div>

<div class="form-group">
    <label for="business_phone">Phone Number *</label>
    <input type="text" class="form-control" id="business_phone" placeholder="Enter Phone Number" name="business_phone" value="">
</div>

<div class="form-group">
    <label for="business_address">Description</label>
    <input type="text" class="form-control" id="business_description" placeholder="Enter Description" name="business_description" value="">
</div>

<div class="form-group">
    <label for="business_phone">Fax Number</label>
    <input type="text" class="form-control" id="business_fax" placeholder="Enter Fax Number" name="business_fax" value="">
</div>

<div class="form-group">
    <label for="business_phone">Website</label>
    <input type="text" class="form-control" id="business_website" placeholder="Enter Website" name="business_website" value="">
</div>

<div class="form-group">
    <label for="business_phone">Email</label>
    <input type="text" class="form-control" id="business_email" placeholder="Enter Email" name="business_email" value="">
</div>

    </div>
<div class="panel-footer">
	<button class="btn btn-success" type="submit">Submit</button>
</div>
  </div>
</form>
	
<?php } else { ?>

<div class="alert alert-success">
   <strong>Business Successfully Submitted!</strong> 
</div>

<div class="page-header" style="margin-top:0;">
	<h1><?php echo $directory->dir_name ?></h1>
	<p>Thank you so much for your submission! You can show share your business on your Timeline.</p>
</div>

	<form method="post" action="<?php echo current_url(); ?>?new_business=<?php echo $directory->dir_id; ?>">
		<input type="hidden" name="action" value="share_to_facebook">
		<input type="hidden" name="business_name" value="<?php echo $directory->dir_name ?>">
		<input type="hidden" name="url" value="<?php echo site_url(array("company", $directory->dir_id, $directory->dir_slug, $this->session->userdata('user_id'))); ?>">
		<button class="btn btn-primary btn-lg" type="submit">Share on Facebook >></button>
	</form>

	
<?php } ?>
</div>
		</div>
	</div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
           <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="<?php echo site_url("my/{$current_user_id}/bookmarks"); ?>">My Bookmarks <span class="badge"><?php echo $activity_stats->total_bookmarks; ?></span></a></li>
          </ul>
              

<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="bookmarks">

<?php if( $bookmarks ) { ?>
				<div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Object Title</th>
                      <th class="text-center" width="20%">Type</th>
                      <th class="text-center" width="10%">Remove</th>
                    </tr>
                  </thead>
                  <tbody id="bookmark-items">
					  <?php foreach($bookmarks as $bookmark ) { 
						if( $bookmark->object_type == 'realestate' ) {
							$bookmark_url = site_url(array("property", $bookmark->object_id, $bookmark->re_slug));
							?>
							<tr id="bookmark-item-<?php echo $bookmark->bookmark_id; ?>">
								<td><a href="<?php echo $bookmark_url; ?>" target="_blank"><?php echo $bookmark->re_title; ?></td>
								<td class="text-center">Property</td>
								<td class="text-center"><button class="btn btn-danger btn-xs remove-bookmark" data-id="<?php echo $bookmark->bookmark_id; ?>"><i class="glyphicon glyphicon-remove"></i></button></td>
							</tr>
							<?php
						}
						elseif( $bookmark->object_type == 'business' ) {
							$bookmark_url = site_url(array("company", $bookmark->object_id, $bookmark->dir_slug));
							?>
							<tr id="bookmark-item-<?php echo $bookmark->bookmark_id; ?>">
								<td><a href="<?php echo $bookmark_url; ?>" target="_blank"><?php echo $bookmark->dir_title; ?></td>
								<td class="text-center">Local Business</td>
								<td class="text-center"><button class="btn btn-danger btn-xs remove-bookmark" data-id="<?php echo $bookmark->bookmark_id; ?>"><i class="glyphicon glyphicon-remove"></i></button></td>
							</tr>
							<?php
						}
					  ?>
						<?php } ?>
				</tbody>
				</table>
				

<?php if( $pages > 1 ) { ?>
	<hr>
   <nav class="text-center">
  <ul class="pagination">
  <?php for($i=1;$i<=$pages;$i++) { 
		if($current_page == $i) {
			echo '<li class="active"><a href="#current-page" DISABLED>'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.site_url("my/{$current_user_id}/bookmarks").'?page='.$i.'">'.$i.'</a></li>';
		}
  }
  ?>
  </ul>
</nav>
<?php } ?>
<script>
<!--
$(function() {
	$('.remove-bookmark').click(function(){
		var self = $(this);
		var bookmark_id = self.attr('data-id');
		$.post(ajaxPostURL, {
			bookmark_id : bookmark_id,
			action : 'remove_bookmark',
		}, function(msg) {
			console.log( msg );
			if( msg.error == false ) {
				$('#bookmark-item-'+msg.bookmark_id).fadeOut('slow', function(){
					$(this).remove();
				});
			}
		}, 'json').fail(function(xhr, textStatus, errorThrown){
			console.log( xhr.responseText );
		});
		
	});
});
-->
</script>
				</div>
<?php } else { ?>
<p class="alert alert-danger text-center"><strong>No Bookmarks Found!</strong></p>
<?php } ?>
</div><!--/tab-pane-->




        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>





<?php if( $this->session->userdata('isPoints') ) { ?>
<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">My Points</h3>
  </div>
	<div class="list-group">
            <a href="<?php echo site_url("my/{$current_user_id}/points"); ?>" class="list-group-item text-right <?php echo ($sub_page == 'points') ? 'active' : ''; ?>"><span class="pull-left"><strong>Total Points</strong></span> <span id="total-points-<?php echo $this->session->userdata('user_id'); ?>"><?php echo ($activity_stats->sum_points) ? $activity_stats->sum_points : 0; ?></span></a>
            <a href="<?php echo site_url("my/{$current_user_id}/redemptions"); ?>" class="list-group-item text-right <?php echo ($sub_page == 'redemptions') ? 'active' : ''; ?>"><span class="pull-left "><strong>Redemptions</strong></span> <?php echo ($activity_stats->sum_withdrawals) ? $activity_stats->sum_withdrawals : 0; ?></a>
            <span class="list-group-item text-right" style="font-size:20px;"><span class="pull-left"><strong>Balance</strong></span> <strong id="total-balance-<?php echo $this->session->userdata('user_id'); ?>"><?php echo ( $activity_stats->sum_points - $activity_stats->sum_withdrawals); ?></strong></span>
    </div>  

</div>
<?php if( $this->session->userdata('manager') ) { ?>

<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">My Tasks</h3>
  </div>
	<div class="list-group">
        <a href="<?php echo site_url("my/{$current_user_id}/tasks"); ?>" class="list-group-item">My Tasks</a>
		<a href="<?php echo site_url("my/{$current_user_id}/tasks_to_friends"); ?>" class="list-group-item">Tasks To Friends</a>
		<a href="<?php echo site_url("my/{$current_user_id}/shares"); ?>" class="list-group-item">My Shares</a>
    </div>  
</div>

<?php } //MANAGER ?>

<?php } else { //ispoints ?>

	<p><a href="<?php echo site_url("my/{$current_user_id}/add_business"); ?>" class="btn btn-success btn-block">Post A Business</a></p>

<?php } //ispoints ?>

	
<?php 

if( count( array_intersect($this->session->userdata('permissions'), array('add_business', 'add_realestate', 'add_jobs') ) ) > 0 ) { ?>
<div class="panel panel-info">
  <div class="panel-heading">
    <h3 class="panel-title">My Posts</h3>
  </div>
<ul class="list-group">
	<?php if( array_search('add_business', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/business"); ?>" class="list-group-item <?php echo ($sub_page == 'business') ? 'active' : ''; ?>">My Business</a>
	<?php } ?>
	
	<?php if( array_search('add_realestate', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/properties"); ?>" class="list-group-item <?php echo ($sub_page == 'properties') ? 'active' : ''; ?>">My Properties</a>
	<?php } ?>
	
	<?php if( array_search('add_jobs', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/jobs"); ?>" class="list-group-item <?php echo ($sub_page == 'jobs') ? 'active' : ''; ?>">My Job Posts</a>
	<?php } ?>
	
</ul>
</div>
<?php } ?>

<?php if( $this->session->userdata('manager') ) { ?>

<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">Manage</h3>
  </div>
<ul class="list-group">


	
	<?php if( array_search('manage_users', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>" class="list-group-item <?php echo ($sub_page == 'manage_users') ? 'active' : ''; ?>">Users</a>
	<?php } ?>
	
	<?php if( array_search('manage_business', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/manage/business"); ?>" class="list-group-item <?php echo ($sub_page == 'manage_business') ? 'active' : ''; ?>">Business</a>
	<?php } ?>
	
	<?php if( array_search('manage_realestate', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/manage/realestate"); ?>" class="list-group-item <?php echo ($sub_page == 'manage_realestate') ? 'active' : ''; ?>">Real Estate</a>
	<?php } ?>
	
	<?php if( array_search('manage_jobs', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/manage/jobs"); ?>" class="list-group-item <?php echo ($sub_page == 'manage_jobs') ? 'active' : ''; ?>">Job Posts</a>
	<?php } ?>
	
	<?php if( array_search('manage_taxonomies', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/manage/taxonomies"); ?>" class="list-group-item <?php echo ($sub_page == 'manage_taxonomies') ? 'active' : ''; ?>">Taxonomies</a>
	<?php } ?>
	
	<a href="<?php echo site_url("my/{$current_user_id}/manage/tasks"); ?>" class="list-group-item <?php echo ($sub_page == 'manage_tasks') ? 'active' : ''; ?>">Tasks</a>
	
</ul>
</div>

<?php } // admin ?>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">My Links</h3>
  </div>
<div class="list-group">
	    <a href="<?php echo site_url("my/{$current_user_id}/account"); ?>" class="list-group-item text-left <?php echo ($sub_page == 'account') ? 'active' : ''; ?>"><strong>Account Settings</strong></a>
	    <?php if( $this->session->userdata('manager') ) { ?>
            <a href="<?php echo site_url("my/{$current_user_id}/referrals"); ?>" class="list-group-item text-right <?php echo ($sub_page == 'referrals') ? 'active' : ''; ?>"><span class="pull-left"><strong>My Referrals</strong></span> <?php echo $activity_stats->total_referrals; ?></a>
        <?php } // manager ?>
            
            <a href="<?php echo site_url("my/{$current_user_id}/bookmarks"); ?>" class="list-group-item text-right <?php echo ($sub_page == 'bookmarks') ? 'active' : ''; ?>"><span class="pull-left"><strong>My Bookmarks</strong></span> <?php echo $activity_stats->total_bookmarks; ?></a>
<?php if( $this->session->userdata('isPoints') ) { ?>
		 <a href="<?php echo site_url("my/{$current_user_id}/facebook_friends"); ?>" class="list-group-item text-right <?php echo ($sub_page == 'facebook_friends') ? 'active' : ''; ?>"><span class="pull-left"><strong>My Facebook Friends</strong></span> &nbsp;</a>
<?php } ?>
	</div>
</div>

<div class="panel panel-default hidden-xs">
  <div class="panel-heading">
    <h3 class="panel-title">Login Record</h3>
  </div>
	<ul class="list-group">
            <li class="list-group-item text-right"><span class="pull-left"><strong>Joined</strong></span> <?php echo $this->session->userdata('date_joined'); ?></li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Last seen</strong></span> <?php echo $this->session->userdata('last_login'); ?></li>
     </ul> 
</div>
<!--
<div class="panel panel-default hidden-xs">
  <div class="panel-body">
  
<fb:login-button max_rows="1" size="medium" data-width="180" show_faces="true" auto_logout_link="true" data-scope="<?php echo implode(",", $facebook_permissions); ?>" onlogin="checkLoginState();">Login</fb:login-button>
</div>
</div>-->
<div class="panel panel-default">
<ul class="list-group">
	 <a href="<?php echo site_url("my/logout"); ?>" class="list-group-item text-left"><strong>Logout</strong></a>    
</ul>
</div>

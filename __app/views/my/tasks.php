<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
<ul class="nav nav-tabs" id="myTab">
	<li class="<?php echo ($this->input->get('type')==FALSE||$this->input->get('type')=='realestate') ? 'active' : ''; ?>"><a href="<?php echo current_url(); ?>?type=realestate">Real Estate</a></li>
	<li class="<?php echo ($this->input->get('type')=='business') ? 'active' : ''; ?>"><a href="<?php echo current_url(); ?>?type=business">Business</a></li>
</ul>




<div class="brdr bgc-fff pad-10 box-shad" id="tasks">

<?php 

if( isset($current_task) ) { 
	
	?>
	
<div class="row" style="margin-top:20px;">
	<div class="col-md-8 col-md-offset-2">
		
<div class="panel panel-primary">
	<div class="panel-heading"><h3 class="panel-title">Share : <?php echo $current_task->title; ?></h3></div>
	<div class="panel-body">
		<div class="form-group">
			<input id="task_share_url" data-message="<?php echo ($current_task->task_message != '') ? $current_task->task_message : $current_task->title; ?>" data-task_id="<?php echo ($current_task->own_profile_post_id=='') ? $this->session->userdata('user_id') : $current_task->tu_task_id; ?>" data-object_id="<?php echo $current_task->object_id; ?>" data-object_type="<?php echo $current_task->type; ?>" class="form-control"  value="<?php echo site_url( array($current_task->type , $current_task->object_id, $current_task->slug , $this->session->userdata('user_id')) ); ?>" onclick="this.select();" type="hidden">
			<p class="text-center text-warning"><strong>NOTE</strong>: Share this URL as a <strong>LINK</strong>. Please <strong>WAIT</strong> for the <strong>PREVIEW</strong> to finish loading before click the post button. This link is your referral link as well. It contains your ID (the numbers in the middle) which will be stored on a browser's cookie when opened. This is to ensure that you will get all the referrals from sharing this link!</p>
			<button class="btn btn-danger btn-block btn-lg" id="tasks_grab_google_shorturl">Show Content To Share</button>
		</div>
	</div>
	
		<div class="panel-heading task_review hidden">
			<a href="<?php echo site_url("redirect"); ?>?url=<?php echo ($current_task->own_profile_post_id=='') ? "http://fb.com/{$this->session->userdata('user_id')}" : $current_task->page_url; ?>" target="_blank" class="btn btn-xs btn-danger pull-right">Open</a>
			<h3 class="panel-title">To : <?php echo ($current_task->own_profile_post_id=='') ? "Your Timeline" : $current_task->page_name; ?></h3></div>
		<div class="panel-body task_review hidden">
			<div class="form-group">
				<input id="task_destination_id" data-page_id="<?php echo ($current_task->own_profile_post_id=='') ? $this->session->userdata('user_id') : $current_task->page_id; ?>" class="form-control" type="text" value="<?php echo ($current_task->own_profile_post_id=='') ? "http://fb.com/{$this->session->userdata('user_id')}" : $current_task->page_url; ?>" onclick="this.select();">
				<p class="text-center text-warning"><strong>NOTE</strong>: Like the page to be able to post the link.</p>
			</div>
		</div>
		<div class="panel-heading task_review hidden"><h3 class="panel-title">Submit Post URL</h3></div>
		<div class="panel-body task_review hidden">
			<div class="form-group">
				<p class="text-center text-warning"><strong>NOTE</strong>: After posting, copy the URL of the time to get Post URL.</p>
				<input id="task_verify_url" class="form-control" type="text" value="" placeholder="Enter Post URL Here">
			</div>
		</div>
		<div class="panel-footer task_review hidden">
			<button class="btn btn-block btn-warning btn-lg" id="task_verify_button" DISABLED>Submit for Review</button>
		</div>
	
</div>

</div>
</div>

<script>
<!--
var taskVerifyPostURL = '<?php echo site_url("my/{$current_user_id}/ajax"); ?>';
var taskURL = '<?php echo current_url(); ?>';

(function($) {
	$('#tasks_grab_google_shorturl').click(function() {
		var longurl = $('#task_share_url').val(),
		message = $('#task_share_url').attr('data-message');
		var apiKey = '<?php echo get_settings_value('GOOGLE_API_KEY'); ?>';
		gapi.client.setApiKey(apiKey);
		
		var textarea = 
		
		gapi.client.load('urlshortener', 'v1', function() {
			var request = gapi.client.urlshortener.url.insert({
				'resource': {
					'longUrl': longurl
				}
			});
			var resp = request.execute(function(resp) {
				var showMessage = function( url ) {
					$('#task_share_url').attr('data-short_url', url);
					$('#tasks_grab_google_shorturl').hide();
					$('<textarea />').addClass('form-control').val( message + " " + url).insertAfter( $('#tasks_grab_google_shorturl') ).click(function(){
						$(this).select();
					});
				}
				if (resp.error) {
					console.log( resp.error.message );
					showMessage( $('#task_share_url').val() );
				} else {
					console.log( resp.id );
					showMessage( resp.id );
				}
				$('.task_review').removeClass('hidden');
			});
		});
	});

	$('#task_verify_button1').click(function(){
					$(this).prop('disabled', true);
					$('#task_error_msg').remove();
					var task_share_url = $('#task_share_url').val(),
					task_short_url = $('#task_share_url').attr('data-short_url'),
					object_id = $('#task_share_url').attr('data-object_id'),
					object_type = $('#task_share_url').attr('data-object_type'),
					task_id = $('#task_share_url').attr('data-task_id'),
					task_destination_id = $('#task_destination_id').attr('data-page_id'),
					task_verify_post_id = $('#task_verify_post_id').attr('data-post_id');
					FB.api("/"+task_verify_post_id,"GET",function (response) {
						console.log( response );
							if (response && !response.error) {
								if(( response.from.id == task_destination_id ) && (response.link == task_share_url)) {
									$.post(taskVerifyPostURL, {
														task_id : task_id,
														object_id : object_id,
														object_type : object_type,
														post_id : task_verify_post_id,
														action : 'task_verified',
													}, function(msg) {
														if( msg.error == false ) {
															window.location.reload();
														}
													}, 'json');
								}
							} else {
								var error_msg = $('#task_error_msg');
								if( error_msg.length == 0 ) {
									error_msg = $('<p />').attr('id', 'task_error_msg').addClass('text-center text-danger').insertAfter( $('#task_verify_button') );
								}
								error_msg.text('Unable to verify your post! Make sure you entered the correct SHARE URL!');
							}
						});
				});
	
	$('#task_verify_url').keyup(function(){
		$('#task_verify_button').prop('disabled', false);
	});
	
	$('#task_verify_button').click(function(){
		
		$(this).prop('disabled', true);
		
		var object_id = $('#task_share_url').attr('data-object_id'),
		object_type = $('#task_share_url').attr('data-object_type'),
		task_id = $('#task_share_url').attr('data-task_id'),
		post_url = $('#task_verify_url').val();
		
		$.post(taskVerifyPostURL, {
			task_id : task_id,
			object_id : object_id,
			object_type : object_type,
			post_url : post_url,
			action : 'task_verified',
		}, function(msg) {
			console.log( msg );
			if( msg.error == false ) {
				window.location.reload();
			}
		}, 'json').fail(function(xhr, textStatus, errorThrown){
				console.log( xhr.responseText );
			});
		
	});
	
	$('#task_verify_url1').keyup(function(){
		var url = $(this).val();
		if( url.length > 0 ) { 
			var url_decoded = decodeURIComponent( url );
			
			var qmark = url_decoded.search(/\?/i);
			
			if( qmark > 0 ) {
				url_decoded = url_decoded.substring((qmark+1), url_decoded.length);
			}

			var share_source_type = url_decoded.search(/&share_source_type=/i);
			if( share_source_type > 0 ) {
				url_decoded = url_decoded.substring(0, share_source_type);
			}
			
			var url_decoded_split = url_decoded.split('&');
			
			var list_group = $('#task_post_id_list');
			if( list_group.length == 0) {
				list_group = $('<div />').addClass('list-group').attr('id', 'task_post_id_list').css('margin-top', '15px');
			}
			for( i in url_decoded_split ) {
				if( $('#task_post_id_list-'+i).length == 0 ) {
					var list_item = $('<a/>').attr('href', 'javascript:void(0);');
					list_item.attr('id', 'task_post_id_list-'+i);
					list_item.addClass('list-group-item');
					list_item.appendTo(list_group);
					
					var line_text = url_decoded_split[i].split('=');
					if( typeof line_text[1] != 'undefined' ) {
						list_item.html( line_text[1] + '<span class="badge">'+line_text[0] +'</span>' );
						list_item.attr('data-post_id', line_text[1]);
					} else {
						list_item.html( url_decoded_split[i] );
						list_item.attr('data-post_id', url_decoded_split[i]);
					}
					list_item.click(function() { 
						var post_id_val = $(this).attr('data-post_id');
						var post_id_div = $('#task_verify_post_id');
				
						if( post_id_div.length == 0) {
							post_id_div = $('<div/>').css('margin-top', '15px').addClass('well well-sm').attr('id', 'task_verify_post_id').insertAfter( $(this) );
						}
						
						post_id_div.html('<strong>POST ID : </strong> ' + post_id_val).attr('data-post_id', post_id_val);
						$('#task_verify_button').prop('disabled', false);
					});
				}
			}
			list_group.insertAfter( $(this) );
		} 
		if( url.length == 0 ) {
			$('#task_verify_post_id').remove();
		}
	});
})(jQuery); 

-->
</script>

<?php } else { ?>
	<p class="alert alert-danger text-center"><strong>No Tasks Found!</strong></p>
<?php } ?>
</div>





        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

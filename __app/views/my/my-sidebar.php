<aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="https://graph.facebook.com/<?php echo $this->session->userdata('user_id'); ?>/picture?type=small" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $this->session->userdata('user_name'); ?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
<?php 
$user_id = $this->session->userdata('user_id');
$user_id_e = md5(sha1($this->session->userdata('user_id')));
$isPoints = $this->session->userdata('isPoints');
$isManager = $this->session->userdata('manager');
$nav_list = array(
	array(
		'id' => 'dashboard',
		'label' => 'Dashboard',
		'icon' => 'fa fa-dashboard',
		'url' => site_url(array('my', $user_id, "dashboard")),
		'children' => array(),
		'isManager' => 0,
		'isPoints'=> 0,
	),
	/*
	array(
		'id' => 'realestate',
		'label' => 'Real Estate',
		'icon' => 'fa fa-home',
		'url' => '#', //site_url(array('my', $user_id, "dashboard")),
		'children' => array(
				array(
					'id' => 'my-properties',
					'label' => 'My Properties',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "properties")),
				),
				array(
					'id' => 'search-properties',
					'label' => 'Search Properties',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "search-properties")),
				),
				array(
					'id' => 'sell-properties',
					'label' => 'Sell Properties',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "sell-properties")),
				),
				array(
					'id' => 'refer-buyer',
					'label' => 'Refer a Buyer',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "refer-property-buyer")),
				),
		),
		'isManager' => 0,
		'isPoints'=> 0,
	),
	array(
		'id' => 'jobs',
		'label' => 'Jobs',
		'icon' => 'fa fa-briefcase',
		'url' => '#', //site_url(array('my', $user_id, "jobs")),
		'children' => array(
				array(
					'id' => 'jobposts',
					'label' => 'My Job Posts',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "jobposts")),
				),
				array(
					'id' => 'resume',
					'label' => 'My Resume',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "resume")),
				),
				array(
					'id' => 'application-letters',
					'label' => 'My Letters',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "application-letters")),
				),
				array(
					'id' => 'search-jobs',
					'label' => 'Search Jobs',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "search-jobs")),
				),
		),
		'isManager' => 0,
		'isPoints'=> 0,
	),
	*/
	array(
		'id' => 'business',
		'label' => 'Business Directory',
		'icon' => 'fa fa-map-marker',
		'url' => '#', //site_url(array('my', $user_id, "jobs")),
		'children' => array(
				array(
					'id' => 'add_business',
					'label' => 'Add a Business',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "add_business")),
				),
				array(
					'id' => 'my-business',
					'label' => 'My Businesses',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "business")),
				),
				/*
				array(
					'id' => 'search-business',
					'label' => 'Search Business',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "search-business")),
				),
				
				array(
					'id' => 'products-services',
					'label' => 'Products and Services',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "products-services")),
				),
				*/
		),
		'isManager' => 0,
		'isPoints'=> 0,
	),
	/*
	array(
		'id' => 'network-marketing',
		'label' => 'Network Marketing',
		'icon' => 'fa fa-sitemap',
		'url' => '#', //site_url(array('my', $user_id, "jobs")),
		'children' => array(
				array(
					'id' => 'my-network',
					'label' => 'My Network',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "network")),
				),
				array(
					'id' => 'my-friends',
					'label' => 'My Friends',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "friends")),
				),
				array(
					'id' => 'my-referrals',
					'label' => 'My Referrals',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "referrals")),
				),
		),
		'isManager' => 0,
		'isPoints'=> 0,
	),
	*/
	array(
		'id' => 'account',
		'label' => 'My Account',
		'icon' => 'fa fa-user',
		'url' => '#', //site_url(array('my', $user_id, "jobs")),
		'children' => array(
				array(
					'id' => 'account-settings',
					'label' => 'Account Settings',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "account")),
				),
				array(
					'id' => 'profile-settings',
					'label' => 'Profile Settings',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "profile")),
				),
				/*
				array(
					'id' => 'messages',
					'label' => 'Messages',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "messages")),
				),
				array(
					'id' => 'points',
					'label' => 'My Points',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "points")),
					'isPoints'=> 1,
				),
				*/
		),
		'isManager' => 0,
		'isPoints'=> 0,
	),
	/*
	array(
		'id' => 'manage',
		'label' => 'Manage Site',
		'icon' => 'fa fa-wrench',
		'url' => '#', //site_url(array('my', $user_id, "jobs")),
		'children' => array(
				array(
					'id' => 'manage-users',
					'label' => 'Users',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "manage", "users")),
				),
				array(
					'id' => 'manage-realestate',
					'label' => 'Real Estate',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "manage", "realestate")),
				),
				array(
					'id' => 'manage-jobs',
					'label' => 'Jobs',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "manage", "jobs")),
				),
				array(
					'id' => 'manage-business',
					'label' => 'Business',
					'icon' => '',
					'url' => site_url(array('my', $user_id_e, "manage", "business")),
				),
		),
		'isManager' => 1,
		'isPoints'=> 0,
	),
	*/
); 
foreach( $nav_list as $nav ) {
	if( isset($nav['isManager']) && $nav['isManager'] == 1 && $isManager != 1 ) {
		continue;
	}
	if( isset($nav['isPoints']) && $nav['isPoints'] == 1 && $isPoints != 1 ) {
		continue;
	}
	$treeview = '';
	$active = '';
	if( count( $nav['children'] ) > 0 ) {
		$treeview = 'treeview';
	}
	if( isset($sidebar_nav) && $nav['id'] == $sidebar_nav ) {
		$active = 'active';
	}
echo "<li class=\"{$treeview} {$active}\">
<a href=\"{$nav['url']}\" id=\"sidebar-menu-{$nav['id']}\">
<i class=\"{$nav['icon']}\"></i> <span>{$nav['label']}</span>";
if( count( $nav['children'] ) > 0 ) {
	echo "<i class=\"fa fa-angle-left pull-right\"></i>";
}
echo "</a>";
if( count( $nav['children'] ) > 0 ) {
	echo "<ul class=\"treeview-menu\">";
	foreach( $nav['children'] as $nav_child ) {
		if( isset($nav['isManager']) && $nav['isManager'] == 1 && $isManager != 1 ) {
			continue;
		}
		if( isset($nav['isPoints']) && $nav['isPoints'] == 1 && $isPoints != 1 ) {
			continue;
		}
		echo "<li><a href=\"{$nav_child['url']}\"><i class=\"fa fa-angle-double-right\"></i> {$nav_child['label']}</a></li>";
	}
	echo "</ul>";
}
echo "</li>";

 } ?> 
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

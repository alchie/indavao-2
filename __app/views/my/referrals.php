<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
           <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="<?php echo site_url("my/{$current_user_id}/referrals"); ?>">My Referrals <span class="badge"><?php echo $activity_stats->total_referrals; ?></span></a></li>
          </ul>
              
         
<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="referrals">
	<div class="well">
		<div class="input-group">
  <span class="input-group-addon">Your Referral Link: </span>
  <input type="text" class="form-control" value="<?php echo site_url('r/'. $this->session->userdata('user_id') ); ?>" onclick="this.focus();this.select();">
  <span class="input-group-addon">
  <div class="fb-like" data-href="<?php echo site_url('r/'. $this->session->userdata('user_id') ); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
  </span>
</div>
</div>
<?php if( $referrals ) { ?>
				<div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>User Name</th>
                      <th class="text-center" width="10%">Referrals</th>
                      <?php if( $this->session->userdata('isPoints') ) { ?>
						<th class="text-center" width="10%">Shares</th>
                      <?php } ?>
                    </tr>
                  </thead>
                  <tbody id="tasks-items">
					  <?php foreach($referrals as $referral ) { ?>
						<tr>
							<td><?php echo $referral->name; ?></td>
							<td class="text-center"><?php echo $referral->total_referrals; ?></td>
							<?php if( $this->session->userdata('isPoints') ) { ?>
								<td class="text-center"><?php echo $referral->total_shares; ?></td>
							<?php } ?>
						</tr>
						<?php } ?>
				</tbody>
				</table>
				
<hr>

                  <?php if( $pages > 1 ) { ?>
   <nav class="text-center">
  <ul class="pagination">
  <?php for($i=1;$i<=$pages;$i++) { 
		if($current_page == $i) {
			echo '<li class="active"><a href="#current-page" DISABLED>'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.site_url('my/referrals').'?page='.$i.'">'.$i.'</a></li>';
		}
  }
  ?>
  </ul>
</nav>
<?php } ?>
				
				</div>
<?php } else { ?>
<p class="alert alert-danger text-center"><strong>No Referral Found!</strong></p>
<?php } ?>
</div><!--/tab-pane-->

          

        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

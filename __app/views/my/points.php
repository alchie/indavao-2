<?php $this->load->view('adminlte/overall_header'); ?>
<?php $this->load->view('my/my-sidebar'); ?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			My Account
			<small>My Points</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url("my/{$current_user_id}/dashboard"); ?>"><i class="fa fa-dashboard"></i> My Account</a></li>
			<li class="active">My Points</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	
		<div class="row">
                        <div class="col-lg-4 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        <?php echo ( $activity_stats->sum_points - $activity_stats->sum_withdrawals); ?>
                                    </h3>
                                    <p>
                                        Balance
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-4 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        <?php echo ($activity_stats->sum_points) ? $activity_stats->sum_points : 0; ?>
                                    </h3>
                                    <p>
                                        Points
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-plus-circle"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-4 col-xs-4">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        <?php echo ($activity_stats->sum_withdrawals) ? $activity_stats->sum_withdrawals : 0; ?>
                                    </h3>
                                    <p>
                                        Withdrawals
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-minus-circle"></i>
                                </div>
                                <a href="#" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                    </div>

	</section><!-- /.content -->
</aside><!-- /.right-side -->

<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
           <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#">My Points <span class="badge"><?php echo ($activity_stats->sum_points) ? $activity_stats->sum_points : 0; ?></span></a></li>
          </ul>


<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="referrals">

<?php if( $points ) { ?>
				<div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
					<th class="text-center" width="20%">Date Earned</th>
                      <th>Desription</th>
                      <th class="text-center" width="10%">Points Earned</th>
                    </tr>
                  </thead>
                  <tbody id="tasks-items">
					  <?php foreach($points as $point ) { 
							switch($point->points_type) {
								case 'REF':
									echo "<tr>";
									echo "<td>{$point->date_added}</td>";
									echo "<td>Referral Points</td>";
									echo "<td class=\"text-center\">{$point->points_credited}</td>";
									echo "</tr>";
								break;
								case 'SHARE':
									if( $point->tv_object_type == '' ) {
										echo "<tr class=\"danger\">";
									} else {
										echo "<tr>";
									}
									echo "<td>{$point->date_added}</td>";
									if( $point->tv_object_type == 'realestate' ) {
										echo "<td>Task Points - {$point->re_title}</td>";
									} else {
										echo "<td><em>Something is wrong here! Your points will be adjusted accordingly!</em></td>";
									}
									if( $point->tv_object_type == '' ) {
										echo "<td class=\"text-center\">(-{$point->points_credited})</td>";
									} else {
										echo "<td class=\"text-center\">{$point->points_credited}</td>";
									}
									echo "</tr>";
								break;
								case 'DIR_APRVD':
									echo "<tr>";
									echo "<td>{$point->date_added}</td>";
									echo "<td><strong>Business Approved</strong> - {$point->dir_name}</td>";
									echo "<td class=\"text-center\">{$point->points_credited}</td>";
									echo "</tr>";
								break;
							}
						} ?>
				</tbody>
				</table>

                  <?php if( $pages > 1 ) { ?>
					  <hr>
   <nav class="text-center">
  <ul class="pagination">
  <?php for($i=1;$i<=$pages;$i++) { 
		if($current_page == $i) {
			echo '<li class="active"><a href="#current-page" DISABLED>'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.site_url('my/referrals').'?page='.$i.'">'.$i.'</a></li>';
		}
  }
  ?>
  </ul>
</nav>
<?php } ?>
				
				</div>
<?php } else { ?>
<p class="alert alert-danger text-center"><strong>You haven't yet earned any points! Try referring someone!</strong></p>
<?php } ?>
</div><!--/tab-pane-->


        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('adminlte/overall_footer'); ?>

<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
           <ul class="nav nav-tabs" id="myTab">
           <li class="active"><a href="#">My Redemptions <span class="badge"><?php echo ($activity_stats->sum_withdrawals) ? $activity_stats->sum_withdrawals : 0; ?></span></a></li>
          </ul>


<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="referrals">

<?php if( $redemptions ) { ?>
				<div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
					<th class="text-center" width="20%">Date Redeemed</th>
                      <th>Reason for Redemption</th>
                      <th class="text-center" width="10%">Points Redeemed</th>
                    </tr>
                  </thead>
                  <tbody id="tasks-items">
					  <?php foreach($redemptions as $redemption ) { ?>
						<tr>
							<td><?php echo $redemption->date_withdrawn; ?></td>
							<td><?php echo $redemption->withdrawal_reason; ?></td>
							<td class="text-center"><?php echo $redemption->points_withdrawn; ?></td>
						</tr>
						<?php } ?>
				</tbody>
				</table>
				

                  <?php if( $pages > 1 ) { ?>
<hr>
   <nav class="text-center">
  <ul class="pagination">
  <?php for($i=1;$i<=$pages;$i++) { 
		if($current_page == $i) {
			echo '<li class="active"><a href="#current-page" DISABLED>'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.site_url('my/referrals').'?page='.$i.'">'.$i.'</a></li>';
		}
  }
  ?>
  </ul>
</nav>
<?php } ?>
				
				</div>
<?php } else { ?>
<p class="alert alert-danger text-center"><strong>No Points Redemptions!</strong></p>
<?php } ?>
</div><!--/tab-pane-->

        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

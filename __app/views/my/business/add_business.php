<?php $this->load->view('adminlte/overall_header'); ?>
<?php $this->load->view('my/my-sidebar'); ?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Add a Business
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
	<div id="alert-container"></div>
		<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12">
<div class="box-group" id="accordion">
  <div class="panel box box-success" id="basic-information">
	<div class="box-header">
		<h3 class="box-title">
		<a data-toggle="collapse" data-parent="#accordion" href="#basic-information-body" class="business-collapse">
          Basic Information
        </a>
		</h3>
	</div>

	<div id="basic-information-body" class="box-body panel-collapse collapse in">
<div class="form-group">
    <label for="business_name">Business Name</label>
		<input type="text" class="form-control" id="business_name" placeholder="Enter Business Name" name="business_name" value="">
</div>


	<div class="box-footer">
		<button type="button" class="btn btn-success btn-xs auto-update-fields" data-api="business_directory/add_new" data-box="basic-information" data-fields="business_name">Submit</button>
	</div>
</div><!-- /.box-body -->
</div>
  
 

 
</div>
</div>

        </div><!--/col-9-->
    </div><!--/row-->

	</div>
</div><!--/row-->



	</section><!-- /.content -->
</aside><!-- /.right-side -->
         
<?php $this->load->view('adminlte/overall_footer'); ?>



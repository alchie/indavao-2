<?php $this->load->view('adminlte/overall_header'); ?>
<?php $this->load->view('my/my-sidebar'); ?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo $current_directory->dir_name; ?>
		</h1>
	</section>

	<!-- Main content -->
	<section class="content">
	<div id="alert-container"></div>
		<div class="row">
		
		<div class="col-lg-12 col-md-12 col-sm-12">
<span id="business_id" data-value="<?php echo $current_directory->dir_id; ?>"></span>
<div class="box-group" id="accordion">
  <div class="panel box box-success" id="basic-information">
	<div class="box-header">
		<h3 class="box-title">
		<a data-toggle="collapse" data-parent="#accordion" href="#basic-information-body" class="business-collapse">
          Basic Information
        </a>
		</h3>
	</div>

	<div id="basic-information-body" class="box-body panel-collapse collapse in">
<div class="form-group">
    <label for="business_name">Business Name</label>
		<input type="text" class="form-control" id="business_name" placeholder="Enter Business Name" name="business_name" value="<?php echo $current_directory->dir_name; ?>">
</div>

<div class="form-group">
    <label for="business_address">Address</label>
		<input type="text" class="form-control" id="business_address" placeholder="Enter Address" name="business_address" value="<?php echo $current_directory->address; ?>">
</div>

<div class="form-group">
    <label for="business_phone">Phone Number</label>
		<input type="text" class="form-control" id="business_phone" placeholder="Enter Phone Number" name="business_phone" value="<?php echo $current_directory->phone; ?>">
</div>

<div class="form-group">
    <label for="business_phone">Fax Number</label>
		<input type="text" class="form-control" id="business_fax" placeholder="Enter Fax Number" name="business_fax" value="<?php echo $current_directory->fax; ?>">
</div>

<div class="form-group">
    <label for="business_description">Short Description</label>
		<textarea class="form-control" id="business_description" placeholder="Add Short Description" name="business_description" rows="3"><?php echo $current_directory->abstract; ?></textarea>
</div>

<div class="form-group">
    <label for="business_website">Website URL</label>
		<input type="text" class="form-control" id="business_website" placeholder="Enter Website URL" name="business_website" value="<?php echo $current_directory->website; ?>">
</div>

<div class="form-group">
    <label for="business_map_long">Email</label>
		<input type="text" class="form-control" id="business_email" placeholder="Enter Business Email" name="business_email" value="<?php echo $current_directory->email; ?>">
</div>

	<div class="box-footer">
		<button type="button" class="btn btn-success btn-xs auto-update-fields" data-api="business_directory/update_basic/<?php echo $current_directory->dir_id; ?>" data-box="basic-information" data-fields="business_name,business_address,business_phone,business_fax,business_description,business_website,business_email">Save Changes</button>
	</div>
</div><!-- /.box-body -->
</div>
  
  <div class="panel box box-success" id="map-coordinates">
    <div class="box-header">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#business-map" class="business-collapse">
          Map Coordinates
        </a>
      </h4>
    </div>
    <div id="business-map" class="panel-collapse collapse box-body">

  <div class="form-group">
    <label for="business_map_lat">Map Latitude</label>
		<input type="text" class="form-control" id="business_map_lat" placeholder="Enter Map Latitude" name="business_map_lat" value="<?php echo $current_directory->map_lat; ?>">
</div>

<div class="form-group">
    <label for="business_map_long">Map Longitude</label>
		<input type="text" class="form-control" id="business_map_lng" placeholder="Enter Map Longitude" name="business_map_lng" value="<?php echo $current_directory->map_long; ?>">
</div>
  
<div class="box-footer">
		<button type="button" class="btn btn-success btn-xs auto-update-fields" data-api="business_directory/update_map/<?php echo $current_directory->dir_id; ?>" data-box="map-coordinates" data-fields="business_map_lat, business_map_lng">Save Changes</button>
</div>
</div><!-- .panel-collapse -->
  </div>
  

 <div class="panel box box-success">
    <div class="box-header">
		  <h4 class="box-title">
		
        <a data-toggle="collapse" data-parent="#accordion" href="#business-photos" class="business-collapse click-load-box" data-id="business-photos" data-loaded="0">
          Photos
        </a>
      </h4>
    </div>
    <div id="business-photos" class="box-body panel-collapse collapse" data-api="business_directory/get_photos/<?php echo $current_directory->dir_id; ?>" data-limit="0" data-start="0">
   
   <label id="business_photos_input_button" for="business_photos_input" class="btn btn-success btn-xs pull-right" style="margin:-40px 5px 0 0"><i class="glyphicon glyphicon-plus"></i></label>
	<input type="file" multiple id="business_photos_input" name="userfile" class="btn btn-default auto-upload hidden image-upload-preview" data-id="business-photos" data-container_id="business_directory_photos-preview" data-action="image-upload" data-api="business_directory/add_photos/<?php echo $current_directory->dir_id; ?>" data-delete_api="business_directory/delete_photo/<?php echo $current_directory->dir_id; ?>">

	<div class="current-box-display" id="loading-img">
		<img src="<?php echo base_url('assets/images/ajax-loader.gif'); ?>">
	</div>
	
	</div><!-- .panel-collapse -->
  </div>

  <div class="panel box box-success">
    <div class="box-header">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#business-categories" class="business-collapse click-load-box" data-id="business-categories" data-loaded="0">
          Categories
        </a>
      </h4>
    </div>
    <div id="business-categories" class="box-body panel-collapse collapse" data-api="business_directory/get_categories/<?php echo $current_directory->dir_id; ?>" data-limit="0" data-start="0">
       	
	<div class="current-box-display" id="loading-img">
		<img src="<?php echo base_url('assets/images/ajax-loader.gif'); ?>">
	</div>
									
	</div><!-- .panel-collapse -->
  </div>
 
  <div class="panel box box-success">
    <div class="box-header">
      <h4 class="box-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#business-location" class="business-collapse click-load-box" data-id="business-location" data-loaded="0">
          Location
        </a>
      </h4>
    </div>
     <div id="business-location" class="box-body panel-collapse collapse" data-api="business_directory/get_location/<?php echo $current_directory->dir_id; ?>" data-limit="0" data-start="0">
       	
	<div class="current-box-display" id="loading-img">
		<img src="<?php echo base_url('assets/images/ajax-loader.gif'); ?>">
	</div>
									
	</div><!-- .panel-collapse -->
  </div>
  

  <div class="panel box box-success">
    <div class="box-header">
      <h4 class="box-title">
       <a data-toggle="collapse" data-parent="#accordion" href="#business-tags" class="business-collapse click-load-box" data-id="business-tags" data-loaded="0">
         Tags
        </a>
      </h4>
    </div>
    <div id="business-tags" class="box-body panel-collapse collapse" data-api="business_directory/get_tags/<?php echo $current_directory->dir_id; ?>" data-limit="0" data-start="0">
       	
	<div class="current-box-display" id="loading-img">
		<img src="<?php echo base_url('assets/images/ajax-loader.gif'); ?>">
	</div>

	</div><!-- .panel-collapse -->
  </div>

</div>
</div>

        </div><!--/col-9-->
    </div><!--/row-->

	</div>
</div><!--/row-->



	</section><!-- /.content -->
</aside><!-- /.right-side -->
         
<?php $this->load->view('adminlte/overall_footer'); ?>



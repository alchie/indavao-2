
<?php $this->load->view('adminlte/overall_header'); ?>
<?php $this->load->view('my/my-sidebar'); ?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			My Businesses
			<small>Add New</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url("my/{$current_user_id}/dashboard"); ?>"><i class="fa fa-dashboard"></i> My Account</a></li>
			<li><a href="#"><i class="fa fa-map-marker"></i> Business Directory</a></li>
			<li class="active">My Businesses</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	
		<div class="row">
		
		<div class="col-md-6">
		
<div class="box box-primary">
<div class="box-header">
	<h3 class="box-title">Check Duplicate Entries</h3>
</div><!-- /.box-header -->
<!-- form start -->
<form role="form" method="POST">
<div class="box-body">

	<input type="hidden" name="action" value="check">
  <div class="form-group">
    <label>Property Title</label>
    <input type="text" name="property_title" class="form-control" id="property_title" placeholder="Enter Property Title" value="<?php echo $this->input->post('property_title'); ?>">
  </div>

</div><!-- /.box-body -->

	<div class="box-footer">
		<button type="submit" class="btn btn-warning text-right">Check Existing Entry</button>
	</div>
</form>
</div>

<?php if(isset( $properties_matches ) ) {  ?>
	<?php if( isset( $properties_matches ) && count( $properties_matches ) > 0 ) { ?>
<div class="box box-danger">
<div class="box-header">
	<h3 class="box-title">Duplicate Entries Found!</h3>
</div><!-- /.box-header -->
<!-- form start -->

<div class="box-body">

<div class="list-group">
<?php foreach( $properties_matches as $match ) { 
	$match_link = site_url(array("realestate", $match->re_id, $match->re_slug));
	echo "<a href=\"{$match_link}\" target=\"_blank\" class=\"list-group-item\">{$match->re_title} <span class=\"badge\">{$match->re_slug}</span></a>";
} ?>
</div>

</div><!-- /.box-body -->



</div>
<?php } else { ?>
	<div class="alert alert-danger">
		<p>No Match Found!</p>
	</div>
<?php } ?>

	
	
<div class="box box-primary">
<div class="box-header">
	<h3 class="box-title">Check Duplicate Entries</h3>
</div><!-- /.box-header -->
<!-- form start -->
<form role="form" method="POST">
<div class="box-body">
<input type="hidden" name="action" value="add">
<div class="form-group">
    <label>Property Title</label>
    <input type="hidden" name="add_property_title" class="form-control" id="business-name" placeholder="Enter business name" value="<?php echo $this->input->post('property_title'); ?>">
    <span class="form-control"><?php echo $this->input->post('property_title'); ?></span>
  </div>
  <div class="form-group">
    <label>URL Slug</label>
    <input type="hidden" name="add_property_slug" class="form-control" id="business-name" placeholder="Enter business name" value="<?php echo url_title( $this->input->post('property_title'), '-', TRUE ); ?>">
    <span class="form-control"><?php echo url_title( $this->input->post('property_title'), '-', TRUE ); ?></span>
  </div>

</div><!-- /.box-body -->

	<div class="box-footer">
		<button type="submit" class="btn btn-warning text-right">Submit</button>
	</div>
</form>
</div>

	<?php } ?>
	
</div>
		

		</div>

	</section><!-- /.content -->
</aside><!-- /.right-side -->
         
<?php $this->load->view('adminlte/overall_footer'); ?>



<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="<?php echo site_url(array("my", $this->session->userdata('user_id') , "business")); ?>">My Business</a></li>
</ul>

<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="business-list">
	
<div class="page-header" style="margin-top:0;">
	<a href="<?php echo site_url(array("my", $this->session->userdata('user_id') , "business")); ?>" class="btn btn-xs btn-danger pull-right"><i class="glyphicon glyphicon-remove"></i> Cancel Add New</a>
		<h1>My Business <small>Add New</small></h1>
	</div>
	
<div class="well">
<form role="form" method="POST">
	<input type="hidden" name="action" value="check">
  <div class="form-group">
    <label>Business Name</label>
    <input type="text" name="business_name" class="form-control" id="business-name" placeholder="Enter business name" value="<?php echo $this->input->post('business_name'); ?>">
  </div>
  
  <button type="submit" class="btn btn-warning text-right">Check Existing Entry</button>
</form>
</div>



<?php if(isset( $business_matches ) ) {  ?>
	<?php if( isset( $business_matches ) && count( $business_matches ) > 0 ) { ?>
		<div class="panel panel-danger">
			<div class="panel-heading">
				<h3 class="panel-title"><?php echo count( $business_matches ); ?> Found</h3>
			</div>
<div class="list-group">
<?php foreach( $business_matches as $match ) { 
	$match_link = site_url(array("company", $match->dir_id,  $match->dir_slug));
	echo "<a href=\"{$match_link}\" target=\"_blank\" class=\"list-group-item\">{$match->dir_name} <span class=\"badge\">{$match->dir_slug}</span></a>";
} ?>
</div>
		</div>
	<?php } else { ?>
	<div class="alert alert-danger">
		<p>No Match Found!</p>
	</div>
	<?php } ?>
	<div class="well">
<form role="form" method="POST">
	<input type="hidden" name="action" value="add">
  <div class="form-group">
    <label>Business Name</label>
    <input type="hidden" name="add_business_name" class="form-control" id="business-name" placeholder="Enter business name" value="<?php echo trim($this->input->post('business_name')); ?>">
    <span class="form-control"><?php echo $this->input->post('business_name'); ?></span>
  </div>
  <div class="form-group">
    <label>URL Slug</label>
    <input type="hidden" name="add_business_slug" class="form-control" id="business-name" placeholder="Enter business name" value="<?php echo url_title( trim($this->input->post('business_name')), '-', TRUE ); ?>">
    <span class="form-control"><?php echo url_title( $this->input->post('business_name'), '-', TRUE ); ?></span>
  </div>
  <div class="form-group">
    <label><input type="checkbox" name="add_business_branch" value="1"> Branch Office</label>
    
  </div>
  <button type="submit" class="btn btn-success text-right">Submit</button>
</form>
	</div>
<?php } ?>
</div>

        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

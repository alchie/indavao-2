<?php $this->load->view('adminlte/overall_header'); ?>
<?php $this->load->view('my/my-sidebar'); ?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			My Businesses
		</h1>

	</section>

	<!-- Main content -->
	<section class="content">
	<div id="alert-container"></div>
		<div id="business_directory-page" data-colwidth="4" data-api="business_directory" data-limit="12">
			<div class="current-box-display" id="loading-img">
				<img src="<?php echo base_url('assets/adminlte/img/ajax-loader1.gif'); ?>">
			</div>
		</div>
		<script>
		<!--
			var init = setInterval(function(){
				if( typeof window.jQuery !== 'undefined' && typeof window.inDavaoApp !== 'undefined' ) {
					clearInterval( init );
					(function($){
						window.inDavaoApp.load( '#business_directory-page' );
					})(jQuery);
				}
			},1000);
		-->
		</script>

	</section><!-- /.content -->
</aside><!-- /.right-side -->
         
<?php $this->load->view('adminlte/overall_footer'); ?>

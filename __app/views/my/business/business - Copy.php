<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#">My Business</a></li>
</ul>

<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="business-list">

	<div class="page-header" style="margin-top:0;">
		<a href="<?php echo site_url(array("my", $this->session->userdata('user_id') , "business", "add")); ?>" class="btn btn-xs btn-success pull-right"><i class="glyphicon glyphicon-plus"></i> Add New</a>
		<h1>My Business</h1>
	</div>
	
<div class="table-responsive">
<table class="table table-hover">
	<thead>
          <tr>
            <th>Name</th>
			<th width="70px">Update</th>
            <th width="120px">Action</th>
          </tr>
        </thead>
     <tbody>
		
<?php foreach( $my_directories as $mydir ) { 

$mydir_link_update = site_url(array("my", $this->session->userdata('user_id'), "business", "update", $mydir->dir_id, ));

	if( $mydir->dir_status == 'draft' ) {
	?>
	<tr class="<?php if ($mydir->dir_status == 'publish') { echo 'success'; } elseif ($mydir->dir_status == 'pending') { echo 'warning'; } ?>" id="business-item-<?php echo $mydir->dir_id; ?>">
		<td>
			<?php echo $mydir->dir_name; ?>
		</td>
		<td><a href="<?php echo $mydir_link_update; ?>" class="btn btn-xs btn-warning">Update</a></td>
		<td>
			<?php if( $mydir->dir_status == 'draft' ) { ?>
				<a href="javascript:void(0);" class="btn btn-xs btn-danger delete-my-business-item" data-id="<?php echo $mydir->dir_id; ?>">Delete</a>
			<?php } ?>
			</td>
	</tr>
<?php  }
 } // foreach?>

<?php foreach( $my_directories as $mydir ) { 

$mydir_link_update = site_url(array("my", $this->session->userdata('user_id'), "business", "update", $mydir->dir_id, ));

	if( $mydir->dir_status == 'pending' ) {
	?>
	<tr class="<?php if ($mydir->dir_status == 'publish') { echo 'success'; } elseif ($mydir->dir_status == 'pending') { echo 'warning'; } ?>" id="business-item-<?php echo $mydir->dir_id; ?>">
		<td><?php echo $mydir->dir_name; ?></td>
		<td><a href="<?php echo $mydir_link_update; ?>" class="btn btn-xs btn-warning">Update</a></td>
		<td>
			<?php 
				echo "Pending";
			?>
			</td>
	</tr>
<?php  }
 } // foreach?>
 
 <?php foreach( $my_directories as $mydir ) {  

$mydir_link_update = site_url(array("my", $this->session->userdata('user_id'), "business", "update", $mydir->dir_id, ));
$data_url = site_url(array("company", $mydir->dir_id, $mydir->dir_slug));

	if( $mydir->dir_status == 'publish' ) {
	?>
	<tr class="<?php if ($mydir->dir_status == 'publish') { echo 'success'; } elseif ($mydir->dir_status == 'pending') { echo 'warning'; } ?>" id="business-item-<?php echo $mydir->dir_id; ?>">
		<td>
			<a href="<?php echo site_url(array("company", $mydir->dir_id, $mydir->dir_slug )); ?>" target="_blank">
			<strong><?php echo $mydir->dir_name; ?></strong>
			</a>
			<td><a href="<?php echo $mydir_link_update; ?>" class="btn btn-xs btn-warning">Update</a></td>
			</td>
		<td>
			<?php 
				if( ($mydir->points_claimed != '') && ($mydir->points_claimed == 0) ) {
					if(($last_claim == FALSE) || ( ($last_claim != FALSE) && $last_claim->meta_value == '') || (($last_claim != FALSE) && (time() - $last_claim->meta_value) >= (60*60*3)) ) {
						echo "<button DISABLED class='btn btn-xs btn-success business-claim-points fb-ready' data-id='{$mydir->dir_id}' data-url='{$data_url}' data-msg='{$mydir->dir_name}' data-points='{$mydir->points_credited}'>Claim {$mydir->points_credited} Points</button>";
					}
				} else {
					echo "Claimed";
				}
			?>
			</td>
	</tr>
<?php  }
 } // foreach?>
 
</tbody>
</table>
<p class="small text-right"><strong>Note:</strong> Claiming of points is every after 3 hours.</p>
</div>

<?php bootstrap_pagination( $pages, $current_page, current_url() . "?", 10 ); ?>

</div>

<script>
(function($) {
	
	$('.business-claim-points').click(function(){ 
		
		var self = $(this);
		self.prop('disabled', true);
		
		var data_points = $(this).attr('data-points');
		var data_msg = $(this).attr('data-msg');
		var data_url = $(this).attr('data-url');
		var data_id = $(this).attr('data-id');
		
		var incrementPoints = function(user_id, points) {
			var total_points = $('#total-points-'+user_id);
			var total_balance = $('#total-balance-'+user_id);
			
			total_points.text( parseInt(total_points.text()) + parseInt( points ) );
			total_balance.text( parseInt(total_balance.text()) + parseInt( points ) );
		}
		
		var ClaimPoints = function( obj_id, post_id ) {
				var postData = {
						action : 'claim_business_points',
						fb_obj_id : post_id,
						obj_id : obj_id
					};
					
				$.post(ajaxPostURL, postData, function(msg) {
					self.prop('disabled', false);
					console.log( msg );
					if( msg.error == false ) {
						incrementPoints(msg.user_id, data_points);
						$('<span>').text( "Claimed" ).insertAfter( self );
						self.remove();
						$('.business-claim-points').remove();
					}
				}, 'json').fail(function(xhr, textStatus, errorThrown){
					console.log( xhr.responseText );
				});
		};
		
		var FBSendUpdate = function(data_id, link, post_id) {
			FB.api(
			  'me/indavao:add',
			  'post',
			  {
				business: data_url
			  },
			  function(response) {
				console.log( response );
				ClaimPoints(data_id, post_id);
			  }
			);
		};
		
		var FBPostLink = function(data_id, link,message) {
			FB.api(
				"/me/feed",
				"POST",
				{
					"link" : link,
					"message" : message
				},
				function (response) {
					console.log( response );
				  if (response && !response.error) {
					FBSendUpdate(data_id, link, response.id);
				  } else {
					ClaimPoints(data_id);
				  }
				}
			);
		};
		
		FBPostLink(data_id, data_url, data_msg );

		
	});
})(jQuery);
</script>
        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

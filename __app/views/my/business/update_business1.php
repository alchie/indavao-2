<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="<?php echo site_url(array("my", $this->session->userdata('user_id'), "business" )); ?>">My Business</a></li>
</ul>

<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="business-list">
	
<div class="page-header" style="margin-top:0;">
<a href="<?php echo site_url(array("company", $current_directory->dir_id, $current_directory->dir_slug )); ?>" class="btn btn-xs btn-success" target="_blank">Live URL</a>
	<a href="<?php echo site_url(array("my", $this->session->userdata('user_id') , "business")); ?>" class="btn btn-xs btn-danger pull-right"><i class="glyphicon glyphicon-remove"></i> Cancel Update</a>
		<h1><?php echo $current_directory->dir_name; ?> <span class="badge"><?php echo $current_directory->dir_status; ?></span></h1>
	</div>
	
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
		
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#business-details" class="business-collapse" data-id="details">
          Details
        </a>
      </h4>
    </div>
    <div id="business-details" class="panel-collapse collapse in">
      <div class="panel-body">
<form method="post">
	<input type="hidden" name="action" value="update_details">
	<input type="hidden" name="reload" value="1">
	
<div class="form-group">
    <label for="business_name">Business Name</label>
	<?php if( $current_directory->dir_status == 'publish' && $current_directory->dir_status != '' && $current_directory->dir_name != '' ) { ?>
		<span class="form-control" style="border:1px solid #b94a48"><?php echo $current_directory->dir_name; ?></span>
	<?php } else { ?>
		<input type="text" class="form-control" id="business_name" placeholder="Enter Business Name" name="business_name" value="<?php echo $current_directory->dir_name; ?>">
	<?php } ?>
</div>

<div class="form-group">
    <label for="business_address">Address</label>
	<?php if( $current_directory->dir_status == 'publish' && $current_directory->dir_status != '' && $current_directory->address != '') { ?>
		<span class="form-control" style="border:1px solid #b94a48"><?php echo $current_directory->address; ?></span>
	<?php } else { ?>
		<input type="text" class="form-control" id="business_address" placeholder="Enter Address" name="business_address" value="<?php echo $current_directory->address; ?>">
	<?php } ?>
</div>

<div class="form-group">
    <label for="business_phone">Phone Number</label>
	<?php if( $current_directory->dir_status == 'publish' && $current_directory->dir_status != '' && $current_directory->phone != '') { ?>
		<span class="form-control" style="border:1px solid #b94a48"><?php echo $current_directory->phone; ?></span>
	<?php } else { ?>
		<input type="text" class="form-control" id="business_phone" placeholder="Enter Phone Number" name="business_phone" value="<?php echo $current_directory->phone; ?>">
	<?php } ?>
</div>

<div class="form-group">
<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>
</form>
      </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
		
      <h4 class="panel-title">
		  
        <a data-toggle="collapse" data-parent="#accordion" href="#business-more-details" class="business-collapse" data-id="details">
          More Details
        </a>
      </h4>
    </div>
    <div id="business-more-details" class="panel-collapse collapse">
      <div class="panel-body">
       
<form method="post">
	<input type="hidden" name="action" value="update_more_details">
	<input type="hidden" name="reload" value="1">
	
<div class="form-group">
    <label for="business_description">Short Description</label>
	<?php if( $current_directory->dir_status == 'publish' && $current_directory->dir_status != '' && $current_directory->abstract != '') { ?>
		<span class="form-control" style="border:1px solid #b94a48"><?php echo $current_directory->abstract; ?></span>
	<?php } else { ?>
		<textarea class="form-control" id="business_description" placeholder="Add Description" name="business_description" rows="3"><?php echo $current_directory->abstract; ?></textarea>
	<?php } ?>
</div>

<div class="form-group">
    <label for="business_website">Website URL</label>
	<?php if( $current_directory->dir_status == 'publish' && $current_directory->dir_status != '' && $current_directory->website != '') { ?>
		<span class="form-control" style="border:1px solid #b94a48"><?php echo $current_directory->website; ?></span>
	<?php } else { ?>
		<input type="text" class="form-control" id="business_website" placeholder="Enter Website URL" name="business_website" value="<?php echo $current_directory->website; ?>">
	<?php } ?>
</div>

<div class="form-group">
    <label for="business_map_long">Email</label>
	<?php if( $current_directory->dir_status == 'publish' && $current_directory->dir_status != '' && $current_directory->email != '') { ?>
		<span class="form-control" style="border:1px solid #b94a48"><?php echo $current_directory->email; ?></span>
	<?php } else { ?>
		<input type="text" class="form-control" id="business_email" placeholder="Enter Business Email" name="business_email" value="<?php echo $current_directory->email; ?>">
	<?php } ?>
</div>

  <div class="form-group">
    <label for="business_map_lat">Map Latitude</label>
	<?php if( $current_directory->dir_status == 'publish' && $current_directory->dir_status != '' && $current_directory->map_lat != '') { ?>
		<span class="form-control" style="border:1px solid #b94a48"><?php echo $current_directory->map_lat; ?></span>
	<?php } else { ?>
		<input type="text" class="form-control" id="business_map_lat" placeholder="Enter Map Latitude" name="business_map_lat" value="<?php echo $current_directory->map_lat; ?>">
	<?php } ?>
</div>

<div class="form-group">
    <label for="business_map_long">Map Longitude</label>
	<?php if( $current_directory->dir_status == 'publish' && $current_directory->dir_status != '' && $current_directory->map_long != '') { ?>
		<span class="form-control" style="border:1px solid #b94a48"><?php echo $current_directory->map_long; ?></span>
	<?php } else { ?>
		<input type="text" class="form-control" id="business_map_long" placeholder="Enter Map Longitude" name="business_map_long" value="<?php echo $current_directory->map_long; ?>">
	<?php } ?>
</div>

<div class="form-group">
<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>
</form>
      </div>
    </div>
  </div>
  
<?php if( $current_directory->dir_status != 'publish' || $current_directory->logo == '') { ?>

   <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
		  
        <a data-toggle="collapse" data-parent="#accordion" href="#business-add-logo" class="business-collapse" data-id="details">
          Add Logo
        </a>
      </h4>
    </div>
    <div id="business-add-logo" class="panel-collapse collapse">
      <div class="panel-body">
       
<form method="post" enctype="multipart/form-data">
	<input type="hidden" name="action" value="update_logo">
	<input type="hidden" name="reload" value="1">
	
<div class="form-group">
    <label for="business_map_long">Logo</label>
    <img data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $current_directory->logo; ?>" id="business_logo_preview" style="max-width:100%;margin-bottom:10px;" class="lazy" />
    <p><input type="file" id="business_logo" name="userfile"></p>
    <!--<button class="btn btn-xs btn-success" id="business-update-logo" type="button" DISABLED>Upload Logo</button>-->
</div>

<div class="form-group">
<button class="btn btn-xs btn-success pull-right no-js" type="submit">Upload Logo</button>
</div>
</form>
      </div>
    </div>
  </div>
<?php } ?>

<?php if( $current_directory->dir_status != 'publish' ) { ?>

  <div class="panel panel-default">
    <div class="panel-heading">
		
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="business-collapse" data-id="category">
          Categories
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">

<form method="post">
	<input type="hidden" name="action" value="update_categories">
	<input type="hidden" name="reload" value="1">
	
<div class="table-responsive">
<table class="table table-hover">
	<thead>
          <tr>
			<th width="10px">Check</th>
            <th>Name</th>
          </tr>
        </thead>
     <tbody>
<?php 

$old_cats = array();

foreach( $old_categories as $oldc ) {
	$old_cats[] = $oldc->tax_id;
}

foreach($business_categories as $bCat ) { ?>
		 <tr id="category-parent-<?php echo $bCat->tax_id; ?>" class="<?php echo (in_array($bCat->tax_id, $old_cats)) ? 'success' : ''; ?>">
			<td><input type="checkbox" name="new_categories[]" class="category-checkbox category-item" data-id="<?php echo $bCat->tax_id; ?>" value="<?php echo $bCat->tax_id; ?>" <?php echo (in_array($bCat->tax_id, $old_cats)) ? 'checked="checked"' : ''; ?>></td>
			<td><strong><?php echo $bCat->tax_label; ?></strong>
			
			<?php if( count( $bCat->children ) > 0 ) { ?>
				<div id="category-children-<?php echo $bCat->tax_id; ?>" class="<?php echo (in_array($bCat->tax_id, $old_cats)) ? '' : 'hidden'; ?>">
			<br><br>
				<table class="table table-hover" >
					 <tbody>
						 <?php foreach($bCat->children as $sCat ) { ?>
						 <tr>
							<td width="10px"><input type="checkbox" name="new_categories[]" class="category-checkbox category-item category-child-<?php echo $bCat->tax_id; ?>" data-id="<?php echo $sCat->tax_id; ?>" value="<?php echo $sCat->tax_id; ?>" <?php echo (in_array($sCat->tax_id, $old_cats)) ? 'checked="checked"' : ''; ?>></td>
							<td><?php echo $sCat->tax_label; ?></td>
						 </tr>
						 <?php } ?>
					</tbody>
				</table>
				</div>
			<?php } ?>
			</td>
		 </tr>
		 <?php } ?>
	</tbody>
</table>
<?php echo "<input type='hidden' name='old_categories' id='old_categories' value='". implode( ",", $old_cats ) ."'>"; ?>
</div>

<div class="form-group">
<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>
</form>

      </div>
    </div>
  </div>

  
  <div class="panel panel-default">
    <div class="panel-heading">
		
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="business-collapse" data-id="location">
          Location
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
		  
<form method="post">
	<input type="hidden" name="action" value="update_locations">
	<input type="hidden" name="reload" value="1">
			  
        <div class="table-responsive">
<table class="table table-hover">
	<thead>
          <tr>
			<th width="10px">Check</th>
            <th>Name</th>
          </tr>
        </thead>
     <tbody>
		 
		  <tr>
			<td><input type="radio" name="new_locations[]" class="location-checkbox location-item" value="" data-id="" checked="checked"></td>
			<td><strong>None</strong></td>
			</tr>
			
<?php 
$old_locs = array();

foreach( $old_locations as $oldl ) {
	$old_locs[] = $oldl->loc_id;
}
		 foreach($locations as $mLoc ) { ?>
		 <tr id="location-parent-<?php echo $mLoc->loc_id; ?>" class="<?php echo (in_array($mLoc->loc_id, $old_locs)) ? 'success' : ''; ?>">
			<td><input type="radio" name="new_locations[]" class="location-checkbox location-item" value="<?php echo $mLoc->loc_id; ?>" data-id="<?php echo $mLoc->loc_id; ?>" <?php echo (in_array($mLoc->loc_id, $old_locs)) ? 'checked="checked"' : ''; ?>></td>
			<td><strong><?php echo $mLoc->loc_name; ?></strong>
			
			<?php if( count( $mLoc->children ) > 0 ) { ?>
				<div id="location-children-<?php echo $mLoc->loc_id; ?>" class="<?php echo (in_array($mLoc->loc_id, $old_locs)) ? '' : 'hidden'; ?>">
					<br><br>
				<table class="table table-hover" >
					 <tbody>
						 <?php foreach($mLoc->children as $sLoc ) { ?>
						 <tr>
							<td><input type="radio" name="sub_locations[<?php echo $sLoc->loc_id; ?>][]" class="location-checkbox location-item location-child-<?php echo $mLoc->loc_id; ?>" value="<?php echo $sLoc->loc_id; ?>" data-id="<?php echo $sLoc->loc_id; ?>" <?php echo (in_array($sLoc->loc_id, $old_locs)) ? 'checked="checked"' : ''; ?>></td>
							<td><?php echo $sLoc->loc_name; ?>
						 </tr>
						 <?php } ?>
					</tbody>
				</table>
				</div>
			<?php } ?>
			</td>
		 </tr>
		 <?php } ?>
	</tbody>
</table>
<?php echo "<input type='hidden' name='old_locations' id='old_locations' value='". implode( ",", $old_locs ) ."'>"; ?>
</div>
    
    <div class="form-group">
<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>
</form>
    
      </div>
    </div>
  </div>
  

<div class="panel panel-default">
	<div class="panel-heading">
		
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTags" class="business-collapse" data-id="tags">
          Tags
        </a>
      </h4>
    </div>
    <div id="collapseTags" class="panel-collapse collapse">
		<div class="panel-body">
<p>
<form method="post">
	<input type="hidden" name="action" value="update_tags">
	<input type="hidden" name="reload" value="1">
<div class="row">
  <div class="col-lg-12">
    <div class="input-group">
      <input type="text" class="form-control" id="business_tag_name" placeholder="Enter A Tag" name="business_tag" value="" maxlength="50">
      <span class="input-group-btn">
        <button class="btn btn-success pull-right" id="business_tag_add_action1" type="submit">Add Tag</button>
      </span>
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->
<form>
</p>
<div class="row">
  <div class="col-lg-12">
<ul class="list-group" id="business-tags">
	<?php foreach( $tags as $tag ) { ?>
  <li id="tag-item-<?php echo $tag->dir_tag_id; ?>" class="list-group-item tag-item <?php echo ($tag->dir_tag_active==1) ? 'enabled' : 'disabled'; ?>">
  <div class="btn-group pull-right">
	<!--<a data-tag_id="<?php echo $tag->dir_tag_id; ?>" data-tag_enable="<?php echo $tag->dir_tag_active; ?>" href="javascript:void(0);" class="business_tag_enable btn btn-xs btn-<?php echo ($tag->dir_tag_active==1) ? 'warning' : 'success'; ?>"><?php echo ($tag->dir_tag_active==1) ? 'Disable' : 'Enable'; ?></a>-->
	<a data-tag_id="<?php echo $tag->dir_tag_id; ?>" href="javascript:void(0);" class="business_tag_delete_action btn btn-xs btn-danger">Delete</a>
	</div>
    <?php echo $tag->tag_name; ?>
  </li>
	<?php } ?>
</ul>
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->

		</div>
    </div>
  </div>
  
    <?php } ?>
	
<?php if($current_directory->dir_name != '' && $current_directory->address != '' && $current_directory->phone != '' && $current_directory->dir_status != 'pending' ) {  ?>
  <div class="panel panel-danger" id="panel-review-approval">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseApprovalRequest" class="business-collapse">
          Submit for Review and Approval
        </a>
      </h4>
    </div>
    <div id="collapseApprovalRequest" class="panel-collapse collapse">
      <div class="panel-body">
		  <p>You will not be able to update this listing upon submission unless it is rejected by our reviewers.</p>
			<form method="post" action="">
				<input type="hidden" name="action" value="request_approval">
				<button type="submit" class="btn btn-lg btn-success" id="business-request-approval1">Submit Now</button>
			</form>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
</div>

        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

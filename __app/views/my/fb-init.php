<div id="fb-root"></div>
<script>
  function statusChangeCallback(response) {
    if (response.status === 'connected') {
      FB.api(
			"/me/picture?redirect=0&height=100&type=normal&width=100",
			function (response) {
			  if (response && !response.error) {
				/* handle the result */
				$('img.profile-image').attr('src', response.data.url);
			  }
			}
		);
		$('.enable-on-fb-ready').prop('disabled', false);
		$('.show-on-fb-ready').removeClass('hidden').show();
    } else {
		window.location.replace("<?php echo site_url('my/logout'); ?>");
	}
  }
	function logoutUser() {
		window.location.replace("<?php echo site_url('my/logout'); ?>");
	}

function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }
  
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '<?php echo $facebook_app_id; ?>',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.1
  });

FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
});
	
FB.Event.subscribe('auth.statusChange', auth_status_change_callback);

var auth_status_change_callback = function(response) {
	statusChangeCallback( response );
};

FB.Canvas.setAutoGrow();

  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

 var check_if_like = function() {
	FB.api(
		'/me/likes/1422670927991234',
		'get',
		function(resp){
			if( resp.data.length == 0 ) {
				FB.Event.subscribe('edge.create', function(url, html_element){
					$('#facebook_page_like_block').remove();
					$('#facebook_page_like_box').remove();
				});
				setTimeout(function(){
					var block = $('<div>').appendTo( $('body') );
					block.attr('id', 'facebook_page_like_block');
					block.css({
						width : '100%',
						height: '100%',
						position: 'fixed',
						zIndex: '99998',
						background: '#000',
						top: '0',
						opacity: '.1',
					});

					var like_box = $('#facebook_page_like_box');
					like_box.css({
						position: 'fixed',
						zIndex: '99999',
						background: '#FFF',
						top: (($(window).height()-288) / 2),
						left : (($(window).width()-320) / 2),
						padding : '10px',
					});
					like_box.removeClass('hidden');
					$('#facebook_page_like_box .skip').click(function(){
						$('#facebook_page_like_block').remove();
						$('#facebook_page_like_box').remove();
					});
				}, 10000);
			}
	});
 };
var checkFBInit = setInterval( function() {
		 window.FB.getLoginStatus(function(response){
			if (response.status === 'connected') {
				$('.fb-ready').prop('disabled', false);
				//check_if_like();
			}
			if( response ) {
				clearInterval( checkFBInit );
			}
		 });
	}, 5000 );
	
</script>
<!--
<div class="hidden" id="facebook_page_like_box">
<p><div class="fb-like-box" data-href="https://www.facebook.com/indavao" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
</p>
<p class="text-center"><a href="javascript:void(0);" class="skip">skip</a></p>
</div>
-->
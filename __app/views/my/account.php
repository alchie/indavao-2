<?php $this->load->view('adminlte/overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<?php $this->load->view('my/my-sidebar'); ?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Account Settings
		</h1>
		
	</section>

	<!-- Main content -->
	<section class="content">
	<div id="alert-container"></div>
		<div class="row">
			<div class="col-lg-6 col-xs-12">
			
			
<div class="box box-primary" id="personal-information">
                                <div class="box-header">
                                    <h3 class="box-title">Personal Information</h3>
                                </div><!-- /.box-header -->
                                <!-- form start -->
                                    <div class="box-body">

  <div class="form-group">
    <label for="name">Name</label>
    <?php 
	 
		$meta_updated_name = ( $user_meta != FALSE ) ? $user_meta->name : '';
		$cookie_meta_name = get_cookie( $this->config->item('cookie_prefix') . 'meta_updated_name'); 
		if( $this->input->post('name') !== false && $cookie_meta_name != $this->input->post('name') ) {
			$meta_updated_name = $this->input->post('name');
		} elseif( $cookie_meta_name != '' ) {
			$meta_updated_name = $cookie_meta_name;
		}
    ?>
    <input name="name" type="text" class="form-control" id="name_field" placeholder="Enter Name" value="<?php echo $meta_updated_name; ?>">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <?php 
		$meta_updated_email = ( $user_meta != FALSE ) ? $user_meta->email : '';
		$cookie_meta_email = get_cookie( $this->config->item('cookie_prefix') . 'meta_updated_email'); 
		if( $this->input->post('email') !== false && $cookie_meta_email != $this->input->post('email') ) {
			$meta_updated_email = $this->input->post('email');
		} elseif( $cookie_meta_email != '' ) {
			$meta_updated_email = $cookie_meta_email;
		}
    ?>
    <input name="email" type="email" class="form-control" id="email_field" placeholder="Enter email" value="<?php echo $meta_updated_email; ?>">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Phone</label>
    <?php 
		$meta_updated_phone = ( $user_meta != FALSE ) ? $user_meta->phone : '';
		$cookie_meta_phone = get_cookie( $this->config->item('cookie_prefix') . 'meta_updated_phone'); 
		if( $this->input->post('phone') !== false && $cookie_meta_phone != $this->input->post('phone') ) {
			$meta_updated_phone = $this->input->post('phone');
		} elseif( $cookie_meta_phone != '' ) {
			$meta_updated_phone = $cookie_meta_phone;
		}
    ?>
    <input name="phone" type="phone" class="form-control" id="phone_field" placeholder="Enter phone" value="<?php echo $meta_updated_phone; ?>">
  </div>

		
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button data-api="account/update" data-box="personal-information" data-fields="name_field,email_field,phone_field" type="button" class="btn btn-success auto-update-fields">Save Changes</button>
                                    </div>
                            </div>

			</div>
		</div>

	</section><!-- /.content -->
</aside><!-- /.right-side -->
         
<?php $this->load->view('adminlte/overall_footer'); ?>

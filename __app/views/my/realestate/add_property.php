<?php $this->load->view('adminlte/overall_header'); ?>
<?php $this->load->view('my/my-sidebar'); ?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			My Properties
			<small>Add New</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url("my/{$current_user_id}/dashboard"); ?>"><i class="fa fa-dashboard"></i> My Account</a></li>
			<li><a href="#"><i class="fa fa-home"></i> Real Estate</a></li>
			<li class="active">My Properties</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	
		<div class="row">
		
		<div class="col-md-6">
		
<div class="box box-primary">
<div class="box-header">
	<h3 class="box-title">Check Duplicate Entries</h3>
</div><!-- /.box-header -->
<!-- form start -->
<form role="form" method="POST">
<div class="box-body">

	<input type="hidden" name="action" value="check">
  <div class="form-group">
    <label>Property Title</label>
    <input type="text" name="property_title" class="form-control" id="property_title" placeholder="Enter Property Title" value="<?php echo $this->input->post('property_title'); ?>">
  </div>

</div><!-- /.box-body -->

	<div class="box-footer">
		<button type="submit" class="btn btn-warning text-right">Check Existing Entry</button>
	</div>
</form>
</div>

<?php if(isset( $properties_matches ) ) {  ?>
	<?php if( isset( $properties_matches ) && count( $properties_matches ) > 0 ) { ?>
<div class="box box-danger">
<div class="box-header">
	<h3 class="box-title">Duplicate Entries Found!</h3>
</div><!-- /.box-header -->
<!-- form start -->

<div class="box-body">

<div class="list-group">
<?php foreach( $properties_matches as $match ) { 
	$match_link = site_url(array("realestate", $match->re_id, $match->re_slug));
	echo "<a href=\"{$match_link}\" target=\"_blank\" class=\"list-group-item\">{$match->re_title} <span class=\"badge\">{$match->re_slug}</span></a>";
} ?>
</div>

</div><!-- /.box-body -->



</div>
<?php } else { ?>
	<div class="alert alert-danger">
		<p>No Match Found!</p>
	</div>
<?php } ?>

	
	
<div class="box box-primary">
<div class="box-header">
	<h3 class="box-title">Check Duplicate Entries</h3>
</div><!-- /.box-header -->
<!-- form start -->
<form role="form" method="POST">
<div class="box-body">
<input type="hidden" name="action" value="add">
<div class="form-group">
    <label>Property Title</label>
    <input type="hidden" name="add_property_title" class="form-control" id="business-name" placeholder="Enter business name" value="<?php echo $this->input->post('property_title'); ?>">
    <span class="form-control"><?php echo $this->input->post('property_title'); ?></span>
  </div>
  <div class="form-group">
    <label>URL Slug</label>
    <input type="hidden" name="add_property_slug" class="form-control" id="business-name" placeholder="Enter business name" value="<?php echo url_title( $this->input->post('property_title'), '-', TRUE ); ?>">
    <span class="form-control"><?php echo url_title( $this->input->post('property_title'), '-', TRUE ); ?></span>
  </div>

</div><!-- /.box-body -->

	<div class="box-footer">
		<button type="submit" class="btn btn-warning text-right">Submit</button>
	</div>
</form>
</div>

	<?php } ?>
	
</div>
		

		</div>

	</section><!-- /.content -->
</aside><!-- /.right-side -->
         
<?php $this->load->view('adminlte/overall_footer'); ?>


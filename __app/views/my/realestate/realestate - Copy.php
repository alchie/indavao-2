<?php $this->load->view('overall_header');  ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#">My Properties</a></li>
	
</ul>

<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="business-list">
	
<div class="page-header" style="margin-top:0;">
	<a href="<?php echo site_url(array("my", $this->session->userdata('user_id') , "properties", "add")); ?>" class="btn btn-xs btn-success pull-right">Add New Property</a>
		<h1>My Properties </h1>
</div>

<div class="table-responsive">
<table class="table table-hover">
	<thead>
          <tr>
            <th>Property Title</th>
            <th width="120px">Action</th>
          </tr>
        </thead>
     <tbody>
		 <?php foreach( $my_properties as $prop ) { 
			 
			 $prop_link_update = site_url(array("my", $this->session->userdata('user_id'), "properties", "update", $prop->re_id ));
			 
			 ?>
			 <tr class="<?php echo ($prop->re_status=='publish') ? 'success' : ''; ?>">
				<td>
					<?php if($prop->re_status=='publish') { ?>
						<a href="<?php echo site_url(array('realestate', $prop->re_id, $prop->re_slug, $this->session->userdata('user_id'))); ?>" target="_blank">
					<?php } ?>
					<?php echo $prop->re_title; ?>
					<?php if($prop->re_status=='publish') { ?>
						</a>
					<?php } ?>
				</td>
				<td><a href="<?php echo $prop_link_update; ?>" class="btn btn-xs btn-warning">Update</a> <a href="javascript:void(0);" class="btn btn-xs btn-danger delete-my-property-item" data-id="<?php echo $prop->re_id; ?>">Delete</a></td>
			 </tr>
		 <?php } ?>
	</tbody>
</table>
</div>

<?php bootstrap_pagination( $pages, $current_page, current_url() . "?", 10 ); ?>

        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

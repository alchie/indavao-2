<?php $this->load->view('adminlte/overall_header'); ?>
<?php $this->load->view('my/my-sidebar'); ?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			My Properties
			<small><?php echo $current_property->re_title; ?></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url("my/{$current_user_id}/dashboard"); ?>"><i class="fa fa-dashboard"></i> My Account</a></li>
			<li><a href="#"><i class="fa fa-home"></i> Real Estate</a></li>
			<li class="active">My Properties</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	
		<div class="row">
		
		<div class="col-md-6">
		
<div class="panel-group" id="accordion">

<!-- start -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#property-details" class="property-collapse">
          Property Details
        </a>
      </h4>
    </div>
    <div id="property-details" class="panel-collapse collapse <?php echo ($current_tab==FALSE||$current_tab=='update_property') ? 'in': ''; ?>">
      <div class="panel-body">
	<form method="post" action="<?php echo current_url() . "?tab=update_property"; ?>">
		<input type="hidden" name="action" value="update_property" />
		<input type="hidden" name="reload" value="1">

<div class="form-group">
    <label for="business_name">Property Title</label>
    <input type="text" class="form-control" id="property_title" placeholder="Enter Property Title" name="property_title" value="<?php echo $current_property->re_title; ?>">
</div>

<div class="form-group">
    <label for="business_name">Price</label>
    <input type="text" class="form-control" id="price" placeholder="Enter Price" name="price" value="<?php echo $current_property->price; ?>">
</div>

<div class="form-group">
    <label for="business_name">Abstract</label>
    <input type="text" class="form-control" id="abstract" placeholder="Enter Abstract" name="abstract" value="<?php echo $current_property->abstract; ?>">
</div>

<div class="form-group">
    <label for="business_name">Description</label>
    <textarea class="form-control" id="description" placeholder="Enter Description" name="description" rows="5"><?php echo $current_property->description; ?></textarea>
</div>

<div class="form-group">
    <label for="business_name">Lot Area</label>
    <input type="text" class="form-control" id="lot_area" placeholder="Enter Lot Area" name="lot_area" value="<?php echo $current_property->lot_area; ?>">
</div>

<div class="form-group">
<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>

	</form>
	</div></div></div>
<!-- end -->


<!-- start -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#house-details" class="property-collapse">
          House
        </a>
      </h4>
    </div>
    <div id="house-details" class="panel-collapse collapse <?php echo ($current_tab=='update_house') ? 'in': ''; ?>">
      <div class="panel-body">
	<form method="post" action="<?php echo current_url() . "?tab=update_house"; ?>">
		<input type="hidden" name="action" value="update_house" />
		<input type="hidden" name="reload" value="1">

<div class="form-group">
    <label for="business_name">Beds</label>
    <input type="text" class="form-control" id="beds" placeholder="Enter Beds" name="beds" value="<?php echo $current_property->beds; ?>">
</div>

<div class="form-group">
    <label for="business_name">Baths</label>
    <input type="text" class="form-control" id="baths" placeholder="Enter Baths" name="baths" value="<?php echo $current_property->baths; ?>">
</div>

<div class="form-group">
    <label for="business_name">Floor Area</label>
    <input type="text" class="form-control" id="floor_area" placeholder="Enter Floor Area" name="floor_area" value="<?php echo $current_property->floor_area; ?>">
</div>

<div class="form-group">
<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>

	</form>
	</div></div></div>
<!-- end -->

<!-- start -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#locations" class="property-collapse">
          Location
        </a>
      </h4>
    </div>
    <div id="locations" class="panel-collapse collapse <?php echo ($current_tab=='update_location') ? 'in': ''; ?>">
      <div class="panel-body">
	<form method="post" action="<?php echo current_url() . "?tab=update_location"; ?>">
		<input type="hidden" name="action" value="update_location" />
		<input type="hidden" name="reload" value="1">

<div class="form-group">
    <label for="business_name">Address</label>
    <input type="text" class="form-control" id="address" placeholder="Enter Address" name="address" value="<?php echo $current_property->address; ?>">
</div>

<div class="form-group">
    <label for="business_name">Lot #</label>
    <input type="text" class="form-control" id="address_lot" placeholder="Enter Lot Number" name="address_lot" value="<?php echo $current_property->address_lot; ?>">
</div>

<div class="form-group">
    <label for="business_name">Block #</label>
    <input type="text" class="form-control" id="address_block" placeholder="Enter Block Number" name="address_block" value="<?php echo $current_property->address_block; ?>">
</div>

<div class="form-group">
    <label for="business_name">Map Latitude</label>
    <input type="text" class="form-control" id="map_lat" placeholder="Enter Map Latitude" name="map_lat" value="<?php echo $current_property->map_lat; ?>">
</div>

<div class="form-group">
    <label for="business_name">Map Longitude</label>
    <input type="text" class="form-control" id="map_lng" placeholder="Enter Map Longitude" name="map_lng" value="<?php echo $current_property->map_lng; ?>">
</div>

<div class="form-group">
<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>

	</form>
	</div></div></div>
<!-- end -->

<!-- start -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#update_types" class="property-collapse">
          Property Types
        </a>
      </h4>
    </div>
    <div id="update_types" class="panel-collapse collapse <?php echo ($current_tab=='update_types') ? 'in': ''; ?>">
      <div class="panel-body">
	<form method="post" action="<?php echo current_url() . "?tab=update_types"; ?>">
		<input type="hidden" name="action" value="update_types" />
		

		<div class="form-group">
    <label for="business_name">Types</label>
   
		<?php 
		
		foreach(array(
						'HOUSE' => 'House and Lot',
						'PROJ' => 'Development Projects', 
						'COMM' => 'Commercial Properties',
						'RESID' => 'Residential Properties',
						'LOT' => 'Open Lot / Land',
						'CONDO' => 'Condominiums',
						'APART' => 'Apartments',
						'FCL' => 'Foreclosures',
						) as $key=>$label) { 
								$enabled = (in_array( $key, $property_types)) ? 'CHECKED' : '';
								$disabled = (in_array( $key, $property_types)) ? '' : 'CHECKED';
								echo "<p><label><input type='radio' name='property_type[{$key}]' value=\"1\" {$enabled}>ENABLE </label> &middot; <label><input type='radio' name='property_type[{$key}]' value=\"0\" {$disabled}>DISABLE </label> - {$label}</p>";
							 
						} ?>
	
</div>

<div class="form-group">
<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>

	</form>
	</div></div></div>
<!-- end -->

<!-- start -->
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#more-options" class="property-collapse">
          Options
        </a>
      </h4>
    </div>
    <div id="more-options" class="panel-collapse collapse <?php echo ($current_tab=='update_options') ? 'in': ''; ?>">
      <div class="panel-body">
	<form method="post" action="<?php echo current_url() . "?tab=update_options"; ?>">
		<input type="hidden" name="action" value="update_options" />
		<input type="hidden" name="reload" value="1">

<div class="form-group">
    <label for="business_name">Listing Type</label>
	
	<p>
		<label><input type="radio" name="listing_type" value="sale" <?php echo ('sale' == $current_property->listing_type) ? 'CHECKED' : ''; ?>> For Sale</label>
		<label><input type="radio" name="listing_type" value="rent" <?php echo ('rent' == $current_property->listing_type) ? 'CHECKED' : ''; ?>> For Rent</label>
		<label><input type="radio" name="listing_type" value="assume" <?php echo ('assume' == $current_property->listing_type) ? 'CHECKED' : ''; ?>> For Assume</label>
		<label><input type="radio" name="listing_type" value="sold" <?php echo ('sold' == $current_property->listing_type) ? 'CHECKED' : ''; ?>> SOLD</label>
	</p>

</div>

<div class="form-group">
    <label for="business_name">Type</label>
    <select class="form-control selectpicker" name="property_type">
	<option value="">- - Select Primary Type - -</option> 
		<?php foreach(array(
						'MODEL' => 'Model House',
						'HOUSE' => 'House and Lot',
						'PROJ' => 'Development Projects', 
						'COMM' => 'Commercial Properties',
						'RESID' => 'Residential Properties',
						'LOT' => 'Open Lot / Land',
						'CONDO' => 'Condominiums',
						'APART' => 'Apartments',
						'FCL' => 'Foreclosures',
						) as $key=>$label) { 
							if($key == $current_property->re_type) {
								echo "<option value=\"{$key}\" SELECTED>{$label}</option>";
							 } else { 
								 echo "<option value=\"{$key}\">{$label}</option>"; 
							} 
						} ?>
	</select>
</div>

<div class="form-group">
    <label for="business_name">Parent</label>
    <select class="form-control selectpicker" data-live-search="true" name="property_parent">
		<option value="">No Parent</option>
		<?php foreach( $projects as $proj ) { ?>
			<option value="<?php echo $proj->re_id; ?>" <?php echo ($proj->re_id == $current_property->re_parent) ? 'SELECTED' : ''; ?>><?php echo $proj->re_title; ?></option>
		<?php } ?>
	</select>
</div>

<div class="form-group">
    <label for="business_name">Status</label>
	<p>
	<label><input type="radio" name="property_status" value="draft" <?php echo ('draft' == $current_property->re_status) ? 'CHECKED' : ''; ?>> Draft</label>
	<label><input type="radio" name="property_status" value="pending" <?php echo ('pending' == $current_property->re_status) ? 'CHECKED' : ''; ?>> Pending</label>
	<label><input type="radio" name="property_status" value="publish" <?php echo ('publish' == $current_property->re_status) ? 'CHECKED' : ''; ?>> Publish</label>
	
	</p>
</div>

<div class="form-group">
	<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>

	</form>
	</div></div></div>
<!-- end -->

<?php if( $this->session->userdata('manager') ) { ?>

<!-- start -->
  <div class="panel panel-danger">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#more-update_tasks" class="property-collapse">
          Tasks and Points Options
        </a>
      </h4>
    </div>
    <div id="more-update_tasks" class="panel-collapse collapse <?php echo ($current_tab=='update_tasks') ? 'in': ''; ?>">
      <div class="panel-body">
	<form method="post" action="<?php echo current_url() . "?tab=update_tasks"; ?>">
		<input type="hidden" name="action" value="update_tasks" />
		<input type="hidden" name="reload" value="1">

<div class="form-group">
    <label for="business_name" class="pull-right"><input type="checkbox" name="add_to_tasks" value="1" <?php echo ($current_property->is_task==1) ? 'CHECKED' : ''; ?>> Add to Tasks</label>
 </div>
<div class="form-group">
    <label for="share_message">Share Message</label>
    <input type="text" class="form-control" id="task_message" placeholder="Enter Share Message" name="task_message" value="<?php echo $current_property->task_message; ?>">
</div>



<div class="form-group">
	<button class="btn btn-xs btn-success pull-right no-js" type="submit">Save Changes</button>
</div>

	</form>
	</div></div></div>
<!-- end -->

<?php } ?>

</div></div></div><!--/row-->



	</section><!-- /.content -->
</aside><!-- /.right-side -->
         
<?php $this->load->view('adminlte/overall_footer'); ?>



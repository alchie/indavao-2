<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
           <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#">My Shares</a></li>
          </ul>


<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="referrals">

<?php if( $shares ) { ?>
<div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
					<th class="text-center" width="20%">Date Verified</th>
                      <th>Desription</th>
                      <th class="text-center" width="20%">Claim Points</th>
                      <?php /* <th class="text-center" width="20%">Points Earned</th> */ ?>
                    </tr>
                  </thead>
                  <tbody id="tasks-items">
					  <?php foreach($shares as $share ) { ?>
						<tr id="task-share-<?php echo $share->tasks_ver_id; ?>">
							<td><?php echo $share->date_verified; ?></td>
							<td><?php 
							switch($share->tv_object_type) {
								case 'realestate':
									$to = ($share->page_name == '') ? $share->tv_tasks_url_id . ' - Timeline' : $share->page_name;
									echo "Real Estate - {$share->re_title} to {$to}" ;
									echo " <a href='{$share->post_url}' target='_blank' class='small'><i class='glyphicon glyphicon-link'></i></a>";
								break;
							}
							?>
							</td>
							<td class="text-center">
								<?php if($share->points_credited == '') { ?>
									<?php if( abs($share->days) > 7 ) { ?>
									<button DISABLED type="button" class="btn btn-success btn-xs btn-claim-points enable-on-fb-ready" data-post_id="<?php echo $share->tv_post_id; ?>" data-tasks_url_id="<?php echo $share->tv_tasks_url_id; ?>" data-task_id="<?php echo $share->tasks_ver_id; ?>"  data-object_id="<?php echo $share->tv_object_id; ?>" data-object_type="<?php echo $share->tv_object_type; ?>" data-destination_id="<?php echo ($share->page_id != '') ? $share->page_id : $current_user_id; ?>" data-share_url="<?php echo site_url($share->tv_object_type . '/' . $share->re_slug . '/' . $current_user_id); ?>">Claim Points</button>
									<?php } else { ?>
										<?php echo (7 - abs($share->days)); ?> days left
									<?php } ?>
								<?php } else { ?>
									<span class="btn btn-success btn-xs"><i class="glyphicon glyphicon-ok"></i></span>
								<?php } ?>
								</td>
								<?php /*
								<td class="text-center" id="points-td-<?php echo $share->tasks_ver_id; ?>">
								<?php if($share->points_credited == '') { ?>
									<img src="<?php echo base_url('assets/images/ajax-loader.gif'); ?>" class="hidden">
								<?php } else { ?>
									<?php echo $share->points_credited; ?>
								<?php } ?>
								</td>
								*/ ?>
						</tr>
						<?php } ?>
				</tbody>
				</table>

                  <?php if( $pages > 1 ) { ?>
					  <hr>
   <nav class="text-center">
  <ul class="pagination">
  <?php for($i=1;$i<=$pages;$i++) { 
		if($current_page == $i) {
			echo '<li class="active"><a href="#current-page" DISABLED>'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.site_url("my/{$current_user_id}/shares").'?page='.$i.'">'.$i.'</a></li>';
		}
  }
  ?>
  </ul>
</nav>
<?php } ?>
<script>
<!--
var ajaxPostURL = '<?php echo site_url("my/{$current_user_id}/ajax"); ?>';
(function($) {
	$('.btn-claim-points').click(function(){ 
		var self = $(this);
		var post_id = $(this).attr('data-post_id');
		var destination_id = $(this).attr('data-destination_id');
		var share_url = $(this).attr('data-share_url');
		var task_id = $(this).attr('data-task_id');
		var tasks_url_id = $(this).attr('data-tasks_url_id');
		var object_id = $(this).attr('data-object_id');
		var object_type = $(this).attr('data-object_type');
		$(this).prop('disabled', true);
		$('#points-td-'+task_id+' img').removeClass('hidden');
		var revertTask = function(task_id) {
			var revertButton = $('<button />').addClass('btn btn-warning btn-xs').text('Revert Back');
			revertButton.attr('data-id', task_id)
;			revertButton.click(function(){
				if( confirm('Are you sure?') == true ) {
					$.post(ajaxPostURL, {task_id : task_id, action : 'tasks_revert'}, function(msg) {
						if( msg.error == false ) {
							$('#task-share-'+task_id).fadeOut('slow', function(){
								$(this).remove();
							});
						}
					}, 'json').fail(function(xhr, textStatus, errorThrown){
						console.log( xhr.responseText );
					});
				}
			});
			$('#points-td-'+task_id).html( revertButton );
		}
		FB.api("/"+post_id,"GET", function (response) {
			if (response && !response.error) {
				if(( response.from.id == destination_id ) && (response.link == share_url)) {
					var postData = {
							task_id : task_id,
							tasks_url_id : tasks_url_id,
							object_id : object_id,
							object_type : object_type,
							post_id : post_id,
							action : 'claim_task_points',
						};
					$.post(ajaxPostURL, postData, function(msg) {
						if( msg.error == false ) {
							if( typeof msg.results != 'undefined' && typeof msg.results.points_credited != 'undefined' ) {
								self.html( $('<i />').addClass('glyphicon glyphicon-ok') );
								$('#points-td-'+msg.results.object_id).html( msg.results.points_credited );
							}
						}
					}, 'json').fail(function(xhr, textStatus, errorThrown){
						console.log( xhr.responseText );
					});
				} else {
					$('<span />').addClass('btn btn-danger btn-xs').text('Error Found!').insertAfter( self );
					self.remove();
					revertTask( task_id );
				}
			} else {
				$('<span />').addClass('btn btn-danger btn-xs').text('Post Not Found!').insertAfter( self );
				self.remove();
				revertTask( task_id );
			}
		});
	});
})(jQuery);
-->
</script>
</div>
<?php } else { ?>
<p class="alert alert-danger text-center"><strong>You haven't shared anything yet! Go to Tasks!</strong></p>
<?php } ?>

</div><!--/tab-pane-->


        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

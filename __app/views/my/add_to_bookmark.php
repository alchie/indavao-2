<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
		
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#">Add Bookmark</a></li>
</ul>

<div class="brdr bgc-fff pad-10 box-shad" id="tasks">

<div class="alert alert-success">
   <strong>Item Successfully Bookmarked!</strong> 
</div>

<div class="page-header" style="margin-top:0;">
	<h1><?php echo $bookmark_title; ?></h1>
</div>
<?php if( ! is_null( $bookmark_url ) ) { ?>
	<button class="btn btn-primary btn-lg" type="button" id="bookmark_to_fb" data-type="<?php echo $this->input->get('type'); ?>" data-url="<?php echo $bookmark_url; ?>">Share on Facebook >></button>
<?php } ?>

</div>
		</div>
	</div><!--/row-->
</div>


<script>
<!--
$(function() {
	$('#bookmark_to_fb').click(function(){
		var self = $(this);
		var bookmark_url = self.attr('data-url');
		var bookmark_type = self.attr('data-type');
		var postData = {};
		if( bookmark_type == 'realestate' ) {
			postData = {
				property: bookmark_url
			  };
		} else if( bookmark_type == 'business' ) {
			postData = {
				business: bookmark_url
			  };
		}
		FB.api(
			  'me/indavao:bookmark',
			  'post',
			  postData
			  ,
			  function(response) {
				console.log( response );
			  }
			);
		window.location.href=bookmark_url;
	});
});
-->
</script>  
<?php $this->load->view('overall_footer'); ?>

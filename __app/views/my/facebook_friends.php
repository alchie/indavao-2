<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
           <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="<?php echo site_url("my/{$current_user_id}/facebook_friends"); ?>">My Facebook Friends</a></li>
          </ul>
              

<div class="tab-pane brdr bgc-fff pad-10 box-shad active" id="friends">

<p class="text-center"><button DISABLED class="btn btn-xs btn-success fb-ready" id="fetch_friends" data-uid="<?php echo $this->session->userdata('user_id'); ?>">Invite My Friends</button><p>
				<div class="table-responsive">
                <table class="table table-hover" id="friends_list">
                  <thead>
                    <tr>
                      <th>Name</th>
					  <th width="10%"></th>
                    </tr>
                  </thead>
                  <tbody id="friend-items">
				  <?php if( $friends ) { ?>
					  <?php foreach($friends as $friend ) { ?>
						<tr>
							<td id="friend-<?php echo $friend->friend_id; ?>" class="friend-<?php echo $friend->friend_id; ?> friend-item" data-id="<?php echo $friend->friend_id; ?>"><?php echo ($friend->friend_name == '') ? $friend->friend_id : $friend->friend_name; ?></td>
							<td></td>
						</tr>
						<?php } ?>
					<?php }  ?>
				</tbody>
				</table>
<script>
<!--
$(function() { 
	$('#fetch_friends').click(function() { 

		var MY_USER_ID = $(this).attr('data-uid');

		var GG_NUM=50;
		var mshuffle = function(o){
			for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
			return o;
		};

		var loop = function(list){

			if(list.length != 0) {
			  var string = '';
			  var shifting = 0;
			  
			  if (list.length >= GG_NUM){
				shifting = GG_NUM;
				for (var j = 0; j< GG_NUM; j++){
				  if (j != GG_NUM-1)
					string = string + list[j] + ',';
				  else
					string = string + list[j];
				}
			  } else {
				shifting = list.length;
				for (var j = 0; j< list.length; j++){
				  if (j != list.length - 1)
					string = string + list[j] + ',';
				  else
					string = string + list[j];
				}
			  }

			  FB.ui({
					method: 'apprequests', 
					data: MY_USER_ID, 
					message: "Join The #1 Online Listings In Davao City Now!!!", 
					title: "Invite Your Friends", 
					to : string
				},
				function(response) {
					if (response) {
						if(response.error_code != '4201')  {
							for (var i = 0; i < shifting; i++) {
								list.shift();
							}
							if(list.length != 0){
								if(response.error_code != '4201') {
									loop(list);
								} else {}
							} else {
								(function() {
									window.top.location = "<?php echo current_url(); ?>"
									document.getElementById('fb-root').appendChild(e);
									}());
							}
						} else { 
							//alert("Send requests to your friends!");
						}
					} else{
						//alert("Send requests to your friends!");
						}
				});
			}
		};
		
		var invite_friends = function(){
			var friends = new Array();
			FB.api('/me/invitable_friends', function(response) {
				console.log( response );
				for (var i=0; i<response.data.length; i++) {
					friends[i] = response.data[i].id;
				}
				mshuffle(friends);
				loop(friends);
			});
		};
		
		FB.getLoginStatus(function(response) {
			console.log( response );
			$(this).prop('disabled', true);
			if (response.status === 'connected') {
				$(this).remove();
				invite_friends();
			} else {
				FB.login(function(response) {
					console.log( response );
					if (response.status === 'connected') {
						$(this).remove();
						invite_friends();
					}
				});
			}
			$(this).prop('disabled', false);
		});
		
		FB.api("/me/friends", "GET", function( resp ){
			console.log( resp );
			if( typeof resp.data != 'undefined' ) { 
				var newfriends = [];
				var items = $('#friend-items');
				for( i in resp.data ) { 
					var exists = $( '.friend-' + resp.data[i].id );
					if( exists.length == 0 ) {
						var tr = $('<tr>').appendTo( items ).attr('id', 'friend-'+resp.data[i].id).attr('data-friend_id', resp.data[i].id);
						var td = $('<td>').appendTo( tr ).text( resp.data[i].name );
						var td = $('<td>').appendTo( tr ).text( 'New' );
						newfriends.push( resp.data[i].id );
					} else {
						exists.text( resp.data[i].name );
					}
				}
				if( newfriends.length > 0 ) {
					$.post(ajaxPostURL , {
						action : 'add_new_friends',
						friend_ids : newfriends
					}, function(msg) {
						
					}, 'json').fail(function(xhr, textStatus, errorThrown){
						console.log( xhr.responseText );
					});
				}
			}
		});
	});
});
-->
</script>
</div>


</div><!--/tab-pane-->




        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-xs-9 col-sm-10"><h1><?php echo $this->session->userdata('user_name'); ?></h1></div>
    	<div class="col-xs-3 col-sm-2"><img title="profile image" class="img-circle img-responsive pull-right hidden-xs" id="profile-image" src="" style="display:none"></div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('my/account-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
<ul class="nav nav-tabs" id="myTab">
	<li class="<?php echo ($this->input->get('type')==FALSE||$this->input->get('type')=='realestate') ? 'active' : ''; ?>"><a href="<?php echo current_url(); ?>?type=realestate">Real Estate</a></li>
	<li class="<?php echo ($this->input->get('type')=='business') ? 'active' : ''; ?>"><a href="<?php echo current_url(); ?>?type=business">Business</a></li>
</ul>




<div class="brdr bgc-fff pad-10 box-shad" id="tasks">
<?php 

if( $this->input->get('task_id') != FALSE && $this->input->get('friend_id') != FALSE ) {

if( isset($current_task) ) { 
	
	?>
	
<div class="row" style="margin-top:20px;">
	<div class="col-md-8 col-md-offset-2">
		
<div class="panel panel-primary">
	<div class="panel-heading"><h3 class="panel-title">Share : <?php echo $current_task->title; ?></h3></div>
	<div class="panel-body">
		<div class="form-group">
			<input id="task_share_url" data-message="<?php echo ($current_task->task_message != '') ? $current_task->task_message : $current_task->title; ?>" data-task_id="<?php echo $this->input->get('friend_id'); ?>" data-object_id="<?php echo $current_task->object_id; ?>" data-object_type="<?php echo $post_type; ?>" class="form-control"  value="<?php echo site_url( array($post_type , $current_task->object_id, $current_task->slug , $this->session->userdata('user_id')) ); ?>" onclick="this.select();" type="hidden">
			<p class="text-center text-warning"><strong>NOTE</strong>: Share this URL as a <strong>LINK</strong>. Please <strong>WAIT</strong> for the <strong>PREVIEW</strong> to finish loading before click the post button. This link is your referral link as well. It contains your ID (the numbers in the middle) which will be stored on a browser's cookie when opened. This is to ensure that you will get all the referrals from sharing this link!</p>
			<button class="btn btn-danger btn-block btn-lg" id="tasks_grab_google_shorturl">Show Content To Share</button>
		</div>
	</div>
	
		<div class="panel-heading task_review hidden">
			<a href="<?php echo site_url("redirect"); ?>?url=http://fb.com/<?php echo $this->input->get('friend_id'); ?>" target="_blank" class="btn btn-xs btn-danger pull-right">Open</a>
			<h3 class="panel-title">To Friend ID: <?php echo $this->input->get('friend_id'); ?></h3></div>
		<div class="panel-body task_review hidden">
			<div class="form-group">
				<input id="task_destination_id" data-page_id="http://fb.com/<?php echo $this->input->get('friend_id'); ?>" class="form-control" type="text" value="http://fb.com/<?php echo $this->input->get('friend_id'); ?>" onclick="this.select();">
				<p class="text-center text-warning"><strong>NOTE</strong>: Like the page to be able to post the link.</p>
			</div>
		</div>
		<div class="panel-heading task_review hidden"><h3 class="panel-title">Submit Post URL</h3></div>
		<div class="panel-body task_review hidden">
			<div class="form-group">
				<p class="text-center text-warning"><strong>NOTE</strong>: After posting, copy the URL of the time to get Post URL.</p>
				<input id="task_verify_url" class="form-control" type="text" value="" placeholder="Enter Post URL Here">
			</div>
		</div>
		<div class="panel-footer task_review hidden">
			<button class="btn btn-block btn-warning btn-lg" id="task_verify_button" DISABLED>Submit for Review</button>
		</div>
	
</div>

</div>
</div>

<script>
<!--
var taskVerifyPostURL = '<?php echo site_url("my/{$current_user_id}/ajax"); ?>';
var taskURL = '<?php echo current_url(); ?>';
(function($) {
	$('#tasks_grab_google_shorturl').click(function() {
		var longurl = $('#task_share_url').val(),
		message = $('#task_share_url').attr('data-message');
		var apiKey = '<?php echo get_settings_value('GOOGLE_API_KEY'); ?>';
		gapi.client.setApiKey(apiKey);
		
		var textarea = 
		
		gapi.client.load('urlshortener', 'v1', function() {
			var request = gapi.client.urlshortener.url.insert({
				'resource': {
					'longUrl': longurl
				}
			});
			var resp = request.execute(function(resp) {
				var showMessage = function( url ) {
					$('#task_share_url').attr('data-short_url', url);
					$('#tasks_grab_google_shorturl').hide();
					$('<textarea />').addClass('form-control').val( message + " " + url).insertAfter( $('#tasks_grab_google_shorturl') ).click(function(){
						$(this).select();
					});
				}
				if (resp.error) {
					console.log( resp.error.message );
					showMessage( $('#task_share_url').val() );
				} else {
					console.log( resp.id );
					showMessage( resp.id );
				}
				$('.task_review').removeClass('hidden');
			});
		});
	});
	
	$('#task_verify_url').keyup(function(){
		$('#task_verify_button').prop('disabled', false);
	});
	
	$('#task_verify_button').click(function(){
		
		$(this).prop('disabled', true);
		
		var object_id = $('#task_share_url').attr('data-object_id'),
		object_type = $('#task_share_url').attr('data-object_type'),
		task_id = $('#task_share_url').attr('data-task_id'),
		post_url = $('#task_verify_url').val();
		
		$.post(taskVerifyPostURL, {
			task_id : task_id,
			object_id : object_id,
			object_type : object_type,
			post_url : post_url,
			action : 'task_verified',
		}, function(msg) {
			console.log( msg );
			if( msg.error == false ) {
				window.location.href = taskURL;
			}
		}, 'json').fail(function(xhr, textStatus, errorThrown){
				console.log( xhr.responseText );
			});
		
	});
	
})(jQuery); 

-->
</script>
<?php } ?>
<?php } elseif( $this->input->get('task_id') != FALSE ) { ?>
<?php if( count( $friends ) > 0 ) { ?>
<div class="list-group">
	<?php foreach($friends as $friend) { ?>	
	<a href="<?php echo current_url() . "?type=".$post_type."&task_id=" . $this->input->get('task_id') . "&friend_id=" . $friend->friend_id; ?>" class="list-group-item">
		<h4 class="list-group-item-heading"><?php echo ($friend->friend_name == '') ? $friend->friend_id : $friend->friend_name; ?></h4>
	</a>
	<?php } ?>
</div>
<?php } ?>
<?php } else { ?>
<div class="list-group">
	<?php foreach($tasks as $task) { ?>	
	<a href="<?php echo current_url() . "?type=".$post_type."&task_id=" . $task->re_id; ?>" class="list-group-item">
		<h4 class="list-group-item-heading"><?php echo $task->re_title; ?></h4>
	</a>
	<?php } ?>
</div>
<?php } ?>
</div>





        </div><!--/col-9-->
    </div><!--/row-->
</div>             
<?php $this->load->view('overall_footer'); ?>

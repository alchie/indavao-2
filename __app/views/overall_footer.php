</div>
<div id="footer" class="hidden-print">
      <div class="container" id="bottom-links">
	
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-8 pull-left left">
			<ul>
				<li><a href="<?php echo site_url('about-us'); ?>">About Us</a></li>
				<li><a href="<?php echo site_url('terms-and-conditions'); ?>">Terms and Conditions</a></li>
				<li><a href="<?php echo site_url('privacy-policy'); ?>">Privacy Policy</a></li>
				<li><a href="<?php echo site_url('contact-us'); ?>">Contact Us</a></li>
				<?php echo ($referrer != '') ? '<li>REFERRED BY : ' . $referrer . '</li>' : '' ; ?>
			</ul>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 right hidden-xs">
		    <p class="pull-right">
		    <a title="Trokis Philippines, LLC" href="https://www.trokis.com/" target="_blank"><img border="0" src="https://2.bp.blogspot.com/-oor14dWMUvU/VJ3Aeia9KxI/AAAAAAAAFHE/6CI-5gH6My8/s1600/trokis-logo-32.png" /></a>
		    </p>
		</div>
	</div>

<?php if( $this->session->userdata('manager') ) { ?>
<center>
<div class="btn-group btn-group-xs">
 <button type="button" class="btn btn-danger" id="clear-current-page-cache">Clear Page Cache</button>
  <?php if( isset($manage_page_url) ) { ?>
	<a href="<?php echo $manage_page_url; ?>" class="btn btn-success">Manage Page</a>
  <?php } ?>
</div>
</center>
	<script>
	<!--
	$('#clear-current-page-cache').click(function(){
		$.post(ajaxPostURL, { uri1 : '<?php echo ($this->uri->segment(1)) ? $this->uri->segment(1) : 'default'; ?>', 
			uri2 : '<?php echo ($this->uri->segment(2)) ? $this->uri->segment(2) : 'index'; ?>',
			action : 'clear_cache' }, 
		function(msg) {
			if( msg.error == false ) {
				window.location.reload();
			}
		}, 'json').fail(function(xhr, textStatus, errorThrown){
			console.log( xhr.responseText );
		});
	});
	-->
	</script>
<?php } ?>
      </div>
    </div>

<?php echo $footer_top; ?>
<?php $footer_javascripts = (array(
			0 => "//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js", //base_url('assets/js/bootstrap.min.js'),
			50 => base_url('assets/js/plugins/bootstrap-select/bootstrap-select.min.js'),
			100 => base_url('assets/js/jquery.lazyload.min.js'),
			150 => base_url('assets/js/plugins/bxslider/jquery.bxslider.min.js'),
			151 => base_url('assets/js/plugins/fancyBox/source/jquery.fancybox.pack.js'),
			200 => base_url('assets/js/indavao.custom.js'),
		) + $footer_js); 
		ksort($footer_javascripts);
		if( $footer_javascripts ) {
			foreach( $footer_javascripts as $fjs ) {
				echo '<script type="text/javascript" src="'.$fjs.'"></script>' . "\n";
			}
		}
?>
<?php echo $footer; ?>
<?php echo $footer_bottom; ?>
</body>
</html>

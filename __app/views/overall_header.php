<!DOCTYPE html>
<html lang="en" xmlns:fb="http://ogp.me/ns/fb#" itemscope itemtype="http://schema.org/<?php echo $itemscope; ?>"> 
  <head <?php echo $head_attr; ?>>
<?php echo $header_top; ?>
    <title><?php echo $page_title; ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="https://2.bp.blogspot.com/-oor14dWMUvU/VJ3Aeia9KxI/AAAAAAAAFHE/6CI-5gH6My8/s1600/trokis-logo-32.png" type="image/x-icon" />
    <?php echo $meta_tags; ?>
    <?php echo $opengraph; ?>
    <?php echo $itemprop; ?>
    
<?php $header_stylesheet = array_merge($header_css, array(
			"//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css", //base_url('assets/css/bootstrap.min.css'),
			base_url('assets/js/plugins/bootstrap-select/bootstrap-select.min.css'),
			base_url('assets/font-awesome/css/font-awesome.min.css'),
			base_url('assets/js/plugins/bxslider/jquery.bxslider.css'),
			base_url('assets/js/plugins/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.min.css'),
			base_url('assets/js/plugins/fancyBox/source/jquery.fancybox.css'),
			base_url('assets/css/styles.css'),
			base_url('assets/front/stylesheets/superslides.css'),
		)); 
		if( $header_stylesheet ) {
			foreach( $header_stylesheet as $hcss ) {
				echo '<link href="'.$hcss.'" rel="stylesheet">' . "\n";
			}
		}
?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<?php $header_javascripts = array_merge($header_js, array(
			"//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js", //base_url('assets/js/jquery-1.10.2.min.js'),
			"//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js", //base_url('assets/js/plugins/jquery-ui/js/jquery-ui-1.10.4.min.js'),
		)); 
		if( $header_javascripts ) {
			foreach( $header_javascripts as $hjs ) {
				echo '<script type="text/javascript" src="'.$hjs.'"></script>' . "\n";
			}
		}
?>    
<?php echo $header; ?>
<?php echo $header_bottom; ?>
<?php if( $isLoggedIn ) { ?>
<script type="text/javascript">
<!--
var currentURL = '<?php echo current_url(); ?>';
<?php if( isset( $ajax_url ) ) { ?>
var ajaxPostURL = '<?php echo $ajax_url; ?>';
<?php } ?>
-->
</script>
<?php } ?>

</head>

  <body>

<div id="wrap">  
  <!-- Fixed navbar -->
  <div id="header">
    <div id="topnav" class="navbar navbar-inverse" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url(); ?>" title="<?php echo get_settings_value('site_name','The #1 Online Listings In Davao City'); ?>"><?php echo get_settings_value('site_name','InDavao.Net'); ?></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo site_url('realestate'); ?>">Find a Home <span class="caret"></span></a>
					<ul class="dropdown-menu">
						 <li><a href="<?php echo site_url('realestate/projects'); ?>">Development Projects</a></li>
						 <li><a href="<?php echo site_url('realestate/residential'); ?>">Residential Properties</a></li>
						 <li><a href="<?php echo site_url('realestate/commercial'); ?>">Commercial Properties</a></li>
						 <li><a href="<?php echo site_url('realestate/foreclosures'); ?>">Foreclosures</a></li>
						 <li><a href="<?php echo site_url('realestate/open_lots'); ?>">Open Lots / Land</a></li>
						 <li><a href="<?php echo site_url('realestate/condo'); ?>">Condominiums</a></li>
						 <li><a href="<?php echo site_url('realestate/apartments'); ?>">Apartments</a></li>
						 <li><a href="<?php echo site_url('realestate/house_and_lot'); ?>">House &amp; Lot</a></li>
						 <li><a href="<?php echo site_url('realestate/browse'); ?>">Browse All Properties</a></li>
					</ul>
				</li>
				<?php /* <li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo site_url('jobs'); ?>">Find a Job <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<?php if($job_functions) { 
							foreach( $job_functions as $navjob ) { 
								echo "<li><a href='" . site_url('jobs/' . $navjob->tax_name) ."'>{$navjob->tax_label} Jobs</a></li>";
							}
						} ?>
						<li><a href="<?php echo site_url('jobs/browse'); ?>">Browse All Jobs</a></li>
					</ul>
				</li>
				*/ ?>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo site_url('local_business'); ?>">Find a Business <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<?php if($business_categories) { 
							foreach( $business_categories as $navbc ) { 
								echo "<li><a href='" . site_url('local_business/' . $navbc->tax_name) ."'>{$navbc->tax_label}</a></li>";
							}
						} ?>
						 <li><a href="<?php echo site_url('local_business/browse'); ?>">Browse All Businesses</a></li>
					</ul>
				</li>
				
<?php if ($isLoggedIn) { ?>
               <li class="dropdown visible-xs">
				   <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo site_url("my/{$current_user_id}/account"); ?>"><i class="glyphicon glyphicon-user"></i> My Account <span class="caret"></span></a>
				   <ul class="dropdown-menu">
					   <li><a href="<?php echo site_url("my/{$current_user_id}/account"); ?>">My Account</a></li>
					   
					   <li class="divider"></li>
					   <li><a href="<?php echo site_url("my/{$current_user_id}/bookmarks"); ?>">My Bookmarks</a></li>
					   <li><a href="<?php echo site_url("my/{$current_user_id}/referrals"); ?>">My Referrals</a></li>
					   
					   <?php  if( count( array_intersect($this->session->userdata('permissions'), array('add_business', 'add_realestate') ) ) > 0 ) { ?>
					   <li class="divider"></li>
					   <li class="dropdown-header">My Posts</li>
							<?php if( array_search('add_business', $this->session->userdata('permissions') ) !== false) { ?>
							<li><a href="<?php echo site_url("my/{$current_user_id}/business"); ?>">My Business</a></li>
						   <?php } ?>
							
							<?php if( array_search('add_realestate', $this->session->userdata('permissions') ) !== false) { ?>
							<li><a href="<?php echo site_url("my/{$current_user_id}/properties"); ?>">My Properties</a></li>
						   <?php } ?>
						
					   <?php } ?>

					</ul>
			   </li>
<?php } else { ?>
	<li class="visible-xs"><a href="<?php echo site_url('login'); ?>"><i class="glyphicon glyphicon-log-in"></i> Login to My Account</a></li>
<?php } ?>
				
			</ul>
 <ul class="nav navbar-nav pull-right hidden-xs hidden-sm">
<?php if ($isLoggedIn) { ?>
               <li class="dropdown">
				   <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo site_url("my/{$current_user_id}/account"); ?>"><i class="glyphicon glyphicon-user"></i> My Account <span class="caret"></span></a>
				   <ul class="dropdown-menu">
					   <li><a href="<?php echo site_url("my/{$current_user_id}/account"); ?>">My Account</a></li>

					  <?php  if( count( array_intersect($this->session->userdata('permissions'), array('add_business', 'add_realestate') ) ) > 0 ) { ?>
					   <li class="divider"></li>
					   <li class="dropdown-header">My Posts</li>
						   <?php if( array_search('add_business', $this->session->userdata('permissions') ) !== false) { ?>
							<li><a href="<?php echo site_url("my/{$current_user_id}/business"); ?>">My Business</a></li>
						   <?php } ?>
						   <?php if( array_search('add_realestate', $this->session->userdata('permissions') ) !== false) { ?>
							<li><a href="<?php echo site_url("my/{$current_user_id}/properties"); ?>">My Properties</a></li>
						   <?php } ?>
					   <?php } ?>
					   
					   <li class="divider"></li>
					   <li class="dropdown-header">My Links</li>
					   <li><a href="<?php echo site_url("my/{$current_user_id}/bookmarks"); ?>">My Bookmarks</a></li>
					   
					   <?php if( $this->session->userdata('manager') ) { ?>
					   <li><a href="<?php echo site_url("my/{$current_user_id}/referrals"); ?>">My Referrals</a></li>
					   <?php } ?>
					   
					   <?php if( $this->session->userdata('isPoints') ) { ?>
						   <li class="divider"></li>
						   <li class="dropdown-header">My Points</li>
						   <li><a href="<?php echo site_url("my/{$current_user_id}/points"); ?>">My Points</a></li>
						   <li><a href="<?php echo site_url("my/{$current_user_id}/redemptions"); ?>">My Redemptions</a></li>
						<?php } ?>
					 
					</ul>
			   </li>
<?php } else { ?>
	<li><a href="<?php echo site_url('login'); ?>"><i class="glyphicon glyphicon-log-in"></i> Login to My Account</a></li>
<?php } ?>
            </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>
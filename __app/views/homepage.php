<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title><?php echo $page_title; ?></title>
  <link rel="stylesheet" href="<?php echo base_url('assets/front/stylesheets/superslides.css'); ?>">
</head>
<body>
  <div id="slides">
    <div class="slides-container">
      <img src="<?php echo base_url('assets/front/images/people.jpeg'); ?>" alt="Cinelli">
      <img src="<?php echo base_url('assets/front/images/surly.jpeg'); ?>" width="1024" height="682" alt="Surly">
      <img src="<?php echo base_url('assets/front/images/cinelli-front.jpeg'); ?>" width="1024" height="683" alt="Cinelli">
      <img src="<?php echo base_url('assets/front/images/affinity.jpeg'); ?>" width="1024" height="685" alt="Affinity">
    </div>

    <nav class="slides-navigation">
      <a href="#" class="next">Next</a>
      <a href="#" class="prev">Previous</a>
    </nav>
  </div>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <script src="<?php echo base_url('assets/front/javascripts/jquery.easing.1.3.js'); ?>"></script>
  <script src="<?php echo base_url('assets/front/javascripts/jquery.animate-enhanced.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/front/jquery.superslides.js'); ?>" type="text/javascript" charset="utf-8"></script>
  <script>
    $('#slides').superslides({
      animation: 'fade'
    });
  </script>
</body>
</html>
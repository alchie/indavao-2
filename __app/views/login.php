<?php $this->load->view('overall_header'); ?>
<div class="container main-body" id="frontpage">
	
<div id="fb-root"></div>
<script>
  function statusChangeCallback(response) {
    if (response.status === 'connected') {
      checkUserRecord();
    } 
  }

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '<?php echo $facebook_app_id; ?>',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.1
  });

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });

  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function checkUserRecord() {
    FB.api('/me', function(response) {
		console.log( response );
      $.ajax({
		  url : '<?php echo site_url('login/facebook'); ?>',
		  data : response,
		  type : 'POST',
		  dataType: "json",
		  success : function(msg){
			  console.log( msg );
				 if( msg['loggedIn'] == true ) {
					window.location.replace("<?php echo site_url('my/account'); ?>");
				 }
			},
			error: function(xhr, desc, err) {
                  console.log(xhr);
                  console.log("Details: " + desc + "\\nError:" + err);
                }
		  });
    });
  }
</script>
	<div class="row" style="margin-top:40px;">
		<div class="col-md-6 col-md-offset-3">
			
		<div class="panel panel-default panel-facebook">
		  <div class="panel-heading">
			<h3 class="panel-title">Login through Facebook</h3>
		  </div>
		  <div class="panel-body">
			<fb:login-button max_rows="1" size="xlarge" data-width="430" show_faces="true" auto_logout_link="true" data-scope="<?php echo implode(",", $facebook_permissions); ?>" onlogin="checkLoginState();">Login with Facebook</fb:login-button>
		  </div>
		</div>
			
		</div>
	</div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

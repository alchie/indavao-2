<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
       
      <div class="row">

        <div class="col-md-8">

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="glyphicon glyphicon-envelope"></i> Contact Us</h3>
  </div>
  <div class="panel-body">
<?php if( isset($message_sent) && $message_sent === TRUE ) { ?>

<div class="alert alert-success alert-dismissable">
  <strong>Your message has been sent! We will contact you as soon as possible! Thanks!</strong> 
</div>

<?php } else { ?>
	
<?php if( $alert===TRUE ) { ?>
  <div class="alert alert-<?php echo $alert_status; ?> alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php echo $alert_message; ?>
</div>
<?php } ?>

<form action="" method="POST">
  <div class="form-group">
    <label>Name</label>
    <input name="firstname" type="text" class="form-control" placeholder="Enter Your Name Here" value="<?php echo ($this->input->post('firstname') != "") ? $this->input->post('firstname') : $this->session->userdata('meta_name');  ?>">
  </div>
  <div class="form-group">
    <label>Phone</label>
    <input name="phone" type="phone" class="form-control" placeholder="Enter Your Phone" value="<?php echo ($this->input->post('phone')) ? $this->input->post('phone') : $this->session->userdata('meta_phone'); ?>">
  </div>
  <div class="form-group">
    <label>Email</label>
    <input name="email" type="email" class="form-control" placeholder="Enter Your Email" value="<?php echo ($this->input->post('email')) ? $this->input->post('email') : $this->session->userdata('meta_email'); ?>">
  </div>
  <div class="form-group">
    <label>Message</label>
    <textarea name="message" class="form-control" rows="5" placeholder="What's Your Message"><?php echo ($this->input->post('message')!==FALSE) ? $this->input->post('message') : ''; ?></textarea>
  </div>
  <button type="submit" class="btn btn-warning btn-block btn-lg">Submit</button>
</form>
<?php } ?>
  </div>
</div>



      </div>  <!-- /col-md-6 -->
		<div class="col-md-4">

		</div>
      </div>  <!-- /row -->

    </div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
<?php 
$main_link = site_url(array('local_business'));
if( isset( $slug ) ) {
	$main_link = site_url(array('local_business', $slug));
}
	echo "\t<url>\n";
	echo "\t\t<loc>" .  $main_link . "</loc>\n";
	echo "\t\t<changefreq>daily</changefreq>\n";
	echo "\t</url>\n";
 

foreach( $directories as $dir ) {
	//print_r($dir);
	echo "\t<url>\n";
	echo "\t\t<loc>" . site_url(array('company', $dir->dir_id, $dir->dir_slug)) . "</loc>\n";
	
	if( $dir->thumbnail != "") {
		echo "\t\t<image:image>\n";
		echo "\t\t\t<image:loc>". get_settings_value('upload_url') . $dir->thumbnail ."</image:loc>\n"; 
		echo "\t\t</image:image>\n";
	}
	
	echo "\t\t<changefreq>yearly</changefreq>\n";
	echo "\t</url>\n";
	
	echo "\t<url>\n";
	echo "\t\t<loc>" . site_url(array('company', $dir->dir_id, $dir->dir_slug, "related")) . "</loc>\n";
	echo "\t\t<changefreq>yearly</changefreq>\n";
	echo "\t</url>\n";
}
?></urlset>

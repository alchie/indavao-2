<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php 

	echo "\t<sitemap>\n";
	echo "\t\t<loc>" .  base_url() . implode( "/", array('sitemap', 'local_business')) . ".xml</loc>\n";
	echo "\t</sitemap>\n";
	
foreach( $business_categories as $dir_cat ) {
	echo "\t<sitemap>\n";
	echo "\t\t<loc>" . base_url() . implode( "/", array('sitemap', 'local_business', $dir_cat->tax_name)) . ".xml</loc>\n";
	echo "\t</sitemap>\n";
	
	if( count($dir_cat->children) > 0 ) {
		foreach( $dir_cat->children as $dir_cat_child ) {
			echo "\t<sitemap>\n";
			echo "\t\t<loc>" . base_url() . implode( "/", array('sitemap', 'local_business', $dir_cat_child->tax_name)) . ".xml</loc>\n";
			echo "\t</sitemap>\n";
		}
	}
}

	echo "\t<sitemap>\n";
	echo "\t\t<loc>" .  base_url() . implode( "/", array('sitemap', 'realestate')) . ".xml</loc>\n";
	echo "\t</sitemap>\n";
	
foreach( array('projects',
'residential',
'commercial',
'foreclosures',
'open_lots',
'condo',
'apartments',
'house_and_lot'
) as $re_cat ) {
	echo "\t<sitemap>\n";
	echo "\t\t<loc>" . base_url() . implode( "/", array('sitemap', 'realestate', $re_cat)) . ".xml</loc>\n";
	echo "\t</sitemap>\n";
}
?>
</sitemapindex>

<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php 
foreach( $tags as $tag ) {

	$local_business = site_url(array('local_business', 'tag', $tag->tag_slug));
	
	echo "\t<url>\n";
	echo "\t\t<loc>" . $local_business . "</loc>\n";
	echo "\t\t<changefreq>monthly</changefreq>\n";
	echo "\t</url>\n";

}
?> 
</urlset>

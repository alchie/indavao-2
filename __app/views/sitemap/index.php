<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php foreach( array('about-us', 
'terms-and-conditions', 
'privacy-policy',
'contact-us',
'realestate/browse',
'realestate/projects',
'realestate/residential',
'realestate/commercial',
'realestate/foreclosures',
'realestate/open_lots',
'local_business/browse',
) as $links ) {
	echo "\t<url>\n";
	echo "\t\t<loc>" . site_url($links) . "</loc>\n";
	echo "\t</url>\n";
} 

foreach( $business_categories as $dir_cat ) {
	echo "\t<url>\n";
	echo "\t\t<loc>" . site_url(array('local_business', $dir_cat->tax_name)) . "</loc>\n";
	echo "\t\t<changefreq>monthly</changefreq>\n";
	echo "\t</url>\n";
}
?> 
</urlset>

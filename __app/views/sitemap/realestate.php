<?php echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n"; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php 
	if( $slug != "") {
		echo "\t<url>\n";
		echo "\t\t<loc>" . site_url(array('realestate', $slug)) . "</loc>\n";
		echo "\t\t<changefreq>monthly</changefreq>\n";
		echo "\t</url>\n";
	}
foreach( $properties as $property ) {

	$loc = site_url(array('property', $property->re_id, $property->re_slug));
	$loc_related = site_url(array('property', $property->re_id, $property->re_slug, "related"));
	if( $slug == 'projects' ) {
		$loc = site_url(array('realestate', $property->re_slug));
	}
	echo "\t<url>\n";
	echo "\t\t<loc>" . $loc . "</loc>\n";
	echo "\t\t<changefreq>monthly</changefreq>\n";
	echo "\t</url>\n";
	echo "\t<url>\n";
	echo "\t\t<loc>" . $loc_related . "</loc>\n";
	echo "\t\t<changefreq>monthly</changefreq>\n";
	echo "\t</url>\n";
}
?> 
</urlset>

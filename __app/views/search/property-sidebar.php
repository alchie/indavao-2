<form method="POST" action="<?php echo site_url('search'); ?>" style="margin:0;">
	<input type="hidden" name="object" value="realestate">
<div class="panel panel-primary" id="refine-search">
	<div class="panel-heading">
		<h3 class="panel-title">Refine Search</h3>
	</div>
	<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">Listing Types</label>
					<?php foreach(array('sale' => 'For Sale', 'rent' => 'For Rent', 'assume' => 'For Assume') as $key=>$label) { 
							if(in_array($key, ((isset($search_keys->status)) ? $search_keys->status : array()))) { ?>
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input name="status[]" type="checkbox" value="<?php echo $key; ?>" class="hidden" CHECKED><span><?php echo $label; ?></span></label>
						<?php } else { ?>
							<label class="btn btn-default btn-xs btn-block"><i class="glyphicon glyphicon-remove"></i> <input name="status[]" type="checkbox" value="<?php echo $key; ?>" class="hidden"><span><?php echo $label; ?></span></label>
						<?php } ?>
					<?php } ?>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					<label class="control-label">Bedrooms</label>
					<div>
					 <select class="form-control selectpicker show-menu-arrow" name="beds">
						 <option value="">Any</option>
						  <?php for($i=1;$i<(get_settings_value('realestate_search_bathrooms', 5)+1);$i++) { 
							  echo "<option";
							  if(isset($search_keys->beds) && $i==$search_keys->beds) { echo " SELECTED"; }
							  echo ">$i</option>"; 
							  } ?>
						</select>
					</div>
				  </div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<label class="control-label">Bathrooms</label>
					<div>
					  <select class="form-control selectpicker show-menu-arrow" name="baths">
						  <option value="">Any</option>
						  <?php for($i=1;$i<(get_settings_value('realestate_search_bathrooms', 5)+1);$i++) { 
							  echo "<option";
							  if( isset($search_keys->baths) && $i==$search_keys->baths) { echo " SELECTED"; }
							  echo ">$i</option>";
							  } ?>
						</select>
					</div>
				  </div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">Property Types</label>

						<?php foreach(array(
						'PROJ' => 'Development Projects', 
						'COMM' => 'Commercial Properties',
						'RESID' => 'Residential Properties',
						'HOUSE' => 'House and Lot',
						'LOT' => 'Lot / Land',
						'CONDO' => 'Condominiums',
						'APART' => 'Apartments',
						'FCL' => 'Forclusures',
						) as $key=>$label) { 
							if(in_array($key, ((isset($search_keys->property_type)) ? $search_keys->property_type : array()))) { ?>
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input name="property_type[]" type="checkbox" value="<?php echo $key; ?>" class="hidden" CHECKED><span><?php echo $label; ?></span></label>
						<?php } else { ?>
							<label class="btn btn-default btn-xs  btn-block"><i class="glyphicon glyphicon-remove"></i> <input name="property_type[]" type="checkbox" value="<?php echo $key; ?>" class="hidden"><span><?php echo $label; ?></span></label>
						<?php } ?>
					<?php } ?>

					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Location</label>
					<div>
						  <?php 
							function echo_locations($locations, $prefix='', $selected=array()) {
								foreach($locations as $location) { 
									if(in_array($location->loc_id, ((isset($selected)) ? $selected : array()))) { 
										echo "<label class=\"btn btn-success btn-{$prefix}  btn-block\"><i class=\"glyphicon glyphicon-ok\"></i> <input name=\"location[]\" type=\"checkbox\" value=\"{$location->loc_id}\" class=\"hidden\" CHECKED><span>{$location->loc_name}</span></label>";
									} else {
										echo "<label class=\"btn btn-default btn-{$prefix}  btn-block\"><i class=\"glyphicon glyphicon-remove\"></i> <input name=\"location[]\" type=\"checkbox\" value=\"{$location->loc_id}\" class=\"hidden\"><span>{$location->loc_name}</span></label>";
									}
									if( isset($location->children) && (count($location->children) > 0) ) {
										echo_locations($location->children, "xs", $selected);
									}
								} 
							}
							if( $locations ) {
								echo_locations($locations, 'xs', ((isset($search_keys->location)) ? $search_keys->location : array()));
							}
						   ?>
					</div>
				  </div>
				</div>
			</div>

	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-warning btn-block"><i class="glyphicon glyphicon-search"></i> Refine Search</button>
	</div>
	
</div>
</form>


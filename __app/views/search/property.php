<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
      <div class="row">

		<div class="col-md-8">

<a href="#refine-search" class="btn btn-warning btn-block visible-xs" style="margin-bottom:15px;">Refine Your Search</a>
<div class="container-pad" id="property-listings">
			
<div class="row">
				<?php foreach($properties as $property) {  ?>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                    <!-- Begin Listing: 609 W GRAVERS LN-->
                    <div class="brdr bgc-fff pad-10 box-shad btm-mrg-20 property-listing">
                        <div class="media">
                            <a class="pull-left image" href="<?php echo site_url('realestate/'. $property->re_slug ); ?>" target="_parent">
<?php if ( $property->thumbnail == '' ) { ?>
	 <img alt="image" class="img-responsive lazy" data-original="<?php echo base_url() .'assets/images/photo-icon.png'; ?>">
<?php } else { ?>
	<img alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $property->thumbnail; ?>">
<?php } ?>
                           </a>

                            <div class="media-body fnt-smaller">
                                <a href="<?php echo site_url('realestate/'. $property->re_slug ); ?>" target="_parent"></a>

                                <h4 class="media-heading">
                                  <a href="<?php echo site_url('realestate/'. $property->re_slug ); ?>" target="_parent"><?php echo $property->re_title; ?> <br>
                                  <small class="pull-right"><?php echo  $property->address; ?></small>
                                  </a>
                                </h4>
                                <ul class="list-inline mrg-0 btm-mrg-10 clr-535353">
<?php if( $property->floor_area != '' ) { ?><li><?php echo  $property->floor_area; ?> SqFt</li><?php } ?>
<?php if( $property->beds != '' ) { ?><li style="list-style: none">&middot;</li><li><?php echo  $property->beds; ?> Beds</li><?php } ?>
<?php if( $property->baths != '' ) { ?><li style="list-style: none">&middot;</li><li><?php echo  $property->baths; ?> Baths</li><?php } ?>
                                </ul>

                              <p class="abstract"><?php echo  $property->abstract; ?></p>
<?php if ( $property->developer != '') { ?>
                                <span class="fnt-smaller fnt-lighter fnt-arial developer"><strong>Developer:</strong> <?php echo  $property->developer; ?></span>
<?php } ?>
                            </div>
                        </div>
                    </div><!-- End Listing-->
				</div>
				<?php } ?>
				
            </div><!-- End row -->
        </div><!-- End container-pad -->


<?php bootstrap_pagination( $pages, $current_page, current_url() . "?", 10 ); ?>

		</div>
		
		
		
		        <div class="col-md-4">
			<?php $this->load->view('search/property-sidebar'); ?>
		</div>
		
      </div>  <!-- /row -->
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

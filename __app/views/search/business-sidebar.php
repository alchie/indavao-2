<form method="POST" action="<?php echo site_url('search'); ?>" style="margin:0;">
	<input type="hidden" name="object" value="business">
<div class="panel panel-primary" id="refine-search">
	<div class="panel-heading">
		<h3 class="panel-title">Refine Search</h3>
	</div>
	<div class="panel-body">
		
		<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Business Category</label>
					<div>
					   <select class="form-control selectpicker show-menu-arrow" data-live-search="true" multiple title="Select a Business Category" name="category[]">
						  <?php 
								foreach($business_categories as $business_category) { 
									echo "<optgroup label=\"{$business_category->tax_label}\">";
									
									if( isset($business_category->children) && (count($business_category->children) > 0) ) {
										foreach($business_category->children as $business_category_child) {
											echo "<option value=\"{$business_category_child->tax_id}\"";
											if( isset($search_keys->category) && in_array($business_category_child->tax_id, $search_keys->category) ) {
												echo "selected=\"selected\"";
											}
											echo ">{$business_category_child->tax_label}</option>";
										}
									}
									
									echo "</optgroup>";
								} 
						   ?>
						</select>
					</div>
				  </div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Location</label>
					<div>
						  <?php 
							function echo_locations($locations, $prefix='', $selected=array()) {
								foreach($locations as $location) { 
									if(in_array($location->loc_id, ((isset($selected)) ? $selected : array()))) { 
										echo "<label class=\"btn btn-success btn-{$prefix}  btn-block\"><i class=\"glyphicon glyphicon-ok\"></i> <input name=\"location[]\" type=\"checkbox\" value=\"{$location->loc_id}\" class=\"hidden\" CHECKED><span>{$location->loc_name}</span></label>";
									} else {
										echo "<label class=\"btn btn-default btn-{$prefix}  btn-block\"><i class=\"glyphicon glyphicon-remove\"></i> <input name=\"location[]\" type=\"checkbox\" value=\"{$location->loc_id}\" class=\"hidden\"><span>{$location->loc_name}</span></label>";
									}
									if( isset($location->children) && (count($location->children) > 0) ) {
										echo_locations($location->children, "xs", $selected);
									}
								} 
							}
							if( $locations ) {
								echo_locations($locations, 'xs', ((isset($search_keys->location)) ? $search_keys->location : array()));
							}
						   ?>
					</div>
				  </div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group"> 
						<label class="control-label">Attributes</label>
					<?php foreach(array('verified' => 'Verified', 'phone' => 'With Phone', 'email' => 'With Email') as $key=>$label) { 
							if((isset($search_keys->$key)) && $search_keys->$key == '1' ) { ?>
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input name="<?php echo $key; ?>" type="checkbox" value="1" class="hidden" CHECKED><span><?php echo $label; ?></span></label>
						<?php } else { ?>
							<label class="btn btn-default btn-xs btn-block"><i class="glyphicon glyphicon-remove"></i> <input name="<?php echo $key; ?>" type="checkbox" value="1" class="hidden"><span><?php echo $label; ?></span></label>
						<?php } ?>
					<?php } ?>
					</div>
				</div>
			</div>
			
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-warning btn-block"><i class="glyphicon glyphicon-search"></i> Refine Search</button>
	</div>
	
</div>
</form>


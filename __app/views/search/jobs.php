<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
      <div class="row">

		<div class="col-md-8">

		<a href="#refine-search" class="btn btn-warning btn-block visible-xs" style="margin-bottom:15px;">Refine Your Search</a>


		</div>
		
		<div class="col-md-4">
			<?php $this->load->view('search/jobs-sidebar'); ?>
		</div>
		
      </div>  <!-- /row -->
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

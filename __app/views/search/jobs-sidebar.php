<form method="POST" action="<?php echo site_url('search'); ?>" style="margin:0;">
	<input type="hidden" name="object" value="jobs">
<div class="panel panel-primary" id="refine-search">
	<div class="panel-heading">
		<h3 class="panel-title">Refine Search</h3>
	</div>
	<div class="panel-body">
		
		<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Job Function</label>
					<div>
					  <select class="form-control selectpicker show-menu-arrow" data-live-search="true" multiple title="Select a Job Function" name="job_function[]">
						  <?php 
								foreach($job_functions as $job_function) { 
									echo "<optgroup label=\"{$job_function->tax_label}\">";
									
									if( isset($job_function->children) && (count($job_function->children) > 0) ) {
										foreach($job_function->children as $job_function_child) {
											echo "<option value=\"{$job_function_child->tax_id}\"";
											if( isset($search_keys->job_function) && in_array($job_function_child->tax_id, $search_keys->job_function) ) {
												echo "selected=\"selected\"";
											}
											echo ">{$job_function_child->tax_label}</option>";
										}
									}
									
									echo "</optgroup>";
								} 
						   ?>
						</select>
					</div>
				  </div>
				</div>
			</div>
		
			<?php if( isset($locations) ) { ?>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Location</label>
					<div>
						  <?php 
							function echo_locations($locations, $prefix='', $selected=array()) {
								foreach($locations as $location) { 
									if(in_array($location->loc_id, ((isset($selected)) ? $selected : array()))) { 
										echo "<label class=\"btn btn-success btn-{$prefix}  btn-block\"><i class=\"glyphicon glyphicon-ok\"></i> <input name=\"location[]\" type=\"checkbox\" value=\"{$location->loc_id}\" class=\"hidden\" CHECKED><span>{$location->loc_name}</span></label>";
									} else {
										echo "<label class=\"btn btn-default btn-{$prefix}  btn-block\"><i class=\"glyphicon glyphicon-remove\"></i> <input name=\"location[]\" type=\"checkbox\" value=\"{$location->loc_id}\" class=\"hidden\"><span>{$location->loc_name}</span></label>";
									}
									if( isset($location->children) && (count($location->children) > 0) ) {
										echo_locations($location->children, "xs", $selected);
									}
								} 
							}
							
								echo_locations($locations, 'xs', ((isset($search_keys->location)) ? $search_keys->location : array()));
						   ?>
					</div>
				  </div>
				</div>
			</div>
			<?php } ?>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">Job Status</label>
					<?php foreach(array('full_time' => 'Full Time', 'part_time' => 'Part Time', 'internship' => 'Internship') as $key=>$label) { 
							if(in_array($key, ((isset($search_keys->job_status)) ? $search_keys->job_status : array()))) { ?>
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input name="job_status[]" type="checkbox" value="<?php echo $key; ?>" class="hidden" CHECKED><span><?php echo $label; ?></span></label>
						<?php } else { ?>
							<label class="btn btn-default btn-xs btn-block"><i class="glyphicon glyphicon-remove"></i> <input name="job_status[]" type="checkbox" value="<?php echo $key; ?>" class="hidden"><span><?php echo $label; ?></span></label>
						<?php } ?>
					<?php } ?>
					</div>
				</div>
			</div>
			
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-warning btn-block"><i class="glyphicon glyphicon-search"></i> Refine Search</button>
	</div>
	
</div>
</form>


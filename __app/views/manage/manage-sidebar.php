
<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">Manage</h3>
  </div>
<ul class="list-group">
	<?php if( array_search('manage_users', $this->session->userdata('permissions') ) !== false) { ?>
	    <a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>" class="list-group-item text-left <?php echo ($main_page == 'manage_users') ? 'active' : ''; ?>"><strong>Users</strong></a>   
	<?php } ?> 
	<?php if( array_search('manage_business', $this->session->userdata('permissions') ) !== false) { ?>
	    <a href="<?php echo site_url("my/{$current_user_id}/manage/business"); ?>" class="list-group-item text-left <?php echo ($main_page == 'manage_business') ? 'active' : ''; ?>"><strong>Business</strong></a>   
	<?php } ?> 
	
	<?php if( array_search('manage_realestate', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/manage/realestate"); ?>" class="list-group-item <?php echo ($main_page == 'manage_realestate') ? 'active' : ''; ?>"><strong>Real Estate</strong></a>
	<?php } ?>
	
	<?php if( array_search('manage_taxonomies', $this->session->userdata('permissions') ) !== false) { ?>
	<a href="<?php echo site_url("my/{$current_user_id}/manage/taxonomies"); ?>" class="list-group-item <?php echo ($main_page == 'manage_taxonomies') ? 'active' : ''; ?>"><strong>Taxonomies</strong></a>
	<?php } ?>
	
	<a href="<?php echo site_url("my/{$current_user_id}/manage/tasks"); ?>" class="list-group-item <?php echo ($main_page == 'manage_tasks') ? 'active' : ''; ?>"><strong>Tasks</strong></a>
	
</ul>
</div>


<div class="panel panel-default">
<ul class="list-group">
	    <a href="<?php echo site_url("my/{$current_user_id}/account"); ?>" class="list-group-item text-left <?php echo ($sub_page == 'account') ? 'active' : ''; ?>"><strong>My Account</strong></a>    
</ul>
</div>

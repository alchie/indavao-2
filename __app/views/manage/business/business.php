<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('manage/manage-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
<ul class="nav nav-tabs" id="myTab">
            
           
				<li class="<?php echo ($sub_page=='pending') ? 'active' : ''; ?>"><a href="<?php echo site_url("my/{$current_user_id}/manage/business"); ?>?view=pending">Pending Requests</a></li>
				<li class="<?php echo ($sub_page=='published') ? 'active' : ''; ?>"><a href="<?php echo site_url("my/{$current_user_id}/manage/business"); ?>?view=published">Published</a></li>
				<li class="<?php echo ($sub_page=='deleted') ? 'active' : ''; ?>"><a href="<?php echo site_url("my/{$current_user_id}/manage/business"); ?>?view=deleted">Deleted</a></li>
				<li class="<?php echo ($sub_page=='draft') ? 'active' : ''; ?>"><a href="<?php echo site_url("my/{$current_user_id}/manage/business"); ?>?view=draft">Draft</a></li>
          </ul>
<div class="brdr bgc-fff pad-10 box-shad">


<?php  
	switch( $this->input->get('view') ) { 
		case 'published': 
			$this->load->view('manage/business/published');
		break;
		case 'deleted': 
			$this->load->view('manage/business/deleted');
		break;
		case 'draft': 
			$this->load->view('manage/business/draft');
		break; 
		case 'reviewer':
		case 'review':
		case 'update': 
			$this->load->view('manage/business/update_business');
		break; 
		case 'promote': 
			$this->load->view('manage/business/promote_business');
		break; 
		case 'pending': 
		default: 
			$this->load->view('manage/business/pending-requests');
		break; 
		
 } ?>
 </form>






	
</div>
        </div><!--/col-9-->
    </div><!--/row-->

</div>             
<?php $this->load->view('overall_footer'); ?>

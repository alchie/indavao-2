<div class="page-header" style="margin-top:0;">
<?php if( count($business_directory) > 0 ) { ?>
	<a href="<?php echo current_url(); ?>?view=reviewer&status=draft&start=<?php echo ($current_page!='') ? (($current_page-1) * 20) : 0; ?>" class="btn btn-xs btn-warning pull-right">Reviewer</a>
<?php } ?>
  <h1>Business Directory <small>Drafts</small></h1>
</div>

<?php if( count($business_directory) > 0 ) { ?>
<div class="table-responsive">
                <table class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th class="text-center" width="170">Actions</th>
                    </tr>
                  </thead>
                  <tbody id="request-items">
					  <?php foreach( $business_directory as $dir ) { ?>
					  <tr id="dir-<?php echo $dir->dir_id; ?>">
                      <td><span class="h4"><strong>
						  <a href="<?php echo site_url(array('company', $dir->dir_id, $dir->dir_slug)); ?>" target="_blank">
						  <?php echo $dir->dir_name; ?> 
						  </a> 
						  <small><a href="<?php echo site_url(array("my", $this->session->userdata('user_id'), "manage", "users" )); ?>?view=summary&user_id=<?php echo $dir->user_id; ?>"><?php echo $dir->users_name; ?></a></small>
						  </strong></span>
                      </td>
                      <td>
						<div class="btn-group btn-group-xs">
							<a href="<?php echo current_url(); ?>?view=update&id=<?php echo $dir->dir_id; ?>" class="btn btn-warning">Update</a>
						  <button type="button" class="btn btn-success business-publish" data-id="<?php echo $dir->dir_id; ?>">Publish</button>
						  <button type="button" class="btn btn-danger business-delete" data-id="<?php echo $dir->dir_id; ?>">Delete</button>
						</div>
                      </td>
                    </tr>
                    <?php } ?>
				</tbody>
				</table>
</div>

<?php bootstrap_pagination( $pages, $current_page, current_url() . "?view=draft&", 10 ); ?>

<?php } ?>

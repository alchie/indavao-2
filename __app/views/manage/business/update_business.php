<?php if( $business_directory ) { ?>
<div class="page-header" style="margin-top:0;">
	<a href="javascript:history.back(-1);" class="btn btn-xs btn-danger pull-right"><i class="glyphicon glyphicon-remove"></i> Back</a>
	<a target="_blank" href="<?php echo site_url(array('company', $business_directory->dir_id, $business_directory->dir_slug)); ?>" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-search"></i> Preview</a>
		<h1><?php echo $business_directory->dir_name; ?></h1>
		 <input type="hidden" class="hidden" id="business_id" value="<?php echo $business_directory->dir_id; ?>">
	</div>
	
	<?php if( count( $matches ) > 0 ) { ?>
	
<div class="list-group">
	 <div class="list-group-item">
		This Post
		<span class="badge"><?php echo $business_directory->dir_added; ?></span>
	  </div>
	<?php foreach( $matches as $match) { ?>
	  <a href="<?php echo current_url(); ?>?view=review&id=<?php echo $match->dir_id; ?>" class="list-group-item">
		<?php echo $match->dir_name; ?> - <?php echo $match->dir_status; ?>
		<span class="badge"><?php echo $match->dir_added; ?></span>
	  </a>
	<?php } ?>
</div>
<?php }  ?>

<?php if( $this->input->get('view') == 'review' || $this->input->get('view') == 'reviewer') { ?>
<p>
  <?php if($business_directory->dir_status=='pending') { ?>
<?php 
$points = 0;
if( $business_directory->dir_name != '' && $business_directory->address != '' && $business_directory->phone != '' ) {
	$points = 1;
} 
if( $business_directory->abstract != '' ) {
	$points = $points + 1;
}
if( $business_directory->website != '' ) {
	$points = $points + 1;
}
if( $business_directory->map_lat != '' && $business_directory->map_long != '' ) {
	$points = $points + 1;
}
if( $business_directory->logo != '' ) {
	$points = $points + 1;
}

?>
<form method="post">
<input type="hidden" name="action" value="business-reject">
<input type="hidden" name="business_id" value="<?php echo $business_directory->dir_id; ?>">
<button type="submit" class="btn btn-danger pull-right" id="business-reject-update1" data-id="<?php echo $business_directory->dir_id; ?>"><i class="glyphicon glyphicon-remove"></i>  Reject</button>
</form>

<?php if( $points > 0 ) { ?>

<form method="post">
<input type="hidden" name="action" value="business-approve">
<input type="hidden" name="business_id" value="<?php echo $business_directory->dir_id; ?>">
<?php if( $business_directory->add_points == 1 ) { ?>
	<input type="hidden" name="points" value="<?php echo $points; ?>">
	<input type="hidden" name="user_id" value="<?php echo $business_directory->user_id; ?>">
	<?php echo ($business_directory->points_id != '') ? '<input type="hidden" name="points_id" value="'.$business_directory->points_id.'">' : ''; ?>
<?php } ?>
<button type="submit" class="btn btn-success" id="business-approve-update1" data-id="<?php echo $business_directory->dir_id; ?>" data-points="<?php echo $points; ?>"><i class="glyphicon glyphicon-ok"></i>  Approve <?php echo $points; ?> Point(s)</button>
</form>
	
	<?php } ?>
</p>
<?php } ?>
<?php } ?>	
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
		<button class="btn btn-xs btn-success pull-right business-update details" id="business-update-details" type="button">Save Changes</button>
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#business-details" class="business-collapse" data-id="details">
          Details
        </a>
      </h4>
    </div>
    <div id="business-details" class="panel-collapse collapse">
      <div class="panel-body">

<div class="form-group">
    <label for="business_name">Business Name</label>
    <input type="text" class="form-control" id="business_name" placeholder="Enter Business Name" name="business_name" value="<?php echo $business_directory->dir_name; ?>">
</div>

<div class="form-group">
    <label for="business_name">Business Slug</label>
    <input type="text" class="form-control" id="business_slug" placeholder="Enter Business Slug" name="business_slug" value="<?php echo $business_directory->dir_slug; ?>">
</div>

<div class="form-group">
    <label for="business_address">Address</label>
    <input type="text" class="form-control" id="business_address" placeholder="Enter Address" name="business_address" value="<?php echo $business_directory->address; ?>">
</div>

<div class="form-group">
    <label for="business_phone">Phone Number</label>
    <input type="text" class="form-control" id="business_phone" placeholder="Enter Phone Number" name="business_phone" value="<?php echo $business_directory->phone; ?>">
</div>

<div class="form-group">
    <label for="business_description">Short Description</label>
    <textarea class="form-control" id="business_description" placeholder="Add Description" name="business_description" rows="3"><?php echo $business_directory->abstract; ?></textarea>
</div>

<div class="form-group">
    <label for="business_website">Website URL</label>
    <input type="text" class="form-control" id="business_website" placeholder="Enter Website URL" name="business_website" value="<?php echo $business_directory->website; ?>">
</div>
  
  <div class="form-group">
    <label for="business_map_long">Email</label>
    <input type="text" class="form-control" id="business_email" placeholder="Enter Business Email" name="business_email" value="<?php echo $business_directory->email; ?>">
</div>

  <div class="form-group">
    <label for="business_map_lat">Map Latitude</label>
    <input type="text" class="form-control" id="business_map_lat" placeholder="Enter Map Latitude" name="business_map_lat" value="<?php echo $business_directory->map_lat; ?>">
</div>

<div class="form-group">
    <label for="business_map_long">Map Longitude</label>
    <input type="text" class="form-control" id="business_map_long" placeholder="Enter Map Longitude" name="business_map_long" value="<?php echo $business_directory->map_long; ?>">
</div>

<div class="form-group">
	<label for="business_logo">Logo</label>
	<?php if( $business_directory->logo != '' ) { ?>
	<div id="logo-preview-<?php echo $business_directory->dir_id; ?>"><a href="javascript:void(0);" class="btn btn-danger btn-xs pull-right" id="business-remove-logo" data-id="<?php echo $business_directory->dir_id; ?>">Remove Logo</a>
    <img data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $business_directory->logo; ?>" id="business_logo_preview" style="max-width:100%;margin-bottom:10px;" class="lazy" />
    </div>
    <?php } ?>
    <p><input type="file" id="business_logo"></p>
    <button class="btn btn-xs btn-success" id="business-update-logo" type="button" DISABLED>Upload Logo</button>
</div>

      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
		<button class="btn btn-xs btn-success pull-right business-update category hidden" id="business-update-category" type="button">Save Changes</button>
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="business-collapse" data-id="category">
          Categories
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">

<div class="table-responsive">
<table class="table table-hover">
	<thead>
          <tr>
			<th width="10px">Check</th>
            <th>Name</th>
          </tr>
        </thead>
     <tbody>
<?php 

$old_cats = array();

foreach( $old_categories as $oldc ) {
	$old_cats[] = $oldc->tax_id;
}

foreach($business_categories as $bCat ) { ?>
		 <tr id="category-parent-<?php echo $bCat->tax_id; ?>" class="<?php echo (in_array($bCat->tax_id, $old_cats)) ? 'success' : ''; ?>">
			<td><input type="checkbox" class="category-checkbox category-item" data-id="<?php echo $bCat->tax_id; ?>" <?php echo (in_array($bCat->tax_id, $old_cats)) ? 'checked="checked"' : ''; ?>></td>
			<td><strong><?php echo $bCat->tax_label; ?></strong>
			
			<?php if( count( $bCat->children ) > 0 ) { ?>
				<div id="category-children-<?php echo $bCat->tax_id; ?>" class="<?php echo (in_array($bCat->tax_id, $old_cats)) ? '' : 'hidden'; ?>">
			<br><br>
				<table class="table table-hover" >
					 <tbody>
						 <?php foreach($bCat->children as $sCat ) { ?>
						 <tr>
							<td width="10px"><input type="checkbox" class="category-checkbox category-item category-child-<?php echo $bCat->tax_id; ?>" data-id="<?php echo $sCat->tax_id; ?>" <?php echo (in_array($sCat->tax_id, $old_cats)) ? 'checked="checked"' : ''; ?>></td>
							<td><?php echo $sCat->tax_label; ?></td>
						 </tr>
						 <?php } ?>
					</tbody>
				</table>
				</div>
			<?php } ?>
			</td>
		 </tr>
		 <?php } ?>
	</tbody>
</table>
<?php echo "<input type='hidden' id='old_categories' value='". implode( ",", $old_cats ) ."'>"; ?>
</div>

      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
		<button class="btn btn-xs btn-success pull-right business-update location hidden" type="button" id="business-update-location">Save Changes</button>
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="business-collapse" data-id="location">
          Location
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse">
      <div class="panel-body">
        <div class="table-responsive">
<table class="table table-hover">
	<thead>
          <tr>
			<th width="10px">Check</th>
            <th>Name</th>
          </tr>
        </thead>
     <tbody>
<?php 
$old_locs = array();

foreach( $old_locations as $oldl ) {
	$old_locs[] = $oldl->loc_id;
}
		 foreach($locations as $mLoc ) { ?>
		 <tr id="location-parent-<?php echo $mLoc->loc_id; ?>" class="<?php echo (in_array($mLoc->loc_id, $old_locs)) ? 'success' : ''; ?>">
			<td><input type="radio" name="main_location" class="location-checkbox location-item" data-id="<?php echo $mLoc->loc_id; ?>" <?php echo (in_array($mLoc->loc_id, $old_locs)) ? 'checked="checked"' : ''; ?>></td>
			<td><strong><?php echo $mLoc->loc_name; ?></strong>
			
			<?php if( count( $mLoc->children ) > 0 ) { ?>
				<div id="location-children-<?php echo $mLoc->loc_id; ?>" class="<?php echo (in_array($mLoc->loc_id, $old_locs)) ? '' : 'hidden'; ?>">
					<br><br>
				<table class="table table-hover" >
					 <tbody>
						 <?php foreach($mLoc->children as $sLoc ) { ?>
						 <tr>
							<td><input type="radio" name="sub_location" class="location-checkbox location-item location-child-<?php echo $mLoc->loc_id; ?>" data-id="<?php echo $sLoc->loc_id; ?>" <?php echo (in_array($sLoc->loc_id, $old_locs)) ? 'checked="checked"' : ''; ?>></td>
							<td><?php echo $sLoc->loc_name; ?>
						 </tr>
						 <?php } ?>
					</tbody>
				</table>
				</div>
			<?php } ?>
			</td>
		 </tr>
		 <?php } ?>
	</tbody>
</table>
<?php echo "<input type='hidden' id='old_locations' value='". implode( ",", $old_locs ) ."'>"; ?>
</div>
        
      </div>
    </div>
  </div>
 
 
 
<div class="panel panel-default">
	<div class="panel-heading">
		
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTags" class="business-collapse" data-id="tags">
          Tags
        </a>
      </h4>
    </div>
    <div id="collapseTags" class="panel-collapse collapse">
		<div class="panel-body">
<p>
	
<div class="row">
  <div class="col-lg-12">
    <div class="input-group">
      <input type="text" class="form-control" id="business_tag_name" placeholder="Enter A Tag" name="business_tag" value="" maxlength="50">
      <span class="input-group-btn">
        <button class="btn btn-success pull-right" id="business_tag_add_action" type="button">Add Tag</button>
      </span>
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->

</p>
<div class="row">
  <div class="col-lg-12">
<ul class="list-group" id="business-tags">
	<?php foreach( $tags as $tag ) { ?>
  <li id="tag-item-<?php echo $tag->dir_tag_id; ?>" class="list-group-item tag-item <?php echo ($tag->dir_tag_active==1) ? 'enabled' : 'disabled'; ?>">
  <div class="btn-group pull-right">
	<!--<a data-tag_id="<?php echo $tag->dir_tag_id; ?>" data-tag_enable="<?php echo $tag->dir_tag_active; ?>" href="javascript:void(0);" class="business_tag_enable btn btn-xs btn-<?php echo ($tag->dir_tag_active==1) ? 'warning' : 'success'; ?>"><?php echo ($tag->dir_tag_active==1) ? 'Disable' : 'Enable'; ?></a>-->
	<a data-tag_id="<?php echo $tag->dir_tag_id; ?>" href="javascript:void(0);" class="business_tag_delete_action btn btn-xs btn-danger">Delete</a>
	</div>
    <?php echo $tag->tag_name; ?>
  </li>
	<?php } ?>
</ul>
  </div><!-- /.col-lg-6 -->
</div><!-- /.row -->

		</div>
    </div>
  </div>
 
 
 
<!-- start -->
  <div class="panel panel-danger">
    <div class="panel-heading">
	<button class="btn btn-xs btn-success pull-right business-update tasks hidden" type="button" id="business-update-tasks">Save Changes</button>
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#more-update_tasks" class="business-collapse" data-id="tasks">
          Tasks
        </a>
      </h4>
    </div>
    <div id="more-update_tasks" class="panel-collapse collapse">
      <div class="panel-body">
	
<div class="form-group">
    <label class="pull-right"><input type="checkbox" name="add_to_tasks" value="1" id="business_add_to_tasks" <?php echo ($business_directory->is_task == 1) ? 'CHECKED' : ''; ?>> Add to Tasks</label>
 </div>
<div class="form-group">
    <label for="share_message">Share Message</label>
    <input type="text" class="form-control" id="business_task_message" placeholder="Enter Share Message" name="task_message" value="<?php echo $business_directory->task_message; ?>">
</div>


	</div></div></div>
<!-- end -->


<!-- start -->
  <div class="panel panel-danger">
    <div class="panel-heading">
	  <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#more-update_user_points" class="business-collapse" data-id="user_points">
          User Points Claimed
        </a>
      </h4>
    </div>
    <div id="more-update_user_points" class="panel-collapse collapse">
      <div class="panel-body">
	
<?php echo ( $business_directory->points_claimed == 1 ) ? 'CLAIMED' : 'NOT YET!!!'; ?>

	</div></div></div>
<!-- end -->
 
  </div>
 <?php if( $this->input->get('view') == 'reviewer' ) { 
 $review_url = current_url();
 $review_url .= '?view=reviewer';
 $review_url .= '&status='.(($this->input->get('status'))?$this->input->get('status'):'pending');
 $review_url .= '&start='.(($this->input->get('start'))?($this->input->get('start')+1):1);
 ?>
<p class="text-right">
	<a href="<?php echo $review_url; ?>" class="btn btn-warning">Next <i class="glyphicon glyphicon-arrow-right"></i></a>
</p>
 <?php } ?>
</div>
</div>

</div>
<?php } else {
	echo "No Business Found!";
} ?>
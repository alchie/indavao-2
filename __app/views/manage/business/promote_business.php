<?php if( $business_directory ) { ?>
<div class="page-header" style="margin-top:0;">
	<a href="javascript:history.back(-1);" class="btn btn-xs btn-danger pull-right"><i class="glyphicon glyphicon-remove"></i> Back</a>
	<a target="_blank" href="<?php echo site_url(array('company', $business_directory->dir_id, $business_directory->dir_slug)); ?>" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-search"></i> Preview</a>
		<h1><?php echo $business_directory->dir_name; ?></h1>
		 <input type="hidden" class="hidden" id="business_id" value="<?php echo $business_directory->dir_id; ?>">
	</div>
	
<div class="panel-group" id="accordion">
<?php 
$marketing_strategy = array(
	'promotional_video' => array(
		'label' => 'Promotional Videos',
		'forms' => array(
			array('name'=>'youtube_video', 'label'=>'Youtube URL'),
			array('name'=>'vimeo_video', 'label'=>'Vimeo URL'),
		),
	),
	'promotional_images' => array(
		'label' => 'Promotional Images',
		'forms' => array(
			array('name'=>'panoramio_image', 'label'=>'Panoramio URL'),
			array('name'=>'photobucket_image', 'label'=>'Photobucket URL'),
		),
	),
	'google_maps' => array(
		'label' => 'Google Maps',
		'forms' => array(
			array('name'=>'google_map', 'label'=>'Google Map URL'),
		),
	),
	'social_media_pages' => array(
		'label' => 'Social Media Pages',
		'forms' => array(
			array('name'=>'facebook_page', 'label'=>'Facebook Page URL'),
			array('name'=>'google_page', 'label'=>'Google Plus Page URL'),
		),
	),
	'applications' => array(
		'label' => 'Desktop / Mobile Applications',
		'forms' => array(
			array('name'=>'android_app', 'label'=>'Google Play URL'),
			array('name'=>'desktop_app', 'label'=>'Desktop App Download URL'),
		),
	),
	'free_websites' => array(
		'label' => 'Free Hosting Websites',
		'forms' => array(
			array('name'=>'blogger_site', 'label'=>'Blogger Blog URL'),
			array('name'=>'wordpress_site', 'label'=>'Wordpress.Com URL'),
		),
	),
);
foreach( $marketing_strategy as $strat_key => $strat ) { ?>
  <div class="panel panel-default">
    <div class="panel-heading">
		  <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $strat_key; ?>">
          <?php echo $strat['label']; ?>
        </a>
      </h4>
    </div>
    <div id="<?php echo $strat_key; ?>" class="panel-collapse collapse">
      <div class="panel-body">
<?php if( isset($strat['forms']) && count( $strat['forms'] ) > 0 ) { 
	foreach( $strat['forms'] as $form ) {
?>

<div class="form-group">
    <label for="business_name"><?php echo $form['label']; ?></label>
	<div class="input-group">
    <input type="text" class="form-control" id="<?php echo $form['name']; ?>-input" placeholder="Enter <?php echo $form['label']; ?>" name="<?php echo $form['name']; ?>" value="<?php echo (isset($promotions[$form['name']]))? $promotions[$form['name']] : ''; ?>">
	<span class="input-group-btn">
        <button class="btn btn-success business_promote_save" type="button" data-name="<?php echo $form['name']; ?>"><i class="glyphicon glyphicon-floppy-disk"></i></button>
      </span>
	</div>
</div>
<?php } // foreach
}?>
      </div>
    </div>
  </div>
<?php } ?>
</div>
</div>
</div>


<?php } else {
	echo "No Business Found!";
} ?>
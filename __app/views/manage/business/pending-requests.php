<div class="page-header" style="margin-top:0;">
<?php if( count($business_directory) > 0 ) { ?>
	<a href="<?php echo current_url(); ?>?view=reviewer" class="btn btn-xs btn-warning pull-right">Reviewer</a>
<?php } ?>
	<h1>Business Directory <small>Pending Requests</small></h1>
</div>

<?php if( count($business_directory) > 0 ) { ?>
<div class="table-responsive">
                <table class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th class="text-center" width="70">Actions</th>
                    </tr>
                  </thead>
                  <tbody id="request-items">
					  <?php foreach( $business_directory as $dir ) { ?>
					  <tr id="dir-<?php echo $dir->dir_id; ?>">
                      <td><span class="h4"><strong>
						  <a href="<?php echo site_url(array('company', $dir->dir_id, $dir->dir_slug)); ?>" target="_blank">
						  <?php echo $dir->dir_name; ?>
						  </a>  <small><a href="<?php echo site_url(array("my", $this->session->userdata('user_id'), "manage", "users" )); ?>?view=summary&user_id=<?php echo $dir->user_id; ?>"><?php echo $dir->users_name; ?></a></small>
						  </strong></span>
                      </td>
                      <td>
						<div class="btn-group btn-group-xs">
							<a href="<?php echo current_url(); ?>?view=review&id=<?php echo $dir->dir_id; ?>" class="btn btn-warning">Review</a>
						 </div>
                      </td>
                    </tr>
                    <?php } ?>
				</tbody>
				</table>
</div>

<?php bootstrap_pagination( $pages, $current_page, current_url() . "?view=pending&", 10 ); ?>

<?php } ?>

<div class="page-header" style="margin-top:0;">
<?php if( count($business_directory) > 0 ) { ?>
	<a href="<?php echo current_url(); ?>?view=reviewer&status=publish&start=<?php echo ($current_page!='') ? (($current_page-1) * 20) : 0; ?>" class="btn btn-xs btn-warning pull-right">Reviewer</a>
<?php } ?>
  <h1>Business Directory <small>Published</small></h1>
</div>

<?php if( count($business_directory) > 0 ) { ?>
<div class="table-responsive">
                <table class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>Title</th>
                      
                    </tr>
                  </thead>
                  <tbody id="request-items">
					  <?php foreach( $business_directory as $dir ) { ?>
					  <tr id="dir-<?php echo $dir->dir_id; ?>">
                      <td><span class="h4"><strong>
						  
						  <?php echo $dir->dir_name; ?>
						  
						  </strong></span>
<small class="pull-right"><a href="<?php echo current_url() . "?view=published&user_id=" . $dir->user_id; ?>"><?php echo $dir->users_name; ?></a></small>
						
<p><div class="btn-group btn-group-xs">
<a target="_blank" href="<?php echo site_url(array('company', $dir->dir_id, $dir->dir_slug)); ?>" class="btn btn-primary">Preview</a>
							<a href="<?php echo current_url(); ?>?view=update&id=<?php echo $dir->dir_id; ?>" class="btn btn-warning">Update</a>
						  <button type="button" class="btn btn-danger business-delete" data-id="<?php echo $dir->dir_id; ?>">Delete</button>
						  <button type="button" class="btn btn-info business-reject" data-id="<?php echo $dir->dir_id; ?>">Draft</button>
						<a href="<?php echo current_url(); ?>?view=promote&id=<?php echo $dir->dir_id; ?>" class="btn btn-success">Promote</a>
						</div>
</p>
                      </td>
                      
                    </tr>
                    <?php } ?>
				</tbody>
				</table>
</div>

<?php 
$page_url = current_url() . "?view=published&";
if( $this->input->get('user_id') != FALSE ) {
	$page_url .= "user_id=".$this->input->get('user_id')."&";
}
bootstrap_pagination( $pages, $current_page, $page_url, 10 ); 
?>

<?php } ?>

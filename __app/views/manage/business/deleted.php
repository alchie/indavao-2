
<div class="page-header" style="margin-top:0;">
  <h1>Business Directory <small>Deleted</small></h1>
</div>

<?php if( count($business_directory) > 0 ) { ?>
<div class="table-responsive">
                <table class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th class="text-center" width="240">Actions</th>
                    </tr>
                  </thead>
                  <tbody id="request-items">
					  <?php foreach( $business_directory as $dir ) { ?>
					  <tr id="dir-<?php echo $dir->dir_id; ?>">
                      <td><span class="h4"><strong>
						  <a href="<?php echo site_url(array('company', $dir->dir_id, $dir->dir_slug)); ?>" target="_blank">
						  <?php echo $dir->dir_name; ?> 
						  </a>  <small>(<?php echo $dir->users_name; ?>)</small>
						  </strong></span>
                      </td>
                      <td>
						<div class="btn-group btn-group-xs pull-right">
							<?php if( $dir->user_id != '' ) { ?>
						<button type="button" class="btn btn-success business-reclaim" data-id="<?php echo $dir->dir_id; ?>">Reclaim</button>
						<?php } else { ?>
							<button type="button" class="btn btn-success business-reclaim" data-id="<?php echo $dir->dir_id; ?>">Publish</button>
							 <?php } ?>
						  <button type="button" class="btn btn-info business-reject" data-id="<?php echo $dir->dir_id; ?>">Draft</button>
						  <?php if( array_search('delete_business', $this->session->userdata('permissions') ) !== false) { ?>
						  <button type="button" class="btn btn-danger business-delete-permanently" data-id="<?php echo $dir->dir_id; ?>">Delete Permanently</button>
						  <?php } ?>
						</div>
                      </td>
                    </tr>
                    <?php } ?>
				</tbody>
		</table>
</div>
<?php } ?>

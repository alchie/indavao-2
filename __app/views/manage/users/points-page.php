<div class="page-header" style="margin-top:0;">
  <h1><?php echo $user_selected->name; ?> <small>Points</small></h1>
</div>
				
<div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
					<th class="text-center" width="20%">Date Earned</th>
                      <th>Desription</th>
                      <th class="text-center" width="10%">Points Earned</th>
                    </tr>
                  </thead>
                  <tbody id="tasks-items">
					  <?php foreach($user_points as $point ) { 
							switch($point->points_type) {
								case 'REF':
									echo "<tr id=\"points-item-{$point->points_id}\">";
									echo "<td>{$point->date_added}</td>";
									echo "<td>Referral Points</td>";
									echo "<td class=\"text-center\">{$point->points_credited}</td>";
									echo "</tr>";
								break;
								case 'SHARE':
									if( $point->tv_object_type == '' ) {
										echo "<tr class=\"danger\" id=\"points-item-{$point->points_id}\">";
									} else {
										echo "<tr>";
									}
									echo "<td>{$point->date_added}</td>";
									if( $point->tv_object_type == 'realestate' ) {
										echo "<td>Task Points - {$point->re_title}</td>";
									} else {
										echo "<td><em>Something is wrong here! Your points will be adjusted accordingly!</em></td>";
									}
									if( $point->tv_object_type == '' ) {
										echo "<td class=\"text-center\"><a href=\"javascript:void(0);\" class=\"btn btn-danger btn-xs remove-users-points\" data-id=\"{$point->points_id}\" data-uid=\"{$user_selected->user_id}\">Remove</a></td>";
									} else {
										echo "<td class=\"text-center\" data-points=\"{$point->points_credited}\">{$point->points_credited}</td>";
									}
									echo "</tr>";
								break;
							}
						} ?>
				</tbody>
				</table>
</div>

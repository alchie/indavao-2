<?php
$mypemissions  = array();
		foreach( $permissions as $permission ) {
			$mypemissions[$permission->permission] = $permission;
		}
		
	?>
	
	<div class="page-header" style="margin-top:0;">
  <h1><?php echo $user_selected->name; ?> <small>Services</small></h1>
</div>

<table class="table table-hover table-condensed table-striped">
  <thead>
	  <tr>
		<th>Permission</th>
		<th width="50px">Action</th>
	  </tr>
  </thead>
<tbody>
	<tr id="permission-add_business" class="<?php echo ( isset( $mypemissions['add_business'] ) && $mypemissions['add_business']->active == 1) ? 'success' : 'danger'; ?>">
		<td><h4>Add Business</h4>
		<p>User will be able to submit business directory.</p>
		</td>
		<td>
		<input <?php echo ( isset( $mypemissions['add_business'] ) && $mypemissions['add_business']->active == 1) ? 'CHECKED="CHECKED"' : ''; ?> type="checkbox" class="form-control toggle-permission" data-permission="add_business" data-uid="<?php echo $user_selected->user_id; ?>">
		</td>
	</tr>
</tbody>
</table>

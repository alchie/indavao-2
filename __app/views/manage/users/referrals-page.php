<div class="page-header" style="margin-top:0;">
  <h1><?php echo $user_selected->name; ?> <small>Referrals</small></h1>
</div>


<table class="table table-hover table-condensed table-striped">
  <thead>
	  <tr>
		<th>ID</th>
		<th>Name</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th width="145px">Actions</th>
	  </tr>
  </thead>
  <tbody>
	  <?php foreach( $referrals as $user ) { ?>
	<tr class="<?php echo ($user->add_points == 1) ? 'success' : ''; ?>">
		<td><?php echo $user->user_id; ?></td>
		<td><?php echo $user->name; ?></td>
		<td><?php echo $user->first_name; ?></td>
		<td><?php echo $user->last_name; ?></td>
		<td>
			<?php if( $user->manager == 0 ) { ?>
			<div class="btn-group">
			<a class="btn btn-xs btn-info" href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>?view=points&user_id=<?php echo $user->user_id; ?>">Points</a> <a class="btn btn-xs btn-danger" href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>?view=services&user_id=<?php echo $user->user_id; ?>">Services</a>
			</div>
			<?php } ?>
			</td>
	</tr>
	<?php } ?>
  </tbody>
</table>

<div class="page-header" style="margin-top:0;margin-bottom: 0;">
  <h1><?php echo $user_selected->name; ?> <small>Summary</small></h1>
</div>

<?php if( $user_selected->add_points == 1 ) { 
	
	$overall_points = (!is_null($user_selected->overall_points)) ? $user_selected->overall_points : 0;
	$overall_withdrawals = (!is_null($user_selected->overall_withdrawals)) ? $user_selected->overall_withdrawals : 0;
	$remaining_points = $overall_points - $overall_withdrawals;

?>
<div class="well">
	<strong class="pull-right h4"><?php echo $overall_points; ?></strong>
<h4>Overall Points <small><a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>?view=points&user_id=<?php echo $user_selected->user_id; ?>">show details</a></small></h4>

</div>
<div class="well">
	<strong class="pull-right h4"><?php echo $overall_withdrawals; ?></strong>
<h4>Overall Withdrawals <small><a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>?view=withdrawals&user_id=<?php echo $user_selected->user_id; ?>">show details</a></small></h4>

<?php if( count( $withdrawals ) > 0 ) { ?>
<ul class="list-group">
	<?php foreach( $withdrawals as $withdrawal ) { ?>
  <li class="list-group-item"><?php echo $withdrawal->date_withdrawn . " - " . $withdrawal->withdrawal_reason; ?>
  <span class="badge"><?php echo $withdrawal->points_withdrawn; ?></span>
  </li>
  <?php } ?>
</ul>
<?php } ?>
</div>
<div class="well">
	<span class="pull-right h4"><strong><?php echo $remaining_points; ?></strong></span>
<h4>Redeemable Points</h4>
<?php if( $remaining_points > 0 ) { 
	
	$qualified_items = array();
	foreach($redeemable_items as $item) {
		if( $item->points <= $remaining_points ) {
			$qualified_items[] = $item;
		}
	}
	if( count( $qualified_items ) > 0 && $user_selected->alchienetcafe_username != '') {
	?>
<p><select id="redeem-points-item">
	<?php
	foreach($qualified_items as $item) {
		if( $item->points <= $remaining_points ) {
			echo "<option data-points='{$item->points}' data-item='{$item->attr_name} - {$item->attr_label}'>{$item->attr_name} - {$item->attr_label} ( {$item->points} points)</option>";
		}
	} ?>
</select></p>
<p><button type="button" class="btn btn-xs btn-danger" id="redeem-points-submit" data-id="<?php echo $user_selected->user_id; ?>">Redeem</button></p>
<p class="h5"><strong>Alchie Netcafe Username:</strong> <u><?php echo $user_selected->alchienetcafe_username; ?></u></p>
<?php }
 } ?>
</div>
<?php } else { ?>
	<div class="well">
<h4>Register to Points Program </h4>
<div class="form-group" id="register-points-program-container">
<div class="input-group">
<input type="text" id="register-points-program-username" value="" class="form-control input-lg" placeholder="Enter Alchie Netcafe Username Here...">
<span class="input-group-btn">
<button type="button" class="btn btn-success btn-lg" id="register-points-program" data-id="<?php echo $user_selected->user_id; ?>">Register Now</button>
</span>
</div>
</div>
</div>

<?php } ?>

<div class="page-header" style="margin-top:0;">
  <h1><?php echo $user_selected->name; ?> <small>Withdrawals</small></h1>
</div>

<?php if( $withdrawals ) { ?>
				<div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
					<th class="text-center" width="20%">Date Redeemed</th>
                      <th>Reason for Redemption</th>
                      <th class="text-center" width="10%">Points Redeemed</th>
                    </tr>
                  </thead>
                  <tbody id="tasks-items">
					  <?php foreach($withdrawals as $redemption ) { ?>
						<tr>
							<td><?php echo $redemption->date_withdrawn; ?></td>
							<td><?php echo $redemption->withdrawal_reason; ?></td>
							<td class="text-center"><?php echo $redemption->points_withdrawn; ?></td>
						</tr>
						<?php } ?>
				</tbody>
				</table>
</div>
<?php } ?>

<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('manage/manage-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
<ul class="nav nav-tabs" id="myTab">
            
            <?php if( isset( $user_selected ) ) { ?>
				
				<li><a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>">Manage Users</a></li>
				
				<?php if( $user_selected->add_points == 1 ) { ?>
				
				<li class="<?php echo ($sub_page == 'summary') ? 'active' : ''; ?>"><a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>?view=summary&user_id=<?php echo $this->input->get('user_id'); ?>">Summary</a></li>
				
				<li class="<?php echo ($sub_page == 'points') ? 'active' : ''; ?>"><a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>?view=points&user_id=<?php echo $this->input->get('user_id'); ?>">Points</a></li>
				
				<li class="<?php echo ($sub_page == 'withdrawals') ? 'active' : ''; ?>"><a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>?view=withdrawals&user_id=<?php echo $this->input->get('user_id'); ?>">Withdrawals</a></li>
				<?php } ?>
				
				<li class="<?php echo ($sub_page == 'referrals') ? 'active' : ''; ?>"><a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>?view=referrals&user_id=<?php echo $this->input->get('user_id'); ?>">Referrals</a></li>
				<li class="<?php echo ($sub_page == 'services') ? 'active' : ''; ?>"><a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>?view=services&user_id=<?php echo $this->input->get('user_id'); ?>">Services</a></li>
				
				
			<?php } else { ?>
				<li class="active"><a href="<?php echo site_url("my/{$current_user_id}/manage/users"); ?>">Manage Users</a></li>
			<?php } ?>
          </ul>
<div class="brdr bgc-fff pad-10 box-shad">


<?php  if( $this->input->get('view') !== FALSE ) { 
	switch( $this->input->get('view') ) { 
		case 'all': 
		case 'search': 
			$this->load->view('manage/users/search-page');
		break; 
		case 'summary': 
		$this->load->view('manage/users/summary-page');
		 break; 
		
		case 'points': 
		$this->load->view('manage/users/points-page');
		 break;
		case 'withdrawals': 
			$this->load->view('manage/users/withdrawals-page');
		break; 
		case 'referrals': 
			$this->load->view('manage/users/referrals-page');
		break;
		case 'services': 
			$this->load->view('manage/users/services-page');
		break; ?>
		
<?php }
 } else { ?>
	<form method="get">
	<input type="hidden" name="view" value="search">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Search User</h3>
  </div>
  <div class="panel-body">
	  <div class="form-group">
		 <select name="by" class="form-control">
		<option value="name">Name</option>
		<option value="email">Email</option>
		<option value="first_name">First Name</option>
		<option value="last_name">Last Name</option>
	  </select>
	  </div>
	  <div class="form-group">
	  <input name="keyword" type="text" class="form-control">
	  </div>
  </div>
  <div class="panel-footer"><input type="submit" class="btn btn-success" value="Search"></div>
</div>
	<a href="<?php echo current_url(); ?>?view=all">Browse All Users</a>
<?php } ?>
 </form>

</div>
        </div><!--/col-9-->
    </div><!--/row-->

</div>             
<?php $this->load->view('overall_footer'); ?>

<?php 

$meta_key = ($this->input->get('meta_key')=='NEW_META_KEY') ? $this->input->get('new_meta_key') : $this->input->get('meta_key');

?>
<div class="page-header" style="margin-top:0;">
  <h1>Update Meta <small><?php echo $meta_key; ?></small></h1>
</div>
<form method="POST" action="">
<div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
						<th width="30%">Taxonomy Label</th>
						<th>Taxonomy Name</th>
						<th class="text-center" width="30%">Meta Value</th>
                    </tr>
                  </thead>
                  <tbody id="tasks-items">
					  <?php foreach( $taxonomies as $tax ) { ?>
						  <tr>
							<td><?php echo $tax->tax_name; ?></td>
							<td><?php echo $tax->tax_label; ?></td>
							<td align="center"><input type="text" name="meta_value[<?php echo $tax->tax_id; ?>]" value="<?php echo $tax->meta_value; ?>"></td>
						  </tr>
					  <?php } ?>
				</tbody>
				</table>
</div>
<input type="submit" class="btn btn-success" value="Submit">
</form>

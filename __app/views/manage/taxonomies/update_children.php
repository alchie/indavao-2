<div class="page-header" style="margin-top:0;">
  <h1>Update Children <small><?php echo $current_tax->tax_label; ?></small></h1>
</div>
<form method="POST" action="">
<div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr>
						<th width="30%">Taxonomy Label</th>
						<th>Taxonomy Name</th>
						<th align="center" class="text-center" width="10%">Add</th>
						<th align="center" class="text-center" width="10%">Remove</th>
                    </tr>
                  </thead>
                  <tbody id="tasks-items">
					  <?php foreach( $taxs as $tax ) { ?>
						  <tr>
							<td><?php echo $tax->tax_name; ?></td>
							<td><?php echo $tax->tax_label; ?></td>
							<td align="center">
								<?php if($tax->child != 1) { ?>
								<input <?php echo ($tax->child == 1) ? "CHECKED" : NULL; ?> type="checkbox" value="1" name="add_child[<?php echo $tax->tax_id; ?>]">
								<?php } else { ?>
									<span class="glyphicon glyphicon-ok"></span>
								<?php } ?>
							</td>
							<td align="center">
								<?php if($tax->child == 1) { ?>
								<input type="checkbox" value="1" name="remove_child[<?php echo $tax->tax_id; ?>]">
								<?php } ?>
							</td>
						  </tr>
					  <?php } ?>
				</tbody>
				</table>
</div>
<input type="submit" class="btn btn-success" value="Submit">
</form>

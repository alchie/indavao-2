<?php $this->load->view('overall_header'); ?>
<?php $this->load->view('my/fb-init'); ?>
<div class="container main-body">
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <?php $this->load->view('manage/manage-sidebar'); ?>
          
        </div><!--/col-3-->
    	<div class="col-sm-9">
          
<ul class="nav nav-tabs" id="myTab">
	<li class="active"><a href="#">Manage Taxonomies</a></li>
</ul>
<div class="brdr bgc-fff pad-10 box-shad">


<?php  if( $this->input->get('view') !== FALSE ) { 
	switch( $this->input->get('view') ) { 
		case 'update_meta': 
			$this->load->view('manage/taxonomies/update_meta');
		break; 
		case 'update_children': 
			$this->load->view('manage/taxonomies/update_children');
		break;
	}
 } else { ?>
	<form method="get">
	<input type="hidden" name="view" value="update_meta">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Update Meta Value</h3>
  </div>
  <div class="panel-body">
	  <div class="form-group">
		  <label>Taxonomy Group</label>
		 <select name="group" class="form-control selectpicker" data-live-search="true">
			<option value="<?php echo get_settings_value('main_business_category_id'); ?>" SELECTED>Business Categories</option>
<?php 
foreach( $business_categories as $navbc ) { 
	echo "<option value=\"{$navbc->tax_id}\">- - - {$navbc->tax_label}</option>";
}
?>
			<option value="<?php echo get_settings_value('main_job_function_id'); ?>">Jobs Functions</option>
		  </select>
	  </div>
	  <div class="form-group">
		  <label>Meta Key</label>
		  <p><select name="meta_key" class="form-control selectpicker" data-live-search="true">
			<?php foreach( $meta_keys as $meta_key ) {
				  echo "<option>{$meta_key->meta_key}</option>";
				 }
			?>
				<option value="NEW_META_KEY">- - Add New Key - -</option>
		  </select></p>
	  <input name="new_meta_key" type="text" class="form-control">

	  </div>
  </div>
  <div class="panel-footer"><input type="submit" class="btn btn-success" value="Submit"></div>
</div>
	 </form>
	 
	 
	 	<form method="get">
	<input type="hidden" name="view" value="update_children">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Update Children</h3>
  </div>
  <div class="panel-body">
	  <div class="form-group">
		  <label>Taxonomy</label>
		 <select name="tax_id" class="form-control selectpicker" data-live-search="true">
<?php 
foreach( $taxs as $tax ) { 
	echo "<option value=\"{$tax->tax_id}\">{$tax->tax_label}</option>";
}
?>
			
		  </select>
	  </div>
	  
  </div>
  <div class="panel-footer"><input type="submit" class="btn btn-success" value="Submit"></div>
</div>
	 </form>
	 
<?php } ?>


</div>
        </div><!--/col-9-->
    </div><!--/row-->

</div>             
<?php $this->load->view('overall_footer'); ?>

<form method="POST" action="<?php echo site_url('business/search'); ?>">
		<div class="panel panel-danger homepage-searchbox">
			<div class="panel-heading">
				<img data-original="<?php echo base_url("assets/images/folder-documents-yellow-icon.png"); ?>" class="panel-heading-icon lazy hidden-sm hidden-xs" width="80" height="80">
				<h3 class="panel-title">Find a Business</h3>
			</div>
			<div class="panel-body">
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Business Category</label>
					<div>
					   <select class="form-control selectpicker show-menu-arrow" data-live-search="true" name="category">
						   <option value="any">All Business Category</option>
						   <?php 
								foreach($business_categories as $business_category) { 
									echo "<option value=\"{$business_category->tax_name}\" ";
									if( isset($search_keys->category) && ($business_category->tax_name == $search_keys->category) ) {
										echo "selected=\"selected\"";
									}
									echo ">{$business_category->tax_label}</option>";
									
									if( isset($business_category->children) && (count($business_category->children) > 0) ) {
										foreach($business_category->children as $business_category_child) {
											echo "<option value=\"{$business_category_child->tax_name}\" ";
											if( isset($search_keys->category) && ($business_category_child->tax_name == $search_keys->category) ) {
												echo "selected=\"selected\"";
											}
											echo ">- - {$business_category_child->tax_label}</option>";
										}
									}
								} 
						   ?>
						</select>
					</div>
				  </div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input type="checkbox" value="1" class="hidden" CHECKED name="verified"><span>Verified</span></label>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input type="checkbox" value="1" class="hidden" CHECKED name="phone"><span>With Phone</span></label>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input type="checkbox" value="1" class="hidden" name="email" CHECKED><span>With Email</span></label>
					</div>
				</div>
			</div>
			
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-warning btn-block"><i class="glyphicon glyphicon-search"></i> Display Businesses</button>
			</div>
		</div>
</form>

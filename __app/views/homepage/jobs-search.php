<form method="POST" action="<?php echo site_url('jobs/search'); ?>">
		<div class="panel panel-warning homepage-searchbox">
			<div class="panel-heading">
				<img data-original="<?php echo base_url("assets/images/policeman-icon2.png"); ?>" class="panel-heading-icon lazy  hidden-sm hidden-xs" width="80" height="80">
				<h3 class="panel-title">Find a Job</h3>
			</div>
			<div class="panel-body">
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Job Function</label>
					<div>
					  <select class="form-control selectpicker show-menu-arrow" data-live-search="true" multiple title="Select a Job Function" name="job_function[]">
						  <?php 
								foreach($job_functions as $job_function) { 
									echo "<optgroup label=\"{$job_function->tax_label}\">";
									
									if( isset($job_function->children) && (count($job_function->children) > 0) ) {
										foreach($job_function->children as $job_function_child) {
											echo "<option value=\"{$job_function_child->tax_id}\">{$job_function_child->tax_label}</option>";
										}
									}
									
									echo "</optgroup>";
								} 
						   ?>
						</select>
					</div>
				  </div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input type="checkbox" value="full_time" class="hidden" CHECKED name="job_status[]"><span>Full Time</span></label>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input type="checkbox" value="part_time" class="hidden" CHECKED name="job_status[]"><span>Part Time</span></label>
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="form-group">
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input type="checkbox" value="internship" class="hidden" CHECKED name="job_status[]"><span>Internship</span></label>
					</div>
				</div>
			</div>
			
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-warning btn-block"><i class="glyphicon glyphicon-search"></i> Display Jobs</button>
			</div>
		</div>
	</form>

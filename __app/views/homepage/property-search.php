<form method="POST" action="<?php echo site_url('realestate/search'); ?>">
		<div class="panel panel-success homepage-searchbox">
			<div class="panel-heading">
				<img data-original="<?php echo base_url("assets/images/house-icon.png"); ?>" class="panel-heading-icon lazy  hidden-sm hidden-xs" width="80" height="80">
				<h3 class="panel-title">Find a Home</h3>
			</div>
			<div class="panel-body">
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						
						<div class="btn-group btn-group-justified btn-group-xs" role="group">
							<?php foreach(array('sale' => 'For Sale', 'rent' => 'For Rent', 'assume' => 'For Assume') as $key=>$label) { 
							$extra_class='';
							if('assume' == $key) {
								$extra_class='hidden-sm';
							}
							if('sale' == $key) { ?>
						<label class="btn btn-success btn-xs btn-block btn-radio"><input name="status" type="radio" value="<?php echo $key; ?>" class="hidden" CHECKED><span><?php echo $label; ?></span></label>
						<?php } else { ?>
							<label class="btn btn-default btn-xs btn-block btn-radio <?php echo $extra_class; ?>"><input name="status" type="radio" value="<?php echo $key; ?>" class="hidden"><span><?php echo $label; ?></span></label>
						<?php } ?>
					<?php } ?>
						</div>
					</div>
					
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-6 col-md-6">
					<div class="form-group">
					<label class="control-label">Bedrooms</label>
					<div>
					  <select class="form-control selectpicker show-menu-arrow" name="beds">
						   <option value="">Any</option>
						  <?php for($i=1;$i<(get_settings_value('realestate_search_bathrooms', 5)+1);$i++) { echo "<option>$i</option>"; } ?>
						</select>
					</div>
				  </div>
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="form-group">
					<label class="control-label">Bathrooms</label>
					<div>
					  <select class="form-control selectpicker show-menu-arrow" name="baths">
						  <option value="">Any</option>
						  <?php for($i=1;$i<(get_settings_value('realestate_search_bathrooms', 5)+1);$i++) { echo "<option>$i</option>"; } ?>
						</select>
					</div>
				  </div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Property Type</label>
					<div>
					  <select class="form-control selectpicker show-menu-arrow" title="Any Property Type" name="property_type[]">
						  <option value="HOUSE">House and Lot</option>
						  <option value="PROJ">Development Projects</option>
						  <option value="COMM">Commercial Properties</option>
						  <option value="RESID">Residential Properties</option>
						  <option value="LOT">Open Lot / Land</option>
						  <option value="CONDO">Condominiums</option>
						  <option value="APART">Apartments</option>
						  <option value="FCL">Forclusures</option>
						</select>
					</div>
				  </div>
				</div>
			</div>
			
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-warning btn-block btn-search-property"><i class="glyphicon glyphicon-search"></i> Display Properties</button>
			</div>
		</div>
</form>

<?php $this->load->view('overall_header'); ?>

<div class="main-body" style="display:block;height: 800px;width:100%;padding:0;">
	<iframe src="https://www.google.com/maps/embed/v1/place?key=<?php echo get_settings_value('GOOGLE_API_KEY'); ?>&q=<?php echo $directory->dir_name; ?>,<?php echo $directory->address; ?>" width="99.9%" height="350" frameborder="0" style="border:0"></iframe>
</div> <!-- /container -->

<?php $this->load->view('overall_footer'); ?>

<?php $this->load->view('overall_header'); ?>
<div class="container main-body">

		  <div class="row">
				<div class="col-md-12">
				
<ol class="breadcrumb">
  <li><a href="<?php echo base_url(); ?>">Home</a></li>
  <li><a href="<?php echo site_url(array("local_business", "browse")); ?>">Local Business</a></li>
  <li class="active"><a href="<?php echo site_url(array("company", $directory->dir_id, $directory->dir_slug)); ?>"><?php echo $directory->dir_name; ?></a></li>
   <li class="active">Related Businesses</li>
</ol>

<div class="panel panel-primary">
  <div class="panel-body">

<?php if ( $directory->thumbnail != '' ) { ?>
	<div class="pull-right hidden-xs"><img itemprop="image" width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $directory->thumbnail; ?>"></div>
<?php } ?>

		<h2 class="title"><span itemprop="name"><?php echo $directory->dir_name; ?></span>
		<br>
		 <small>
			<?php if( $directory->address != "") { ?>
			<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<span itemprop="streetAddress"><?php echo $directory->address; ?></span>,
				<span itemprop="addressLocality">Davao City</span>
			</span>
			<?php } ?>
			</small>
		</h2>
	<span class="abstract">
		<?php if( $directory->abstract != "") { 
			echo $directory->abstract; 
		} ?>
	</span>
	
	
<?php if( count($categories) > 0 ) { 
	echo "<p>";
	foreach( $categories as $category ) {
		$cat_url = site_url(array("local_business", $category->tax_name));
		echo "<a href=\"{$cat_url}\" class=\"label label-info\">{$category->tax_label}</a> ";
	}
	echo "</p>";
} ?>

  </div>
  <div class="panel-footer">
	<div class="btn-group btn-group-justified hidden-xs">
		<?php 
			if ( $directory->phone != '' ) {
				echo "<span class=\"btn btn-success\" itemprop=\"telephone\"><i class=\"glyphicon glyphicon-earphone\"></i> {$directory->phone}</span>";
			} 
			if ( $directory->map != '' ) {
				echo "<a class=\"btn btn-info\" target=\"_blank\" href=\"{$directory->map}\"><i class=\"glyphicon glyphicon-map-marker\"></i> Map</a>";
			} 
			if ( $directory->website != '' ) {
				$prep_website = prep_url( $directory->website );
				echo "<a class=\"btn btn-danger\" target=\"_blank\" href=\"{$prep_website}\" itemprop=\"url\"><i class=\"glyphicon glyphicon-globe\"></i> Website</a>";
			} 
		?>
	</div>
	<div class="visible-xs">
		<?php 
			if ( $directory->phone != '' ) {
				echo "<span class=\"btn btn-success btn-block\" itemprop=\"telephone\">{$directory->phone}</span>";
			} 
			if ( $directory->map != '' ) {
				echo "<a class=\"btn btn-info btn-block\" target=\"_blank\" href=\"{$directory->map}\">Map</a>";
			} 
			if ( $directory->website != '' ) {
				$prep_website = prep_url( $directory->website );
				echo "<a class=\"btn btn-danger btn-block\" target=\"_blank\" href=\"{$prep_website}\">Website</a>";
			} 
		?>
	</div>
  </div>
</div>
					
					
				</div>
		  </div>  <!-- /row -->
<p>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Business Related Ads -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:250px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="2187722920"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</p>
	  <?php if( count( $related ) > 0 ) {  ?>
<div class="panel panel-default">
  <div class="panel-heading">
	      <h3 class="panel-title"><i class="glyphicon glyphicon-folder-open"></i> Related Businesses</h3>
  </div>
  <div class="list-group">
	<?php foreach( $related as $rel ) {
	?>
  <a href="<?php echo site_url(array("company",  $rel->dir_id, $rel->dir_slug)); ?>" class="list-group-item">
	  
<?php if ( $rel->thumbnail != '' ) { ?>
	<div class="pull-right hidden-xs"><img itemprop="logo" width="80px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $rel->thumbnail; ?>"></div>
<?php } ?>

    <h4 class="list-group-item-heading"><?php echo $rel->dir_name; ?></h4>
    <p class="list-group-item-text"><strong>Address:</strong> <?php echo $rel->address; ?></p>
	<p class="list-group-item-text"><strong>Phone:</strong> <?php echo $rel->phone; ?></p>
    <div class="clearfix"></div>
  </a>
   <?php } ?>
</div>
</div>

<?php bootstrap_pagination( $pages, $current_page, current_url() . "?", 10 ); ?>
   <?php } ?>
	  
		  
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
<?php $this->load->view('overall_header'); ?>
<div class="container main-body">

		  <div class="row">
				<div class="col-md-12">
				
<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
	<ol class="breadcrumb">
	  <li>
		<a href="<?php echo base_url(); ?>" itemprop="url"><span itemprop="title">InDavao.Net</span></a>
	  </li>
	  <li>
		<span itemprop="child" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
			<a href="<?php echo site_url(array("local_business")); ?>" itemprop="url">
				<span itemprop="title">Local Business</span>
			</a>
		</span>
	  </li>
<?php /* if( count($categories) > 0 ) {
	foreach( $categories as $category ) {
		echo "<li>";
			$cat_url = site_url(array("local_business", $category->tax_name));
			echo "<span itemscope itemtype=\"http://data-vocabulary.org/Breadcrumb\">";
				echo "<a href=\"{$cat_url}\" itemprop=\"url\">";
					echo "<span itemprop=\"title\">{$category->tax_label}</span>";
				echo "</a>";
			echo "</span>&nbsp;";
		echo "</li>";
	}
} */ ?>
	  <li class="active"><?php echo $directory->dir_name; ?></li>
	</ol>
</div>

<div class="panel panel-primary">
  <div class="panel-body">

<?php if ( $directory->thumbnail != '' ) { ?>
	<div class="pull-right hidden-xs"><img itemprop="image" width="128px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $directory->thumbnail; ?>"></div>
<?php } ?>

		<h2 class="title"><span itemprop="name"><?php echo $directory->dir_name; ?></span>
		<br>
		 <small>
			<?php if( $directory->address != "") { ?>
			<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<span itemprop="streetAddress"><?php echo $directory->address; ?></span>,
				<span itemprop="addressLocality">Davao City</span>
			</span>
			<?php } ?>
			</small>
		</h2>
	<span class="abstract">
		<?php if( $directory->abstract != "") { 
			echo $directory->abstract; 
		} ?>
	</span>
	

<?php if( count($categories) > 0 ) { 
	echo "<p>";
	foreach( $categories as $category ) {
		$cat_url = site_url(array("local_business", $category->tax_name));
		echo "<span itemscope itemtype=\"http://data-vocabulary.org/Breadcrumb\">";
		echo "<a href=\"{$cat_url}\" class=\"label label-info\" itemprop=\"url\">";
		echo "<span itemprop=\"title\">{$category->tax_label}</span>";
		echo "</a>";
		echo "</span>&nbsp;";
	}
	echo "</p>";
} ?>

<?php if( count($tags) > 0 ) { 
	
	$tags_text = array();
	foreach( $tags as $tag ) {
		$tag_url = site_url(array("local_business", "tag", $tag->tag_slug));
		$tag_line = "<span itemscope itemtype=\"http://data-vocabulary.org/Breadcrumb\">";
		$tag_line .= "<a href=\"{$tag_url}\" itemprop=\"url\">";
		$tag_line .= "<span itemprop=\"title\">{$tag->tag_name}</span>";
		$tag_line .= "</a>";
		$tag_line .= "</span>";
		$tags_text[] = $tag_line;
	}
	echo "<small><em>";
	echo "Tags: " . implode(", ", $tags_text);
	echo "</em></small>";
} ?>

  </div>
  <div class="panel-footer">
	<div class="btn-group btn-group-justified hidden-xs">
		<?php 
			if ( $directory->phone != '' ) {
				echo "<span class=\"btn btn-success\" itemprop=\"telephone\"><i class=\"glyphicon glyphicon-earphone\"></i> {$directory->phone}</span>";
			} 
			if ( $directory->map != '' ) {
				echo "<a class=\"btn btn-info\" target=\"_blank\" href=\"{$directory->map}\"><i class=\"glyphicon glyphicon-map-marker\"></i> Map</a>";
			} 
			if ( $directory->website != '' ) {
				$prep_website = prep_url( trim($directory->website) );
				echo "<a class=\"btn btn-danger\" target=\"_blank\" href=\"{$prep_website}\" itemprop=\"url\"><i class=\"glyphicon glyphicon-globe\"></i> Website</a>";
			} 
		?>
	</div>
	<div class="visible-xs">
		<?php 
			if ( $directory->phone != '' ) {
				echo "<span class=\"btn btn-success btn-block\" itemprop=\"telephone\">{$directory->phone}</span>";
			} 
			if ( $directory->map != '' ) {
				echo "<a class=\"btn btn-info btn-block\" target=\"_blank\" href=\"{$directory->map}\">Map</a>";
			} 
			if ( $directory->website != '' ) {
				$prep_website = prep_url( trim($directory->website) );
				echo "<a class=\"btn btn-danger btn-block\" target=\"_blank\" href=\"{$prep_website}\">Website</a>";
			}
		?>
	</div>
  </div>
</div>
					
					
				</div>
		  </div>  <!-- /row -->
      

		  <div class="row">
				<div class="col-md-8">
					
<?php if($directory->address != '') { ?>

<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="glyphicon glyphicon-time"></i> Map</h3>
  </div>
  <iframe src="https://www.google.com/maps/embed/v1/place?key=<?php echo get_settings_value('GOOGLE_API_KEY'); ?>&q=<?php echo $directory->dir_name; ?>,<?php echo $directory->address; ?>" width="99.9%" height="350" frameborder="0" style="border:0"></iframe>
</div>

<?php } ?>

<div class="panel panel-info">
  <div class="panel-heading">
	  <a href="#" class="btn btn-danger btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i></a>
    <h3 class="panel-title"><i class="glyphicon glyphicon-time"></i> Opening Hours</h3>
  </div>
  <div class="panel-body">
    Coming Soon...
  </div>
</div>

<div class="panel panel-info">
  <div class="panel-heading">
	  <a href="#" class="btn btn-danger btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i></a>
    <h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Business Details</h3>
  </div>
  <div class="panel-body">
    Coming Soon...
  </div>
</div>

<div class="panel panel-info">
  <div class="panel-heading">
	  <a href="#" class="btn btn-danger btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i></a>
    <h3 class="panel-title"><i class="glyphicon glyphicon-star-empty"></i> Ratings &amp; Reviews</h3>
  </div>
  <div class="panel-body">
    Coming Soon...
  </div>
</div>

<?php if( count( $related ) > 0 ) {  ?>
<div class="panel panel-default">
  <div class="panel-heading">
	  <a href="<?php echo site_url(array("company", $directory->dir_id, $directory->dir_slug, "related")); ?>" class="btn btn-xs btn-default pull-right">Show More</a>
    <h3 class="panel-title"><i class="glyphicon glyphicon-folder-open"></i> Related Businesses</h3>
  </div>
  <div class="list-group">
	<?php foreach( $related as $rel ) {
	?>
  <a href="<?php echo site_url(array("company",  $rel->dir_id, $rel->dir_slug)); ?>" class="list-group-item">
	  
<?php if ( $rel->thumbnail != '' ) { ?>
	<div class="pull-right hidden-xs"><img itemprop="logo" width="80px" alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $rel->thumbnail; ?>"></div>
<?php } ?>

    <h4 class="list-group-item-heading"><?php echo $rel->dir_name; ?></h4>
    <p class="list-group-item-text"><strong>Address:</strong> <?php echo $rel->address; ?></p>
	<p class="list-group-item-text"><strong>Phone:</strong> <?php echo $rel->phone; ?></p>
    <div class="clearfix"></div>
  </a>
   <?php } ?>
</div>
</div>
   <?php } ?>


				</div>
				<div class="col-sm-6 col-md-4">

<?php if($isLoggedIn) { ?>
<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="glyphicon glyphicon-cog"></i> My Tools</h3>
  </div>
  <div class="list-group">
	  <a href="<?php echo site_url(array("my", $this->session->userdata('user_id'), "add_bookmark")); ?>?type=business&id=<?php echo $directory->dir_id; ?>" class="list-group-item" id="save-to-bookmarks-<?php echo $directory->dir_id; ?>"><i class="glyphicon glyphicon-bookmark"></i> Save to Bookmarks</a>
	   <a href="javascript:void(0);" class="list-group-item print-me"><i class="glyphicon glyphicon-print"></i> Print This Page</a>
	</div>
</div>

<?php } ?>			
				
<?php $this->load->view('business/searchform'); ?>

<?php $share_url = ( $isLoggedIn ) ? site_url( array("company", $directory->dir_slug,  $this->session->userdata('user_id')) ) : current_url(); ?>
<div class="social-media-sidebar">

<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode( $share_url ); ?>" class="btn btn-default btn-block btn-lg btn-facebook" >Share to Facebook</a>
	
<a target="_blank" href="https://twitter.com/home?status=<?php echo $directory->dir_name . ' - ' . urlencode( $share_url ); ?>" class="btn btn-default btn-block btn-lg btn-twitter">Tweet on Twitter</a>
	
	
<a target="_blank" href="https://plus.google.com/share?url=<?php echo urlencode( $share_url ); ?>" class="btn btn-default btn-block btn-lg btn-google">Share on Google+</a>
	
	
<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode( $share_url ); ?>&title=<?php echo $directory->dir_name; ?>" class="btn btn-default btn-block btn-lg btn-linkedin">Share on LinkedIn</a>
	
	
<?php if( $directory->thumbnail ) { ?>
	
<a target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo urlencode( $share_url ); ?>&media=<?php echo urlencode( get_settings_value('upload_url') . $directory->thumbnail ); ?>&description=<?php echo $directory->dir_name; ?>" class="btn btn-default btn-block btn-lg btn-pinterest">Pin on Pinterest</a>
	
<?php } ?>

</div>
				</div>
			</div>
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>
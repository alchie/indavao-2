<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
      <div class="row">

		<div class="col-md-8">

		<a href="#refine-search" class="btn btn-warning btn-block visible-xs" style="margin-bottom:15px;">Refine Your List</a>

<div class="alert alert-success">
	<p><strong><?php echo ($total_items > 1) ? $total_items . " Establishments Found!" : $total_items . " Establishment Found!"; ?></strong></p>
</div>
<div class="row">
				<?php foreach($businesses as $business) {   ?>
                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12"> 
						<div class=" bgc-fff box-shad btm-mrg-20 property-listing">
<?php
if( $business->dir_verified == 1 ) {
	echo "<span class='ribbon verified'>Verified</span>";
} else {
	echo "<span class='ribbon unverified'>Unverified</span>";
}
if( $business->dir_branch == 1 ) {
	echo "<span class='ribbon ribbon2 branch_office'>Branch Office</span>";
}
?>
                        
                        <div class="row inner">
							<div class="col-md-4">
								<div class="thumb">
								<a href="<?php echo site_url(array('company', $business->dir_id , $business->dir_slug) ); ?>" target="_parent" title="<?php echo  $business->dir_name; ?>">
<?php if ( $business->thumbnail == '' ) { ?>
	 <img alt="image" class="img-responsive lazy no-image" data-original="<?php echo base_url() .'assets/images/photo-icon.png'; ?>">
<?php } else { ?>
	<img alt="image" class="img-responsive lazy" data-original="<?php echo get_settings_value('upload_url'); ?><?php echo $business->thumbnail; ?>">
<?php } ?> </a>					</div>
							</div>
							<div class="col-md-8">
								<div class="info">
								<h4 class="title">
                                  <a href="<?php echo site_url(array('company', $business->dir_id , $business->dir_slug) ); ?>" target="_parent" title="<?php echo  $business->dir_name; ?>"><?php echo  $business->dir_name; ?></a>
                                </h4>
                                <span class="pull-right">
                                <?php 
                                /*
                                if( $business->loc_name != '') {
									echo "<p class=\"attr icon-only\" title=\"Location: {$business->loc_name}\"><i class=\"property-icon map-marker\"></i></p>";
								}
                                if( $business->phone != '') {
									echo "<p class=\"attr icon-only\" title=\"Phone: {$business->phone}\"><i class=\"property-icon phone\"></i></p>";
								}
								if( $business->address != '') {
									echo "<p class=\"attr icon-only\" title=\"Address: {$business->address}\"><i class=\"property-icon street\"></i></p>";
								}
								if( $business->website != '') {
									echo "<p class=\"attr icon-only\" title=\"Website: {$business->website}\"><a target=\"_blank\" href=\"{$business->website}\"><i class=\"property-icon website\"></i></a></p>";
								}
								* */
								?>
								</span>
                                <?php
								if( $business->address != '') {
									echo "<p class=\"business-address\" title=\"Abstract\">{$business->address}</p>";
								}
								?>
                                
                                </div>
							</div>
							
							<div class="footer col-md-8 hidden-xs hidden-sm">
								
								<?php if( $business->phone != '') {
									$business_phone_label = ( $business->phone_label != '') ? $business->phone_label : 'Phone';
									echo "<span class=\"phone\" title=\"{$business_phone_label}\">{$business->phone}</span>";
								}
								?>

								
								
								<a class="btn btn-primary btn-xs pull-right" href="<?php echo site_url(array('company', $business->dir_id , $business->dir_slug) ); ?>">Show Details <i class="glyphicon glyphicon-play"></i></a>
								
								<?php if( $business->email != '') { ?>
								<a class="btn btn-primary btn-xs pull-right email" href="<?php echo site_url(array('company', $business->dir_id , $business->dir_slug, 'contact') ); ?>"><i class="glyphicon glyphicon-envelope"></i> Send Email</a>
								<?php } ?>
								
							</div>
                        </div>
                        
                    </div><!-- End Listing-->
				</div>
				<?php } ?>
				
            </div><!-- End row -->

<?php bootstrap_pagination( $pages, $current_page, current_url() . "?", 10 ); ?>


		</div>
		
		<div class="col-sm-6 col-md-4">
			<?php $this->load->view('business/searchform'); ?>
			<p>
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- InDavao Sidebar -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="4162800524"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
			</p>
		</div>
		
      </div>  <!-- /row -->
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

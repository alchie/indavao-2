<?php $this->load->view('overall_header'); ?>
<div class="container main-body">
      <div class="row">

		<div class="col-md-8">

<div itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
	<ol class="breadcrumb">
	  <li>
		<a href="<?php echo base_url(); ?>" itemprop="url"><span itemprop="title">InDavao.Net</span></a>
	  </li>
	  <li class="active">Local Business</li>
	</ol>
</div>


		<a href="#refine-search" class="btn btn-warning btn-block visible-xs" style="margin-bottom:15px;">Refine Your List</a>

<?php 
foreach($business_categories as $business_category) { ?>
<div class="page-header">
  <h1><a href="<?php echo site_url('local_business/' . $business_category->tax_name); ?>"><?php echo ucwords($business_category->tax_label); ?></a></h1>
</div>


<?php 

if( isset($business_category->children) && (count($business_category->children) > 0) ) {
	foreach($business_category->children as $business_category_child) {
		echo "<a class='btn btn-default' href=\"". site_url('local_business/' . $business_category_child->tax_name)."\">{$business_category_child->tax_label}</a>";
	}
}

} ?>

		</div>
		
		<div class="col-sm-6 col-md-4">
			<?php $this->load->view('business/searchform'); ?>

			<p>
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- InDavao Sidebar -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="4162800524"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
			</p>
		</div>
		
      </div>  <!-- /row -->
</div> <!-- /container -->
<?php $this->load->view('overall_footer'); ?>

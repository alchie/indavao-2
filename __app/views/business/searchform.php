<p>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- InDavao Front -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-7233800271694028"
     data-ad-slot="4721203728"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</p>

<form method="POST" action="<?php echo site_url('local_business/search'); ?>" style="margin:0;">
<div class="panel panel-primary" id="refine-search">
	<div class="panel-heading">
		<h3 class="panel-title">Refine Search</h3>
	</div>
	<div class="panel-body">
		
		<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Business Category</label>
					<div>
					   <select class="form-control selectpicker show-menu-arrow" data-live-search="true" name="category">
						   <option value="any">All Business Category</option>
						  <?php 
								$business_category_children = array();
								foreach($business_categories as $business_category) { 
									echo "<option value=\"{$business_category->tax_name}\" ";
									if( isset($search_keys->category) && ($business_category->tax_name == $search_keys->category) ) {
										echo "selected=\"selected\"";
									}
									echo ">{$business_category->tax_label}</option>";
									
									if( isset($business_category->children) && (count($business_category->children) > 0) ) {
										if( isset($search_keys->category) && ($business_category->tax_name == $search_keys->category) ) {
											$business_category_children = $business_category->children;
										}
										foreach($business_category->children as $business_category_child) {
											echo "<option value=\"{$business_category_child->tax_name}\" ";
											if( isset($search_keys->category) && ($business_category_child->tax_name == $search_keys->category) ) {
												echo "selected=\"selected\"";
											}
											echo ">- - {$business_category_child->tax_label}</option>";
										}
									}
									
								} 
						   ?>
						</select>
					</div>
				  </div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-12">
					<div class="form-group"> 
						<div class="row">
						<span class="col-md-12"><label class="control-label">Attributes</label></span>
					<?php foreach(array(
					'verified' => 'Verified', 
					'main' => 'Main Office', 
					'phone' => 'With Phone', 
					'email' => 'With Email',
					'address' => 'With Address',
					'website' => 'With Website',
					) as $key=>$label) { 
							if((isset($search_keys->$key)) && $search_keys->$key == '1' ) { ?>
						<span class="col-md-6" style="margin-bottom:5px;"><label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input name="<?php echo $key; ?>" type="checkbox" value="1" class="hidden" CHECKED><span><?php echo $label; ?></span></label></span>
						<?php } else { ?>
							<span class="col-md-6" style="margin-bottom:5px;"><label class="btn btn-default btn-xs btn-block col-md-6"><i class="glyphicon glyphicon-remove"></i> <input name="<?php echo $key; ?>" type="checkbox" value="1" class="hidden"><span><?php echo $label; ?></span></label></span>
						<?php } ?>
					<?php } ?>
					</div>
					</div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Location</label>
					<div>
						<select class="form-control selectpicker show-menu-arrow" multiple title="Any Location" name="location[]">
						<?php 
							function echo_locations_options($locations, $prefix='', $selected=array()) { 
								foreach($locations as $location) { 
									if(in_array($location->loc_slug, ((isset($selected)) ? $selected : array()))) { 
										echo "<option value=\"{$location->loc_slug}\" SELECTED>{$prefix}{$location->loc_name}</option>";
									} else {
										echo "<option value=\"{$location->loc_slug}\">{$prefix}{$location->loc_name}</option>";
									}
									if( isset($location->children) && (count($location->children) > 0) ) {
										echo_locations_options($location->children, $prefix." - ", $selected);
									}
								} 
							}
							if( $locations ) {
								echo_locations_options($locations, '', ((isset($search_keys->location)) ? $search_keys->location : array()));
							}
						   ?>
						   
						</select>
					</div>
				  </div>
				</div>
			</div>
			
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-warning btn-block"><i class="glyphicon glyphicon-search"></i> Refine Search</button>
	</div>
	
</div>
</form>

<?php if( count( $business_category_children ) > 0 ) { ?>
<div class="list-group">
	<?php foreach( $business_category_children as $business_category_child ) { ?>
		<a href="<?php echo site_url('local_business/' . $business_category_child->tax_name); ?>" class="list-group-item"><?php echo $business_category_child->tax_label; ?></a>
	<?php } ?>
</div>
<?php } ?>
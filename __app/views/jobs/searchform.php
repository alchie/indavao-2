<form method="POST" action="<?php echo site_url('jobs/search'); ?>" style="margin:0;">
<div class="panel panel-primary" id="refine-search">
	<div class="panel-heading">
		<h3 class="panel-title">Refine Search</h3>
	</div>
	<div class="panel-body">
		
		<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Job Function</label>
					<div>
					  <select class="form-control selectpicker show-menu-arrow" data-live-search="true" multiple title="Select a Job Function" name="job_function[]">
						  <?php 
								foreach($job_functions as $job_function) { 
									echo "<optgroup label=\"{$job_function->tax_label}\">";
									
									if( isset($job_function->children) && (count($job_function->children) > 0) ) {
										foreach($job_function->children as $job_function_child) {
											echo "<option value=\"{$job_function_child->tax_name}\"";
											if( isset($search_keys->job_function) && in_array($job_function_child->tax_name, $search_keys->job_function) ) {
												echo "selected=\"selected\"";
											}
											echo ">{$job_function_child->tax_label}</option>";
										}
									}
									
									echo "</optgroup>";
								} 
						   ?>
						</select>
					</div>
				  </div>
				</div>
			</div>
		

			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">Job Status</label>
					<?php foreach(array('full_time' => 'Full Time', 'part_time' => 'Part Time', 'internship' => 'Internship') as $key=>$label) { 
							if(in_array($key, ((isset($search_keys->job_status)) ? $search_keys->job_status : array()))) { ?>
						<label class="btn btn-success btn-xs btn-block"><i class="glyphicon glyphicon-ok"></i> <input name="job_status[]" type="checkbox" value="<?php echo $key; ?>" class="hidden" CHECKED><span><?php echo $label; ?></span></label>
						<?php } else { ?>
							<label class="btn btn-default btn-xs btn-block"><i class="glyphicon glyphicon-remove"></i> <input name="job_status[]" type="checkbox" value="<?php echo $key; ?>" class="hidden"><span><?php echo $label; ?></span></label>
						<?php } ?>
					<?php } ?>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<label class="control-label">Location</label>
					<div>
						<select class="form-control selectpicker show-menu-arrow" multiple title="Any Location" name="location[]">
						<?php 
							function echo_locations_options($locations, $prefix='', $selected=array()) { 
								foreach($locations as $location) { 
									if(in_array($location->loc_slug, ((isset($selected)) ? $selected : array()))) { 
										echo "<option value=\"{$location->loc_slug}\" SELECTED>{$prefix}{$location->loc_name}</option>";
									} else {
										echo "<option value=\"{$location->loc_slug}\">{$prefix}{$location->loc_name}</option>";
									}
									if( isset($location->children) && (count($location->children) > 0) ) {
										echo_locations_options($location->children, $prefix." - ", $selected);
									}
								} 
							}
							if( $locations ) {
								echo_locations_options($locations, '', ((isset($search_keys->location)) ? $search_keys->location : array()));
							}
						   ?>
						   
						</select>
					</div>
				  </div>
				</div>
			</div>
			
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-warning btn-block"><i class="glyphicon glyphicon-search"></i> Refine Search</button>
	</div>
	
</div>
</form>

